import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/Categorias/bloc/categorias_bloc/categorias_bloc.dart';
import 'package:maxxshop/src/Inventario/Categorias/bloc/seleccionar_categoria_bloc/seleccionar_categoria_bloc.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/seleccionar_categoria_bloc/seleccionar_categoria_bloc.dart';
import 'package:maxxshop/src/Home/pages/menus_page.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/Provider/escanerProvider.dart';
import 'package:maxxshop/src/Inventario/Productos/bloc/crear_producto_bloc/crear_producto_bloc.dart';
import 'package:maxxshop/src/Inventario/Productos/bloc/obtener_productos_bloc/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:connectivity/connectivity.dart';

import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/Home/pages/home_page.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Home/pages/inicio_page.dart';
import 'package:maxxshop/src/Home/pages/ajustes_page.dart';
import 'package:maxxshop/src/Conexiones/pages/nueva_conexion.dart';
import 'package:maxxshop/src/Usuario/pages/sin_conexion_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Conexiones/pages/conexiones_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/helpers/BlocObserver/simple_bloc_observer.dart';
import 'package:maxxshop/src/Facturacion/Comanda/services/wrapper_productos_comanda.dart';
import 'package:maxxshop/src/Conexiones/services/conexiones_repositories.dart';
import 'package:maxxshop/src/Facturacion/Comanda/bloc/operaciones_comanda/operaciones_comanda_bloc.dart';
import 'package:maxxshop/src/Conexiones/blocs/listado_conexiones_bloc/listado_conexiones_bloc.dart';
import 'package:maxxshop/src/Conexiones/blocs/operaciones_conexiones_bloc/operaciones_conexiones_bloc.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  Bloc.observer = SimpleBlocObserver();

  final Connectivity _connectivity = Connectivity();

  initConexionInternet(_connectivity);
}

//Revisa si hay conexión a intenet por Wifi y datos móviles
Future<void> initConexionInternet(Connectivity connectivity) async {
  ConnectivityResult result;

  try {
    result = await connectivity.checkConnectivity();
  } catch (e) {
    print(e.toString());
  }

  return _updateConnectionStatus(result);
}

//Determina que rutas seguir según la informacion de conexión
Future<void> _updateConnectionStatus(ConnectivityResult result) async {
  switch (result) {
    case ConnectivityResult.wifi:
    case ConnectivityResult.mobile:
      autenticacionInicialDeLaApp();
      break;
    case ConnectivityResult.none:
      runApp(MaxxMeserosApp(primeraRuta: 'sinConexion'));
      break;
    default:
      runApp(MaxxMeserosApp(primeraRuta: 'nuevaConexion'));
      break;
  }
}

void autenticacionInicialDeLaApp() async {
  List<ConexionesModel> conexiones = await DBProvider.db.getTodasConexiones();

  final prefs = PreferenciasUsuario();
  //Se guarda el UID en las preferencias de usuario
  //prefs.uid = user != null ? user.uid : '';
  //print(prefs.uid);
  if (conexiones.length > 0) {
    if (!prefs.sesionActiva) {
      runApp(
        MaxxMeserosApp(
          primeraRuta: 'home',
        ),
      );
    }
    else{
      runApp(
        MaxxMeserosApp(primeraRuta: 'menusPage')
      );
    }
  } else {
    runApp(
      MaxxMeserosApp(
        primeraRuta: 'nuevaConexion',
      ),
    );
  }
  
}

// ignore: must_be_immutable
class MaxxMeserosApp extends StatefulWidget {
  String primeraRuta;

  MaxxMeserosApp({@required this.primeraRuta});

  @override
  _MaxxMeserosAppState createState() => _MaxxMeserosAppState();
}

class _MaxxMeserosAppState extends State<MaxxMeserosApp> {
  @override
  Widget build(BuildContext context) {
    //
    //BlocProvider será el nuevo manejador de estado de la aplicación. Primeramente en sus
    //áreas de operaciones más críticas
    //
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => OperacionesConexionesBloc(
            wrapperOperacionesConexiones: ConexionesRepositories(),
          ),
        ),
        BlocProvider(
          create: (context) => ListadoConexionesBloc(
            operacionesConexionesBloc:
                BlocProvider.of<OperacionesConexionesBloc>(context),
          ),
        ),
        BlocProvider(
          create: (context) => OperacionesComandaBloc(
            wrapperProductos: WrapperProductosComanda(),
          ),
        ),
        BlocProvider(create: (BuildContext context) => CategoriasBloc()),
        BlocProvider(
            create: (BuildContext context) => SeleccionarCategoriaBloc(
                  blocCategorias: BlocProvider.of<CategoriasBloc>(context),
                )),
        BlocProvider(create: (BuildContext context) => CrearProductoBloc()),
        BlocProvider(
            create: (BuildContext context) => ProductosBloc(
                blocCrearProducto:
                    BlocProvider.of<CrearProductoBloc>(context))),
        BlocProvider(create: (BuildContext context) => Categorias2Bloc()),
        BlocProvider(
          create: (BuildContext context) => SeleccionarCategoria2Bloc(
            blocCategorias: BlocProvider.of<Categorias2Bloc>(context),
          ),
        ),
      ],
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => CabeceraService(),
          ),
          ChangeNotifierProvider(
            create: (context) => EscanerProvider(),
          ),
          
          ChangeNotifierProvider(
            create: (context) => TotalVentasProvider(),
          ),
        ],
        //Combina el Provider originalmente creado con inherited widgte y el multiprovider
        //directamente del provider del pub
        child: ProviderInherited(
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Maxxshop',
            theme: ThemeData(primaryColor: Color(0xff008DBA)),
            initialRoute: widget.primeraRuta,
            routes: {
              'home': (BuildContext context) => HomePage(),
              'inicio': (BuildContext context) => InicioPage(),
              'sinConexion': (BuildContext context) => SinConexionPage(),
              'conexiones': (BuildContext context) => ConexionesPage(),
              'nuevaConexion': (BuildContext context) => NuevaConexionPage(),
              'ajustes': (BuildContext context) => AjustesPage(),
              'menusPage': (BuildContext context) => MenusPage(),
            },
          ),
        ),
      ),
    );
  }
}
