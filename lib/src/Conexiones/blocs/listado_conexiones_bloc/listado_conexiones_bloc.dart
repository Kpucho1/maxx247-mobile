import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Conexiones/blocs/operaciones_conexiones_bloc/operaciones_conexiones_bloc.dart';

part 'listado_conexiones_event.dart';
part 'listado_conexiones_state.dart';

class ListadoConexionesBloc
    extends Bloc<ListadoConexionesEvent, ListadoConexionesState> {
  //
  //=== Este bloc estará comunicado con el bloc de OperacionesConexiones para reaccionar
  //a los diferentes eventos que ocurran como agregar o eliminar conexiones ===

  final OperacionesConexionesBloc operacionesConexionesBloc;
  StreamSubscription suscripcionOperacionesConexiones;

  ListadoConexionesBloc({this.operacionesConexionesBloc})
      : super(ListadoConexionesInitial()) {
    //
    suscripcionOperacionesConexiones =
        operacionesConexionesBloc.listen((state) {
      //

      if (state is EliminarConexionSuccess) {
        //
        add(OnCargarConexiones());
      }
      //
    });
  }

  @override
  Future<void> close() {
    suscripcionOperacionesConexiones.cancel();
    return super.close();
  }

  @override
  Stream<ListadoConexionesState> mapEventToState(
    ListadoConexionesEvent event,
  ) async* {
    //
    final currentState = state;

    if (event is OnCargarConexiones) {
      try {
        //
        if (currentState is ListadoConexionesInitial) {
          //
          yield ListadoConexionesLoadInProgress();

          final listaConexiones = await DBProvider.db.getTodasConexiones();

          yield listaConexiones.isEmpty
              ? ListadoConexionesVacioState()
              : ListaConexionesCargadaState(lista: listaConexiones);
        }

        if (currentState is ListaConexionesCargadaState) {
          //
          final listaConexiones = await DBProvider.db.getTodasConexiones();

          yield listaConexiones.isEmpty
              ? ListadoConexionesVacioState()
              : currentState.copyWith(listado: listaConexiones);
        }

        if (currentState is ListadoConexionesVacioState) {
          //
          yield ListadoConexionesLoadInProgress();

          final listaConexiones = await DBProvider.db.getTodasConexiones();

          yield listaConexiones.isEmpty
              ? ListadoConexionesVacioState()
              : ListaConexionesCargadaState(lista: listaConexiones);
        }
        //

      } catch (e) {
        //
        print(e);
        yield ErrorCargaListadoConexiones();
      }
    }
  }
}
