part of 'listado_conexiones_bloc.dart';

@immutable
abstract class ListadoConexionesState {}

class ListadoConexionesInitial extends ListadoConexionesState {}

class ListadoConexionesLoadInProgress extends ListadoConexionesState {}

class ListadoConexionesVacioState extends ListadoConexionesState {}

class ErrorCargaListadoConexiones extends ListadoConexionesState {}

class ListaConexionesCargadaState extends ListadoConexionesState {
  final List<ConexionesModel> listaConexiones;

  ListaConexionesCargadaState({List<ConexionesModel> lista})
      : this.listaConexiones = lista ?? [];

  ListaConexionesCargadaState copyWith({
    List<ConexionesModel> listado,
  }) =>
      ListaConexionesCargadaState(lista: listado ?? this.listaConexiones);
}
