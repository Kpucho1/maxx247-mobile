part of 'operaciones_conexiones_bloc.dart';

@immutable
abstract class OperacionesConexionesEvent {}

class OnGuardarNuevaConexionQR extends OperacionesConexionesEvent {
  final Map<String, dynamic> nuevaConexion;

  OnGuardarNuevaConexionQR({this.nuevaConexion});
}

class OnGuardarNuevaConexionLicencia extends OperacionesConexionesEvent {
  final String licencia;

  OnGuardarNuevaConexionLicencia({this.licencia});
}

class OnRestaurarConexionesInitialState extends OperacionesConexionesEvent {}

class OnEliminarConexion extends OperacionesConexionesEvent {
  final int idConexion;

  OnEliminarConexion({this.idConexion});
}
