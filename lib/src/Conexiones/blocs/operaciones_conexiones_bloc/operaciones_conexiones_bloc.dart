import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Conexiones/services/operaciones_conexiones_interface.dart';

part 'operaciones_conexiones_event.dart';
part 'operaciones_conexiones_state.dart';

class OperacionesConexionesBloc
    extends Bloc<OperacionesConexionesEvent, OperacionesConexionesState> {
  final IOperacionesConexiones wrapperOperacionesConexiones;

  OperacionesConexionesBloc({this.wrapperOperacionesConexiones})
      : super(OperacionesConexionesInitial());

  @override
  Stream<OperacionesConexionesState> mapEventToState(
    OperacionesConexionesEvent event,
  ) async* {
    //

    final currentState = state;

    if (event is OnGuardarNuevaConexionLicencia) {
      //

      try {
        //
        if (currentState is OperacionesConexionesInitial) {
          yield GuardarNuevaConexionLoadInProgress();

          final comprobarExistencia =
              await DBProvider.db.getConexionLicencia(event.licencia);

          if (comprobarExistencia != null) {
            //
            yield ExisteLicenciaGuardada();
            //
          } else {
            //
            final conexion = await wrapperOperacionesConexiones
                .getInfoParaNuevaConexion(licencia: event.licencia);

            await DBProvider.db.nuevaConexion(conexion);

            yield GuardarNuevaConexionSuccess();
          }
        }

        if (currentState is ErrorGuardarNuevaConexion) {
          yield GuardarNuevaConexionLoadInProgress();

          final comprobarExistencia =
              DBProvider.db.getConexionLicencia(event.licencia);

          if (comprobarExistencia != null) {
            //
            yield ExisteLicenciaGuardada();
            //
          } else {
            //
            final conexion = await wrapperOperacionesConexiones
                .getInfoParaNuevaConexion(licencia: event.licencia);

            await DBProvider.db.nuevaConexion(conexion);

            yield GuardarNuevaConexionSuccess();
          }
        }
      } catch (e) {
        //
        print(e);
        yield ErrorGuardarNuevaConexion();
      }
    }

    if (event is OnGuardarNuevaConexionQR) {
      //

      try {
        //
        if (currentState is OperacionesConexionesInitial) {
          yield GuardarNuevaConexionLoadInProgress();

          final conexion = await wrapperOperacionesConexiones
              .getInfoParaNuevaConexion(licencia: event.nuevaConexion);

          await DBProvider.db.nuevaConexion(conexion);

          yield GuardarNuevaConexionSuccess();
          // }
        }

        if (currentState is ErrorGuardarNuevaConexion) {
          yield GuardarNuevaConexionLoadInProgress();

          final comprobarExistencia = DBProvider.db
              .getConexionLicencia(event.nuevaConexion["licencia"]);

          if (comprobarExistencia != null) {
            //
            yield ExisteLicenciaGuardada();
            //
          } else {
            //
            final conexion = await wrapperOperacionesConexiones
                .getInfoParaNuevaConexion(licencia: event.nuevaConexion);

            await DBProvider.db.nuevaConexion(conexion);

            yield GuardarNuevaConexionSuccess();
          }
        }
      } catch (e) {
        //
        print(e);
        yield ErrorGuardarNuevaConexion();
      }
    }

    //===== Se usa para restaurar el estado al inicio y para que así el usuario
    //pueda ingresar una nueva licencia =====

    if (event is OnRestaurarConexionesInitialState) {
      yield OperacionesConexionesInitial();
    }

    //==== Operacion asociada a la eliminación de conexiones ====

    if (event is OnEliminarConexion) {
      //

      try {
        //
        await DBProvider.db.deleteConexion(event.idConexion);

        yield EliminarConexionSuccess();
      } catch (e) {
        //
        print(e);
        yield ErrorEliminarConexion();
      }
    }
  }
}
