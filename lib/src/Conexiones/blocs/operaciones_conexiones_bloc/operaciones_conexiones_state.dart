part of 'operaciones_conexiones_bloc.dart';

@immutable
abstract class OperacionesConexionesState {}

class OperacionesConexionesInitial extends OperacionesConexionesState {}

class GuardarNuevaConexionLoadInProgress extends OperacionesConexionesState {}

class ErrorGuardarNuevaConexion extends OperacionesConexionesState {}

class ExisteLicenciaGuardada extends OperacionesConexionesState {}

class GuardarNuevaConexionSuccess extends OperacionesConexionesState {}

class EliminarConexionSuccess extends OperacionesConexionesState {}

class ErrorEliminarConexion extends OperacionesConexionesState {}
