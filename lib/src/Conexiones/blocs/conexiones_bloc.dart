import 'dart:async';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';

class ConexionesBloc {
  static final ConexionesBloc _singletonConexiones =
      new ConexionesBloc._internal();

  factory ConexionesBloc() {
    return _singletonConexiones;
  }

  ConexionesBloc._internal() {
    obtenerConexiones();
  }

  final _conexionesController =
      StreamController<List<ConexionesModel>>.broadcast();

  final _conexionActivaController =
      StreamController<ConexionesModel>.broadcast();

  Stream<List<ConexionesModel>> get conexionesStream =>
      _conexionesController.stream;

  dispose() {
    _conexionesController?.close();
    _conexionActivaController?.close();
  }

  obtenerConexiones() async {
    _conexionesController.sink.add(await DBProvider.db.getTodasConexiones());
  }

  agregarConexiones(ConexionesModel conexion) async {
    await DBProvider.db.nuevaConexion(conexion);
    obtenerConexiones();
  }

  borrarConexion(int id) async {
    await DBProvider.db.deleteConexion(id);

    obtenerConexiones();
  }

  //**Conexion Activa**

  agregarConexionActiva(ConexionesModel conexionActiva) async {
    await DBProvider.db.guardarConexionActiva(conexionActiva);
    print('Agregada');
  }

  obtenerConexionActiva() async {
    _conexionActivaController.sink.add(await DBProvider.db.getConexionActiva());
  }

  borrarConexionActiva() async {
    await DBProvider.db.deleteConexionActiva();
  }
}
