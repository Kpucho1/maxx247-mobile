import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NuevaConexionFragment extends StatelessWidget {
  final Widget botones;

  const NuevaConexionFragment({Key key, this.botones}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(
          top: size.height * 0.05,
          left: size.height * 0.01,
          right: size.height * 0.01,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SvgPicture.asset(
              'assets/conectate.svg',
              height: 250.0,
              width: 500,
            ),
            SizedBox(height: 20.0),
            Text(
              '¡Conéctate!',
              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20.0),
            Text(
              'Puedes hacerlo de diferentes maneras',
              style: TextStyle(fontSize: 25.0),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20.0),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: size.width * 0.2,
              ),
              child: Text(
                'IMPORTANTE: El número de licencia debe ser solicitado al administrador del establecimiento.',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            this.botones,
          ],
        ),
      ),
    );
  }
}
