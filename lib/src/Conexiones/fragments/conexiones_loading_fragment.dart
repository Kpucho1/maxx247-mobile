import 'package:flutter/material.dart';

class ConexionesLoadingFragment extends StatelessWidget {
  //

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: CircularProgressIndicator(
            strokeWidth: 1.5,
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          'Cargando establecimientos...',
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }
}
