import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxxshop/src/Home/pages/login_home_page.dart';
import 'package:maxxshop/src/Home/pages/menus_page.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Conexiones/blocs/operaciones_conexiones_bloc/operaciones_conexiones_bloc.dart';

class ListaConexionesFragment extends StatelessWidget {
  //
  final List<ConexionesModel> listaConexiones;

  const ListaConexionesFragment({Key key, this.listaConexiones})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: listaConexiones.length,
      itemBuilder: (context, index) {
        //

        return Column(
          children: [
            SizedBox(height: 20.0),
            _TarjetaDescripcionConexion(conexion: this.listaConexiones[index]),
          ],
        );
      },
    );
  }
}

class _TarjetaDescripcionConexion extends StatelessWidget {
  final ConexionesModel conexion;

  const _TarjetaDescripcionConexion({Key key, this.conexion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(
        top: size.width * 0.01,
        right: size.width * 0.1,
        left: size.width * 0.1,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 8.0,
        child: Column(
          children: [
            ListTile(
              leading: Icon(Icons.signal_cellular_alt,
                  color: Theme.of(context).primaryColor),
              title: Text(conexion.licencia),
              subtitle: Text(conexion.nombreempresa ?? ''),
              trailing: IconButton(
                onPressed: () {
                  _dialogoInfoConexion(
                    context,
                    conexion.ipservidor,
                    conexion.basedatos,
                  );
                },
                icon: Icon(
                  Icons.info_outline,
                  size: 27.0,
                ),
              ),
            ),
            _BotonesInferioresTarjeta(conexion: this.conexion),
          ],
        ),
      ),
    );
  }

  Future _dialogoInfoConexion(
    BuildContext context,
    String ipServidor,
    String baseDatos,
  ) {
    //
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text('IP: $ipServidor / BD: $baseDatos',
              style: TextStyle(fontSize: 16.0)),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Volver',
                style: TextStyle(fontSize: 16.0),
              ),
            ),
          ],
        );
      },
    );
  }
}

class _BotonesInferioresTarjeta extends StatelessWidget {
  final prefs = PreferenciasUsuario();
  final mesasRefreshBloc = MesasRefreshBloc();

  final ConexionesModel conexion;

  _BotonesInferioresTarjeta({Key key, this.conexion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(width: 30.0),
        TextButton(
          child: Text(
            'Conectar',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed: () async{
            mesasRefreshBloc.borrarMesas();
            DBProvider.db.borrarVendedor();
            DBProvider.db.borrarEsquemas();
            
            DBProvider.db.deleteConexionActiva();
            
            DBProvider.db.borrarSucursales();


            if (prefs.nombreEmpresa != conexion.nombreempresa) {
              DBProvider.db.borrarProductosGustos();
              DBProvider.db.deleteProductos();
              DBProvider.db.deleteCategorias();
              DBProvider.db.borrarProductosCombos();
              DBProvider.db.borrarProductosEquivalencias();
            }

            prefs.ipServidor = conexion.ipservidor;
            prefs.nombreEmpresa = conexion.nombreempresa;

            final conexionActiva = ConexionesModel(
              ipservidor: conexion.ipservidor,
              puertobd: conexion.puertobd,
              basedatos: conexion.basedatos,
              usuario: conexion.usuario,
              password: conexion.password,
            );

            await DBProvider.db.guardarConexionActiva(conexionActiva);

            

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LoginHomePage(), //ConexionesLoadingPage(conexion: conexion),
              ),
            );
          },
        ),
        Expanded(
          child: SizedBox(),
        ),
        TextButton(
          child: Icon(
            Icons.delete_forever,
            color: Colors.red,
          ),
          onPressed: () {
            context
                .read<OperacionesConexionesBloc>()
                .add(OnEliminarConexion(idConexion: conexion.id));

            mesasRefreshBloc.borrarMesas();
          },
        )
      ],
    );
  }
}
