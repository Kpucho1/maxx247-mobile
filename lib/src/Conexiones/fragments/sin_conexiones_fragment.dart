import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SinConexionesFragment extends StatelessWidget {
  //

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 60.0,
        ),
        SvgPicture.asset(
          'assets/conexiones.svg',
          width: 500,
          height: 350,
        ),
        SizedBox(
          height: 30.0,
        ),
        Text(
          'Agrega nuevos establecimientos para comenzar.',
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
