import 'package:flutter/material.dart';

class FondoConexiones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(196, 187, 175, 0.1),
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
        painter: _FondoConexionesPainter(),
      ),
    );
  }
}

class _FondoConexionesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint1 = new Paint();

    paint1.color = Color.fromRGBO(178, 203, 210, 0.4);
    paint1.style = PaintingStyle.fill;
    paint1.strokeWidth = 5;

    final paint2 = new Paint();

    paint2.color = Color.fromRGBO(178, 203, 210, 0.7);
    paint2.style = PaintingStyle.fill;
    paint2.strokeWidth = 5;

    final path1 = new Path();

    path1.moveTo(0, size.height);
    path1.lineTo(size.width, size.height * 0.7);
    path1.lineTo(size.width, size.height);

    final path2 = new Path();

    path2.moveTo(0, size.height * 0.6);
    path2.lineTo(size.width * 0.5, size.height);
    path2.lineTo(0, size.height);

    canvas.drawPath(path1, paint1);
    canvas.drawPath(path2, paint2);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

//Dark Purple: 0xff2B061E RGB: 43, 6, 30
//Old Rose: 0xffC78283 RGB: 199, 130, 131
