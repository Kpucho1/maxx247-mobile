import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxxshop/src/Conexiones/pages/crear_empresa_page.dart';

import 'package:maxxshop/src/Home/pages/home_page.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';
import 'package:maxxshop/src/Conexiones/fragments/nueva_conexion_fragment.dart';
import 'package:maxxshop/src/Conexiones/blocs/operaciones_conexiones_bloc/operaciones_conexiones_bloc.dart';

class NuevaConexionPage extends StatefulWidget {
  @override
  _NuevaConexionPageState createState() => _NuevaConexionPageState();
}

class _NuevaConexionPageState extends State<NuevaConexionPage> {
  //

  @override
  void initState() {
    context
        .read<OperacionesConexionesBloc>()
        .add(OnRestaurarConexionesInitialState());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Nueva Conexión'),
          centerTitle: true,
        ),
        body: _CuerpoNuevaConexion(),
      ),
    );
  }
}

class _CuerpoNuevaConexion extends StatelessWidget {
  //====== Se encarga de renderizar la IU según los diferentes estados que estén pasando
  //por el bloc. El bloc se encarga de manejar también la navegación si y solo si el guardado
  //de una conexión es exitoso ======

  @override
  Widget build(BuildContext context) {
    //

    return BlocListener<OperacionesConexionesBloc, OperacionesConexionesState>(
      listener: (context, state) {
        //

        if (state is GuardarNuevaConexionSuccess) {
          //==== Navega a la home page una vez se confirma que la licencia se
          //guardó exitosamente ====

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => HomePage(),
              ),
              (route) => false);
        }
      },
      child: BlocBuilder<OperacionesConexionesBloc, OperacionesConexionesState>(
        builder: (context, state) {
          Widget _cuerpoScreen;

          if (state is OperacionesConexionesInitial) {
            //

            _cuerpoScreen = NuevaConexionFragment(
              botones: _BotonesLicenciaQR(),
            );
            //
          } else if (state is GuardarNuevaConexionLoadInProgress) {
            //

            _cuerpoScreen = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(strokeWidth: 1.5),
                  SizedBox(height: 10.0),
                  Text('Guardando establecimiento. Un momento por favor.'),
                ],
              ),
            );
          } else if (state is GuardarNuevaConexionSuccess) {
            //

            _cuerpoScreen = Center(
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
              ),
            );
            //
          } else if (state is EliminarConexionSuccess) {
            //

            _cuerpoScreen = Container();
            //
          } else if (state is ExisteLicenciaGuardada) {
            //
            _cuerpoScreen = Center(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      CupertinoIcons.exclamationmark_circle,
                      size: 70.0,
                      color: Colors.yellow[600],
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'La licencia que intenta guardar ya existe.',
                      style: TextStyle(fontSize: 16.0),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 30.0),
                    OutlinedButton(
                      child: Text('Regresar'),
                      onPressed: () => context
                          .read<OperacionesConexionesBloc>()
                          .add(OnRestaurarConexionesInitialState()),
                    ),
                  ],
                ),
              ),
            );
            //
          } else if (state is ErrorGuardarNuevaConexion) {
            //

            _cuerpoScreen = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.exclamationmark_circle,
                    size: 70.0,
                    color: Colors.red,
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    'Ocurrió un error al intentar guardar el establecimiento. Intente de nuevo.\n\nEsto puede ser generado por una licencia inválida o el campo de la licencia fue enviado vacío.',
                    style: TextStyle(fontSize: 16.0),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 30.0),
                  OutlinedButton(
                    child: Text('Reintentar'),
                    onPressed: () => context
                        .read<OperacionesConexionesBloc>()
                        .add(OnRestaurarConexionesInitialState()),
                  ),
                ],
              ),
            );
          }

          return _cuerpoScreen;
        },
      ),
    );
  }
}

class _BotonesLicenciaQR extends StatefulWidget {
  @override
  __BotonesLicenciaQRState createState() => __BotonesLicenciaQRState();
}

//=========Es un stateful widget para poder distribuir el context en todos los widgets que lo requieren.
//En este caso el diálogo para ingresar la licencia========
class __BotonesLicenciaQRState extends State<_BotonesLicenciaQR> {
  @override
  Widget build(BuildContext context) {
    //

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: CupertinoIcons.doc_text_fill,
                  texto: 'Conectar con licencia',
                ),
                onTap: () {
                  return showDialog(
                    context: context,
                    builder: (context) {
                      return _DialogoIngresarLicencia();
                    },
                  ).then((value) {
                    if (value != null) {
                      //

                      context.read<OperacionesConexionesBloc>().add(
                            OnGuardarNuevaConexionLicencia(
                              licencia: value,
                            ),
                          );
                    }
                  });
                },
              ),
              InkWell(
                onTap: () {
                  scanQR();
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: CupertinoIcons.qrcode,
                  texto: 'Conectar con QR',
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              SizedBox(height: 10.0,),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CrearCuentaPage()));
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: CupertinoIcons.home,
                  texto: 'Crear nueva empresa',
                ),
              ),
              SizedBox()
            ],
          ),
        ],
      ),
    );
  }

  Future<void> scanQR() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        "#2B061E", "Cancelar", true, ScanMode.QR);

    if (!mounted) return;

    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    String decodedData = stringToBase64.decode(barcodeScanRes);

    Map<String, dynamic> datos = json.decode(decodedData);

    context
        .read<OperacionesConexionesBloc>()
        .add(OnGuardarNuevaConexionQR(nuevaConexion: datos));
  }
}

class _DialogoIngresarLicencia extends StatefulWidget {
  @override
  __DialogoIngresarLicenciaState createState() =>
      __DialogoIngresarLicenciaState();
}

class __DialogoIngresarLicenciaState extends State<_DialogoIngresarLicencia> {
  final _textController = TextEditingController();

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      title: Text('Ingresa el número de licencia'),
      content: _formLicencia(),
      actions: [
        TextButton(
          child: Text(
            'Guardar',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed: () async {
            final licencia = _textController.text;

            Navigator.pop(context, licencia);
          },
        ),
        TextButton(
          child: Icon(
            Icons.remove_circle_outline,
            color: Colors.red,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  Widget _formLicencia() {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(vertical: size.width * 0.1),
      child: TextField(
        controller: _textController,
        autofocus: true,
        textAlign: TextAlign.center,
        autocorrect: false,
        enableSuggestions: false,
        decoration: InputDecoration(
          helperText: 'Número de licencia',
          helperStyle: TextStyle(fontSize: 16.0),
          icon: Icon(
            Icons.add_business,
            size: 35.0,
            color: Color(0xffDD6E42),
          ),
        ),
      ),
    );
  }
}
