import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Conexiones/designs/fondo_conexiones.dart';
import 'package:maxxshop/src/Conexiones/fragments/sin_conexiones_fragment.dart';
import 'package:maxxshop/src/Conexiones/fragments/lista_conexiones_fragment.dart';
import 'package:maxxshop/src/Conexiones/fragments/conexiones_loading_fragment.dart';
import 'package:maxxshop/src/Conexiones/blocs/listado_conexiones_bloc/listado_conexiones_bloc.dart';
import 'package:maxxshop/src/Conexiones/blocs/operaciones_conexiones_bloc/operaciones_conexiones_bloc.dart';

class ConexionesPage extends StatefulWidget {
  @override
  _ConexionesPageState createState() => _ConexionesPageState();
}

class _ConexionesPageState extends State<ConexionesPage> {
  //

  @override
  void initState() {
    super.initState();

    context.read<ListadoConexionesBloc>().add(OnCargarConexiones());
  }

  @override
  Widget build(BuildContext context) {
    //

    return BlocListener<OperacionesConexionesBloc, OperacionesConexionesState>(
      listener: (context, state) {
        //=== Muestra mensaje de confirmación de la eliminación de la conexión ===

        if (state is EliminarConexionSuccess) {
          //
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              content: Text(
                'Establecimiento eliminado con éxito',
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              backgroundColor: Colors.cyan[900],
            ),
          );
        } else if (state is GuardarNuevaConexionSuccess) {
          //=== Muestra mensaje de confirmación de guardado de la conexión
          //únicamente cuando no es la primera conexión que el usuario agrega ===

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              content: Text(
                'Establecimiento guardado con éxito',
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              backgroundColor: Colors.green,
            ),
          );
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55.0),
          child: _AppBarConexionesPage(),
        ),
        body: Stack(
          alignment: Alignment.center,
          children: [
            FondoConexiones(),
            _CuerpoConexiones(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
            size: 30.0,
          ),
          backgroundColor: Color(0xffDD6E42),
          onPressed: () async {
            //
            await Navigator.pushNamed(context, 'nuevaConexion');
          },
        ),
      ),
    );
  }
}

class _AppBarConexionesPage extends StatelessWidget {
  //

  final prefs = PreferenciasUsuario();
  final mesasRefreshBloc = MesasRefreshBloc();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Column(
        children: [
          Text('Tus conexiones'),
          
        ],
      ),
      centerTitle: true,
      automaticallyImplyLeading: false,
      actions: [
        IconButton(
          icon: Icon(Icons.logout),
          onPressed: () async {
            //

            prefs.uid = '';
            await mesasRefreshBloc.borrarMesas();
            // await DBProvider.db.deleteConexiones();

            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => ConexionesPage(),
                ),
                (_) => false);
          },
        )
      ],
    );
  }
}

class _CuerpoConexiones extends StatelessWidget {
  //

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ListadoConexionesBloc, ListadoConexionesState>(
      builder: (context, state) {
        //
        Widget _cuerpoConexiones;

        if (state is ListadoConexionesInitial) {
          //
          _cuerpoConexiones = ConexionesLoadingFragment();
          //
        } else if (state is ListadoConexionesLoadInProgress) {
          //
          _cuerpoConexiones = ConexionesLoadingFragment();
          //
        } else if (state is ListadoConexionesVacioState) {
          //
          _cuerpoConexiones = SinConexionesFragment();
          //
        } else if (state is ErrorCargaListadoConexiones) {
          //
          _cuerpoConexiones = Container();
          //
        } else if (state is ListaConexionesCargadaState) {
          //
          _cuerpoConexiones =
              ListaConexionesFragment(listaConexiones: state.listaConexiones);
        }

        return _cuerpoConexiones;
      },
    );
  }
}
