import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Conexiones/services/connection_service.dart';

class CrearCuentaPage extends StatefulWidget {
  @override
  _CrearCuentaPageState createState() => _CrearCuentaPageState();
}

class _CrearCuentaPageState extends State<CrearCuentaPage> {
  bool _cargando = false;
  String _tipoEmpresa = 'a';
  dynamic _jsonResult;
  int idPais = 82;

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _nombreEmpresaController = TextEditingController();
  TextEditingController _nitEmpresaController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _nombreEmpresaController.dispose();
    _nitEmpresaController.dispose();
    super.dispose();
  }

  @override
  void initState(){ 

    super.initState();
    _loadCountries();
  }

  Future<List> _loadCountries() async{
    // String data = await DefaultAssetBundle.of(context).loadString("assets/countries.json");

    String data = await rootBundle.loadString("assets/countries.json");

    _jsonResult = json.decode(data);

    List<dynamic> _listadoPaises = _jsonResult['countries'];

    return _listadoPaises;

  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: _appBarCrearCuenta(),
        body: SingleChildScrollView(
          child: _cuerpoCrearCuenta(),
        ),
      ),
    );
  }

  Widget _appBarCrearCuenta() {
    return PreferredSize(
      preferredSize: Size.fromHeight(150.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey[400],
              offset: Offset(1.0, 4.0),
              blurRadius: 10.0,
            ),
          ],
        ),
        child: SafeArea(
          child: Center(
            child: Image.asset(
              'assets/maxxcontrol_logo.png',
              height: 150.0,
            ),
          ),
        ),
      ),
    );
  }

  Widget _cuerpoCrearCuenta() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 20.0,
          ),
          _tituloCrearCuenta(),
          SizedBox(
            height: 20.0,
          ),
          _formularioCrearCuenta(),
        ],
      ),
    );
  }

  Widget _tituloCrearCuenta() {
    return Text(
      "Crear cuenta",
      style: Theme.of(context).textTheme.headline6,
    );
  }

  Widget _formularioCrearCuenta() {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Form(
          child: Column(
            children: [
              SizedBox(
                height: 25.0,
              ),
              _inputCorreoElectronico(),
              SizedBox(
                height: 10.0,
              ),
              _inputPassword(),
              SizedBox(
                height: 10.0,
              ),
              // _inputConfirmPassword(),
              // SizedBox(
              //   height: 10.0,
              // ),
               _inputNitEmpresa(),
              SizedBox(
                height: 10.0,
              ),
               _inputNombreEmpresa(),
              SizedBox(
                height: 10.0,
              ),
              _dropdownTipoEmpresa(),
              SizedBox(
                height: 10.0,
              ),
              _dropdownPais(),
              SizedBox(
                height: 25.0,
              ),
              _botonCrearCuenta(),
              SizedBox(
                height: 10.0,
              ),
              // _botonIniciarSesion(),
              SizedBox(
                height: 25.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _inputCorreoElectronico() {
    return TextFormField(
      controller: _emailController,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blueGrey[100],
        prefixIcon: Icon(Icons.email),
        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        labelText: 'Correo electrónico *',
      ),
    );
  }

  Widget _inputPassword() {
    return TextFormField(
      controller: _passwordController,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blueGrey[100],
        prefixIcon: Icon(Icons.lock),
        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        labelText: 'Contraseña *',
      ),
    );
  }

  Widget _inputConfirmPassword() {
    return TextFormField(
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blueGrey[100],
        prefixIcon: Icon(Icons.lock),
        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        labelText: 'Confirmar contraseña',
      ),
    );
  }

  Widget _inputNitEmpresa() {
    return TextFormField(
      controller: _nitEmpresaController,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blueGrey[100],
        prefixIcon: Icon(Icons.lock),
        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        labelText: 'Nit de la empresa',
      ),
    );
  }

  Widget _inputNombreEmpresa() {
    return TextFormField(
      controller: _nombreEmpresaController,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.blueGrey[100],
        prefixIcon: Icon(Icons.lock),
        border: UnderlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        labelText: 'Nombre de la empresa *',
      ),
    );
  }

  Widget _dropdownTipoEmpresa(){
    return DropdownButton(
      style: const TextStyle(color: Colors.black),
      isExpanded: true,
      hint: Text("Tipo empresa"),
      icon: Icon(Icons.food_bank_outlined),
      value: _tipoEmpresa,
      underline: Container(
        height: 2,
        color: Colors.grey,
      ),
      onChanged: (value) {
        setState(() {
          _tipoEmpresa = value;
        });
      },
      items: [
        DropdownMenuItem(
          value: 'a',
          child: Text('Alimentos y bebidas'),
        ),
        DropdownMenuItem(
          value: 'c',
          child: Text('Comercio'),
        ),
      ],
    );
  }

  Widget _dropdownPais(){
    return FutureBuilder(
      future: _loadCountries(),
      builder: (context, snapshot) {

        if (!snapshot.hasData) {
          return SizedBox();
        }
        else{
          List<dynamic> listadoPaises = snapshot.data;

          return DropdownButton(
            style: const TextStyle(color: Colors.black),
            isExpanded: true,
            hint: Text("Pais"),
            icon: Icon(Icons.flag),
            value: idPais,
            underline: Container(
              height: 2,
              color: Colors.grey,
            ),
            onChanged: (value) {
              setState(() {
                idPais = value;
              });
            },
            items: listadoPaises.map((e) {
              return DropdownMenuItem(
                value: e["id"],
                child: Text(e["name"]),
              );
            }).toList(),
          );
        }
        
      }
    );
  }

  Widget _botonCrearCuenta() {
    return OutlinedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 10.0,
        )),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: !_cargando
          ? Text(
              'Crear cuenta',
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 16.0,
              ),
            )
          : LinearProgressIndicator(),
      onPressed: !_cargando
          ? () async{
              setState(() {
                _cargando = true;
              });

              ConnectionService connectionService = ConnectionService();

              if(await connectionService.crearNuevoUsuario(_emailController.text, _passwordController.text, _nombreEmpresaController.text, _nitEmpresaController.text, idPais)){
                Navigator.pushReplacementNamed(context, 'conexiones');
              }
              else{
                final snackbar = SnackBar(content: Text("Ha ocurrido un error", style: TextStyle(color: Colors.white),), backgroundColor: Colors.red[300],);
                ScaffoldMessenger.of(context).showSnackBar(snackbar);
              }

              setState(() {
                _cargando = false;
              });
            }
          : null,
    );
  }

  Widget _botonIniciarSesion() {
    return OutlinedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 10.0,
        )),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: Text(
              'Iniciar Sesion',
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 14.0,
              ),
            ),
      onPressed: () {
        Navigator.pushReplacementNamed(context, '/iniciarSesion');
      },
    );
  }
}