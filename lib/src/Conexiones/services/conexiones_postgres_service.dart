import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/providers/conexion_postgres.dart';

//Se declara un mixin con las funciones predeterminadas para las operaciones sobre las conexiones
//Se usa para evitar instanciamientos

mixin ConexionesPostgresService {
  //

  Future obtenerInfoParaNuevaLicencia(String licencia) async {
    PostgresProvider.conexionPostgres.abrirConexion;

    PostgreSQLConnection conexion =
        await PostgresProvider.conexionPostgres.retornaConexion();

    List<List<dynamic>> results;

    try {
      results = await conexion.query(
          'SELECT ipservidorsql, basedatossql, usuariosql, passwordsql, puertobasedatos, esquemabasedatos, nombreempresa FROM datos.empresas WHERE licencia = @licencia',
          substitutionValues: {'licencia': licencia});
    } catch (e) {
      //
      print(e);
      throw Exception('Error al obtener los datos de conexion');
    }

    await conexion.close();

    return results;
  }
}
