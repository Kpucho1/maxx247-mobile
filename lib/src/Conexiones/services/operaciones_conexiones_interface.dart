import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';

//Definicion de los metodos a utilizar por las conexiones a establecimientos

abstract class IOperacionesConexiones {
  //

  Future<ConexionesModel> getInfoParaNuevaConexion({dynamic licencia});
}
