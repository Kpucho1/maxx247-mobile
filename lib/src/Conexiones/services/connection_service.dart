import 'dart:convert';

import 'package:http/http.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';


///Archivo para conexion con la api de maxxcontrol
///Valida que la licencia exista
class ConnectionService {
  final prefs = PreferenciasUsuario();
  

  crearNuevoUsuario(String email, String password, String nombreEmpresa, String nit, int pais) async{
    String url = 'https://api.maxxcontrol.net/api/maxxpos/users/create';

    Response response = await post(url, body: {
      "email" : email,
      "password": password,
      "nombreEmpresa" : nombreEmpresa,
      "nit": nit,
      "pais": pais.toString()
    });

    if (response.statusCode == 200) {

      final decodedData = json.decode(response.body);
      
      prefs.licencia = decodedData["schema_id"];

      ConexionesModel nuevaConexion = ConexionesModel(
        ipservidor: "maxx247.com",
        basedatos: decodedData["schema_id"],
        usuario: "postgres",
        password: "desarrollo321.*",
        puertobd: "5433",
        licencia: decodedData["schema_id"],
        esquemabd: "datos",
        nombreempresa: nombreEmpresa
      );

      await DBProvider.db.nuevaConexion(nuevaConexion);

      return true;

    }
    else{

      prefs.licencia = "";

      print(response.body);

      return false;
    }

  }
  
}