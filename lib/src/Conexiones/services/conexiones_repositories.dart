import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Conexiones/services/conexiones_postgres_service.dart';
import 'package:maxxshop/src/Conexiones/services/operaciones_conexiones_interface.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';

//Clase encargada de comunicarse directamente con el bloc y las operaciones de BD

class ConexionesRepositories
    with ConexionesPostgresService
    implements IOperacionesConexiones {
  //

  @override
  Future<ConexionesModel> getInfoParaNuevaConexion({dynamic licencia}) async {
    //
    ConexionesModel nuevaConexion;

    if (licencia is String) {
      //

      final List conexion = await obtenerInfoParaNuevaLicencia(licencia);

      if (conexion.isNotEmpty) {
        nuevaConexion = ConexionesModel(
            licencia: licencia,
            ipservidor: conexion[0][0],
            basedatos: conexion[0][1],
            usuario: conexion[0][2],
            password: conexion[0][3],
            puertobd: conexion[0][4],
            esquemabd: conexion[0][5],
            nombreempresa: conexion[0][6]);
      }
    } else if (licencia is Map<String, dynamic>) {
      //
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        licencia["ipservidor"],
        licencia["puerto"],
        licencia["database"],
        "postgres",
        "desarrollo321.*",
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> nombreEmpresa = await conexionNegocio.query("SELECT nombre FROM datos.empresa");

      nuevaConexion = ConexionesModel(
          licencia: licencia["database"],
          ipservidor: licencia["ipservidor"],
          basedatos: licencia["database"],
          usuario: "postgres",
          password: "desarrollo321.*",
          puertobd: licencia["puerto"],
          esquemabd: "datos",
          nombreempresa: nombreEmpresa.first.first ?? licencia["database"]);
    }

    return nuevaConexion ?? {};
  }
}
