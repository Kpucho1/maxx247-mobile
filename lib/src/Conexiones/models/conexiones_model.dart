import 'dart:convert';

ConexionesModel conexionesModelFromJson(String str) =>
    ConexionesModel.fromJson(json.decode(str));

String conexionesModelToJson(ConexionesModel data) =>
    json.encode(data.toJson());

class ConexionesModel {
  ConexionesModel({
    this.id,
    this.licencia,
    this.nombreempresa,
    this.ipservidor,
    this.basedatos,
    this.usuario,
    this.password,
    this.puertobd,
    this.esquemabd,
  });

  int id;
  String licencia;
  String nombreempresa;
  String ipservidor;
  String basedatos;
  String usuario;
  String password;
  String puertobd;
  String esquemabd;

  factory ConexionesModel.fromJson(Map<String, dynamic> json) =>
      ConexionesModel(
        id: json["id"],
        licencia: json["licencia"],
        nombreempresa: json["nombreempresa"],
        ipservidor: json["ipservidor"],
        basedatos: json["basedatos"],
        usuario: json["usuario"],
        password: json["password"],
        puertobd: json["puertobd"],
        esquemabd: json["esquemabd"],
      );

  Map<String, dynamic> toJson() => {
        // "id": id,
        "licencia": licencia,
        "nombreempresa": nombreempresa,
        "ipservidor": ipservidor,
        "basedatos": basedatos,
        "usuario": usuario,
        "password": password,
        "puertobd": puertobd,
        "esquemabd": esquemabd,
      };
}
