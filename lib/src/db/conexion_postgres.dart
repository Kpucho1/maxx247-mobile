import 'package:postgres/postgres.dart';

class PostgresProvider {
  static PostgreSQLConnection connection;

  static final PostgresProvider conexionPostgres = PostgresProvider._private();

  PostgresProvider._private();

  get abrirConexion {
    connection = PostgreSQLConnection(
        'maxxcontrol.com.co', 5437, 'maxxcontrol_master',
        username: 'postgres', password: 'desarrollo321.*');
  }

  Future<PostgreSQLConnection> retornaConexion() async {
    await connection.open();
    return connection;
  }
}
