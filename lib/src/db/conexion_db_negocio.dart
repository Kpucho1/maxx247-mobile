import 'package:postgres/postgres.dart';

class ConexionNegocioProvider {
  static PostgreSQLConnection connectionNegocio;

  static final ConexionNegocioProvider conexionNegocio =
      ConexionNegocioProvider._private();

  ConexionNegocioProvider._private();

  void abrirConexion(
    String ipservidor,
    int puertobd,
    String basedatos,
    String usuario,
    String password,
  ) {
    connectionNegocio = PostgreSQLConnection(
      ipservidor,
      puertobd,
      basedatos,
      username: usuario,
      password: password,
    );
  }

  Future<PostgreSQLConnection> retornaConexion() async {
    await connectionNegocio.open();
    return connectionNegocio;
  }

  Future<void> cerrarConexion() async{
    if (!connectionNegocio.isClosed) {
      await connectionNegocio.close();
    }
  }
}
