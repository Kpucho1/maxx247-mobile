import 'package:postgres/postgres.dart';

class ConexionTimerProvider {
  static PostgreSQLConnection connectionNegocioTimer;

  static final ConexionTimerProvider conexionTimer =
      ConexionTimerProvider._private();

  ConexionTimerProvider._private();

  void abrirConexion(
    String ipservidor,
    int puertobd,
    String basedatos,
    String usuario,
    String password,
  ) {
    
    if (connectionNegocioTimer != null) {
      if (connectionNegocioTimer.isClosed) {
        connectionNegocioTimer = PostgreSQLConnection(
          ipservidor,
          puertobd,
          basedatos,
          username: usuario,
          password: password,
        );
      }
    }
    else{
      connectionNegocioTimer = PostgreSQLConnection(
        ipservidor,
        puertobd,
        basedatos,
        username: usuario,
        password: password,
      );
    }
  }

  Future<PostgreSQLConnection> retornaConexion() async {
    await connectionNegocioTimer.open();
    return connectionNegocioTimer;
  }

  Future<void> cerrarConexion() async{
    if (connectionNegocioTimer != null){
      if (!connectionNegocioTimer.isClosed) {
        await connectionNegocioTimer.close();
      }
    }
  }
}
