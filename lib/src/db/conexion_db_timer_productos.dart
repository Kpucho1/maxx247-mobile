import 'package:postgres/postgres.dart';

class ConexionTimerProductosProvider {
  static PostgreSQLConnection connectionNegocioTimer;

  static final ConexionTimerProductosProvider conexionTimer =
      ConexionTimerProductosProvider._private();

  ConexionTimerProductosProvider._private();

  void abrirConexion(
    String ipservidor,
    int puertobd,
    String basedatos,
    String usuario,
    String password,
  ) {
    
    if (connectionNegocioTimer != null) {
      if (connectionNegocioTimer.isClosed) {
        connectionNegocioTimer = PostgreSQLConnection(
          ipservidor,
          puertobd,
          basedatos,
          username: usuario,
          password: password,
        );
      }
    }
    else{
      connectionNegocioTimer = PostgreSQLConnection(
        ipservidor,
        puertobd,
        basedatos,
        username: usuario,
        password: password,
      );
    }
  }

  Future<PostgreSQLConnection> retornaConexion() async {
    await connectionNegocioTimer.open();
    return connectionNegocioTimer;
  }

  Future<void> cerrarConexion() async{
    if (!connectionNegocioTimer.isClosed) {
      await connectionNegocioTimer.close();
    }
  }
}
