import 'dart:io';
import 'package:maxxshop/src/Home/models/ajustes_model.dart';
import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
export 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';

class DBProvider {
  static Database _database;

  static final DBProvider db = DBProvider._private();

  DBProvider._private();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  //Create tables
  void _createTableConexiones(Batch batch) {
    batch.execute(
        'DROP TABLE IF EXISTS Conexiones'); //Tabla principal de conexiones
    batch.execute('''CREATE TABLE Conexiones (
            id INTEGER PRIMARY KEY,
            licencia TEXT,
            ipservidor TEXT,
            basedatos TEXT,
            usuario TEXT,
            password TEXT,
            puertobd TEXT,
            esquemabd TEXT,
            nombreempresa TEXT
            );''');
  }

  void _createTableMesas(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Mesas');
    batch.execute('''CREATE TABLE Mesas (
            id INTEGER PRIMARY KEY,
            objetoid INTEGER,
            nombre TEXT,
            estado INTEGER,
            consumido INTEGER,
            esquemaid INTEGER,
            numeroCuentas INTEGER,
            nombreVendedor TEXT,
            vendedorid INTEGER
            );''');
  }

  void _createTableConexionActiva(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS ConexionActiva');
    batch.execute('''CREATE TABLE ConexionActiva (
            id INTEGER PRIMARY KEY,
            licencia TEXT,
            ipservidor TEXT,
            basedatos TEXT,
            usuario TEXT,
            password TEXT,
            puertobd TEXT,
            esquemabd TEXT,
            nombreempresa TEXT
            );''');
  }

  void _createTableCategorias(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Categorias');
    batch.execute('''CREATE TABLE Categorias (
            id INTEGER PRIMARY KEY,
            grupoid INTEGER,
            codigogrupo TEXT,
            descripcion TEXT,
            foto TEXT,
            ordenvista INTEGER,
            grupopadreid INTEGER
            );''');
  }

  void _createTableProductos(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Productos');
    batch.execute('''CREATE TABLE Productos (
            id INTEGER PRIMARY KEY,
            productoid INTEGER,
            codigo TEXT,
            nombre TEXT,
            costo REAL,
            grupo1id INTEGER,
            centrocostoid INTEGER,
            impuestoconsumo REAL,
            precioivaventa REAL,
            presentacion TEXT,
            foto TEXT,
            productovalorunitario INTEGER,
            productoimprimecomanda INTEGER,
            productoequivalencia INTEGER,
            productoporiva REAL,
            productoporipo REAL,
            esservicio INTEGER,
            numeromaximocombos INTEGER,
            ordenvista INTEGER,
            estado INTEGER,
            precio REAL,
            codigobarras1 TEXT,
            codigobarras2 TEXT,
            ts TEXT,
            pidepeso INTEGER,
            portiempo INTEGER
            );''');
  }

  void _createTableVendedor(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Vendedor');
    batch.execute('''CREATE TABLE Vendedor (
            vendedorid INTEGER PRIMARY KEY,
            esquemaid INTEGER,
            identificacion TEXT,
            nombre TEXT,
            sucursalid INTEGER,
            controlccosto INTEGER,
            abrirtodascuentas INTEGER,
            bodegaid INTEGER
            );''');
  }

  void _createTableEsquemas(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Esquemas');
    batch.execute('''CREATE TABLE Esquemas (
            id INTEGER,
            esquemaid INTEGER PRIMARY KEY,
            nombre TEXT,
            colorvacialimpiafondo TEXT,
            colorvacialimpiatexto TEXT,
            colorocupadasinpedidofondo TEXT,
            colorocupadasinpedidotexto TEXT,
            colorocupadaconpedidotexto TEXT,
            colorocupadaconpedidofondo TEXT,
            colorsuciafondo TEXT,
            colorsuciatexto TEXT,
            colorpendienteporpagarfondo TEXT,
            colorpendienteporpagartexto TEXT,
            colorreparacionfondo TEXT,
            colorreaparaciontexto TEXT,
            colordisponiblefondo TEXT,
            colordisponbiletexto TEXT,
            colorocupadafondo TEXT,
            colorocupadatexto TEXT,
            colorantesdevencersefondo TEXT,
            colorantesdevencersetexto TEXT
            );''');
  }

  void _createTableProductosGustos(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Productosgustos');
    batch.execute('''CREATE TABLE Productosgustos(
           id INTEGER PRIMARY KEY,
           productoid INTEGER,
           gustoid INTEGER,
           nombre TEXT 
      );''');
  }

  void _createTableProductosCombos(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Productoscombos');
    batch.execute('''
      CREATE TABLE Productoscombos(
        id INTEGER PRIMARY KEY,
        productoid INTEGER,
        productoidhijo INTEGER,
        cantidadcombo INTEGER,
        presentacion TEXT,
        preciocombo REAL,
        valoradicional REAL,
        tipocomboid INTEGER DEFAULT 0,
        nombre TEXT,
        codigohijo TEXT,
        productoimprimecomanda INTEGER,
        centrocostoid INTEGER
      );
    ''');
  }

  void _createTableProductosEquivalencias(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Productosequivalencias');
    batch.execute('''
      CREATE TABLE Productosequivalencias(
        id INTEGER PRIMARY KEY,
        equivalenciaid INTEGER,
        productoid INTEGER,
        presentacion TEXT,
        equivalencia INTEGER,
        porcentajeiva INTEGER,
        porcentajeipo INTEGER,
        precioventaincluido INTEGER,
        estado INTEGER,
        tipoequivalencia INTEGER,
        principal INTEGER
      );
    ''');
  }

  void _createTableSucursal(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS sucursal');
    batch.execute('''
      CREATE TABLE sucursal(
        id INTEGER PRIMARY KEY,
        nombre TEXT,
        codigosucursal TEXT,
        sucursalid INTEGER
      );
    ''');
  }

  void _createTableAjustes(Batch batch) {
    batch.execute('DROP TABLE IF EXISTS Ajustes');
    batch.execute('''
      CREATE TABLE Ajustes(
        id INTEGER PRIMARY KEY,
        fontsize INTEGER DEFAULT 1,
        agruparcomanda INTEGER DEFAULT 0
      );
    ''');

    batch.insert("Ajustes", {
      "fontsize": 1,
      "agruparcomanda": 0,
    });
  }

  void _updateTableEsquemasV1toV2(Batch batch) {
    batch.execute('''
      ALTER TABLE Esquemas ADD COLUMN colorvacialimpiafondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorvacialimpiatexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadasinpedidofondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadasinpedidotexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadaconpedidotexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadaconpedidofondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorsuciafondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorsuciatexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorpendienteporpagarfondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorpendienteporpagartexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorreparacionfondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorreaparaciontexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colordisponiblefondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colordisponbiletexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadafondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorocupadatexto TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorantesdevencersefondo TEXT;
      ALTER TABLE Esquemas ADD COLUMN colorantesdevencersetexto TEXT;

    ''');
  }

  void _updateTableProductosV1toV2(Batch batch) {
    batch.execute(''' 
      ALTER TABLE Productos ADD COLUMN impuestoconsumo REAL DEFAULT 0;
      ALTER TABLE Productos ADD COLUMN precioivaventa REAL DEFAULT 0;
      ALTER TABLE Productos ADD COLUMN presentacion TEXT; 
      ALTER TABLE Productos ADD COLUMN codigo TEXT;
    ''');
  }

  void _updateTableProductosV2toV3(Batch batch) {
    batch.execute('''
      ALTER TABLE Productos ADD COLUMN productovalorunitario INTEGER;
      ALTER TABLE Productos ADD COLUMN productoimprimecomanda INTEGER;
      ALTER TABLE Productos ADD COLUMN productoequivalencia INTEGER;
      ALTER TABLE Productos ADD COLUMN productoporiva REAL;
      ALTER TABLE Productos ADD COLUMN productoporipo REAL;
    ''');
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, 'ConexionesDB.db');

    return await openDatabase(
      path,
      version: 38, 
      onOpen: (db) {},
      onCreate: (db, version) async {
        var batch = db.batch();

        _createTableConexiones(batch);
        _createTableMesas(batch);
        _createTableConexionActiva(batch);
        _createTableCategorias(batch);
        _createTableProductos(batch);
        _createTableVendedor(batch);
        _createTableEsquemas(batch);
        _createTableProductosGustos(batch);
        _createTableProductosCombos(batch);
        _createTableProductosEquivalencias(batch);
        _createTableAjustes(batch);
        _createTableSucursal(batch);

        await batch.commit();
      },
      onDowngrade: onDatabaseDowngradeDelete,
      onUpgrade: (db, oldVersion, newVersion) async {
        var batch = db.batch();
        if (oldVersion < newVersion) {
          db.execute("ALTER TABLE Productos ADD COLUMN pidepeso INTEGER"); 
          db.execute("ALTER TABLE Productos ADD COLUMN portiempo INTEGER"); 

          db.execute("ALTER TABLE Productos ADD COLUMNN ts TEXT");
          
        }
        await batch.commit();
      },
    );
  }

  //Operaciones para las conexiones

  Future<int> nuevaConexion(ConexionesModel nuevaConexion) async {
    final db = await database;

    int res;

    try {
      //
      res = await db.insert('Conexiones', nuevaConexion.toJson());
      //
    } catch (e) {
      //
      print(e);
      throw Exception('Error al guardar en SQLite');
    }

    return res;
  }

  Future<ConexionesModel> getConexionLicencia(String licencia) async {
    final db = await database;
    final res = await db
        .query('Conexiones', where: 'licencia = ?', whereArgs: [licencia]);

    return res.isNotEmpty ? ConexionesModel.fromJson(res.first) : null;
  }

  Future<List<ConexionesModel>> getTodasConexiones() async {
    final db = await database;

    List<ConexionesModel> list;

    try {
      final res = await db.query('Conexiones');

      list = res.isNotEmpty
          ? res.map((c) => ConexionesModel.fromJson(c)).toList()
          : [];
      //

    } catch (e) {
      print(e);
      throw Exception('Error al cargar todas las conexiones');
    }

    return list;
  }

  Future<int> deleteConexion(int id) async {
    final db = await database;

    int res;

    try {
      //
      res = await db.delete('Conexiones', where: 'id = ?', whereArgs: [id]);
      //
    } catch (e) {
      //
      print(e);
      throw Exception('Error al eliminar conexion');
    }

    return res;
  }

  Future<int> deleteConexiones() async {
    final db = await database;

    final res = await db.delete('Conexiones');
    return res;
  }

  //**Operaciones para la conexión activa actual**

  guardarConexionActiva(ConexionesModel conexionActual) async {
    final db = await database;

    final res = await db.insert('ConexionActiva', conexionActual.toJson());

    return res;
  }

  Future<ConexionesModel> getConexionActiva() async {
    final db = await database;
    final res = await db.query('ConexionActiva');

    return res.isNotEmpty ? ConexionesModel.fromJson(res.first) : null;
  }

  Future<int> deleteConexionActiva() async {
    final db = await database;

    final res = await db.delete('ConexionActiva');
    return res;
  }

  //**Operaciones para las mesas**

  nuevaMesa(MesaModel nuevaMesa) async {
    final db = await database;

    final res = await db.insert('Mesas', nuevaMesa.toJson());

    return res;
  }

  Future<List<MesaModel>> getTodasMesas() async {
    final db = await database;

    final res = await db.rawQuery('SELECT * FROM Mesas ORDER BY nombre ASC');

    // print(res);

    List<MesaModel> list =
        res.isNotEmpty ? res.map((c) => MesaModel.fromJson(c)).toList() : [];

    return list;
  }

  Future<List<MesaModel>> getRefreshMesas() async {
    final db = await database;

    final res =
        await db.rawQuery('SELECT MAX(objetoid) AS objetoid FROM Mesas');

    // print(res);

    List<MesaModel> list =
        res.isNotEmpty ? res.map((c) => MesaModel.fromJson(c)).toList() : [];

    return list;
  }

  Future<int> deleteMesas() async {
    final db = await database;

    final res = await db.delete('Mesas');
    return res;
  }

  Future<int> deleteMesasSobrantes(int idPostgres) async {
    final db = await database;

    final res =
        await db.rawDelete('DELETE FROM Mesas WHERE objetoid > $idPostgres');
    return res;
  }

  Future<VendedorModel> obtenerVendedor() async {
    final db = await database;

    final res = await db.query('Vendedor');

    return res.isNotEmpty ? VendedorModel.fromJson(res.first) : null;
  }

  actualizaMesas(MesaModel model, int objetoId) async {
    final db = await database;

    final res = db.update('Mesas', model.toJson(),
        where: 'objetoid = ?', whereArgs: [objetoId]);

    // final res = await db.rawUpdate(
    //     'UPDATE Mesas SET nombre = "$nombre", estado = $estado, consumido = $consumido, esquemaid = $esquemaid WHERE objetoid = $objetoid');

    return res;
  }

  actualizarEstadoMesa(int estado, int objetoid) async {
    final db = await database;

    final res = await db.rawUpdate(
        'UPDATE Mesas SET estado = $estado WHERE objetoid = $objetoid');

    print(objetoid);
    print(estado);

    return res;
  }

  obtenerMesaPorEsquemaId(int esquemaId) async {
    final db = await database;

    final res = await db.rawQuery(
        'SELECT M.* FROM Mesas M LEFT JOIN Esquemas E ON M.esquemaid = E.esquemaid WHERE E.id = $esquemaId ORDER BY nombre ASC');

    List<MesaModel> list =
        res.isNotEmpty ? res.map((c) => MesaModel.fromJson(c)).toList() : [];

    return list;
  }

  obtenerMesaPorEsquemaVendedor(int esquemaid) async {
    final db = await database;

    VendedorModel mesero = await obtenerVendedor();

    int vendedorid = mesero.vendedorid;

    final res = await db.rawQuery(
        'SELECT * FROM Mesas WHERE esquemaid = $esquemaid AND vendedorid = $vendedorid ORDER BY nombre ASC');

    List<MesaModel> list =
        res.isNotEmpty ? res.map((c) => MesaModel.fromJson(c)).toList() : [];

    return list;
  }

  //* Operaciones para las categorías */

  agregarNuevaCategoria(CategoriasModel nuevaCategoria) async {
    final db = await database;

    final res = await db.insert('Categorias', nuevaCategoria.toJson());

    return res;
  }

  Future<List> consultaExistencias() async {
    final db = await database;

    final res = await db.rawQuery('SELECT COUNT (*) AS total FROM Categorias');

    return res;
  }

  Future<List<CategoriasModel>> getTodasCategorias() async {
    final db = await database;

    final res = await db.rawQuery(
        'SELECT * FROM Categorias WHERE grupopadreid = 0 ORDER BY ordenvista ASC, descripcion ASC ');

    List<CategoriasModel> list = res.isNotEmpty
        ? res.map((c) => CategoriasModel.fromJson(c)).toList()
        : [];

    return list;
  }

  Future<List<CategoriasModel>> getSubCategorias(int grupoid) async {
    final db = await database;
    final res = await db
        .rawQuery('SELECT * FROM Categorias WHERE grupopadreid = ?', [grupoid]);

    List<CategoriasModel> list = res.isNotEmpty
        ? res.map((c) => CategoriasModel.fromJson(c)).toList()
        : [];

    return list;
  }

  Future<int> deleteCategorias() async {
    final db = await database;

    final res = await db.delete('Categorias');
    return res;
  }

  //* Operaciones para los productos */

  agregarNuevoProducto(ProductosCuentaModel nuevoProducto) async {
    final db = await database;

    final res = await db.insert('Productos', nuevoProducto.toJson());

    return res;
  }

  actualizarProductosListaPrecio(ProductosCuentaModel producto) async {
    final db = await database;

    await db.rawUpdate(
        "UPDATE Productos SET costo = ? , productoporipo = ?, productoporiva = ?, estado = ? WHERE productoid = ?",
        [
          producto.costo,
          producto.productoPorIpo,
          producto.productoPorIva,
          producto.estado,
          producto.productoid
        ]);
  }

  Future<List> consultaExistenciasProductos(int grupo1id) async {
    final db = await database;

    final res = await db.rawQuery(
        'SELECT COUNT (*) AS total FROM Productos WHERE grupo1id = ? AND estado = 0',
        [grupo1id]);

    return res;
  }

  Future<List<ProductosCuentaModel>> getTodosProductos() async {
    final db = await database;

    final res = await db.query('Productos', where: "estado = 0");

    List<ProductosCuentaModel> list = res.isNotEmpty
        ? res.map((c) => ProductosCuentaModel.fromJson(c)).toList()
        : [];

    return list;
  }

  Future<String> getMaxTsProductos() async {
    final db = await database;

    final res = await db.rawQuery("SELECT MAX(ts) AS ts FROM Productos");

    return res.first["ts"];
  }

  Future<void> actualizarProductoLocal(ProductosCuentaModel producto) async{
    final db = await database;

    await db.update('Productos', producto.toJson(), where: 'productoid = ?', whereArgs: [producto.productoid]);
  }

  Future<List<ProductosCuentaModel>> getProductosPorNombre(String nombre) async {
    final db = await database;

    final res = await db
        .rawQuery("SELECT * FROM Productos"); //WHERE nombre = ?', [nombre]);

    List<ProductosCuentaModel> list = res.isNotEmpty
        ? res.map((c) => ProductosCuentaModel.fromJson(c)).toList()
        : [];

    return list
        .where((element) => element.nombre
            .toLowerCase()
            .contains(nombre.toString().toLowerCase()))
        .toList();
  }

  Future<ProductosCuentaModel> getProductosPorCodigo(String codigo) async {
    final db = await database;

    final res = await db.rawQuery("SELECT * FROM Productos WHERE codigo  = ? OR codigobarras1 = ? OR codigobarras2 = ?",
        [codigo, codigo, codigo]); //WHERE nombre = ?', [nombre]);

    ProductosCuentaModel list =
        res.isNotEmpty ? ProductosCuentaModel.fromJson(res.first) : null;

    return list;
  }

  Future<bool> productoPorCodigoExiste(String codigo) async {
    final db = await database;

    final res = await db.rawQuery("SELECT * FROM Productos WHERE codigo  = ?",
        [codigo]); //WHERE nombre = ?', [nombre]);

    ProductosCuentaModel list =
        res.isNotEmpty ? ProductosCuentaModel.fromJson(res.first) : null;

    return list != null;
  }

  Future<ProductosCuentaModel> getProductosPorProductoId(int id) async {
    final db = await database;

    final res = await db.rawQuery(
        "SELECT * FROM Productos WHERE productoid = ?",
        [id]); //WHERE nombre = ?', [nombre]);

    ProductosCuentaModel list =
        res.isNotEmpty ? ProductosCuentaModel.fromJson(res.first) : null;

    return list;
  }

  Future<List<ProductosCuentaModel>> getProductosPorCategoria(int grupo1id) async {
    final db = await database;

    final res = await db.rawQuery(
        'SELECT * FROM Productos WHERE grupo1id = ? AND estado = 0 ORDER BY ordenvista, nombre ASC',
        [grupo1id]);

    List<ProductosCuentaModel> list = res.isNotEmpty
        ? res.map((c) => ProductosCuentaModel.fromJson(c)).toList()
        : [];

    return list;
  }

   Future<void> actualizarProducto(ProductosCuentaModel producto) async {
    final db = await database;

    await db.rawQuery(
        'UPDATE Productos SET codigo = ?, nombre = ?, grupo1id = ?, precio =  ?, costo = ?, productoimprimecomanda = ?, centrocostoid = ?, foto = ?, ts = ?, esservicio = ?, portiempo = ?, pidepeso = ?, estado = ?  WHERE productoid = ?',
        [producto.codigo, producto.nombre, producto.grupo1id ?? 0, producto.precio ?? 0, producto.costo ?? 0,  producto.productoImprimeComanda, producto.centrocostoid, producto.foto, producto.ts, producto.esServicio, producto.porTiempo, producto.pidePeso, producto.estado, producto.productoid]);

    return;
  }

  Future<void> actualizarProductoBarras(String barcode, int cod, int productoid, DateTime ts) async {
    final db = await database;

    if (cod == 1) {
      await db.update('Productos', {
        "codigobarras1": barcode,
        "ts": ts
      }, where: "productoid = ?", whereArgs: [productoid]);
    }
    else{
      await db.update('Productos', {
        "codigobarras2": barcode,
        "ts": ts
      }, where: "productoid = ?", whereArgs: [productoid]);
    }

    return;
  }

  Future<void> actualizarCategoria(CategoriasModel categoria) async{
    final db = await database;

    await db.update('Categorias', categoria.toJson(), where: "grupoid = ?", whereArgs: [categoria.grupoid]);

    return;
  }

  Future<int> deleteProductos() async {
    final db = await database;

    final res = await db.delete('Productos');
    return res;
  }

  //* Operaciones para vendedor */

  agregarVendedor(VendedorModel nuevoVendedor) async {
    final db = await database;

    final res = await db.insert('Vendedor', nuevoVendedor.toJson());

    return res;
  }

  borrarVendedor() async {
    final db = await database;

    final res = db.delete('Vendedor');

    return res;
  }

  //* Operaciones para los esquemas */

  agregarEsquema(EsquemaModel nuevoEsquema) async {
    final db = await database;

    final res = await db.insert('Esquemas', nuevoEsquema.toJson());

    return res;
  }

  Future<List<EsquemaModel>> obtenerEsquemas() async {
    final db = await database;

    final res = await db.rawQuery('SELECT * FROM Esquemas');

    List<EsquemaModel> list =
        res.isNotEmpty ? res.map((c) => EsquemaModel.fromJson(c)).toList() : [];

    return list;
  }

  Future<List<ProductosGustosModel>> obtenerProductosGustos() async {
    final db = await database;
    final res = await db.rawQuery('SELECT * FROM Productosgustos');

    List<ProductosGustosModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosGustosModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  Future<List<ProductosGustosModel>> obtenerProductosGustosPorId(
      int productoid) async {
    final db = await database;
    final res = await db.rawQuery(
        "SELECT * FROM Productosgustos WHERE productoid = $productoid");

    List<ProductosGustosModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosGustosModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  agregarProductosGustos(ProductosGustosModel nuevoProductosGustos) async {
    final db = await database;

    final res =
        await db.insert('Productosgustos', nuevoProductosGustos.toJson());

    return res;
  }

  eliminarProductosGustos(int gustoid) async {
    final db = await database;

    final res =
        await db.delete('Productosgustos', where: "gustoid = ?", whereArgs: [gustoid]);

    return res;
  }

  //
  Future<List<ProductosCombosModel>> obtenerProductosCombos() async {
    final db = await database;
    final res = await db.rawQuery('SELECT * FROM Productoscombos');

    List<ProductosCombosModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosCombosModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  Future<List<ProductosCombosModel>> obtenerProductosCombosPorId(
      int productoid) async {
    final db = await database;
    final res = await db.rawQuery(
        "SELECT * FROM Productoscombos WHERE productoid = $productoid");

    List<ProductosCombosModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosCombosModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  agregarProductosCombos(ProductosCombosModel nuevoProductosCombos) async {
    final db = await database;

    final res =
        await db.insert('Productoscombos', nuevoProductosCombos.toJson());

    return res;
  }

  borrarProductosCombos() async {
    final db = await database;

    final res = await db.delete('Productoscombos');

    return res;
  }

  //
  Future<List<ProductosEquivalenciasModel>>
      obtenerProductosEquivalencias() async {
    final db = await database;
    final res = await db
        .rawQuery('SELECT * FROM Productosequivalencias WHERE estado = 0');

    List<ProductosEquivalenciasModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosEquivalenciasModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  Future<List<ProductosEquivalenciasModel>> obtenerProductosEquivalenciasPorId(
      int productoid) async {
    final db = await database;
    final res = await db.rawQuery(
        "SELECT * FROM Productosequivalencias WHERE productoid = $productoid AND estado = 0");

    List<ProductosEquivalenciasModel> resultado = res.isNotEmpty
        ? res.map((c) => ProductosEquivalenciasModel.fromJson(c)).toList()
        : [];
    return resultado;
  }

  agregarProductosEquivalencias(
      ProductosEquivalenciasModel nuevoProductosEquivalencias) async {
    final db = await database;

    final res = await db.insert(
        'Productosequivalencias', nuevoProductosEquivalencias.toJson());

    return res;
  }

  borrarProductosEquivalencias() async {
    final db = await database;

    final res = await db.delete('Productosequivalencias');

    return res;
  }

  actualizarProductosEquivalenciasListaPrecio(
      ProductosEquivalenciasModel equivalencia) async {
    final db = await database;

    await db.rawUpdate(
        "UPDATE Productosequivalencias SET porcentajeiva = ?, porcentajeipo = ?, estado = ?, precioventaincluido = ? WHERE equivalenciaid = ?",
        [
          equivalencia.porcentajeiva,
          equivalencia.porcentajeipo,
          equivalencia.estado,
          equivalencia.precioventaincluido,
          equivalencia.equivalenciaid
        ]);
  }

  //

  Future<EsquemaModel> obtenerEsquemaPorId(int id) async {
    final db = await database;

    final res = await db.rawQuery('SELECT * FROM Esquemas WHERE id = $id');

    EsquemaModel esquemaPorId =
        res.isNotEmpty ? EsquemaModel.fromJson(res.first) : null;

    return esquemaPorId;
  }

  borrarEsquemas() async {
    final db = await database;

    final res = await db.delete('Esquemas');

    return res;
  }

  borrarProductosGustos() async {
    final db = await database;

    final res = await db.delete('Productosgustos');

    return res;
  }

  Future<AjustesModel> obtenerAjustes() async {
    final db = await database;

    final res = await db.query('Ajustes');

    AjustesModel ajustes =
        res.isNotEmpty ? AjustesModel.fromJson(res.first) : null;

    return ajustes;
  }

  actualizarAjustes(String column, int data) async {
    final db = await database;

    await db.update('Ajustes', {
      '$column': data,
    });

    return;
  }

  agregarSucursal(SucursalModel sucursalModel) async {
    final db = await database;

    final res =
        await db.insert('sucursal', sucursalModel.toJson());

    return res;
  }

  Future<List<SucursalModel>> obtenerSucursales() async {
    final db = await database;

    final res = await db.query('sucursal');

    List<SucursalModel> resultado = res.isNotEmpty
        ? res.map((c) => SucursalModel.fromJson(c)).toList()
        : [];
        
    return resultado;
  }

  borrarSucursales() async {
    final db = await database;

    final res = await db.delete('sucursal');

    return res;
  }
}
