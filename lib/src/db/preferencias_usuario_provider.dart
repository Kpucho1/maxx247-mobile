import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  //Se crea el singleton por medio de constructor privado para manejar una única instancia

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  //Crea el campo de preferencias si no existe aún
  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET del uid de Firebase
  get uid {
    return _prefs.getString('uid') ?? '';
  }

  set uid(String value) {
    _prefs.setString('uid', value);
  }

  // GET y SET del hashCode del timer que actualiza las mesas
  get timerHashCode {
    return _prefs.getInt('timerHashCode') ?? '';
  }

  set timerHashCode(int value) {
    _prefs.setInt('timerHashCode', value);
  }

  //GET y SET de la IP activa
  get ipServidor {
    return _prefs.getString('ipServidor');
  }

  set ipServidor(String ip) {
    _prefs.setString('ipServidor', ip);
  }

  // GET y SET del email
  get email {
    return _prefs.getString('email') ?? '';
  }

  set email(String value) {
    _prefs.setString('email', value);
  }


  //GET y SET de la licencia de la empresa
  get licencia {
    return _prefs.getString('licencia') ?? '';
  }

  set licencia(String value) {
    _prefs.setString('licencia', value);
  }

  //GET y SET de sesionActiva
  get sesionActiva {
    return _prefs.getBool('sesionActiva') ?? false;
  }

  set sesionActiva(bool value) {
    _prefs.setBool('sesionActiva', value);
  }

  //GET y SET de sucursalid
  get sucursalid {
    return _prefs.getInt('sucursalid') ?? 1;
  }

  set sucursalid(int value) {
    _prefs.setInt('sucursalid', value);
  }

  //GET y SET del usuarioid
  get usuarioid {
    return _prefs.getInt('usuarioid');
  }

  set usuarioid(int usuarioid) {
    _prefs.setInt('usuarioid', usuarioid);
  }


  //GET y SET del nombre de la empresa
  get nombreEmpresa {
    return _prefs.getString('nombreempresa');
  }

  set nombreEmpresa(String nombreempresa) {
    _prefs.setString('nombreempresa', nombreempresa);
  }

  //GET y SET de la categoria seleccionada
  get categoriaSeleccionada {
    return _prefs.getInt('categoria');
  }

  set categoriaSeleccionada(int categoria) {
    _prefs.setInt('categoria', categoria);
  }

  //GET y SET del prefijo del fisico
  get prefijoFisico {
    return _prefs.getString('prefijofisico') ?? '';
  }

  set prefijoFisico(String value) {
    _prefs.setString('prefijofisico', value);
  }

  //GET y SET del numero del fisico
  get numeroFisico {
    return _prefs.getString('numerofisico') ?? '';
  }

  set numeroFisico(String value) {
    _prefs.setString('numerofisico', value);
  }

  //GET y SET del esquemaid
  get esquemaid {
    return _prefs.getInt('esquemaid') ?? 0;
  }

  set esquemaid(int value) {
    _prefs.setInt('esquemaid', value);
  }

  //GET y SET del nombre sucursal
  get nombreSucursal {
    return _prefs.getString('nombreSucursal') ?? 0;
  }

  set nombreSucursal(String value) {
    _prefs.setString('nombreSucursal', value);
  }

  //GET y SET del nombreCajero
  get nombreCajero {
    return _prefs.getString('nombreCajero') ?? 0;
  }

  set nombreCajero(String value) {
    _prefs.setString('nombreCajero', value);
  }

  //GET y SET del modovista mesas
  get modoVistaMesas {
    return _prefs.getInt('modoVistaMesas') ?? 0;
  }

  set modoVistaMesas(int value) {
    _prefs.setInt('modoVistaMesas', value);
  }
}
