//Interface adaptadora para los productos

abstract class IWrapperProductosComanda {
  //

  Future<Map<String, dynamic>> getProductosComanda(String prefijo, int cuenta);

  Future<Map<String, dynamic>> eliminarProductoComanda(
      int id, String prefijo, int cuenta);

  Future<Map<String, dynamic>> actualizarProductoComanda(
    int id,
    double cantidad,
    int productoId,
    int valorUnitario,
    String prefijo,
    int cuenta,
    List<String> _tempSelectedGustos,
  );
}
