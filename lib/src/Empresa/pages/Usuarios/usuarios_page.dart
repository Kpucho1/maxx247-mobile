import 'package:flutter/material.dart';
import 'package:maxxshop/src/Empresa/models/usuario_model.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';
import 'package:maxxshop/src/Empresa/services/usuarios_postgres_service.dart';
import 'package:maxxshop/src/widgets/image_list_tile.dart';

class UsuariosPage extends StatefulWidget {
  UsuariosPage({Key key}) : super(key: key);

  @override
  State<UsuariosPage> createState() => _UsuariosPageState();
}

class _UsuariosPageState extends State<UsuariosPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Usuarios"),
        centerTitle: true,
      ),
      body: _usuariosBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          //TODO: Pagina crear usuario
        },
      ),
    );
  }

  _usuariosBody(){
    UsuariosPostgresService usuariosPostgresService = UsuariosPostgresService();
    return FutureBuilder(
      future: usuariosPostgresService.obtenerUsuarios(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else{
          List<UsuarioModel> listadoUsuarios = snapshot.data;

          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: listadoUsuarios.length,
            itemBuilder: (context, index) {
              return Card(
                elevation: 5.0,
                shadowColor: Colors.grey,
                child: ListTile(
                  title: Text(listadoUsuarios[index].nombre),
                  subtitle: listadoUsuarios[index].correoelectronico != '' && listadoUsuarios[index].correoelectronico != null ? Text(listadoUsuarios[index].correoelectronico) : Text('Sin correo electronico'),
                  leading: ImageListTile(imageString: listadoUsuarios[index].foto, primerLetra: listadoUsuarios[index].nombre[0] ?? 'N',),
                ),
              );
            },
          );
        }
      },
    );
  }
}