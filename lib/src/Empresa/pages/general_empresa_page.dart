import 'package:flutter/material.dart';
import 'package:maxxshop/src/Empresa/models/empresa_model.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';
import 'package:maxxshop/src/widgets/boton_subir_foto.dart';

class GeneralEmpresaPage extends StatefulWidget {
  final EmpresaModel empresaModel;
  GeneralEmpresaPage({Key key, this.empresaModel}) : super(key: key);

  @override
  State<GeneralEmpresaPage> createState() => _GeneralEmpresaPageState();
}

class _GeneralEmpresaPageState extends State<GeneralEmpresaPage> {

  TextEditingController _nitEmpresaController = TextEditingController();
  TextEditingController _nombreEmpresaController = TextEditingController();
  TextEditingController _direccionEmpresaController = TextEditingController();
  TextEditingController _telefonoEmpresaController = TextEditingController();
  TextEditingController _emailEmpresaController = TextEditingController();

  final _botonSubirFoto = BotonSubirFoto();


  @override
  void initState() {
    _nitEmpresaController.text = widget.empresaModel.nit.toString();
    _nombreEmpresaController.text = widget.empresaModel.nombre;
    _direccionEmpresaController.text = widget.empresaModel.direccion;
    _telefonoEmpresaController.text = widget.empresaModel.telefono;
    _emailEmpresaController.text = widget.empresaModel.email;
    super.initState();
    
  }

  @override
  void dispose() {
    _nitEmpresaController?.dispose();
    _nombreEmpresaController?.dispose();
    _direccionEmpresaController?.dispose();
    _telefonoEmpresaController?.dispose();
    _emailEmpresaController?.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Informacion general"),
        centerTitle: true,
      ),
      body: _formularioGeneralEmpresa()
    );
  }

  Widget _formularioGeneralEmpresa() {

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _botonActualizarEmpresa(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nitEmpresaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreEmpresaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _direccionEmpresaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _telefonoEmpresaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _emailEmpresaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              // _botonAgregarFoto()
              _botonSubirFoto
            ],
          ),
        ),
      ),
    );
  }

  Widget _botonActualizarEmpresa(){
    return TextButton(
      onPressed: () async{
        EmpresaModel empresaModel = EmpresaModel(
          nombre: _nombreEmpresaController.text,
          nit: _nitEmpresaController.text,
          direccion: _direccionEmpresaController.text,
          telefono: _telefonoEmpresaController.text,
          email: _emailEmpresaController.text,
        );

        EmpresaPostgresService empresaPostgresService = EmpresaPostgresService();
        
        await empresaPostgresService.actualizarInformacionGeneralEmpresa(empresaModel);

        Navigator.pop(context);
      },
      child: Text("Actualizar"),
    );
  }

  Widget _nitEmpresaInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _nitEmpresaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Nit*',
      ),
    );
  }

  Widget _nombreEmpresaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreEmpresaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre de la empresa*',
      ),
    );
  }

  Widget _direccionEmpresaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _direccionEmpresaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Direccion',
      ),
    );
  }

  Widget _telefonoEmpresaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _telefonoEmpresaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Telefono',
      ),
    );
  }

  Widget _emailEmpresaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _emailEmpresaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Email',
      ),
    );
  }


}