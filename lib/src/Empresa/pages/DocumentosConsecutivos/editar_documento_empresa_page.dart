import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';

class EditarDocumentoEmpresaPage extends StatefulWidget {
  final DocumentoConsecutivoModel documento;
  EditarDocumentoEmpresaPage({Key key, this.documento}) : super(key: key);

  @override
  State<EditarDocumentoEmpresaPage> createState() => _EditarDocumentoEmpresaPageState();
}

class _EditarDocumentoEmpresaPageState extends State<EditarDocumentoEmpresaPage> {
  TextEditingController _prefijoController = TextEditingController();
  TextEditingController _numeroInicialController = TextEditingController();
  TextEditingController _numeroFinalController = TextEditingController();
  TextEditingController _numeroActualController = TextEditingController();
  TextEditingController _resolucionDianController = TextEditingController();

  EmpresaPostgresService _empresaPostgresService = EmpresaPostgresService();

  @override
  void dispose() {
    _prefijoController?.dispose();
    _numeroInicialController?.dispose();
    _numeroFinalController?.dispose();
    _numeroActualController?.dispose();
    _resolucionDianController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _prefijoController.text = widget.documento.prefijo;
    _numeroInicialController.text = widget.documento.numeroinicial.toString();
    _numeroFinalController.text = widget.documento.numerofinal.toString();
    _numeroActualController.text = widget.documento.numeroactual.toString();
    _tipoDocumento = widget.documento.tipodocumento;
    _resolucionDianController.text = widget.documento.resoluciondian;
    _loadTipos();
    super.initState();
  }

  int _tipoDocumento;
  dynamic _jsonResult;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Editar Documento"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Actualizar", style: TextStyle(color: Colors.white),),
            onPressed: () async{
              DocumentoConsecutivoModel documento = DocumentoConsecutivoModel(
                consecutivoid: widget.documento.consecutivoid,
                prefijo: _prefijoController.text,
                numeroactual: int.tryParse(_numeroActualController.text),
                numeroinicial: int.tryParse(_numeroInicialController.text),
                numerofinal: int.tryParse(_numeroFinalController.text),
                resoluciondian: _resolucionDianController.text,
                tipodocumento: _tipoDocumento
              );

              await _empresaPostgresService.actualizarDocumentoConsecutivo(documento);

              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _formularioDocumentosConsecutivos(),
    );
  }

   Widget _formularioDocumentosConsecutivos() {

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _prefijoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroInicialInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroFinalInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroActualInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _dropDownTipoDocumento(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _tipoDocumento == 1 || _tipoDocumento == 18 ? _resolucionDianInput() : SizedBox(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _botonEliminarDocumentoConsecutivo()
              
            ],
          ),
        ),
      ),
    );
  }

  Future<List> _loadTipos() async{
    // String data = await DefaultAssetBundle.of(context).loadString("assets/countries.json");

    String data = await rootBundle.loadString("assets/tipos_documentos.json");

    _jsonResult = json.decode(data);

    List<dynamic> _listadoTipos = _jsonResult['tipos'];

    return _listadoTipos;

  }

  Widget _dropDownTipoDocumento(){
    return FutureBuilder(
      future: _loadTipos(),
      builder: (context, snapshot) {

        if (!snapshot.hasData) {
          return SizedBox();
        }
        else{
          List<dynamic> listadoTipos = snapshot.data;

          return DropdownButton(
            style: const TextStyle(color: Colors.black),
            isExpanded: true,
            hint: Text("Tipo documento"),
            icon: Icon(Icons.arrow_downward),
            value: _tipoDocumento,
            underline: Container(
              height: 2,
              color: Colors.grey,
            ),
            onChanged: (value) {
              setState(() {
                _tipoDocumento = value;
              });
            },
            items: listadoTipos.map((e) {
              return DropdownMenuItem(
                value: e["id"],
                child: Text(e["nombre"]),
              );
            }).toList(),
          );
        }
        
      }
    );
  }

  Widget _prefijoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _prefijoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Prefijo*',
      ),
    );
  }

  Widget _resolucionDianInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _resolucionDianController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Resolucion Dian',
      ),
    );
  }

  Widget _numeroInicialInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroInicialController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Inicial',
      ),
    );
  }

  Widget _numeroFinalInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroFinalController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Final',
      ),
    );
  }

  Widget _numeroActualInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroActualController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Actual',
      ),
    );
  }

  Widget _botonEliminarDocumentoConsecutivo() {
    return OutlinedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 10.0,
        )),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: Text(
        'Eliminar',
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: 14.0,
        ),
      ),
      onPressed: () async{
        await _empresaPostgresService.eliminarDocumentoConsecutivo(widget.documento.consecutivoid);

        Navigator.pop(context);
      },
    );
  }
}