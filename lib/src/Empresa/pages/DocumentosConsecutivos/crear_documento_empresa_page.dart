import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';

class CrearDocumentoEmpresaPage extends StatefulWidget {
  CrearDocumentoEmpresaPage({Key key}) : super(key: key);

  @override
  State<CrearDocumentoEmpresaPage> createState() => _CrearDocumentoEmpresaPageState();
}

class _CrearDocumentoEmpresaPageState extends State<CrearDocumentoEmpresaPage> {
  TextEditingController _prefijoController = TextEditingController();
  TextEditingController _numeroInicialController = TextEditingController();
  TextEditingController _numeroFinalController = TextEditingController();
  TextEditingController _numeroActualController = TextEditingController();
  TextEditingController _resolucionDianController = TextEditingController();


  int _tipoDocumento = 1;
  dynamic _jsonResult;

  @override
  void dispose() {
    _prefijoController?.dispose();
    _numeroInicialController?.dispose();
    _numeroFinalController?.dispose();
    _numeroActualController?.dispose();
    _resolucionDianController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _loadTipos();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Crear Documento"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Crear", style: TextStyle(color: Colors.white),),
            onPressed: () async{
              DocumentoConsecutivoModel documentoConsecutivoModel = DocumentoConsecutivoModel(
                prefijo: _prefijoController.text,
                numeroinicial: int.tryParse(_numeroInicialController.text),
                numerofinal: int.tryParse(_numeroFinalController.text),
                numeroactual: int.tryParse(_numeroActualController.text),
                resoluciondian: _resolucionDianController.text ?? '',
                tipodocumento: _tipoDocumento
              );

              EmpresaPostgresService empresaPostgresService = EmpresaPostgresService();

              await empresaPostgresService.crearDocumentoConsecutivoEmpresa(documentoConsecutivoModel);

              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _formularioDocumentosConsecutivos(),
    );
  }

  Widget _formularioDocumentosConsecutivos() {

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _prefijoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroInicialInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroFinalInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _numeroActualInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _dropDownTipoDocumento(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _tipoDocumento == 1 || _tipoDocumento == 18 ? _resolucionDianInput() : SizedBox()
              
            ],
          ),
        ),
      ),
    );
  }

  Future<List> _loadTipos() async{
    // String data = await DefaultAssetBundle.of(context).loadString("assets/countries.json");

    String data = await rootBundle.loadString("assets/tipos_documentos.json");

    _jsonResult = json.decode(data);

    List<dynamic> _listadoTipos = _jsonResult['tipos'];

    return _listadoTipos;

  }

  Widget _dropDownTipoDocumento(){
    return FutureBuilder(
      future: _loadTipos(),
      builder: (context, snapshot) {

        if (!snapshot.hasData) {
          return SizedBox();
        }
        else{
          List<dynamic> listadoTipos = snapshot.data;

          return DropdownButton(
            style: const TextStyle(color: Colors.black),
            isExpanded: true,
            hint: Text("Tipo documento"),
            icon: Icon(Icons.arrow_downward),
            value: _tipoDocumento,
            underline: Container(
              height: 2,
              color: Colors.grey,
            ),
            onChanged: (value) {
              setState(() {
                _tipoDocumento = value;
              });
            },
            items: listadoTipos.map((e) {
              return DropdownMenuItem(
                value: e["id"],
                child: Text(e["nombre"]),
              );
            }).toList(),
          );
        }
        
      }
    );
  }

  Widget _prefijoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _prefijoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Prefijo*',
      ),
    );
  }

  Widget _resolucionDianInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _resolucionDianController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Resolucion Dian',
      ),
    );
  }

  Widget _numeroInicialInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroInicialController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Inicial',
      ),
    );
  }

  Widget _numeroFinalInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroFinalController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Final',
      ),
    );
  }

  Widget _numeroActualInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _numeroActualController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Numero Actual',
      ),
    );
  }


}