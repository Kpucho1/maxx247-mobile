import 'package:flutter/material.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/Empresa/pages/DocumentosConsecutivos/crear_documento_empresa_page.dart';
import 'package:maxxshop/src/Empresa/pages/DocumentosConsecutivos/editar_documento_empresa_page.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';

class DocumentosConsecutivosEmpresaPage extends StatefulWidget {
  DocumentosConsecutivosEmpresaPage({Key key}) : super(key: key);

  @override
  State<DocumentosConsecutivosEmpresaPage> createState() => _DocumentosConsecutivosEmpresaPageState();
}

class _DocumentosConsecutivosEmpresaPageState extends State<DocumentosConsecutivosEmpresaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Documentos Consecutivos"),
        centerTitle: true,
      ),      
      body: _cuerpoDocumentosConsecutivos(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          await Navigator.push(context, MaterialPageRoute(builder: (context) => CrearDocumentoEmpresaPage())).then((value) {
            setState(() {});
          });
        },
      ),
    );
  }

  Widget _cuerpoDocumentosConsecutivos(context){
    EmpresaPostgresService empresaPostgresService = EmpresaPostgresService();

    return FutureBuilder(
      future: empresaPostgresService.obtenerDocumentosConsecutivosEmpresa(),
      builder: (context, AsyncSnapshot<List<DocumentoConsecutivoModel>> snapshot){
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator()
          );
        }
        else{
          List<DocumentoConsecutivoModel> _listadoDocumentos = snapshot.data;

          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: _listadoDocumentos.length,
            itemBuilder: (context, index){
              return ListTile(
                title: Text( "Prefijo: "+_listadoDocumentos[index].prefijo),
                trailing: Text("Num actual: "+_listadoDocumentos[index].numeroactual.toString()),
                subtitle: Text("Num Inicial: "+_listadoDocumentos[index].numeroinicial.toString() + " Num Final: "+ _listadoDocumentos[index].numerofinal.toString()),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditarDocumentoEmpresaPage( documento: _listadoDocumentos[index],))).then((value) {
                    setState(() {
                      
                    });
                  });
                },
              );
            },
          );
        }
      },
    );
  }
}