import 'package:flutter/material.dart';
import 'package:maxxshop/src/Empresa/models/configuracion_empresa_model.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';

class ConfiguracionEmpresaPage extends StatefulWidget {
  final ConfiguracionEmpresaModel configuracion;
  ConfiguracionEmpresaPage({Key key, this.configuracion}) : super(key: key);

  @override
  State<ConfiguracionEmpresaPage> createState() => _ConfiguracionEmpresaPageState();
}

class _ConfiguracionEmpresaPageState extends State<ConfiguracionEmpresaPage> {
  TextEditingController _direccionIpImpresoraFactura = TextEditingController();
  EmpresaPostgresService _empresaPostgresService = EmpresaPostgresService();

  @override
  void initState() {
    super.initState();
    _direccionIpImpresoraFactura.text = widget.configuracion.ipimpresora; 
  }

  @override
  void dispose() {
    _direccionIpImpresoraFactura?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Configuracion"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Actualizar", style: TextStyle(color: Colors.white),),
            onPressed: () async{
              ConfiguracionEmpresaModel configuracion = ConfiguracionEmpresaModel(
                ipimpresora: _direccionIpImpresoraFactura.text
              );

              await _empresaPostgresService.actualizarConfiguracionEmpresa(configuracion);

              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _formularioConfiguracionEmpresa(),
    );
  }

  Widget _formularioConfiguracionEmpresa() {

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
             _direccionIpImpresoraInput()
            ],
          ),
        ),
      ),
    );
  }

  Widget _direccionIpImpresoraInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _direccionIpImpresoraFactura,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Ip impresora factura',
      ),
    );
  }
}