import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maxxshop/src/Empresa/models/configuracion_empresa_model.dart';
import 'package:maxxshop/src/Empresa/models/empresa_model.dart';
import 'package:maxxshop/src/Empresa/pages/Configuracion/configuracion_empresa_page.dart';
import 'package:maxxshop/src/Empresa/pages/DocumentosConsecutivos/documentos_empresa_page.dart';
import 'package:maxxshop/src/Empresa/pages/Usuarios/usuarios_page.dart';
import 'package:maxxshop/src/Empresa/pages/general_empresa_page.dart';
import 'package:maxxshop/src/Empresa/services/empresa_postgres_service.dart';
import 'package:maxxshop/src/Utils/modulo_creacion_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';

class MenuEmpresaPage extends StatefulWidget {
  MenuEmpresaPage({Key key}) : super(key: key);

  @override
  State<MenuEmpresaPage> createState() => _MenuEmpresaPageState();
}

class _MenuEmpresaPageState extends State<MenuEmpresaPage> {
  EmpresaPostgresService empresaPostgresService = EmpresaPostgresService();


  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Empresa"),
        centerTitle: true,
      ),
      body: Container(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          _imagenDecoracion(),
          _botonesRedondeados(),
        ]),
      ),
    );;
  }

  Widget _imagenDecoracion() {
    return Container(
      child: SafeArea(
        child: Center(
          child: SvgPicture.asset(
            'assets/empresa.svg',
            height: 150.0,
          ),
        ),
      ),
    );
  }


  Widget _botonesRedondeados() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.settings,
                  texto: 'General',
                ),
                onTap: () async {

                  EmpresaModel empresaModel = await empresaPostgresService.obtenerInformacionGeneralEmpresa();

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GeneralEmpresaPage( empresaModel: empresaModel,),
                      ));
                },
              ),
              InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DocumentosConsecutivosEmpresaPage(),
                      ));
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.document_scanner,
                  texto: 'Documentos',
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                ConfiguracionEmpresaModel configuracionEmpresaModel = await empresaPostgresService.obtenerConfiguracionEmpresa();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ConfiguracionEmpresaPage(configuracion: configuracionEmpresaModel,),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.list,
                texto: 'Configuracion',
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UsuariosPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.person,
                texto: 'Usuarios',
              ),
            ),
          ]),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ModuloCreacionPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.home,
                texto: 'Bodegas',
              ),
            ),
            SizedBox()
          ]),
        ],
      ),
    );
  }
}