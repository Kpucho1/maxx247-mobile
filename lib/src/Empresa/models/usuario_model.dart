// To parse this JSON data, do
//
//     final usuarioModel = usuarioModelFromJson(jsonString);

import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) => UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
    UsuarioModel({
        this.usuarioid,
        this.nombre,
        this.contrasena,
        this.foto,
        this.correoelectronico,
        this.estado,
        this.modificareportes,
    });

    int usuarioid;
    String nombre;
    String contrasena;
    String foto;
    String correoelectronico;
    int estado;
    int modificareportes;

    factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        usuarioid: json["usuarioid"],
        nombre: json["nombre"],
        contrasena: json["contrasena"],
        foto: json["foto"],
        correoelectronico: json["correoelectronico"],
        estado: json["estado"],
        modificareportes: json["modificareportes"],
    );

    Map<String, dynamic> toJson() => {
        "usuarioid": usuarioid,
        "nombre": nombre,
        "contrasena": contrasena,
        "foto": foto,
        "correoelectronico": correoelectronico,
        "estado": estado,
        "modificareportes": modificareportes,
    };
}
