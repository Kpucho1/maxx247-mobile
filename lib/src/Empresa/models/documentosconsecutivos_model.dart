// To parse this JSON data, do
//
//     final documentoConsecutivoModel = documentoConsecutivoModelFromJson(jsonString);

import 'dart:convert';

DocumentoConsecutivoModel documentoConsecutivoModelFromJson(String str) => DocumentoConsecutivoModel.fromJson(json.decode(str));

String documentoConsecutivoModelToJson(DocumentoConsecutivoModel data) => json.encode(data.toJson());

class DocumentoConsecutivoModel {
    DocumentoConsecutivoModel({
        this.consecutivoid,
        this.prefijo,
        this.numeroinicial,
        this.numerofinal,
        this.numeroactual,
        this.resoluciondian,
        this.tipodocumento
    });

    int consecutivoid;
    String prefijo;
    int numeroinicial;
    int numerofinal;
    int numeroactual;
    String resoluciondian;
    int tipodocumento;

    factory DocumentoConsecutivoModel.fromJson(Map<String, dynamic> json) => DocumentoConsecutivoModel(
        consecutivoid: json["consecutivoid"],
        prefijo: json["prefijo"],
        numeroinicial: json["numeroinicial"],
        numerofinal: json["numerofinal"],
        numeroactual: json["numeroactual"],
        resoluciondian: json["resoluciondian"],
        tipodocumento: json["tipodocumento"]
    );

    Map<String, dynamic> toJson() => {
        "consecutivoid": consecutivoid,
        "prefijo": prefijo,
        "numeroinicial": numeroinicial,
        "numerofinal": numerofinal,
        "numeroactual": numeroactual,
        "resoluciondian": resoluciondian,
        "tipodocumento": tipodocumento
    };
}
