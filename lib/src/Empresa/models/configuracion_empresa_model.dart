// To parse this JSON data, do
//
//     final configuracionEmpresaModel = configuracionEmpresaModelFromJson(jsonString);

import 'dart:convert';

ConfiguracionEmpresaModel configuracionEmpresaModelFromJson(String str) => ConfiguracionEmpresaModel.fromJson(json.decode(str));

String configuracionEmpresaModelToJson(ConfiguracionEmpresaModel data) => json.encode(data.toJson());

class ConfiguracionEmpresaModel {
    ConfiguracionEmpresaModel({
        this.ipimpresora,
    });

    String ipimpresora;

    factory ConfiguracionEmpresaModel.fromJson(Map<String, dynamic> json) => ConfiguracionEmpresaModel(
        ipimpresora: json["ipimpresora"],
    );

    Map<String, dynamic> toJson() => {
        "ipimpresora": ipimpresora,
    };
}
