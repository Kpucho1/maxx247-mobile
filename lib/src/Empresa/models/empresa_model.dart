// To parse this JSON data, do
//
//     final empresaModel = empresaModelFromJson(jsonString);

import 'dart:convert';

EmpresaModel empresaModelFromJson(String str) => EmpresaModel.fromJson(json.decode(str));

String empresaModelToJson(EmpresaModel data) => json.encode(data.toJson());

class EmpresaModel {
    EmpresaModel({
        this.nit,
        this.nombre,
        this.direccion,
        this.telefono,
        this.ciudad,
        this.email,
    });

    String nit;
    String nombre;
    String direccion;
    String telefono;
    String ciudad;
    String email;

    factory EmpresaModel.fromJson(Map<String, dynamic> json) => EmpresaModel(
        nit: json["nit"],
        nombre: json["nombre"],
        direccion: json["direccion"],
        telefono: json["telefono"],
        ciudad: json["ciudad"],
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "nit": nit,
        "nombre": nombre,
        "direccion": direccion,
        "telefono": telefono,
        "ciudad": ciudad,
        "email": email,
    };
}
