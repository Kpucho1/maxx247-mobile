import 'package:maxxshop/src/Empresa/models/configuracion_empresa_model.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/Empresa/models/empresa_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class EmpresaPostgresService {
  EmpresaPostgresService._privateConstructor();

  static final EmpresaPostgresService _instancia =
      EmpresaPostgresService._privateConstructor();

  factory EmpresaPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<EmpresaModel> obtenerInformacionGeneralEmpresa() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> _info = await conexionNegocio.query("SELECT nit, nombre, direccion, telefonos, email FROM datos.empresa WHERE codigoempresaid = @sucursalid", substitutionValues: {
        "sucursalid": prefs.sucursalid
      });

      EmpresaModel empresaModel = EmpresaModel(nit: _info.first[0], nombre: _info.first[1], direccion: _info.first[2], telefono: _info.first[3], email: _info.first[4]);
      
      await conexionNegocio.close();

      return empresaModel;

    } catch (e) {
      print(e);
    }


  }

  Future<void> actualizarInformacionGeneralEmpresa(EmpresaModel empresaModel) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("UPDATE datos.empresa SET nit = @nit, nombre = @nombre, direccion = @direccion, telefonos = @telefono, email = @email WHERE codigoempresaid = @sucursalid", substitutionValues: {
        "nit": empresaModel.nit,
        "nombre": empresaModel.nombre,
        "direccion": empresaModel.direccion,
        "telefono": empresaModel.telefono,
        "email": empresaModel.email,
        "sucursalid": prefs.sucursalid
      });
      
      await conexionNegocio.close();

    } catch (e) {
      print(e);
    }
  }

  Future<List<DocumentoConsecutivoModel>> obtenerDocumentosConsecutivosEmpresa() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    List<DocumentoConsecutivoModel> _listadoConsecutivos = [];
    try {
      List<List<dynamic>> consecutivos = await conexionNegocio.query("SELECT consecutivoid, prefijo, CAST(numeroactual AS integer), resoliciondian, CAST(numerofinal as integer), CAST(numeroinicial AS integer), tipodocumento FROM datos.documentosconsecutivos WHERE sucursalid = @sucursalid ORDER BY tipodocumento ASC", substitutionValues: {
        "sucursalid": prefs.sucursalid
      });

      for (var consecutivo in consecutivos) {
        DocumentoConsecutivoModel documentoConsecutivoModel = DocumentoConsecutivoModel(consecutivoid: consecutivo[0], prefijo: consecutivo[1], numeroactual: consecutivo[2], resoluciondian: consecutivo[3], numerofinal: consecutivo[4], numeroinicial: consecutivo[5], tipodocumento: int.tryParse(consecutivo[6]));
        _listadoConsecutivos.add(documentoConsecutivoModel);
      }

      await conexionNegocio.close();

    } catch (e) {
      print(e);
    }

    return _listadoConsecutivos;


  }

  Future<void> crearDocumentoConsecutivoEmpresa(DocumentoConsecutivoModel documentoConsecutivoModel) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> maxConsecutivoId = await conexionNegocio.query("SELECT MAX(consecutivoid) + 1 FROM datos.documentosconsecutivos");

      await conexionNegocio.query("INSERT INTO datos.documentosconsecutivos(consecutivoid, prefijo, numeroinicial, numerofinal, numeroactual, resoliciondian, tipodocumento, sucursalid) VALUES(@consecutivoid, @prefijo, @numeroinicial, @numerofinal, @numeroactual, @resoluciondian, @tipodocumento, @sucursalid)", substitutionValues: {
        "consecutivoid": maxConsecutivoId.first.first,
        "prefijo": documentoConsecutivoModel.prefijo,
        "numeroinicial": documentoConsecutivoModel.numeroinicial,
        "numerofinal": documentoConsecutivoModel.numerofinal,
        "numeroactual": documentoConsecutivoModel.numeroactual,
        "tipodocumento": documentoConsecutivoModel.tipodocumento,
        "resoluciondian": documentoConsecutivoModel.resoluciondian,
        "sucursalid": prefs.sucursalid
      });

    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

  }

  Future<void> eliminarDocumentoConsecutivo(int consecutivoid) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("DELETE FROM datos.documentosconsecutivos WHERE consecutivoid = @consecutivoid", substitutionValues: {
        "consecutivoid": consecutivoid
      });

    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

  }

  Future<void> actualizarDocumentoConsecutivo(DocumentoConsecutivoModel documentoConsecutivoModel) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("UPDATE datos.documentosconsecutivos SET prefijo = @prefijo, numeroinicial = @numeroinicial, numerofinal = @numerofinal, numeroactual = @numeroactual, resoliciondian = @resoluciondian, tipodocumento = @tipodocumento WHERE consecutivoid = @consecutivoid", substitutionValues: {
        "consecutivoid": documentoConsecutivoModel.consecutivoid,
        "prefijo": documentoConsecutivoModel.prefijo,
        "numeroinicial": documentoConsecutivoModel.numeroinicial,
        "numerofinal": documentoConsecutivoModel.numerofinal,
        "numeroactual": documentoConsecutivoModel.numeroactual,
        "resoluciondian": documentoConsecutivoModel.resoluciondian,
        "tipodocumento": documentoConsecutivoModel.tipodocumento
      });


    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

  }

  Future<ConfiguracionEmpresaModel> obtenerConfiguracionEmpresa() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> info = await conexionNegocio.query("SELECT matricula_mercantil FROM datos.empresa WHERE codigoempresaid = @sucursalid", substitutionValues: {
        "sucursalid": prefs.sucursalid
      });

      ConfiguracionEmpresaModel configuracionEmpresaModel = ConfiguracionEmpresaModel(
        ipimpresora: info.first[0]
      );

      await conexionNegocio.close();

      return configuracionEmpresaModel;

    } catch (e) {
      print(e);
      await conexionNegocio.close();

    }
  }

  Future<void> actualizarConfiguracionEmpresa(ConfiguracionEmpresaModel configuracion) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("UPDATE datos.empresa SET matricula_mercantil = @ipimpresora WHERE codigoempresaid = @sucursalid", substitutionValues: {
        "ipimpresora": configuracion.ipimpresora,
        "sucursalid": prefs.sucursalid
      });

    } catch (e) {
      print(e);

    }
    await conexionNegocio.close();

  }

}