import 'package:maxxshop/src/Empresa/models/usuario_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class UsuariosPostgresService {
  UsuariosPostgresService._privateConstructor();

  static final UsuariosPostgresService _instancia =
      UsuariosPostgresService._privateConstructor();

  factory UsuariosPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<UsuarioModel>> obtenerUsuarios() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<UsuarioModel> _listadoUsuarios = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT usuarioid, nombre, contrasena, COALESCE(convert_from(foto, 'UTF8'), ''), correoelectronico, estado, modificareportes FROM datos.usuarios ORDER BY nombre ASC");

      for (var item in _data) {
        UsuarioModel usuarioModel = UsuarioModel();

        usuarioModel.usuarioid = item[0];
        usuarioModel.nombre = item[1];
        usuarioModel.contrasena = item[2];
        usuarioModel.foto = item[3];
        usuarioModel.correoelectronico = item[4];
        usuarioModel.estado = item[5];
        usuarioModel.modificareportes = item[6];

        _listadoUsuarios.add(usuarioModel);  
      }
    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

    return _listadoUsuarios;
  }
}