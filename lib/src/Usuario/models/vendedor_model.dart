import 'dart:convert';

VendedorModel vendedorModelFromJson(String str) =>
    VendedorModel.fromJson(json.decode(str));

String vendedorModelToJson(VendedorModel data) => json.encode(data.toJson());

class VendedorModel {
  VendedorModel({
    this.vendedorid,
    this.esquemaid,
    this.identificacion,
    this.nombre,
    this.sucursalid,
    this.numeroFacturasPendientes,
    this.pendientesTotalaPagar,
    this.controlCcosto,
    this.abrirTodasCuentas,
    this.expanded,
    this.bodegaid
  });

  int vendedorid;
  int esquemaid;
  String identificacion;
  String nombre;
  int sucursalid;
  int numeroFacturasPendientes;
  int pendientesTotalaPagar;
  int controlCcosto;
  int abrirTodasCuentas;
  bool expanded = true;
  int bodegaid;

  factory VendedorModel.fromJson(Map<String, dynamic> json) => VendedorModel(
        vendedorid: json["vendedorid"],
        esquemaid: json["esquemaid"],
        identificacion: json["identificacion"],
        nombre: json["nombre"],
        sucursalid: json["sucursalid"],
        controlCcosto: json["controlccosto"],
        abrirTodasCuentas: json["abrirtodascuentas"],
        bodegaid: json["bodegaid"]
      );

  Map<String, dynamic> toJson() => {
        "vendedorid": vendedorid,
        "esquemaid": esquemaid,
        "identificacion": identificacion,
        "nombre": nombre,
        "sucursalid": sucursalid,
        "controlccosto": controlCcosto,
        "abrirtodascuentas": abrirTodasCuentas,
        "bodegaid": bodegaid
      };
}
