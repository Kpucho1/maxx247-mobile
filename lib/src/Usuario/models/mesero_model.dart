import 'dart:convert';

MeseroModel meseroModelFromJson(String str) =>
    MeseroModel.fromJson(json.decode(str));

String meseroModelToJson(MeseroModel data) => json.encode(data.toJson());

class MeseroModel {
  MeseroModel({
    this.id,
    this.nombre,
    this.tipoIdentificacion,
    this.numeroIdentificacion,
    this.fechaNacimiento,
    this.nombreUsuario,
  });

  String id;
  String nombre;
  String tipoIdentificacion;
  String numeroIdentificacion;
  String fechaNacimiento;
  String nombreUsuario;

  factory MeseroModel.fromJson(Map<String, dynamic> json) => MeseroModel(
        id: json["id"],
        nombre: json["nombre"],
        tipoIdentificacion: json["tipoIdentificacion"],
        numeroIdentificacion: json["numeroIdentificacion"],
        fechaNacimiento: json["fechaNacimiento"],
        nombreUsuario: json["nombreUsuario"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "tipoIdentificacion": tipoIdentificacion,
        "numeroIdentificacion": numeroIdentificacion,
        "fechaNacimiento": fechaNacimiento,
        "nombreUsuario": nombreUsuario,
      };
}
