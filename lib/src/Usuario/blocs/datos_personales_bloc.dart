import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/Usuario/blocs/validator.dart';

class DatosPersonalesBloc with Validator {
  List<String> tipoDocumentoId = [
    'Cédula de Ciudadanía',
    'Cédula de Extranjería',
    'Pasaporte',
    'Tarjeta de Identidad'
  ];

  final _nombreController = BehaviorSubject<String>();
  final _identificacionTipoController =
      BehaviorSubject<String>.seeded('Cédula de Ciudadanía');
  final _identificacionController = BehaviorSubject<String>();
  final _fechaNacimientoController = BehaviorSubject<String>();

  //Escuchar y recuperar los datos
  Stream<String> get nombreStream =>
      _nombreController.stream.transform(validarNombre);
  Stream<String> get tipoIdentificacionStream =>
      _identificacionTipoController.stream;
  Stream<String> get identificacionStream =>
      _identificacionController.stream.transform(validarIdentificacion);
  Stream<String> get fechaNacimientoStream =>
      _fechaNacimientoController.stream.transform(validarFechaNacimiento);

  //Combina los streams
  Stream<bool> get formValidStream => Rx.combineLatest4(
        nombreStream,
        tipoIdentificacionStream,
        identificacionStream,
        fechaNacimientoStream,
        (a, b, c, d) => true,
      );

  //Inserta valores al stream por TextControllers
  Function(String) get changeNombre => _nombreController.sink.add;
  Function(String) get changeTipoIdentificacion =>
      _identificacionTipoController.sink.add;
  Function(String) get changeIdentificacion =>
      _identificacionController.sink.add;
  Function(String) get changeFechaNacimiento =>
      _fechaNacimientoController.sink.add;

  //Insertar valores al stream por QR
  insertarNombreCompleto(String nombre) {
    _nombreController.sink.add(nombre);
    print(nombre);
  }

  insertarNumeroIdentificacion(String numeroIdentificacion) {
    _identificacionController.sink.add(numeroIdentificacion);
  }

  insertarFechaNacimiento(String fechaNacimiento){
    _fechaNacimientoController.sink.add(fechaNacimiento);
  }

  //Obtiene los ultimos valores emitidos en los streams
  String get nombre => _nombreController.value;
  String get tipoIdentificacion => _identificacionTipoController.value;
  String get identificacion => _identificacionController.value;
  String get fechaNacimiento => _fechaNacimientoController.value;

  dispose() {
    _nombreController?.close();
    _identificacionTipoController?.close();
    _identificacionController?.close();
    _fechaNacimientoController?.close();
  }
}
