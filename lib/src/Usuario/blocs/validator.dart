import 'dart:async';

class Validator {
  final validarEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = new RegExp(pattern);

      if (regExp.hasMatch(email)) {
        sink.add(email);
      } else {
        sink.addError('Correo inválido');
      }
    },
  );

  final validarPassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
      if (password.length >= 6) {
        sink.add(password);
      } else {
        sink.addError('Mínimo 6 caracteres');
      }
    },
  );

  final validarNombre = StreamTransformer<String, String>.fromHandlers(
    handleData: (nombre, sink) {
      if (nombre.length > 1) {
        sink.add(nombre);
      } else {
        sink.addError('Este campo no debe estar vacío');
      }
    },
  );

  final validarIdentificacion = StreamTransformer<String, String>.fromHandlers(
    handleData: (identificacion, sink) {
      if (identificacion.length > 1) {
        sink.add(identificacion);
      } else {
        sink.addError('Este campo no debe estar vacío');
      }
    },
  );

  final validarFechaNacimiento = StreamTransformer<String, String>.fromHandlers(
    handleData: (fechaNacimiento, sink) {
      if (fechaNacimiento.length == 10) {
        sink.add(fechaNacimiento);
      } else {
        sink.addError('Fecha no válida');
      }
    },
  );

  final validarNombreUsuario = StreamTransformer<String, String>.fromHandlers(
    handleData: (nombreUsuario, sink) {
      if (nombreUsuario.length > 3) {
        sink.add(nombreUsuario);
      } else {
        sink.addError('Nombre de usuario inválido');
      }
    },
  );
}
