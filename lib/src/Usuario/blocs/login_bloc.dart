import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/Usuario/blocs/validator.dart';

class LoginBloc with Validator {
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _passwordConfirmationController = BehaviorSubject<String>();

  //Escuchar y recuperar los datos
  Stream<String> get emailStream =>
      _emailController.stream.transform(validarEmail);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);
  Stream<String> get passwordConfirmationStream =>
      _passwordConfirmationController.stream.transform(validarPassword);

  Stream<bool> get formValidStream =>
      Rx.combineLatest2(emailStream, passwordStream, (e, p) => true);

  Stream<bool> get recoverPasswordFormValidStream =>
      Rx.combineLatest([emailStream], (e) {
        for (String email in e) {
          if (email.length < 2) return false;
        }
        return true;
      });

  //Inserta valores al stream
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changePasswordConfirmation =>
      _passwordConfirmationController.sink.add;

  //Obtiene los ultimo valores emitidos en los streams
  String get email => _emailController.value;
  String get password => _passwordController.value;
  String get passwordConfirmation => _passwordConfirmationController.value;

  dispose() {
    _emailController?.close();
    _passwordController?.close();
    _passwordConfirmationController?.close();
  }
}
