import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maxxshop/src/Conexiones/pages/conexiones_page.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';

class SinConexionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 30.0,
              ),
              Container(
                height: 250.0,
                child: SvgPicture.asset('assets/sin_conexion.svg'),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  '¡Oops! Al parecer la red se encuentra sin acceso a internet. De igual manera podrás conectarte en modo offline a los establecimientos. Toca en el botón de continuar para ver las opciones de conexión, o puedes intentar conectarte a una red.',
                  style: TextStyle(fontSize: 20.0),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              _botonRedondeado(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _botonRedondeado(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        children: [
          Table(
            children: [
              TableRow(
                children: [
                  InkWell(
                    onTap: () => Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => ConexionesPage()),
                      (_) => false,
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                    splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                    highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                    child: BotonRedondeado(
                      color: Colors.grey[700],
                      icono: Icons.next_plan_outlined,
                      texto: 'Continuar',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
