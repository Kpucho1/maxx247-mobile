// To parse this JSON data, do
//
//     final sucursalModel = sucursalModelFromJson(jsonString);

import 'dart:convert';

SucursalModel sucursalModelFromJson(String str) => SucursalModel.fromJson(json.decode(str));

String sucursalModelToJson(SucursalModel data) => json.encode(data.toJson());

class SucursalModel {
    SucursalModel({
        this.sucursalid,
        this.nombre,
        this.codigosucursal,
    });

    int sucursalid;
    String nombre;
    String codigosucursal;

    factory SucursalModel.fromJson(Map<String, dynamic> json) => SucursalModel(
        sucursalid: json["sucursalid"],
        nombre: json["nombre"],
        codigosucursal: json["codigosucursal"],
    );

    Map<String, dynamic> toJson() => {
        "sucursalid": sucursalid,
        "nombre": nombre,
        "codigosucursal": codigosucursal,
    };
}
