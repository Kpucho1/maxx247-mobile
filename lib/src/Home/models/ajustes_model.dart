import 'dart:convert';

AjustesModel ajustesModelFromJson(String str) =>
    AjustesModel.fromJson(json.decode(str));

String ajustesModelToJson(AjustesModel data) => json.encode(data.toJson());

class AjustesModel {
  AjustesModel({
    this.id,
    this.fontsize,
    this.agruparcomanda,
  });

  int id;
  int fontsize;
  int agruparcomanda;

  factory AjustesModel.fromJson(Map<String, dynamic> json) => AjustesModel(
        id: json["id"],
        fontsize: json["fontsize"],
        agruparcomanda: json["agruparcomanda"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fontsize": fontsize,
        "agruparcomanda": agruparcomanda,
      };
}
