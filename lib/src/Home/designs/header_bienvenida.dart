import 'package:flutter/material.dart';

class HeaderBienvenida extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Color.fromRGBO(196, 187, 175, 0.1),
      child: CustomPaint(
        painter: _HeaderBienvenidaPainter(),
      ),
    );
  }
}

class _HeaderBienvenidaPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = new Paint();

    paint.color = Color(0xffFFFFFA);
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 5;

    final path = new Path();

    path.lineTo(0, size.height * 0.30);
    path.lineTo(size.width * 0.5, size.height * 0.40);
    path.lineTo(size.width, size.height * 0.30);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

//Colores
//Spanish gray: 0xff8D918Bsize.height * 0.25
//Platinum: 0xffE9E3E6
//Light Gray: 0xffC7D6D5
//Ash gray: 0xffAFBFC0
//Charcoal: 0xff2C4251
//Gunmetal: 0xff0B3142 (*) RGB: 11, 49, 66
//Midnight Green Eagle Green: 0xff0F5257
//Ming: 0xff3C6E71
//Dark Purple: 0xff2B061E
