import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class LoginHomePostgresService {
  final prefs = PreferenciasUsuario();

  Future<bool> iniciarSesion(String correo, String password) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> usuario = await conexionNegocio.query(
          "SELECT U.usuarioid, A.empresaid AS sucursalid, E.esquemaid, U.nombre FROM datos.acceso A, datos.esquemasusuarios E, datos.usuarios U WHERE U.correoelectronico = @correo AND U.contrasena = @password AND U.usuarioid = A.usuarioid AND U.usuarioid = E.usuarioid AND E.sucursalid= A.empresaid;",
          substitutionValues: {"correo": correo.trim(), "password": password});

      await conexionNegocio.close();

      if (usuario.length > 0) {
        prefs.email = correo;
        prefs.usuarioid = usuario.first[0];
        prefs.sucursalid = usuario.first[1];
        prefs.esquemaid = usuario.first[2];
        prefs.nombreCajero = usuario.first[3] ?? '';

        return true;
      } else {
        return false;
      }
    } catch (e) {
      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
    }
  }

  Future<List<SucursalModel>> getListSucursales() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    List<SucursalModel> listadoSucursales = [];

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> _result = await conexionNegocio.query("SELECT codigoempresaid, nombre, codigosucursal FROM datos.empresa E, datos.acceso A WHERE A.usuarioid = @usuarioid AND E.codigoempresaid = A.empresaid", substitutionValues: {
        "usuarioid": prefs.usuarioid
      });

      for (var res in _result) {
        SucursalModel sucursalModel = SucursalModel();

        sucursalModel.sucursalid = res[0];
        sucursalModel.nombre = res[1];
        sucursalModel.codigosucursal = res[2];

        listadoSucursales.add(sucursalModel);

        DBProvider.db.agregarSucursal(sucursalModel);

      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();

    return listadoSucursales;
  }
}
