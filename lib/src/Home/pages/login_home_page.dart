import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Home/pages/login_loading_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';

class LoginHomePage extends StatefulWidget {
  LoginHomePage({Key key}) : super(key: key);

  @override
  _LoginHomePageState createState() => _LoginHomePageState();
}

class _LoginHomePageState extends State<LoginHomePage> {
  final prefs = PreferenciasUsuario();
  TextEditingController _nombreUsuarioController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _verPassword = true;
  bool _habilitarBotonIniciarSesion = false;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _nombreUsuarioController.text = prefs.email != null ? prefs.email : '';
  }

  @override
  void dispose() {
    _nombreUsuarioController?.dispose();
    _passwordController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          //Esconder el teclado al tocar sobre la pantalla
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          appBar: _appBarConVersionamiento(),
          body: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: _screenSize.height < 850
                      ? _screenSize.height * 0.05
                      : _screenSize.height * 0.12,
                ),
                child: _logoImage(),
              ),
              _inicioDeSesionForm(),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            child: Platform.isIOS
                ? Icon(
                    CupertinoIcons.settings,
                    size: 30.0,
                  )
                : Icon(
                    Icons.settings,
                    size: 30.0,
                  ),
            backgroundColor: Color.fromRGBO(197, 31, 93, 1.0), onPressed: () {  },
            // onPressed: () => _mostrarBottomSheet(context),
          ),
        ),
      ),
    );
  }

  Widget _appBarConVersionamiento() {
    return AppBar(
      centerTitle: true,
     
      automaticallyImplyLeading: true,

      
    );
  }

  Widget _logoImage() {
    return SafeArea(
      top: true,
      child: Image(
        image: AssetImage('assets/maxx247_logo_500x120.png'),
      ),
    );
  }

  Widget _inicioDeSesionForm() {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Form(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _inputNombreUsuario(),
            SizedBox(
              height: size.height * 0.02,
            ),
            _inputPassword(),
            SizedBox(
              height: size.height * 0.040,
            ),
            _botonIniciarSesion(),
            SizedBox(
              height: size.height * 0.030,
            ),
            _botonRecuperarContrasena(),
          ],
        ),
      ),
    );
  }

  Widget _inputNombreUsuario() {
    final Size _screenSize = MediaQuery.of(context).size;

    return Platform.isIOS
        ? CupertinoTextField(
            textInputAction: TextInputAction.next,
            autofocus: false,
            controller: _nombreUsuarioController,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(
                color: Theme.of(context).accentColor,
              ),
            ),
            cursorColor: Theme.of(context).accentColor,
            placeholder: 'Correo electrónico',
            prefix: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Icon(
                CupertinoIcons.mail,
              ),
            ),
            padding: EdgeInsets.all(15.0),
          )
        : TextFormField(
            textInputAction: TextInputAction.next,
            controller: _nombreUsuarioController,
            autofocus: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              labelText: 'Correo electrónico',
              labelStyle: TextStyle(
                fontSize: _screenSize.height < 850 ? 16.0 : 18.0,
              ),
              prefixIcon: Icon(Icons.email_outlined),
            ),
          );
  }

  Widget _botonIniciarSesion() {
    final Size _screenSize = MediaQuery.of(context).size;

    return Platform.isIOS
        ? CupertinoButton(
            disabledColor: Colors.grey,
            color: Color.fromRGBO(197, 31, 93, 1.0),
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.305,
                vertical: MediaQuery.of(context).size.height * 0.014),
            child: Text(
              'Iniciar sesión',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: _nombreUsuarioController.text == '' ||
                    _passwordController.text == '' ||
                    !_habilitarBotonIniciarSesion
                ? null
                : _iniciarSesion,
          )
        : ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Color.fromRGBO(197, 31, 93, 1.0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              padding: EdgeInsets.symmetric(horizontal: 100.0, vertical: 10.0),
            ),
            child: Text(
              'Iniciar sesión',
              style: TextStyle(
                color: Colors.white,
                fontSize: _screenSize.height < 850 ? 17.0 : 19.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: _nombreUsuarioController.text == '' ||
                    _passwordController.text == '' ||
                    !_habilitarBotonIniciarSesion
                ? null
                : _iniciarSesion,
          );
  }


  Widget _inputPassword() {
    final Size _screenSize = MediaQuery.of(context).size;

    return Platform.isIOS
        ? CupertinoTextField(
            controller: _passwordController,
            padding: EdgeInsets.all(15.0),
            obscureText: _verPassword,
            cursorColor: Theme.of(context).accentColor,
            placeholder: 'Contraseña',
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(
                color: Theme.of(context).accentColor,
              ),
            ),
            prefix: Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Icon(
                CupertinoIcons.lock,
              ),
            ),
            suffix: IconButton(
              icon: Icon(CupertinoIcons.eye),
              splashRadius: 0.5,
              splashColor: Colors.transparent,
              onPressed: () {
                if (_verPassword) {
                  setState(() {
                    _verPassword = false;
                  });
                } else {
                  setState(() {
                    _verPassword = true;
                  });
                }
              },
            ),
            onChanged: (value) {
              if (value.length > 5) {
                setState(() {
                  _habilitarBotonIniciarSesion = true;
                });
              } else {
                setState(() {
                  _habilitarBotonIniciarSesion = false;
                });
              }
            },
          )
        : TextFormField(
            textInputAction: TextInputAction.done,
            controller: _passwordController,
            autofocus: false,
            obscureText: _verPassword,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              labelText: 'Contraseña',
              labelStyle: TextStyle(
                fontSize: _screenSize.height < 850 ? 16.0 : 18.0,
              ),
              prefixIcon: Icon(Icons.lock),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye_outlined),
                onPressed: () {
                  if (_verPassword) {
                    setState(() {
                      _verPassword = false;
                    });
                  } else {
                    setState(() {
                      _verPassword = true;
                    });
                  }
                },
              ),
            ),
            onChanged: (value) {
              if (value.length > 5) {
                setState(() {
                  _habilitarBotonIniciarSesion = true;
                });
              } else {
                setState(() {
                  _habilitarBotonIniciarSesion = false;
                });
              }
            },
          );
  }

  void _iniciarSesion() async {
    final bool isValid = EmailValidator.validate(_nombreUsuarioController.text);

    if (isValid) {
      print('Email valido');

      await Navigator.of(context)
          .push(
        MaterialPageRoute(
          settings: RouteSettings(arguments: {
            "usuario": _nombreUsuarioController.text,
            "password": _passwordController.text,
          }),
          builder: (context) => LoginLoadingPage(),
        ),
      )
          .then(
        //Muestra error en pantalla si no existe conexión, o si usuario o contraseña son incorrectos
        (value) {
          if (value != null) {
            final snackBar = SnackBar(
              content: Text(value, style: TextStyle(fontSize: 16.0)),
              backgroundColor: Colors.red,
            );

            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
      );
    } else {
      //Informa que el correo no es válido por medio de snackbar
      final snackBar = SnackBar(
        content: Text("Email inválido",
            style: TextStyle(fontSize: 16.0, color: Colors.white)),
        backgroundColor: Colors.red,
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);

      setState(() {
        _passwordController.text = '';
      });
    }
  }

  Widget _botonRecuperarContrasena() {
    return Platform.isIOS
        ? CupertinoButton(
            child: Text('¿Olvidó su contraseña?'),
            onPressed: () async {
              await Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => null,
                ),
                (route) => false,
              );
            },
          )
        : TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              backgroundColor: Colors.blueGrey[50],
            ),
            child: Text(
              '¿Olvidó su contraseña?',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            onPressed: () async {
              await Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => null,
                ),
                (route) => false,
              );
            },
          );
  }
}