import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maxxshop/src/Conexiones/pages/conexiones_page.dart';
import 'package:maxxshop/src/Empresa/pages/menu_empresa_page.dart';
import 'package:maxxshop/src/Facturacion/menu_facturacion_page.dart';
import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/loading_productos_page.dart';
import 'package:maxxshop/src/Utils/modulo_creacion_page.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';

class MenusPage extends StatefulWidget {
  MenusPage();

  @override
  _MenusPageState createState() => _MenusPageState();
}

class _MenusPageState extends State<MenusPage> {
  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Colors.indigo[50],
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          actions: [
            TextButton(
              child: Text("Salir",
                  style: TextStyle(color: Colors.red, fontSize: 20)),
              onPressed: () async {
                await DBProvider.db.deleteConexionActiva();
                prefs.sesionActiva = false;
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ConexionesPage()));
              },
            ),
          ],
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          automaticallyImplyLeading: false,
        ),
        body: Stack(
          alignment: Alignment.center,
          children: [
            ClipPath(
                clipper: MyClipper(),
                child: Container(
                  color: Colors.white,
                )),
            Positioned(
              top: size.height * .07,
              child: Text(
                "V. 15/03/2022 15:00",
                style: TextStyle(
                    color: Color(0xff008DBA),
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Positioned(
              top: size.height * .1,
              child: Text(
                prefs.nombreEmpresa,
                style: TextStyle(
                    color: Color(0xff008DBA),
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Positioned(
              top: size.height * .14,
              child: FutureBuilder(
                future: DBProvider.db.obtenerSucursales(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Text("Cargando sucursales...");
                  }
                  else{
                    List<SucursalModel> sucursales = snapshot.data;
                    return DropdownButton(
                      value: prefs.sucursalid,
                      onChanged: (value){
                        prefs.sucursalid = value;
                        setState(() {
                          
                        });
                      },
                      items: sucursales.map<DropdownMenuItem>((sucursal) {
                        return DropdownMenuItem(
                          value: sucursal.sucursalid,
                          child: Text(
                            sucursal.nombre,
                            style: TextStyle(
                                color: Color(0xffB2BFC9),
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        );
                      }).toList()
                    );
                  }
                }
              )
            ),
            Positioned(
              top: size.height * .21,
              child: Text(
                DateTime.now().hour < 18 ? 'Buen día, '+prefs.nombreCajero : 'Buenas noches, '+prefs.nombreCajero,
                style: TextStyle(
                    color: Color(0xffB2BFC9),
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: size.height * 0.2),
              child: SingleChildScrollView(
                child:  _TablaBotonesOpcionesMenu(),
              ),
            ),
            Positioned(
              bottom: size.height * -.01,
              width: 150,
              height: 150,
              child: SvgPicture.asset(
                'assets/menu.svg',
                alignment: Alignment.bottomCenter,
              ),
            ),
          ],
        ));
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, 220);
    path.quadraticBezierTo(size.width / 4, 160 /*180*/, size.width / 2, 175);
    path.quadraticBezierTo(3 / 4 * size.width, 190, size.width, 130);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

///Va a mostrar los botones para todas las opciones disponibles
class _TablaBotonesOpcionesMenu extends StatelessWidget {
  //
  @override
  Widget build(BuildContext context) {
    //
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: [
            TableRow(
              children: [
                GestureDetector(
                  onTap: () async {
                    //
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MenuFacturacionPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.store,
                      texto: 'Facturacion',
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {                
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProductosLoadingPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.wine_bar,
                      texto: 'Inventario',
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                GestureDetector(
                  onTap: () async {
                    //
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MenuEmpresaPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.home,
                      texto: 'Empresa',
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    //
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ModuloCreacionPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.request_quote,
                      texto: 'Nomina',
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                GestureDetector(
                  onTap: () async {
                    //
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ModuloCreacionPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.account_balance,
                      texto: 'Contabilidad',
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    //
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ModuloCreacionPage(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    height: 110.0,
                    child: BotonRedondeado(
                      color: Theme.of(context).accentColor,
                      icono: Icons.point_of_sale,
                      texto: 'Tesorería',
                    ),
                  ),
                ),
                
              ],
            ),
          ],
        ),
      ),
    );
  }
}
