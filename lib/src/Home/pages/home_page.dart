import 'package:flutter/material.dart';
import 'package:maxxshop/src/Conexiones/pages/conexiones_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ConexionesPage(),
    );
  }

  // Widget _callPage(int paginaActual) {
  //   switch (paginaActual) {
  //     case 0:
  //       return ConexionesPage();

  //     case 1:
  //       return AjustesPage();

  //     default:
  //       return ConexionesPage();
  //   }
  // }

  // Widget _crearBottomNavBar() {
  //   return Theme(
  //     data: Theme.of(context).copyWith(
  //       // canvasColor: Color(0xff2B061E),
  //       canvasColor: Color(0xffFFFFFA),
  //       primaryColor: Color(0xff005470),
  //       textTheme: Theme.of(context).textTheme.copyWith(
  //             caption: TextStyle(
  //               color: Color(0xff008DBA),
  //             ),
  //           ),
  //     ),
  //     child: BottomNavigationBar(
  //       currentIndex: currentIndex,
  //       onTap: (value) {
  //         setState(() {
  //           currentIndex = value;
  //         });
  //       },
  //       items: [
  //         /*BottomNavigationBarItem(
  //           icon: Icon(
  //             Icons.home,
  //             size: 30.0,
  //           ),
  //           label: 'Inicio',
  //         ),*/
  //         BottomNavigationBarItem(
  //           icon: Icon(
  //             Icons.connect_without_contact,
  //             size: 30.0,
  //           ),
  //           label: 'Conexiones',
  //         ),
  //         BottomNavigationBarItem(
  //           icon: Icon(
  //             Icons.settings,
  //             size: 30.0,
  //           ),
  //           label: 'Ajustes',
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
