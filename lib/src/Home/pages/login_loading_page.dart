import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/Home/pages/list_sucursales_page.dart';
import 'package:maxxshop/src/Home/pages/menus_page.dart';
import 'package:maxxshop/src/Home/services/postgres_service.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:connectivity/connectivity.dart';


class LoginLoadingPage extends StatefulWidget {
  @override
  _LoginLoadingPageState createState() => _LoginLoadingPageState();
}

class _LoginLoadingPageState extends State<LoginLoadingPage> {
  //Definicion de variable para revisar conexion a internet
  final Connectivity _connectivity = Connectivity();
  Map<String, dynamic> _credenciales;
  final prefs = PreferenciasUsuario();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      await _initConexionInternet();
    });
  }

  @override
  Widget build(BuildContext context) {
    //Lee la info de login de la pagina anterior
    _credenciales = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor:
          Platform.isIOS ? Colors.white : Color.fromRGBO(197, 31, 93, 1.0),
      body: Center(
        child: Platform.isIOS
            ? CupertinoActivityIndicator(
                animating: true,
                radius: 25.0,
              )
            : CircularProgressIndicator(
                backgroundColor: Colors.white,
              ),
      ),
    );
  }

  Future<void> _initConexionInternet() async {
    ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return Future.value(null);
    }

    return _actualizarEstadoDeLaConexion(result);
  }

  //Funcion que ejecuta código según sea el estado de la conexión a internet
  Future<void> _actualizarEstadoDeLaConexion(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _iniciaSesionBaseDatos(context);
        break;
      case ConnectivityResult.none:
        Navigator.pop(context, 'Conexión no disponible. Intente de nuevo.');
        break;
    }
  }

  _iniciaSesionBaseDatos(BuildContext context) async {

    LoginHomePostgresService loginHomePostgresService = LoginHomePostgresService();



    try {
      if(await loginHomePostgresService.iniciarSesion(_credenciales['usuario'], _credenciales['password'])){
        List<SucursalModel> listadoSucursales = await loginHomePostgresService.getListSucursales();

        prefs.sesionActiva = true;
      
        if (listadoSucursales.length > 1) {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => ListSucursalesPage(),));
        }
        else{
          await Navigator.push(context, MaterialPageRoute(builder: (context) => MenusPage(),));
        }
      }{
        Navigator.pop(context, 'Correo o contraseña inválidos.');    
      }
    } on Exception catch (e) {
      Navigator.pop(context, 'Ha ocurrido un error de conexion');
    }



        
      
    
  }

}
