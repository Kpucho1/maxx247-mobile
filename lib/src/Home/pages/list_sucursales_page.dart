import 'package:flutter/material.dart';
import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/Home/pages/menus_page.dart';
import 'package:maxxshop/src/Home/services/postgres_service.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';

class ListSucursalesPage extends StatefulWidget {
  ListSucursalesPage({Key key}) : super(key: key);

  @override
  _ListSucursalesPageState createState() => _ListSucursalesPageState();
}

class _ListSucursalesPageState extends State<ListSucursalesPage> {
  final PreferenciasUsuario prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sucursales"),
      ),
      body: _listView(context),     
    );
  }

  
  Widget _listView(BuildContext context){
    return FutureBuilder(
      future: DBProvider.db.obtenerSucursales(),
      // ignore: missing_return
      builder: (context, snapshot){
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List<SucursalModel> _listadoSucursales = snapshot.data;

          if (_listadoSucursales.length == 1) {
            prefs.sucursalid = _listadoSucursales[0].sucursalid;
            prefs.nombreSucursal = _listadoSucursales[0].codigosucursal;
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => MenusPage()));
          }
          else{
            return ListView.builder(
              itemCount: _listadoSucursales.length,
              itemBuilder: (context, index){
                return _listTileSucursal(_listadoSucursales[index], _listadoSucursales);
              },
            );
          }

      },
    );
  }

  Widget _listTileSucursal(SucursalModel sucursal, List<SucursalModel> sucursales){
    return Card(
      child: ListTile(
        title: Text(sucursal.nombre),
        subtitle: Text(sucursal.codigosucursal),
    
        onTap: () async{
          prefs.sucursalid = sucursal.sucursalid;
          prefs.nombreSucursal = sucursal.codigosucursal;
          
          await Navigator.of(context).push(MaterialPageRoute(builder: (context) => MenusPage()));
        },
    
      ),
    );
  }
}