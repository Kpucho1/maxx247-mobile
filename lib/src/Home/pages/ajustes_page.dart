import 'package:flutter/material.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';

class AjustesPage extends StatefulWidget {
  @override
  _AjustesPageState createState() => _AjustesPageState();
}

class _AjustesPageState extends State<AjustesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        centerTitle: true,
      ),
      body: _listadoOpciones(),
    );
  }

  Widget _listadoOpciones() {
    return FutureBuilder<Object>(
        future: DBProvider.db.obtenerAjustes(),
        builder: (context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            int _agruparComandas = snapshot.data.agruparcomanda;
            int _fontSize = snapshot.data.fontsize;

            return ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(),
              itemCount: 1,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    ListTile(
                      title: Text(
                        "Agrupar productos en la comanda",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      trailing: Switch(
                        value: _agruparComandas == 1 ? true : false,
                        onChanged: (value) async {
                          DBProvider.db.actualizarAjustes(
                              'agruparcomanda', value ? 1 : 0);
                          setState(() {
                            _agruparComandas = value ? 1 : 0;
                          });
                        },
                      ),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text("Tamaño de la fuente al imprimir comanda",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      trailing: DropdownButton(
                        value: _fontSize,
                        onChanged: (int newValue) {
                          DBProvider.db.actualizarAjustes('fontsize', newValue);
                          setState(() {
                            _fontSize = newValue;
                          });
                        },
                        items:
                            <int>[1, 2].map<DropdownMenuItem<int>>((int value) {
                          return DropdownMenuItem<int>(
                            value: value,
                            child: Text(value.toString()),
                          );
                        }).toList(),
                      ),
                      subtitle: Text(
                          "Teniendo en cuenta que 1 es el tamaño por defecto y que 2 es un poco más pequeño"),
                      onTap: () {},
                    )
                  ],
                );
              },
            );
          }
        });
  }
}
