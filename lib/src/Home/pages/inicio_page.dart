import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maxxshop/src/Home/designs/header_bienvenida.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';
import 'package:maxxshop/src/Home/pages/home_page.dart';

class InicioPage extends StatefulWidget {
  @override
  _InicioPageState createState() => _InicioPageState();
}

class _InicioPageState extends State<InicioPage> {
  final mesasRefreshBloc = new MesasRefreshBloc();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: [
          HeaderBienvenida(),
          _maxxControlLogo(context),
          Container(
            padding: EdgeInsets.only(top: size.height * 0.21),
            child: Container(
              height: 190.0,
              width: double.infinity,
              child: _imagenFondoSVG(),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: size.height * 0.50),
            child: _botonesRedondeados(),
          ),
        ],
      ),
    );
  }

  Widget _maxxControlLogo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      height: size.height * 0.3,
      // color: Colors.blue,
      width: double.infinity,
      child: Image(
        image: AssetImage('assets/maxx247_logo_500x120.png'),
      ),
    );
  }

  Widget _imagenFondoSVG() {
    final size = MediaQuery.of(context).size;

    return SvgPicture.asset(
      'assets/mesero.svg',
      alignment: Alignment.center,
      height: size.height * 0.2,
      width: size.width * 0.1,
    );
  }

  Widget _botonesRedondeados() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        children: [
          Table(
            children: [
              TableRow(
                children: [
                  InkWell(
                    onTap: () => Navigator.pushNamed(context, 'nuevaConexion'),
                    borderRadius: BorderRadius.circular(20.0),
                    splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                    highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                    child: BotonRedondeado(
                      color: Colors.grey[700],
                      icono: Icons.settings_remote,
                      texto: 'Conéctate a un establecimiento',
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 7.0),
          Table(
            children: [
              TableRow(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomePage(),
                          ),
                          (route) => false);
                    },
                    borderRadius: BorderRadius.circular(20.0),
                    splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                    highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                    child: BotonRedondeado(
                      color: Colors.grey[700],
                      icono: Icons.skip_next_outlined,
                      texto: 'Saltarse este paso',
                    ),
                  ),
                  /*InkWell(
                    onTap: () => Navigator.pushNamed(context, 'nuevaConexion'),
                    borderRadius: BorderRadius.circular(20.0),
                    splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                    highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                    child: BotonRedondeado(
                      color: Colors.grey[700],
                      icono: Icons.settings_input_antenna,
                      texto: 'Conexión activa',
                    ),
                  ),*/
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
