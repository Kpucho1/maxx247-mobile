import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ContadorProductos extends StatefulWidget {
  int cantidadProductos = 1;
  int limite = 0;

  ContadorProductos({this.limite = 0});

  @override
  _ContadorProductosState createState() => _ContadorProductosState();
}

class _ContadorProductosState extends State<ContadorProductos> {
  _incrementaProductos() {
    setState(() {
      if(widget.limite > 0){
        if (widget.cantidadProductos < widget.limite) {
          widget.cantidadProductos++;
        }
      }
      else{
        widget.cantidadProductos++;
      }
    });
  }

  _decrementaProductos() {
    setState(() {
      if (widget.cantidadProductos > 1) widget.cantidadProductos--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
          onPressed: _decrementaProductos,
          onLongPress: _decrementaProductos,
          child: Icon(
            Icons.remove,
            size: 22.0,
          ),
        ),
        Text('${widget.cantidadProductos}'),
        TextButton(
          onPressed: _incrementaProductos,
          onLongPress: _incrementaProductos,
          child: Icon(
            Icons.add,
            size: 22.0,
          ),
        ),
      ],
    );
  }
}
