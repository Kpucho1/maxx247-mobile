import 'dart:math';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/Utils/timer.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/comanda_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesa_individual_page.dart';
import 'package:maxxshop/src/Conexiones/blocs/conexiones_bloc.dart';
import 'package:maxxshop/src/widgets/contador_productos_widget.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesa_cuentas_activas_page.dart';

// ignore: must_be_immutable
class ListaGrid extends StatefulWidget {
  final List<MesaModel> listaMesas;
  final int esquemaId;
  final EsquemaModel esquemaEnUso;

  Function actualizarNumMesasVendedor;

  ListaGrid(
      {@required this.listaMesas,
      @required this.esquemaId,
      @required this.esquemaEnUso,
      this.actualizarNumMesasVendedor});

  @override
  _ListaGridState createState() => _ListaGridState();
}

class _ListaGridState extends State<ListaGrid> {
  final esquemasBloc = new EsquemasBloc();
  final conexionesBloc = new ConexionesBloc();
  final mesasRefreshBloc = new MesasRefreshBloc();
  final timer = TimerConsultaBD.timerConsultaBD;

  final prefs = PreferenciasUsuario();

  final _textController = new TextEditingController();

  var _rng = new Random();

  int numeroValidacion;

  List<List<dynamic>> detalle;
  bool _loading;
  bool _loadingCrearCuenta = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      _loading = false;
    });
    timer.revisarListaPrecio();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderInherited.categoriasBloc(context);
    final blocComanda = ProviderInherited.comandaBloc(context);
    final cabeceraProvider = Provider.of<CabeceraService>(context);
    final blocProductos = ProviderInherited.productosBloc(context);
    final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);

    // timer = iniciaTimer(Duration(seconds: 5), context);

    return Column(
      children: [
        Text(timer.nombreListaPrecio),
        Expanded(
          child: StaggeredGridView.countBuilder(
            crossAxisCount: 4,
            physics: BouncingScrollPhysics(),
            itemCount: widget.listaMesas.length,
            padding: EdgeInsets.symmetric(
                horizontal: size.width * 0.02, vertical: size.height * 0.01),
            itemBuilder: (BuildContext context, int index) {
              List<String> nombreVendedor =
                  widget.listaMesas[index].nombreVendedor.split(RegExp("\\s+"));

              return GestureDetector(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                    //Color del estado de la mesa
                    color: _coloresPorEstado(widget.listaMesas[index].estado),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        '${widget.listaMesas[index].nombre}',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        formatCurrency
                            .format(widget.listaMesas[index].consumido),
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        nombreVendedor[0].toString().toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                      widget.listaMesas[index].numeroCuentas > 1
                          ? Text(
                              widget.listaMesas[index].numeroCuentas.toString(),
                              style: TextStyle(color: Colors.white),
                            )
                          : Text(""),
                    ],
                  ),
                ),
                onTap: () => _seleccionOpcionesPorMesaSegunEstado(
                    widget.listaMesas[index].estado,
                    index,
                    bloc,
                    blocComanda,
                    cabeceraProvider,
                    blocProductos),
              );
            },
            staggeredTileBuilder: (int index) =>
                StaggeredTile.count(1, index.isEven ? 1 : 1),
            mainAxisSpacing: 5.0,
            crossAxisSpacing: 5.0,
          ),
        ),
      ],
    );
  }

  _seleccionOpcionesPorMesaSegunEstado(
      int estado,
      int index,
      CategoriasBloc bloc,
      ComandaBloc blocComanda,
      CabeceraService cabeceraProvider,
      ProductosBloc blocProductos) {
    switch (estado) {
      case 1:
        _opcionesPorMesa(index);
        break;

      case 3:
      case 4:
      case 5:
        _opcionesPorMesaCuentaAbierta(
            index, bloc, cabeceraProvider, blocProductos);
        break;

      case 2:
      case 6:
        _opcionesPorMesaFueraServicio(index);
        break;

      default:
    }
  }

  Future<void> _opcionesPorMesa(int index) async {
    PostgresFunctions postgresFunctions = PostgresFunctions();

    numeroValidacion = _rng.nextInt(100000000);

    await postgresFunctions.actualizarNumeroValidacionMesa(
        numeroValidacion, widget.listaMesas[index].objetoid);

    final contadorPersonas = ContadorProductos();

    contadorPersonas.cantidadProductos = 2;

    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Column(
            children: [
              Text('${widget.listaMesas[index].nombre}'),
              contadorPersonas
            ],
          ),
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: StatefulBuilder(builder: (context, setState) {
                return TextButton(
                  child: !_loadingCrearCuenta
                      ? Text(
                          'Crear cuenta',
                        )
                      : LinearProgressIndicator(),
                  style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue[200],
                      textStyle: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 20.0)),
                  onPressed: () async {
                    timer.detenerTimer;

                    setState(() {
                      _loadingCrearCuenta = true;
                    });

                    if (_loadingCrearCuenta) {
                      bool creaCuenta =
                          await postgresFunctions.validarNumeroValidacionMesa(
                              numeroValidacion,
                              widget.listaMesas[index].objetoid);

                      if (creaCuenta) {
                        bool cuentaCreada =
                            await postgresFunctions.crearCuentaEnBD(
                                widget.listaMesas[index].objetoid,
                                "",
                                context,
                                widget.listaMesas[index].nombre,
                                contadorPersonas.cantidadProductos);

                        if (!cuentaCreada) {
                          mesasRefreshBloc.actualizarEstadoMesa(
                              3,
                              widget.listaMesas[index].objetoid,
                              widget.esquemaId);
                          await _cambiarEstadoMesa(index, 3);

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return MesaIndividualPage(
                                    //mesa: widget.listaMesas[index],
                                    );
                              },
                            ),
                          ).then((_) {
                            timer.actualizarListadoMesas();
                            timer.iniciarTimer;
                            Navigator.popUntil(
                                context, (route) => route.isFirst);
                          });
                        } else {
                          await _alertaMesaOcupada(context);
                          timer.iniciarTimer;
                        }
                      } else {
                        await _alertaMesaOcupada(context);
                        timer.iniciarTimer;
                      }
                    }

                    setState(() {
                      _loadingCrearCuenta = false;
                    });
                  },
                );
              }),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Pasar a mantenimiento',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.blue[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  mesasRefreshBloc.actualizarEstadoMesa(
                      6, widget.listaMesas[index].objetoid, widget.esquemaId);
                  await _cambiarEstadoMesa(index, 6);
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Salir',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.red[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        );
      },
    );
  }

  Future _alertaMesaOcupada(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Icon(
              Icons.warning,
              color: Colors.red,
              size: 50.0,
            ),
            content: Text(
              'La mesa ya ha sido ocupada por otra persona.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Volver',
                  style: TextStyle(fontSize: 18.0),
                ),
              )
            ],
          );
        });
  }

  Future<void> _opcionesPorMesaCuentaAbierta(int index, CategoriasBloc bloc,
      CabeceraService cabeceraProvider, ProductosBloc blocProductos) async {
    return showDialog(
      context: context,
      builder: (context) {
        return _dialogoParaAbrirCuenta(index, cabeceraProvider, bloc);
      },
    );
  }

  Widget _dialogoParaAbrirCuenta(
      int index, CabeceraService cabeceraProvider, CategoriasBloc bloc) {
    return SimpleDialog(
      title: Text('${widget.listaMesas[index].nombre}'),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: TextButton(
            child: Text(
              'Abrir cuenta',
            ),
            style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.blue[200],
                textStyle: TextStyle(
                  fontSize: 22.0,
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 20.0)),
            onPressed: !_loading
                ? () async {
                    //Navigator.pop(context);
                    timer?.detenerTimer;

                    setState(() {
                      _loading = true;
                    });

                    await _consultarCabecera(index, cabeceraProvider, bloc);

                    if (widget.listaMesas[index].numeroCuentas <= 1) {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return MesaIndividualPage();
                          },
                        ),
                      ).then((_) {
                        timer.iniciarTimer;
                        Navigator.popUntil(context, (route) => route.isFirst);
                      });
                    } else {
                      timer.detenerTimer;
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return MesaCuentasActivasPage();
                          },
                        ),
                      ).then((_) {
                        timer.actualizarListadoMesas();
                        timer.iniciarTimer;
                        Navigator.popUntil(context, (route) => route.isFirst);
                      });
                    }
                  }
                : null,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: TextButton(
            child: Text(
              'Adicionar cuenta',
            ),
            style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.blue[200],
                textStyle: TextStyle(
                  fontSize: 22.0,
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 20.0)),
            onPressed: () {
              dialogAdicionarCuenta(context, index);
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: TextButton(
            child: Text(
              'Salir',
            ),
            style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.red[200],
                textStyle: TextStyle(
                  fontSize: 22.0,
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(vertical: 20.0)),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
      ],
    );
  }

  Future<Widget> dialogAdicionarCuenta(BuildContext context, int index) async {
    PostgresFunctions postFunctions = PostgresFunctions();

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Adición de cuenta  la mesa'),
          content: _formNuevaCuenta(context),
          actions: [
            TextButton(
              child: Text(
                'Agregar',
                style: TextStyle(fontSize: 20.0),
              ),
              onPressed: () async {
                timer.detenerTimer;
                await postFunctions.crearCuentaEnBD(
                    widget.listaMesas[index].objetoid,
                    _textController.text,
                    context,
                    widget.listaMesas[index].nombre,
                    1);
                _textController.text = "";
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return MesaIndividualPage(
                          //mesa: widget.listaMesas[index],
                          );
                    },
                  ),
                ).then((_) {
                  timer.iniciarTimer;
                  Navigator.popUntil(context, (route) => route.isFirst);
                });
              },
            ),
            TextButton(
              child: Text(
                "Cancelar",
                style: TextStyle(color: Colors.red, fontSize: 20.0),
              ),
              onPressed: () {
                _textController.text = "";
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Widget _formNuevaCuenta(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(vertical: size.width * 0.1),
      child: TextField(
        controller: _textController,
        autofocus: true,
        textAlign: TextAlign.center,
        autocorrect: false,
        enableSuggestions: false,
        decoration: InputDecoration(
          helperText: 'Alias de cuenta',
          helperStyle: TextStyle(fontSize: 16.0),
          icon: Icon(
            Icons.text_fields,
            size: 35.0,
            color: Color(0xff2B061E),
          ),
        ),
      ),
    );
  }

  Future<void> _opcionesPorMesaFueraServicio(int index) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('${widget.listaMesas[index].nombre}'),
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Activar mesa',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.blue[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  mesasRefreshBloc.actualizarEstadoMesa(
                      1, widget.listaMesas[index].objetoid, widget.esquemaId);
                  await _cambiarEstadoMesa(index, 1);
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Salir',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.red[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _cambiarEstadoMesa(int index, int estado) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      await conexionNegocio.query(
        'UPDATE datos.objetos SET estado = @estado, fechahoraultimoevento = @fecha WHERE objetoid = @objetoid',
        substitutionValues: {
          "objetoid": widget.listaMesas[index].objetoid,
          "estado": estado,
          "fecha": DateTime.now().toLocal()
        },
      );

      await conexionNegocio.close();
    } catch (e) {
      print(e);
    }
  }

  //Convertir el formato del color
  Color _convertHexToColor(String hexColor) {
    // print(hexColor);
    hexColor = hexColor.replaceAll("&c", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 9) {
      return Color(int.parse('0x$hexColor'));
    }

    return Color(int.parse('0x$hexColor'));
  }

  //Asigna color a cada estado según el esquema
  Color _coloresPorEstado(int estado) {
    switch (estado) {
      case 1: //Vacía limpia
        // return widget.esquemaEnUso.colordisponiblefondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorvacialimpiafondo)
        return Colors.green;
        break;

      case 2: // Vacía sucia
        // return widget.esquemaEnUso.colorocupadasinpedidofondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorocupadasinpedidofondo)
        return Colors.yellow[600];
        break;

      case 3: // Ocupada sin pedido
        // return widget.esquemaEnUso.colorocupadaconpedidofondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorocupadaconpedidofondo)
        return Colors.purple;
        break;

      case 4: // Ocupada con pedido
        // return widget.esquemaEnUso.colorpendienteporpagarfondo != null
        //     ? _convertHexToColor(
        //         widget.esquemaEnUso.colorpendienteporpagarfondo)
        return Colors.red[800];
        break;

      case 5: // Pendiente forma de pago
        // return widget.esquemaEnUso.colorocupadasinpedidofondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorocupadasinpedidofondo)
        return Colors.blue;
        break;

      case 6: // Fuera de servicio
        // return widget.esquemaEnUso.colorsuciafondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorsuciafondo)
        return Colors.grey;
        break;

      case 7: // Reservada
        // return widget.esquemaEnUso.colorreparacionfondo != null
        //     ? _convertHexToColor(widget.esquemaEnUso.colorreparacionfondo)
        return Colors.orange;
        break;

      default:
        return _convertHexToColor(widget.esquemaEnUso.colordisponiblefondo);
    }
  }

  Future<void> _consultarCabecera(
    int index,
    CabeceraService cabeceraProvider,
    CategoriasBloc bloc,
  ) async {
    //
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> cabecera = await conexionNegocio.query(
          "SELECT A.prefijocuenta, CAST(A.numerocuenta AS INTEGER), CAST(A.totalapagar AS INTEGER), CAST(A.ts AS TEXT), A.id, A.nombrecuenta, A.nombrecliente, A.direccioncliente, A.telefonocliente, A.clienteid, A.fechahoravencimiento, C.nit, A.ciudadcliente FROM datos.cuentasactivas A LEFT JOIN datos.clientes C ON A.clienteid = C.clienteid WHERE mesahabitacionid = @objetoid AND estadocuenta <> 2 AND A.sucursalid = @sucursalid",
          substitutionValues: {
            "objetoid": widget.listaMesas[index].objetoid,
            "sucursalid": prefs.sucursalid
          });
      final cabeceraModel = MesaCabecera();

      cabeceraModel.prefijocuenta =
          cabecera[0][0] != null && cabecera.length == 1 ? cabecera[0][0] : '';
      cabeceraModel.numerocuenta =
          cabecera[0][1] != null && cabecera.length == 1 ? cabecera[0][1] : 0;
      cabeceraModel.totalapagar = cabecera[0][2] != null ? cabecera[0][2] : 0;
      cabeceraModel.ts = cabecera[0][3] != null ? cabecera[0][3] : '';
      cabeceraModel.idcuenta = cabecera[0][4] != null ? cabecera[0][4] : 0;
      cabeceraModel.objetoid = widget.listaMesas[index].objetoid;
      cabeceraModel.nombreCuenta = cabecera[0][5] != null ? cabecera[0][5] : '';

      cabeceraModel.nombreCliente =
          cabecera[0][6] != null ? cabecera[0][6].toString() : '';
      cabeceraModel.direccionCliente =
          cabecera[0][7] != null ? cabecera[0][7].toString() : '';
      cabeceraModel.telefonoCliente =
          cabecera[0][8] != null ? cabecera[0][8].toString() : '';
      cabeceraModel.clienteid = cabecera[0][9];
      cabeceraModel.fechahoravencimiento =
          cabecera[0][10] != null ? cabecera[0][10].toString() : "";
      cabeceraModel.nitCliente =
          cabecera[0][11] != null ? cabecera[0][11].toString() : "";
      cabeceraModel.ciudadCliente =
          cabecera[0][12] != null ? cabecera[0][12].toString() : "";

      cabeceraModel.nombreMesa = widget.listaMesas[index].nombre;

      //Acá es donde se carga la información de la cabecera al provider
      cabeceraProvider.mesaCabecera = cabeceraModel;

      await conexionNegocio.close();

      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
      await bloc.obtenerCategorias();
    } catch (e) {
      await _dialogoError();

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }
  }

  Future _dialogoError() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          'Error en la creación de la cuenta.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () =>
                Navigator.popUntil(context, (route) => route.isFirst),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }

  //Consulta la cuenta y agrega los productos al stream

}
