import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';

// ignore: must_be_immutable
class BotonSubirFoto extends StatefulWidget {

  PickedFile image;
  File imagenSeleccionada;

  BotonSubirFoto({Key key}) : super(key: key);

  @override
  _BotonSubirFotoState createState() => _BotonSubirFotoState();
}

class _BotonSubirFotoState extends State<BotonSubirFoto> {

  

  ImagePicker imagenPicker = ImagePicker();
  
  @override
  Widget build(BuildContext context) {
     return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.camera,
                  texto: 'Foto',
                ),
                onTap: () async {
                  _showPicker(context);
                },
              ),
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: widget.imagenSeleccionada == null
                    ? Text('No se ha seleccionado una imagen')
                    : Image.file(widget.imagenSeleccionada, cacheHeight: 100, cacheWidth: 100, filterQuality: FilterQuality.low,),
                onTap: () async {
                  _showPicker(context);
                },
              )
            ],
          )
        ],
      ),
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Agregar de la galería'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camara'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _imgFromCamera() async {
    PickedFile image = await imagenPicker.getImage(
      source: ImageSource.camera, imageQuality: 40, maxHeight: 500, maxWidth: 500
    );

    final bytes = (await image.readAsBytes()).lengthInBytes;
    final kb = bytes / 1024;

    //TODO: Condicional de imagen muy pesada

    if (image != null) {
      setState(() {
        widget.image = image;
        widget.imagenSeleccionada = File(widget.image.path);
      });
    }
  }

  _imgFromGallery() async {
    PickedFile image = await  imagenPicker.getImage(
        source: ImageSource.gallery, imageQuality: 40, maxHeight: 500, maxWidth: 500
    );

    if (image != null) {
      setState(() {
        widget.image = image;
        widget.imagenSeleccionada = File(widget.image.path);
      });
    }
  }
}