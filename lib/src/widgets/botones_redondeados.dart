import 'package:flutter/material.dart';

class BotonRedondeado extends StatelessWidget {
  final Color color;
  final IconData icono;
  final String texto;

  BotonRedondeado({
    @required this.color,
    @required this.icono,
    @required this.texto,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(178, 203, 210, 0.4),
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          // BoxShadow(
          //   blurRadius: 5,
          //   spreadRadius: 1,
          // ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: color,
            radius: 35.0,
            child: Icon(
              icono,
              color: Colors.white,
              size: 35.0,
            ),
          ),
          Text(
            texto,
            style: TextStyle(
              color: color,
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          )
        ],
      ),
    );
  }
}
