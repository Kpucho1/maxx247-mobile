import 'package:flutter/material.dart';
import 'package:convert/convert.dart';

class ImageListTile extends StatelessWidget {
  final imageString;
  final primerLetra;

  const ImageListTile({Key key, this.imageString, this.primerLetra}) : super(key: key);

  Widget build(BuildContext context) {
    if (imageString != '') {
      return Container(
        height: 70,
        width: 70,
        child: CircleAvatar(
          radius: 16.0,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.memory(
              hex.decode(imageString),
              gaplessPlayback: true,
              fit: BoxFit.fill,
              filterQuality: FilterQuality.low,
              cacheHeight: 80,
              cacheWidth: 80,
            ),
          )
        ),
      ); 
    }
    else{
      return Container(
        height: 70,
        width: 70,
        child: CircleAvatar(
          backgroundColor: Colors.red[400],
          child: Text(
            primerLetra,
            style: TextStyle(color: Colors.white),
          ),
          radius: 22.0,
        ),
      );
    }
  }
}