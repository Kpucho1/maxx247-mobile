import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/tipo_egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_printer_service.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/db/conexion_db_negocio.dart';

class EgresoPostgresService {
  EgresoPostgresService._privateConstructor();

  static final EgresoPostgresService _instancia =
      EgresoPostgresService._privateConstructor();

  factory EgresoPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<EgresoModel>> obtenerComprobantesEgresos() async{
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    List<EgresoModel> _listadoEgresos = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT id, CAST(fecha AS DATE), prefijoegreso, CAST(numeroegreso AS INTEGER), usuarioid, proveedorid, nitproveedor, tipoegresoid, CAST(valor AS INTEGER), observacion, fechahoraanulacion, motivoanulacion, estado, prefijocierre, CAST(numerocierre AS INTEGER), afecto_tesoreria, nombrecompletoproveedor, COALESCE(convert_from(foto, 'UTF8'), '') FROM datos.comprobantesegresos WHERE sucursalid = @sucursalid ORDER BY fecha DESC", substitutionValues: {
        "sucursalid": prefs.sucursalid
      });

      for (var egreso in _data) {
        EgresoModel nuevoEgreso = EgresoModel();
        nuevoEgreso.id = egreso[0];
        nuevoEgreso.fecha = egreso[1];
        nuevoEgreso.prefijoegreso = egreso[2];
        nuevoEgreso.numeroegreso = egreso[3];
        nuevoEgreso.usuarioid = egreso[4];
        nuevoEgreso.proveedorid = egreso[5];
        nuevoEgreso.nitproveedor = egreso[6];
        nuevoEgreso.tipoegresoid = egreso[7];
        nuevoEgreso.valor = egreso[8];
        nuevoEgreso.observacion = egreso[9];
        nuevoEgreso.fechahoraanulacion = egreso[10];
        nuevoEgreso.motivoanulacion = egreso[11];
        nuevoEgreso.estado = egreso[12];
        nuevoEgreso.prefijocierre = egreso[13];
        nuevoEgreso.numerocierre = egreso[14];
        nuevoEgreso.afectoTesoreria = egreso[15];
        nuevoEgreso.nombreCompletoTercero = egreso[16];
        nuevoEgreso.foto = egreso[17];

        _listadoEgresos.add(nuevoEgreso);
      }

    } catch (e) {
      print(e);
    }
    await conexionNegocio.close();

    return _listadoEgresos;
  }

  Future<List<TipoEgresoModel>> obtenerTiposEgresos() async{
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    List<TipoEgresoModel> _listadoTiposEgresos = [];

    try {
      List<List<dynamic>> _datos = await conexionNegocio.query("SELECT id, tipoegresoscajaid, nombre, puc_gasto, nit_defecto, estado, tipo, descripcion FROM datos.tipoegresoscaja");

      for (var tipo in _datos) {
        TipoEgresoModel tipoEgresoModel = TipoEgresoModel();

        tipoEgresoModel.id = tipo[0];
        tipoEgresoModel.tipoegresocajaid = tipo[1];
        tipoEgresoModel.nombre = tipo[2];
        tipoEgresoModel.pucGasto = tipo[3];
        tipoEgresoModel.nitDefecto = tipo[4];
        tipoEgresoModel.estado = tipo[5];
        tipoEgresoModel.tipo = tipo[6];
        tipoEgresoModel.descripcion = tipo[7];

        _listadoTiposEgresos.add(tipoEgresoModel);
      }

    } catch (e) {
      print(e);
    }
    await conexionNegocio.close();

    return _listadoTiposEgresos;

  }

  Future<void> crearTipoEgreso(TipoEgresoModel tipoEgresoModel) async{
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    try {
      List<List<dynamic>> maxTipoEgresoId = await conexionNegocio.query("SELECT MAX(tipoegresoscajaid) + 1 FROM datos.tipoegresoscaja");
      await conexionNegocio.query("INSERT INTO datos.tipoegresoscaja(tipoegresoscajaid, nombre, descripcion, estado) VALUES (@tipoegresoscajaid, @nombre, @descripcion, @estado)", substitutionValues: {
        "tipoegresoscajaid": maxTipoEgresoId.first.first,
        "nombre": tipoEgresoModel.nombre,
        "descripcion": tipoEgresoModel.descripcion,
        "estado": 0
      });

    } catch (e) {
      print(e);
    }
    await conexionNegocio.close();
  }

  Future<bool> crearEgresoEImprimir(EgresoModel egresoModel) async{
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ImprimirComprobanteEgreso imprimirComprobanteEgreso = ImprimirComprobanteEgreso();


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> _consecutivo = await conexionNegocio.query("SELECT prefijo, CAST(numeroactual AS INTEGER), consecutivoid FROM datos.documentosconsecutivos WHERE tipodocumento = '5'");

      if (_consecutivo.isNotEmpty) {
        await conexionNegocio.query("INSERT INTO datos.comprobantesegresos(fecha, sucursalid, prefijoegreso, numeroegreso, usuarioid, proveedorid, nitproveedor, nombrecompletoproveedor, tipoegresoid, valor, afecto_tesoreria, foto) VALUES(@fecha, @sucursalid, @prefijoegreso, @numeroegreso, @usuarioid, @proveedorid, @nitproveedor, @nombrecompletoproveedor, @tipoegresoid, @valor, @afecto_tesoreria, @foto)", substitutionValues: {
          "fecha": egresoModel.fecha,
          "sucursalid": prefs.sucursalid,
          "prefijoegreso": _consecutivo.first[0],
          "numeroegreso": _consecutivo.first[1],
          "usuarioid": prefs.usuarioid,
          "proveedorid": egresoModel.proveedorid,
          "nitproveedor": egresoModel.nitproveedor,
          "nombrecompletoproveedor": egresoModel.nombreCompletoTercero,
          "tipoegresoid": egresoModel.tipoegresoid,
          "valor": egresoModel.valor,
          "afecto_tesoreria": 0,
          "foto": egresoModel.foto
        });

        await conexionNegocio.query("UPDATE datos.documentosconsecutivos SET numeroactual = @numeroactual WHERE consecutivoid = @consecutivoid", substitutionValues: {
          "numeroactual": _consecutivo.first[1] + 1,
          "consecutivoid": _consecutivo.first[2]
        });

        await conexionNegocio.close();

        await imprimirComprobanteEgreso.imprimir(_consecutivo.first[0], _consecutivo.first[1]);
      }
      else{
        await conexionNegocio.close();
        return false;
      }

      return true;

    } catch (e) {
      print(e);
      await conexionNegocio.close();
      throw(e);

    }

  }

}