import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class ImprimirComprobanteEgreso {
  

  PreferenciasUsuario prefs = PreferenciasUsuario();

  Future<void> imprimir(String prefijo, int numero) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      
      List<List<dynamic>> egreso = await conexionNegocio.query("SELECT id, CAST(fecha AS DATE), prefijoegreso, CAST(numeroegreso AS INTEGER), usuarioid, proveedorid, nitproveedor, tipoegresoid, CAST(valor AS INTEGER), observacion, fechahoraanulacion, motivoanulacion, estado, prefijocierre, CAST(numerocierre AS INTEGER), afecto_tesoreria, nombrecompletoproveedor FROM datos.comprobantesegresos WHERE sucursalid = @sucursalid AND prefijoegreso = @prefijo AND numeroegreso = @numero", substitutionValues: {
        "prefijo": prefijo,
        "numero": numero,
        "sucursalid": prefs.sucursalid
      });

      EgresoModel nuevoEgreso = EgresoModel();
      nuevoEgreso.id = egreso.first[0];
      nuevoEgreso.fecha = egreso.first[1];
      nuevoEgreso.prefijoegreso = egreso.first[2];
      nuevoEgreso.numeroegreso = egreso.first[3];
      nuevoEgreso.usuarioid = egreso.first[4];
      nuevoEgreso.proveedorid = egreso.first[5];
      nuevoEgreso.nitproveedor = egreso.first[6];
      nuevoEgreso.tipoegresoid = egreso.first[7];
      nuevoEgreso.valor = egreso.first[8];
      nuevoEgreso.observacion = egreso.first[9];
      nuevoEgreso.fechahoraanulacion = egreso.first[10];
      nuevoEgreso.motivoanulacion = egreso.first[11];
      nuevoEgreso.estado = egreso.first[12];
      nuevoEgreso.prefijocierre = egreso.first[13];
      nuevoEgreso.numerocierre = egreso.first[14];
      nuevoEgreso.afectoTesoreria = egreso.first[15];
      nuevoEgreso.nombreCompletoTercero = egreso.first[16];

      List<List<dynamic>> impresora = await conexionNegocio.query(
          "SELECT matricula_mercantil FROM datos.empresa WHERE codigoempresaid = @sucursalid", substitutionValues: {
            "sucursalid": prefs.sucursalid
          });

      const PaperSize paper = PaperSize.mm80;

      final profile = await CapabilityProfile.load();

      final printer = NetworkPrinter(paper, profile);

      PosPrintResult res =
          await printer.connect(impresora.first[0], port: 9100);

      if (res == PosPrintResult.success) {
        await _processTicket(nuevoEgreso, printer);

        printer.reset();
        printer.disconnect();

        await conexionNegocio.query("UPDATE datos.comprobantesegresos SET afecto_tesoreria = 1 WHERE prefijoegreso = @prefijo AND numeroegreso = @numero AND sucursalid = @sucursalid", substitutionValues: {
          "prefijo": prefijo,
          "numero": numero,
          "sucursalid": prefs.sucursalid
        });
      }
      else{
        await conexionNegocio.close();
        throw Exception('No se ha podido conectar a la impresora');
      }

      await conexionNegocio.close();
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      throw Exception('Error con la base de datos');
    } on Exception catch (e) {
      await conexionNegocio.close();
      throw e;
    }

    //Se conecta a la impresora
  }

  Future<void> _processTicket(EgresoModel egreso, NetworkPrinter ticket){

    ticket.text("Comprobante Egreso",
      styles: PosStyles(
        bold: true,
        height: PosTextSize.size2,
        width: PosTextSize.size2,
        align: PosAlign.center,
        fontType: PosFontType.fontA,
        reverse: false,
        underline: true,
      ));

    ticket.text(egreso.fecha.toString(),
      styles: PosStyles(
        bold: true,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        align: PosAlign.center,
        fontType: PosFontType.fontA,
        reverse: false,
        underline: true,
      ));


    ticket.text("Consecutivo: "+egreso.prefijoegreso+"-"+egreso.numeroegreso.toString(),
      styles: PosStyles(
        bold: true,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        align: PosAlign.center,
        fontType: PosFontType.fontA,
        reverse: false,
        underline: true,
      ));

    ticket.emptyLines(1);

    ticket.text("DATOS DEL PROVEEDOR",
          styles: PosStyles(align: PosAlign.center));

      ticket.hr(ch: '_');

      ticket.row([
        PosColumn(
          text: 'Nit / cc',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: egreso.nitproveedor.toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      ticket.row([
        PosColumn(
          text: 'Nombre',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: egreso.nombreCompletoTercero.toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      
    
    
  }
}