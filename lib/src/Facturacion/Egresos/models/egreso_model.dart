// To parse this JSON data, do
//
//     final egresoModel = egresoModelFromJson(jsonString);

import 'dart:convert';

EgresoModel egresoModelFromJson(String str) => EgresoModel.fromJson(json.decode(str));

String egresoModelToJson(EgresoModel data) => json.encode(data.toJson());

class EgresoModel {
    EgresoModel({
        this.id,
        this.fecha,
        this.prefijoegreso,
        this.numeroegreso,
        this.usuarioid,
        this.proveedorid,
        this.nitproveedor,
        this.valor,
        this.observacion,
        this.fechahoraanulacion,
        this.motivoanulacion,
        this.estado,
        this.prefijocierre,
        this.numerocierre,
        this.tipoegresoid,
        this.afectoTesoreria,
        this.nombreCompletoTercero,
        this.foto
    });

    int id;
    DateTime fecha;
    String prefijoegreso;
    int numeroegreso;
    int usuarioid;
    int proveedorid;
    String nitproveedor;
    int valor;
    String observacion;
    DateTime fechahoraanulacion;
    String motivoanulacion;
    int estado;
    String prefijocierre;
    int numerocierre;
    int tipoegresoid;
    int afectoTesoreria;
    String nombreCompletoTercero;
    String foto;

    factory EgresoModel.fromJson(Map<String, dynamic> json) => EgresoModel(
        id: json["id"],
        fecha: json["fecha"],
        prefijoegreso: json["prefijoegreso"],
        numeroegreso: json["numeroegreso"],
        usuarioid: json["usuarioid"],
        proveedorid: json["proveedorid"],
        nitproveedor: json["nitproveedor"],
        valor: json["valor"],
        observacion: json["observacion"],
        fechahoraanulacion: json["fechahoraanulacion"],
        motivoanulacion: json["motivoanulacion"],
        estado: json["estado"],
        prefijocierre: json["prefijocierre"],
        numerocierre: json["numerocierre"],
        tipoegresoid: json["tipoegresoid"],
        afectoTesoreria: json["afectoTesoreria"],
        nombreCompletoTercero: json["nombreCompletoTercero"],
        foto: json["foto"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "fecha": fecha,
        "prefijoegreso": prefijoegreso,
        "numeroegreso": numeroegreso,
        "usuarioid": usuarioid,
        "proveedorid": proveedorid,
        "nitproveedor": nitproveedor,
        "valor": valor,
        "observacion": observacion,
        "fechahoraanulacion": fechahoraanulacion,
        "motivoanulacion": motivoanulacion,
        "estado": estado,
        "prefijocierre": prefijocierre,
        "numerocierre": numerocierre,
        "tipoegresoid": tipoegresoid,
        "afectoTesoreria": afectoTesoreria,
        "nombreCompletoTercero": nombreCompletoTercero,
        "foto": foto
    };
}
