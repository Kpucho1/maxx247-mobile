// To parse this JSON data, do
//
//     final tipoEgresoModel = tipoEgresoModelFromJson(jsonString);

import 'dart:convert';

TipoEgresoModel tipoEgresoModelFromJson(String str) => TipoEgresoModel.fromJson(json.decode(str));

String tipoEgresoModelToJson(TipoEgresoModel data) => json.encode(data.toJson());

class TipoEgresoModel {
    TipoEgresoModel({
        this.id,
        this.tipoegresocajaid,
        this.nombre,
        this.pucGasto,
        this.nitDefecto,
        this.estado,
        this.tipo,
        this.descripcion,
    });

    int id;
    int tipoegresocajaid;
    String nombre;
    String pucGasto;
    String nitDefecto;
    int estado;
    int tipo;
    String descripcion;

    factory TipoEgresoModel.fromJson(Map<String, dynamic> json) => TipoEgresoModel(
        id: json["id"],
        tipoegresocajaid: json["tipoegresocajaid"],
        nombre: json["nombre"],
        pucGasto: json["puc_gasto"],
        nitDefecto: json["nit_defecto"],
        estado: json["estado"],
        tipo: json["tipo"],
        descripcion: json["descripcion"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "tipoegresocajaid": tipoegresocajaid,
        "nombre": nombre,
        "puc_gasto": pucGasto,
        "nit_defecto": nitDefecto,
        "estado": estado,
        "tipo": tipo,
        "descripcion": descripcion,
    };
}
