import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_printer_service.dart';

class ReImprimirEgresoLoading extends StatefulWidget {
  final EgresoModel egresoModel;
  ReImprimirEgresoLoading({Key key, this.egresoModel}) : super(key: key);

  @override
  State<ReImprimirEgresoLoading> createState() => _ReImprimirEgresoLoadingState();
}

class _ReImprimirEgresoLoadingState extends State<ReImprimirEgresoLoading> {
   void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _procesarEgreso(context);
    });
    super.didChangeDependencies();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              backgroundColor: Color(0xffDD6E42),
              strokeWidth: 1.5,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Imprimiendo egreso. Por favor espere.'),
          ],
        ),
      ),
    );
  }

  _procesarEgreso(BuildContext context) async{
    try {
      ImprimirComprobanteEgreso imprimirComprobanteEgreso = ImprimirComprobanteEgreso();
      imprimirComprobanteEgreso.imprimir(widget.egresoModel.prefijoegreso, widget.egresoModel.numeroegreso);

      Navigator.pop(context);
    } catch (e) {
      
      await _alertaSinConexion(context, e);
      Navigator.pop(context);
    }
  }

  Future _alertaSinConexion(BuildContext context, Exception e) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          e.toString(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}