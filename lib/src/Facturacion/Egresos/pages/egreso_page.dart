import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/tipo_egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/detalle_egreso_page.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/nuevo_egreso_page.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_postgres_service.dart';
import 'package:intl/intl.dart';


class EgresoPage extends StatefulWidget {
  EgresoPage({Key key}) : super(key: key);

  @override
  State<EgresoPage> createState() => _EgresoPageState();
}

class _EgresoPageState extends State<EgresoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Egresos"),
        centerTitle: true,
      ),
      body: _egresosBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          EgresoPostgresService _egresoPostgresService = EgresoPostgresService();

          List<TipoEgresoModel> tiposEgresos = await _egresoPostgresService.obtenerTiposEgresos();

          Navigator.push(context, MaterialPageRoute(builder: (context) => NuevoEgresoPage(tiposEgresos: tiposEgresos,))).then((value){
            setState(() {
              
            });
          });
        },
      ),
    );
  }

  _egresosBody(){
    EgresoPostgresService egresoPostgresService = EgresoPostgresService();
    return FutureBuilder(
      future: egresoPostgresService.obtenerComprobantesEgresos(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else{
          List<EgresoModel> _listaEgresos = snapshot.data;

          if (_listaEgresos.length > 0) {
            return ListView.separated(
              separatorBuilder: (context, index) => Divider(),
              itemCount: _listaEgresos.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    leading: _listaEgresos[index].afectoTesoreria == 1 && _listaEgresos[index].estado == 0 ? Icon(Icons.check, color: Colors.green,) : Icon(Icons.error, color: Colors.red,),
                    title: _listaEgresos[index].observacion != null ? Text(_listaEgresos[index].observacion, style: TextStyle(fontWeight: FontWeight.bold),) : Text("Sin observación", style: TextStyle(color: Colors.grey),),
                    subtitle: Text(_listaEgresos[index].prefijoegreso +"-"+ _listaEgresos[index].numeroegreso.toString()),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(DateFormat('yyyy-MM-dd kk:mm').format(_listaEgresos[index].fecha.toLocal()).toString()),
                        Text(_listaEgresos[index].nombreCompletoTercero ?? '', style: TextStyle(fontSize: 10),)
                      ],
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => DetalleEgresoPage(egreso: _listaEgresos[index],),));
                    },
                  ),
                );
              },
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/sin_registros.svg', height: 200,),
                  SizedBox(height: 10,),
                  Text("No se han realizado egresos", style: TextStyle(fontSize: 22),)

                ],
              ),
            );
          }
        }
      },
    );
  }
}