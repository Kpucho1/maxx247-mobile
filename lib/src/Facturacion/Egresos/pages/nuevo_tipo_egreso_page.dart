import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/tipo_egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_postgres_service.dart';

class NuevoTipoEgresoPage extends StatefulWidget {
  NuevoTipoEgresoPage({Key key}) : super(key: key);

  @override
  State<NuevoTipoEgresoPage> createState() => _NuevoTipoEgresoPageState();
}

class _NuevoTipoEgresoPageState extends State<NuevoTipoEgresoPage> {
  TextEditingController _nombreController = TextEditingController();
  TextEditingController _descripcionController = TextEditingController();


  @override
  void dispose() {
    _nombreController?.dispose();
    _descripcionController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nuevo Tipo"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Crear", style: TextStyle(color: Colors.white),),
            onPressed: () async{
              TipoEgresoModel tipoEgresoModel = TipoEgresoModel();
              EgresoPostgresService egresoPostgresService = EgresoPostgresService();

              tipoEgresoModel.nombre = _nombreController.text;
              tipoEgresoModel.descripcion = _descripcionController.text;

              await egresoPostgresService.crearTipoEgreso(tipoEgresoModel);


              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _bodyNuevoTipoEgreso(),
    );
  }

  Widget _bodyNuevoTipoEgreso(){
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
             
              _nombreInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _descripcionInput()
              
              
            ],
          ),
        ),
      ),
    ); 
  }


  _nombreInput(){
     return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreController,
      textAlign: TextAlign.start,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Nombre',
      ),
    );
  }

  _descripcionInput(){
     return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _descripcionController,
      textAlign: TextAlign.start,
      maxLines: 4,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Descripcion',
      ),
    );
  }
}