import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_postgres_service.dart';

class ImprimirEgresoLoadingPage extends StatefulWidget {
  final EgresoModel egreso;
  ImprimirEgresoLoadingPage({Key key, this.egreso}) : super(key: key);

  @override
  State<ImprimirEgresoLoadingPage> createState() => _ImprimirEgresoLoadingPageState();
}

class _ImprimirEgresoLoadingPageState extends State<ImprimirEgresoLoadingPage> {
  void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _procesarEgreso(context);
    });
    super.didChangeDependencies();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              backgroundColor: Color(0xffDD6E42),
              strokeWidth: 1.5,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Imprimiendo egreso. Por favor espere.'),
          ],
        ),
      ),
    );
  }

  _procesarEgreso(BuildContext context) async{
    try {
      EgresoPostgresService egresoPostgresService = EgresoPostgresService();
      await egresoPostgresService.crearEgresoEImprimir(widget.egreso);

      Navigator.pop(context);
    } catch (e) {
      
      await _alertaSinConexion(context, e);
      Navigator.pop(context);
    }
  }

  Future _alertaSinConexion(BuildContext context, Exception e) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          e.toString(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}