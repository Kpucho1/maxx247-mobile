import 'package:convert/convert.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/tipo_egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/imprimir_egreso_loading_page.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/nuevo_tipo_egreso_page.dart';
import 'package:maxxshop/src/Facturacion/Egresos/services/egresos_postgres_service.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/models/proveedor_model.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/pages/crear_proveedor_page.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/pages/proveedores_page.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/services/proveedores_postgres_service.dart';
import 'package:maxxshop/src/widgets/boton_subir_foto.dart';

// ignore: must_be_immutable
class NuevoEgresoPage extends StatefulWidget {
  List<TipoEgresoModel> tiposEgresos;
  NuevoEgresoPage({Key key, this.tiposEgresos}) : super(key: key);

  @override
  State<NuevoEgresoPage> createState() => _NuevoEgresoPageState();
}

class _NuevoEgresoPageState extends State<NuevoEgresoPage> {
  EgresoPostgresService _egresoPostgresService = EgresoPostgresService();

  TextEditingController _nitTerceroController = TextEditingController();
  TextEditingController _nombreTerceroController = TextEditingController();
  TextEditingController _observacionesController = TextEditingController();
  TextEditingController _valorController = TextEditingController();

  ProveedorModel _proveedorSeleccionado = ProveedorModel();

  BotonSubirFoto _botonSubirFoto = BotonSubirFoto();

  int _tipoEgresoIdSeleccionado = 0;

  @override
  void dispose() {
    _nitTerceroController?.dispose();
    _nombreTerceroController?.dispose();
    _observacionesController?.dispose();
    _valorController?.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nuevo egreso"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Imprimir", style: TextStyle(color: Colors.white),),
            onPressed: () async{

              List<int> imagenHex = _botonSubirFoto.imagenSeleccionada != null
              ? _botonSubirFoto.imagenSeleccionada.readAsBytesSync()
              : [];

              EgresoModel egresoModel = EgresoModel(
                nitproveedor: _nitTerceroController.text,
                observacion: _observacionesController.text,
                tipoegresoid: _tipoEgresoIdSeleccionado,
                fecha: DateTime.now().toLocal(),
                valor: int.tryParse(_valorController.text),
                nombreCompletoTercero: _proveedorSeleccionado.nombreCompleto ?? _proveedorSeleccionado.razonSocial,
                proveedorid: _proveedorSeleccionado.id,
                foto: hex.encode(imagenHex)
              );

              Navigator.push(context, MaterialPageRoute(builder: (context) => ImprimirEgresoLoadingPage(egreso: egresoModel,))).then((value){
                Navigator.pop(context);
              });

            },
          )
        ],
      ),
      body: _bodyComprobanteEgreso(),      
    );
  }

  Widget _bodyComprobanteEgreso(){
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _nitTerceroInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreTerceroInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _dropDownTipoEgreso(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _fechaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _valorInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _observacionesInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _botonSubirFoto
              
              
            ],
          ),
        ),
      ),
    ); 
  }

  _dropDownTipoEgreso(){
    List<TipoEgresoModel> _listadoTiposEgresos = widget.tiposEgresos;
    _tipoEgresoIdSeleccionado = _listadoTiposEgresos.first.tipoegresocajaid ?? 0;


    return DropdownButtonFormField(
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Tipo Egreso',
        suffixIcon: IconButton(
          icon: Icon(Icons.add_circle),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => NuevoTipoEgresoPage())).then((value) async{
              List<TipoEgresoModel> tiposEgresos = await _egresoPostgresService.obtenerTiposEgresos();

              setState(() {
                widget.tiposEgresos = tiposEgresos;
              });
            });
          },
        )
      ),
      value: _tipoEgresoIdSeleccionado,
      onChanged: (value) {
        setState(() {
          _tipoEgresoIdSeleccionado = value;
        });
      },
      items: _listadoTiposEgresos.map((tipo) {
        return DropdownMenuItem(
          child: Text(tipo.nombre),
          value: tipo.tipoegresocajaid,
        );
      }).toList(),
    );
  }

  _nitTerceroInput(){
    return  TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _nitTerceroController,
      textAlign: TextAlign.start,
      onFieldSubmitted: (value) async{
        ProveedoresPostgresService proveedoresPostgresService = ProveedoresPostgresService();

        ProveedorModel proveedor = await proveedoresPostgresService.obtenerProveedoresPorCedula(value);

        if (proveedor != null) {
          _nitTerceroController.text = proveedor.nit;
          _nombreTerceroController.text = proveedor.nombreCompleto;
        }
        else{
          Navigator.push(context, MaterialPageRoute(builder: (context) => CrearProveedorPage(),));
        }
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Nit tercero',
        suffixIcon: IconButton(
          icon: Icon(Icons.search),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => ProveedoresPage())).then((value){
              _proveedorSeleccionado = value;
              
              setState(() {
                _nitTerceroController.text = _proveedorSeleccionado.nit ?? '';
                _nombreTerceroController.text = _proveedorSeleccionado.nombreCompleto ?? _proveedorSeleccionado.razonSocial ?? 'NN';
              });
            });
          },
        )
      ),
    );
  }

  _nombreTerceroInput(){
     return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      readOnly: true,
      controller: _nombreTerceroController,
      textAlign: TextAlign.start,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Nombre / Razon Social',
      ),
    );
  }

  _fechaInput(){
    return DateTimePicker(
      use24HourFormat: true,
      initialValue: DateTime.now().toLocal().toString(),
      readOnly: true,
      lastDate: DateTime.now(),
      firstDate: DateTime(1990, 12, 31),
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        hintText: "Fecha egreso",
        prefixIcon: Icon(Icons.calendar_today),
      ),
    );
  }

  _valorInput(){
     return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _valorController,
      textAlign: TextAlign.end,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.monetization_on_outlined),
        labelText: 'Valor',
      ),
    );
  }

  _observacionesInput(){
     return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _observacionesController,
      textAlign: TextAlign.start,
      maxLines: 4,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Observaciones',
      ),
    );
  }
}