import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maxxshop/src/Facturacion/Egresos/models/egreso_model.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/re_imprimir_egreso_loading_page.dart';

class DetalleEgresoPage extends StatelessWidget {
  final EgresoModel egreso;
  const DetalleEgresoPage({Key key, this.egreso}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _bodyDetalleEgreso(context),
    );
  }

  _bodyDetalleEgreso(BuildContext context){
    return Center(
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text("Egreso"),
            expandedHeight: 350.0,
            flexibleSpace: FlexibleSpaceBar(
              background: egreso.foto != null && egreso.foto != '' ?  Image.memory(hex.decode(egreso.foto)) : SvgPicture.asset("assets/sin_registros.svg"),
            ),
          ),
          SliverFixedExtentList(
            itemExtent: 150.0,
            delegate: SliverChildListDelegate([
              Card(
                elevation: 10.0,
                child: ListTile(
                  title: Text(egreso.nombreCompletoTercero ?? 'Sin tercero', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Fecha: "+egreso.fecha.toString()),
                      Text("Valor: "+egreso.valor.toString()),
                      TextButton(
                        child: Text("Reimprimir"),
                        onPressed: () async{
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ReImprimirEgresoLoading(egresoModel: egreso,),));
                        },
                      )
                    ],
                  ),
                ),
              )
            ]),
          )
        ],
      ),
    );
  }
}