import 'package:flutter/material.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/Facturacion/Clientes/models/cliente_model.dart';
import 'package:maxxshop/src/Facturacion/Clientes/pages/crear_cliente_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Clientes/search/clientes_search_delegate.dart';

// ignore: must_be_immutable
class MesaClientePage extends StatefulWidget {
  Function callback;
  MesaClientePage({Key key, this.callback}) : super(key: key);

  @override
  _MesaClientePageState createState() => _MesaClientePageState();
}

class _MesaClientePageState extends State<MesaClientePage> {
  List<ClienteModel> listadoClientes = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Seleccionar cliente"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              List<ClienteModel> clientes = await consultarClientes();
              showSearch(
                  context: context,
                  delegate: ClientesSearchDelegate(
                    clientes: clientes,
                    callback: widget.callback,
                  ));
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: consultarClientes(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          listadoClientes = snapshot.data;

          return ListView.builder(
            itemCount: listadoClientes.length,
            itemBuilder: (context, index) {
              return _cardClientes(context, index, listadoClientes);
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CrearClientePage(
                cliente: ClienteModel(clienteid: 0),
                listadoClientes: listadoClientes,
              ),
            ),
          ).then((_) {
            widget.callback();
            setState(() {});
          });
        },
      ),
    );
  }

  Widget _cardClientes(
      BuildContext context, int index, List<ClienteModel> listadoClientes) {
    final size = MediaQuery.of(context).size;

    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    String nombreCompleto =
        listadoClientes[index].nombrecompleto.toString().trimLeft();
    String primerLetra = nombreCompleto.length > 0 ? nombreCompleto[0] : '';

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.05,
        vertical: size.height * 0.002,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundColor:
                    Color((primerLetra.hashCode * 0xFFFFFF).toInt())
                        .withOpacity(1.0),
                child: Text(
                  primerLetra,
                  style: TextStyle(color: Colors.white),
                ),
                radius: 28.0,
              ),
              title: nombreCompleto != null && nombreCompleto != ""
                  ? Text(
                      nombreCompleto,
                    )
                  : Text("-------"),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('NIT/Cédula ${listadoClientes[index].nit.toString()}'),
                ],
              ),
              trailing: cabeceraProvider.mesaCabecera.clienteid ==
                      listadoClientes[index].clienteid
                  ? Icon(
                      Icons.check,
                      color: Colors.green,
                    )
                  : Text(""),
              onTap: () {
                _dialogOpcionesCliente(
                    listadoClientes[index], cabeceraProvider);
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _dialogOpcionesCliente(
      ClienteModel cliente, CabeceraService cabeceraProvider) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          title: Column(
            children: [
              Center(
                  child: Text(
                cliente.nombrecompleto.toString(),
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              )),
              Text(cliente.direccion),
              Text(cliente.ciudad.toUpperCase()),
              Text(cliente.telefono)
            ],
          ),
          children: [
            cabeceraProvider.mesaCabecera.clienteid != cliente.clienteid
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 10.0),
                    child: TextButton(
                      child: Text(
                        'Asociar a esta cuenta',
                      ),
                      style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.blue[200],
                          textStyle: TextStyle(
                            fontSize: 22.0,
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(vertical: 20.0)),
                      onPressed: () async {
                        PostgresFunctions postgresFunctions =
                            PostgresFunctions();
                        await postgresFunctions.asociarCliente(
                            cliente, context);
                        int count = 0;
                        widget.callback();
                        Navigator.popUntil(context, (route) {
                          return count++ == 2;
                        });
                      },
                    ),
                  )
                : SizedBox(),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Editar',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.blue[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CrearClientePage(
                                cliente: cliente,
                                listadoClientes: listadoClientes,
                              ))).then((_) {
                    Navigator.pop(context);
                    setState(() {});
                  });
                },
              ),
            ),
            cabeceraProvider.mesaCabecera.clienteid == cliente.clienteid
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 10.0),
                    child: TextButton(
                      child: Text(
                        'Quitar de esta cuenta',
                      ),
                      style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.blue[200],
                          textStyle: TextStyle(
                            fontSize: 22.0,
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(vertical: 20.0)),
                      onPressed: () async {
                        await quitarClienteCuenta(cabeceraProvider);
                        setState(() {});
                        widget.callback();
                        Navigator.pop(context);
                      },
                    ),
                  )
                : SizedBox(),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: TextButton(
                child: Text(
                  'Cerrar',
                ),
                style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: Colors.red[200],
                    textStyle: TextStyle(
                      fontSize: 22.0,
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 20.0)),
                onPressed: () async {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> quitarClienteCuenta(CabeceraService cabeceraProvider) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    await conexionNegocio.query(
        "UPDATE datos.cuentacabecera SET clienteid = @clienteid, direccioncliente = @direccioncliente, cliente = @cliente, nombrecliente = @nombrecliente, telefonocliente = @telefonocliente, ciudadcliente = @ciudadcliente WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
        substitutionValues: {
          "clienteid": 0,
          "direccioncliente": "",
          "cliente": "",
          "nombrecliente": "",
          "telefonocliente": "",
          "ciudadcliente": "",
          "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
          "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta
        });

    cabeceraProvider.mesaCabecera.nitCliente = "";
    cabeceraProvider.mesaCabecera.nombreCliente = "";
    cabeceraProvider.mesaCabecera.telefonoCliente = "";
    cabeceraProvider.mesaCabecera.direccionCliente = "";
    cabeceraProvider.mesaCabecera.clienteid = 0;

    await conexionNegocio.close();
  }

  Future<List<ClienteModel>> consultarClientes() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    List<ClienteModel> listaClientes = [];

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    try {
      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> clientes = await conexionNegocio.query(
          "SELECT clienteid, COALESCE(nombre1, ''), COALESCE(nombre2, ''), COALESCE(apellido1, ''), COALESCE(apellido2, ''), COALESCE(razonsocial, ''), COALESCE(nombrecompleto, ''), nit, COALESCE(email, ''), COALESCE(telefono, ''), tipo_identificacion, COALESCE(direccion, ''), COALESCE(ciudad, '') FROM datos.clientes ORDER BY nombrecompleto ASC");

      for (var cliente in clientes) {
        ClienteModel temp = ClienteModel();

        if (cliente.length > 0) {
          temp.clienteid = cliente[0];
          temp.primerNombre = cliente[1];
          temp.segundoNombre = cliente[2];
          temp.primerApellido = cliente[3];
          temp.segundoApellido = cliente[4];
          temp.razonsocial = cliente[5];
          temp.nombrecompleto = cliente[6];
          temp.nit = cliente[7];
          temp.email = cliente[8];
          temp.telefono = cliente[9];
          temp.tipoIdentificacion = cliente[10];
          temp.direccion = cliente[11];
          temp.ciudad = cliente[12];

          listaClientes.add(temp);
        }
      }
      await conexionNegocio.close();
    } catch (e) {
      print(e);
    }

    return listaClientes;
  }
}
