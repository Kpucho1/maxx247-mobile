import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';

import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Facturacion/Clientes/models/cliente_model.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';

// ignore: must_be_immutable
class CrearClientePage extends StatefulWidget {
  CrearClientePage({Key key, this.cliente, this.listadoClientes})
      : super(key: key);

  ClienteModel cliente;
  List<ClienteModel> listadoClientes;

  @override
  _CrearClientePageState createState() => _CrearClientePageState();
}

class _CrearClientePageState extends State<CrearClientePage> {
  TextEditingController _nombreController = TextEditingController();
  TextEditingController _identificacionController = TextEditingController();
  TextEditingController _fechaNacimientoController = TextEditingController();
  TextEditingController _numeroCelularController = TextEditingController();
  TextEditingController _direccionResidenciaController =
      TextEditingController();
  TextEditingController _ciudadController = TextEditingController();

  bool _existeCliente = false;

  @override
  void dispose() {
    _nombreController?.dispose();
    _identificacionController?.dispose();
    _fechaNacimientoController?.dispose();
    _numeroCelularController?.dispose();
    _direccionResidenciaController?.dispose();
    _ciudadController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _nombreController.text = widget.cliente.nombrecompleto;
    _identificacionController.text = widget.cliente.nit;
    _numeroCelularController.text = widget.cliente.telefono;
    _direccionResidenciaController.text = widget.cliente.direccion;
    _ciudadController.text = widget.cliente.ciudad;
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [
        GestureType.onTap,
      ],
      child: WillPopScope(
        onWillPop: () {
          if (_nombreController.text != '' ||
              _identificacionController.text != '' ||
              _numeroCelularController.text != '') {
            _dialogoConfirmarSalir();
          } else {
            Navigator.pop(context);
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text('Crear cliente'),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: _formularioCrearCliente(),
          ),
        ),
      ),
    );
  }

  Widget _formularioCrearCliente() {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.1,
        vertical: size.height * 0.05,
      ),
      child: Form(
        child: Column(
          children: [
            _crearIdentificacion(),
            _crearNombre(),
            _crearNumeroCelular(),
            _crearDireccionResidencia(),
            _crearCiudad(),
            SizedBox(
              height: size.height * 0.05,
            ),
            _crearBotonCrearCliente(),
          ],
        ),
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      controller: _nombreController,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        labelText: 'Nombre Completo *',
        helperText: 'Nombres y apellidos',
        suffixIcon: Icon(Icons.account_box),
        labelStyle: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  _validaExistenciaCliente() {
    if (widget.listadoClientes
        .where((element) => element.nit == _identificacionController.text)
        .isNotEmpty) {
      ClienteModel cliente = widget.listadoClientes
          .where((element) => element.nit == _identificacionController.text)
          .first;
      setState(() {
        _nombreController.text = cliente.nombrecompleto;
        _numeroCelularController.text = cliente.telefono;
        _direccionResidenciaController.text = cliente.direccion;
        _ciudadController.text = cliente.ciudad;
        _existeCliente = true;
      });
      widget.cliente = cliente;
    } else {
      setState(() {
        _existeCliente = false;
        _nombreController.text = '';
        _fechaNacimientoController.text = '';
        _numeroCelularController.text = '';
        _direccionResidenciaController.text = '';
        _ciudadController.text = '';
      });
      widget.cliente = ClienteModel(clienteid: 0);
    }
  }

  Widget _crearIdentificacion() {
    return FocusScope(
      onFocusChange: (value) {
        if (!value) {
          _validaExistenciaCliente();
        }
      },
      child: TextFormField(
        onFieldSubmitted: (value) {
          _validaExistenciaCliente();
        },
        controller: _identificacionController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.deny('.'),
          FilteringTextInputFormatter.deny('-'),
          FilteringTextInputFormatter.deny('_'),
          FilteringTextInputFormatter.deny(','),
        ],
        autofocus: true,
        decoration: InputDecoration(
          suffixIcon: Icon(Icons.badge),
          labelText: 'Número de identificación *',
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          helperText: 'NIT/Cédula',
        ),
      ),
    );
  }

  Widget _crearFechaNacimiento() {
    return TextFormField(
      controller: _fechaNacimientoController,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.datetime,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[0-9/]")),
        LengthLimitingTextInputFormatter(10),
        _DateFormatter(),
      ],
      decoration: InputDecoration(
        labelText: 'Fecha de nacimiento',
        suffixIcon: Icon(Icons.cake),
        helperText: 'DD/MM/AAAA',
        labelStyle: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  Widget _crearNumeroCelular() {
    return TextFormField(
      controller: _numeroCelularController,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.deny('.'),
        FilteringTextInputFormatter.deny('-'),
        FilteringTextInputFormatter.deny('_'),
        FilteringTextInputFormatter.deny(','),
      ],
      decoration: InputDecoration(
        labelText: 'Número de celular *',
        suffixIcon: Icon(Icons.phone_android),
        labelStyle: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  Widget _crearDireccionResidencia() {
    return TextFormField(
      controller: _direccionResidenciaController,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        labelText: 'Dirección de correspondencia',
        suffixIcon: Icon(Icons.add_road),
        labelStyle: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  Widget _crearCiudad() {
    return TextFormField(
      controller: _ciudadController,
      decoration: InputDecoration(
        labelText: 'Ciudad',
        suffixIcon: Icon(Icons.location_city),
        labelStyle: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  Widget _crearBotonCrearCliente() {
    final size = MediaQuery.of(context).size;

    return RaisedButton.icon(
      color: Colors.blue,
      padding: EdgeInsets.symmetric(
          horizontal: size.width * 0.2, vertical: size.height * 0.01),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      icon: Icon(
        Icons.save,
        color: Colors.white,
      ),
      label: widget.cliente.clienteid == 0 && !_existeCliente
          ? Text(
              'CREAR CLIENTE',
              style: TextStyle(color: Colors.white),
            )
          : Text(
              'ACTUALIZAR Y ASOCIAR',
              style: TextStyle(color: Colors.white),
            ),
      onPressed:
          widget.cliente.clienteid != 0 ? _actualizarCliente : _crearCliente,
    );
  }

  _actualizarCliente() async {
    await _actualizarClienteEnPostgres(
        _identificacionController.text,
        _nombreController.text,
        _numeroCelularController.text,
        _direccionResidenciaController.text,
        _ciudadController.text);

    setState(() {
      _nombreController.text = '';
      _identificacionController.text = '';
      _fechaNacimientoController.text = '';
      _numeroCelularController.text = '';
      _direccionResidenciaController.text = '';
      _ciudadController.text = '';
    });

    Navigator.pop(context);
  }

  _crearCliente() async {
    await _crearClienteEnPostgres(
        _identificacionController.text,
        _nombreController.text,
        _numeroCelularController.text,
        _direccionResidenciaController.text,
        _ciudadController.text,
        0);
  }

  Future<void> _actualizarClienteEnPostgres(
      String identificacion,
      String nombreCompleto,
      String telefono,
      String direccion,
      String ciudad) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    nombreCompleto =
        nombreCompleto != null && nombreCompleto != "" ? nombreCompleto : "---";

    VendedorModel mesero = await DBProvider.db.obtenerVendedor();

    try {
      await conexionNegocio.transaction((connection) async {
        await connection.query(
            'UPDATE datos.clientes SET nit = @nit, nombrecompleto = @nombrecompleto, telefono = @telefono, direccion = @direccion, ciudad = @ciudad, nombre1 = @nombre1 WHERE clienteid = @clienteid',
            substitutionValues: {
              'clienteid': widget.cliente.clienteid,
              'nit': identificacion,
              'nombrecompleto': nombreCompleto,
              'nombre1': nombreCompleto,
              'telefono': telefono,
              'direccion': direccion,
              'ciudad': ciudad
            });

        ClienteModel nuevoCliente = ClienteModel(
            nit: identificacion,
            nombrecompleto: nombreCompleto,
            telefono: telefono,
            direccion: direccion,
            clienteid: widget.cliente.clienteid,
            ciudad: ciudad);

        List<List<dynamic>> existeCliente = await connection.query(
            "SELECT clienteid, nit FROM datos.clientes WHERE nit = @nit",
            substitutionValues: {"nit": identificacion});

        if (existeCliente[0][0] > 0) {
          await connection.query(
              "UPDATE datos.cuentacabecera SET clienteid = @clienteid, direccioncliente = @direccioncliente, cliente = @cliente, nombrecliente = @nombrecliente, telefonocliente = @telefonocliente, ciudadcliente = @ciudadcliente WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
              substitutionValues: {
                "clienteid": existeCliente[0][0],
                "direccioncliente": nuevoCliente.direccion,
                "cliente": existeCliente[0][1],
                "nombrecliente": nuevoCliente.nombrecompleto,
                "telefonocliente": nuevoCliente.telefono,
                "ciudadcliente": nuevoCliente.ciudad,
                "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                "sucursalid": mesero.sucursalid
              });

          cabeceraProvider.mesaCabecera.nitCliente = nuevoCliente.nit;
          cabeceraProvider.mesaCabecera.nombreCliente =
              nuevoCliente.nombrecompleto;
          cabeceraProvider.mesaCabecera.telefonoCliente = nuevoCliente.telefono;
          cabeceraProvider.mesaCabecera.direccionCliente =
              nuevoCliente.direccion;
          cabeceraProvider.mesaCabecera.clienteid = nuevoCliente.clienteid;
          cabeceraProvider.mesaCabecera.ciudadCliente = nuevoCliente.ciudad;
        } else {
          await _dialogoError();
          connection.cancelTransaction();
        }
      });

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } catch (e) {
      conexionNegocio.cancelTransaction();

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    }
  }

  Future<void> _crearClienteEnPostgres(
      String identificacion,
      String nombreCompleto,
      String telefono,
      String direccion,
      String ciudad,
      int cont) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    bool existe = false;

    VendedorModel mesero = await DBProvider.db.obtenerVendedor();

    nombreCompleto =
        nombreCompleto != null && nombreCompleto != "" ? nombreCompleto : "---";

    try {
      await conexionNegocio.transaction((connection) async {
        List<List<dynamic>> existeClienteAntesInsert = await connection.query(
            "SELECT clienteid FROM datos.clientes WHERE nit = @nit",
            substitutionValues: {
              "nit": identificacion,
            });

        if (existeClienteAntesInsert.length > 0) {
          widget.cliente.clienteid = existeClienteAntesInsert[0][0];
          await _actualizarClienteEnPostgres(
              identificacion, nombreCompleto, telefono, direccion, ciudad);

          Navigator.pop(context);

          return;
        }

        final List<List<dynamic>> clienteID = await connection
            .query('SELECT MAX (clienteid) FROM datos.clientes');
        int nuevoClienteid = clienteID[0][0] + 1;

        await connection.query(
            'INSERT INTO datos.clientes (clienteid, nit, nombrecompleto, telefono, direccion, ciudad, nombre1) VALUES (@clienteid, @nit, @nombrecompleto, @telefono, @direccion, @ciudad, @nombre1)',
            substitutionValues: {
              'clienteid': nuevoClienteid,
              'nit': identificacion,
              'nombrecompleto': nombreCompleto,
              'telefono': telefono,
              'direccion': direccion,
              'ciudad': ciudad,
              'nombre1': nombreCompleto,
            });

        ClienteModel nuevoCliente = ClienteModel(
            nit: identificacion,
            nombrecompleto: nombreCompleto,
            telefono: telefono,
            direccion: direccion,
            clienteid: nuevoClienteid,
            ciudad: ciudad);

        List<List<dynamic>> existeCliente = await connection.query(
            "SELECT clienteid, nit FROM datos.clientes WHERE nit = @nit",
            substitutionValues: {"nit": identificacion});

        if (existeCliente.length > 0) {
          await connection.query(
              "UPDATE datos.cuentacabecera SET clienteid = @clienteid, direccioncliente = @direccioncliente, cliente = @cliente, nombrecliente = @nombrecliente, telefonocliente = @telefonocliente, ciudadcliente = @ciudadcliente WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
              substitutionValues: {
                "clienteid": existeCliente[0][0],
                "direccioncliente": nuevoCliente.direccion,
                "cliente": existeCliente[0][1],
                "nombrecliente": nuevoCliente.nombrecompleto,
                "telefonocliente": nuevoCliente.telefono,
                "ciudadcliente": nuevoCliente.ciudad,
                "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                "sucursalid": mesero.sucursalid
              });

          cabeceraProvider.mesaCabecera.nitCliente = nuevoCliente.nit;
          cabeceraProvider.mesaCabecera.nombreCliente =
              nuevoCliente.nombrecompleto;
          cabeceraProvider.mesaCabecera.telefonoCliente = nuevoCliente.telefono;
          cabeceraProvider.mesaCabecera.direccionCliente =
              nuevoCliente.direccion;
          cabeceraProvider.mesaCabecera.clienteid = nuevoCliente.clienteid;
          cabeceraProvider.mesaCabecera.ciudadCliente = nuevoCliente.ciudad;

          existe = true;
        } else {
          await _dialogoError();
          connection.cancelTransaction();
        }
      });

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      if (existe) {
        setState(() {
          _nombreController.text = '';
          _identificacionController.text = '';
          _fechaNacimientoController.text = '';
          _numeroCelularController.text = '';
          _direccionResidenciaController.text = '';
          _ciudadController.text = '';
        });

        var count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 1;
        });
      }
    } catch (e) {
      print(e);

      conexionNegocio.cancelTransaction();

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      await _dialogoError();
    }
  }

  Future _dialogoError() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          'Error al intentar crear el cliente. Por favor intenta de nuevo.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }

  Future _dialogoConfirmarSalir() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(
              '¿Está seguro que desea salir? La información que no guardó será borrada.'),
          actions: [
            TextButton(
              onPressed: () {
                var count = 0;
                Navigator.popUntil(context, (route) => count++ == 2);
              },
              child: Text(
                'Salir',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text('Cancelar'),
            ),
          ],
        );
      },
    );
  }
}

class _DateFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue prevText, TextEditingValue currText) {
    int selectionIndex;

    // Get the previous and current input strings
    String pText = prevText.text;
    String cText = currText.text;
    // Abbreviate lengths
    int cLen = cText.length;
    int pLen = pText.length;

    if (cLen == 1) {
      // Can only be 0, 1, 2 or 3
      if (int.parse(cText) > 3) {
        // Remove char
        cText = '';
      }
    } else if (cLen == 2 && pLen == 1) {
      // Days cannot be greater than 31
      int dd = int.parse(cText.substring(0, 2));
      if (dd == 0 || dd > 31) {
        // Remove char
        cText = cText.substring(0, 1);
      } else {
        // Add a / char
        cText += '/';
      }
    } else if (cLen == 4) {
      // Can only be 0 or 1
      if (int.parse(cText.substring(3, 4)) > 1) {
        // Remove char
        cText = cText.substring(0, 3);
      }
    } else if (cLen == 5 && pLen == 4) {
      // Month cannot be greater than 12
      int mm = int.parse(cText.substring(3, 5));
      if (mm == 0 || mm > 12) {
        // Remove char
        cText = cText.substring(0, 4);
      } else {
        // Add a / char
        cText += '/';
      }
    } else if ((cLen == 3 && pLen == 4) || (cLen == 6 && pLen == 7)) {
      // Remove / char
      cText = cText.substring(0, cText.length - 1);
    } else if (cLen == 3 && pLen == 2) {
      if (int.parse(cText.substring(2, 3)) > 1) {
        // Replace char
        cText = cText.substring(0, 2) + '/';
      } else {
        // Insert / char
        cText =
            cText.substring(0, pLen) + '/' + cText.substring(pLen, pLen + 1);
      }
    } else if (cLen == 6 && pLen == 5) {
      // Can only be 1 or 2 - if so insert a / char
      int y1 = int.parse(cText.substring(5, 6));
      if (y1 < 1 || y1 > 2) {
        // Replace char
        cText = cText.substring(0, 5) + '/';
      } else {
        // Insert / char
        cText = cText.substring(0, 5) + '/' + cText.substring(5, 6);
      }
    } else if (cLen == 7) {
      // Can only be 1 or 2
      int y1 = int.parse(cText.substring(6, 7));
      if (y1 < 1 || y1 > 2) {
        // Remove char
        cText = cText.substring(0, 6);
      }
    } else if (cLen == 8) {
      // Can only be 19 or 20
      int y2 = int.parse(cText.substring(6, 8));
      if (y2 < 19 || y2 > 20) {
        // Remove char
        cText = cText.substring(0, 7);
      }
    }

    selectionIndex = cText.length;
    return TextEditingValue(
      text: cText,
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}
