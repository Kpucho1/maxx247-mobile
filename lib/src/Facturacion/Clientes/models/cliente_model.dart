// To parse this JSON data, do
//
//     final clienteModel = clienteModelFromJson(jsonString);

import 'dart:convert';

ClienteModel clienteModelFromJson(String str) => ClienteModel.fromJson(json.decode(str));

String clienteModelToJson(ClienteModel data) => json.encode(data.toJson());

class ClienteModel {
    ClienteModel({
        this.clienteid,
        this.primerNombre,
        this.segundoNombre,
        this.primerApellido,
        this.segundoApellido,
        this.razonsocial,
        this.nombrecompleto,
        this.nit,
        this.email,
        this.telefono,
        this.tipoIdentificacion,
        this.direccion,
        this.ciudad
    });

    int clienteid;
    String primerNombre;
    String segundoNombre;
    String primerApellido;
    String segundoApellido;
    String razonsocial;
    String nombrecompleto;
    String nit;
    String email;
    String telefono;
    int tipoIdentificacion;
    String direccion;
    String ciudad;

    factory ClienteModel.fromJson(Map<String, dynamic> json) => ClienteModel(
        clienteid: json["clienteid"],
        primerNombre: json["primer_nombre"],
        segundoNombre: json["segundo_nombre"],
        primerApellido: json["primer_apellido"],
        segundoApellido: json["segundo_apellido"],
        razonsocial: json["razonsocial"],
        nombrecompleto: json["nombrecompleto"],
        nit: json["nit"],
        email: json["email"],
        telefono: json["telefono"],
        tipoIdentificacion: json["tipo_identificacion"],
        direccion: json["direccion"],
        ciudad: json["ciudad"]
    );

    Map<String, dynamic> toJson() => {
        "clienteid": clienteid,
        "primer_nombre": primerNombre,
        "segundo_nombre": segundoNombre,
        "primer_apellido": primerApellido,
        "segundo_apellido": segundoApellido,
        "razonsocial": razonsocial,
        "nombrecompleto": nombrecompleto,
        "nit": nit,
        "email": email,
        "telefono": telefono,
        "tipo_identificacion": tipoIdentificacion,
        "direccion": direccion,
        "ciudad": ciudad
    };
}
