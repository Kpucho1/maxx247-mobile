import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Clientes/models/cliente_model.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';

class ClientesSearchDelegate extends SearchDelegate {
  String get searchFieldLabel => 'Buscar clientes...';

  List<ClienteModel> clientes;
  Function callback;

  ClientesSearchDelegate({@required this.clientes, @required this.callback});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.red,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return Container();
    }
    // var sugerencias;

    // clientes
    //     .where((cliente) =>
    //         cliente.nombrecompleto.toLowerCase().startsWith(query
    //             .toLowerCase()) || //Determina si la busqueda es por nombre o telefono
    //         cliente.telefono.startsWith(query) ||
    //         cliente.nit.startsWith(query) ||
    //         cliente.direccion.toLowerCase().startsWith(query.toLowerCase()))
    //     .toList();

    // return ListView(
    //   children: clientes.map((cliente) {
    //     return Card(
    //       child: ListTile(
    //         title: Text(cliente.nombrecompleto),
    //         subtitle: Text(cliente.telefono),
    //         onTap: () => _dialogOpcionesCliente(context, cliente),
    //       ),
    //     );
    //   }).toList(),
    // );

    return FutureBuilder(
      future: _constructorSugerencias(query),
      builder: (context, AsyncSnapshot<List<ClienteModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          final clientes = snapshot.data;

          return ListView(
            children: clientes.map((cliente) {
              return Card(
                child: ListTile(
                  title: Text(cliente.nombrecompleto),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('NIT/Cédula ${cliente.nit.toString()}'),
                      Text(cliente.direccion.toString())
                    ],
                  ),
                  onTap: () => _dialogOpcionesCliente(context, cliente),
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }

  Future<List<ClienteModel>> _constructorSugerencias(String query) async {
    return clientes
        .where((cliente) =>
            cliente.nombrecompleto.toLowerCase().startsWith(query
                .toLowerCase()) || //Determina si la busqueda es por nombre o telefono
            cliente.telefono.startsWith(query) ||
            cliente.nit.startsWith(query) ||
            cliente.direccion.toLowerCase().startsWith(query.toLowerCase()) ||
            cliente.nombrecompleto.toLowerCase().contains(query))
        .toList();
  }

  Future<void> _dialogOpcionesCliente(
      BuildContext context, ClienteModel cliente) {
    return showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(cliente.nombrecompleto.toString()),
          children: [
            SimpleDialogOption(
              child: Text(
                'Asociar a esta cuenta',
                style: TextStyle(
                  fontSize: 18.0,
                  color: Colors.blue,
                ),
              ),
              onPressed: () async {
                PostgresFunctions postgresFunctions = PostgresFunctions();
                await postgresFunctions.asociarCliente(cliente, context);
                int count = 0;
                callback();
                Navigator.popUntil(context, (route) {
                  return count++ == 3;
                });
              },
            ),
          ],
        );
      },
    );
  }

  // Future<List<ClienteModel>> _consultarClientesSearch(String query) async {
  //   final ConexionesModel conexionActiva =
  //       await DBProvider.db.getConexionActiva();

  //   List<ClienteModel> listaClientes = [];

  //   try {
  //     ConexionNegocioProvider.conexionNegocio.abrirConexion(
  //       conexionActiva.ipservidor,
  //       int.tryParse(conexionActiva.puertobd),
  //       conexionActiva.basedatos,
  //       conexionActiva.usuario,
  //       conexionActiva.password,
  //     );

  //     PostgreSQLConnection conexionNegocio =
  //         await ConexionNegocioProvider.conexionNegocio.retornaConexion();

  //     List<List<dynamic>> clientes = await conexionNegocio.query(
  //         "SELECT clienteid, COALESCE(nombre1, ''), COALESCE(nombre2, ''), COALESCE(apellido1, ''), COALESCE(apellido2, ''), COALESCE(razonsocial, ''), COALESCE(nombrecompleto, ''), nit, COALESCE(email, ''), COALESCE(telefono, ''), tipo_identificacion, COALESCE(direccion, '') FROM datos.clientes");

  //     for (var cliente in clientes) {
  //       ClienteModel temp = ClienteModel();

  //       if (cliente.length > 0) {
  //         temp.clienteid = cliente[0];
  //         temp.primerNombre = cliente[1];
  //         temp.segundoNombre = cliente[2];
  //         temp.primerApellido = cliente[3];
  //         temp.segundoApellido = cliente[4];
  //         temp.razonsocial = cliente[5];
  //         temp.nombrecompleto = cliente[6];
  //         temp.nit = cliente[7];
  //         temp.email = cliente[8];
  //         temp.telefono = cliente[9];
  //         temp.tipoIdentificacion = cliente[10];
  //         temp.direccion = cliente[11];

  //         listaClientes.add(temp);
  //       }
  //     }
  //     await conexionNegocio.close();
  //   } catch (e) {
  //     print(e);
  //   }

  //   return listaClientes
  //       .where((cliente) =>
  //           cliente.nombrecompleto.toLowerCase().startsWith(query
  //               .toLowerCase()) || //Determina si la busqueda es por nombre o telefono
  //           cliente.telefono.startsWith(query) ||
  //           cliente.nit.startsWith(query) ||
  //           cliente.direccion.startsWith(query) ||
  //           cliente.nombrecompleto.contains(query))
  //       .toList();
  // }
}
