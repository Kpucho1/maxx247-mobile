import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/pages/producto_detalle_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/pages/productos_combos_page.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/widgets/contador_productos_widget.dart';
import 'package:provider/provider.dart';

class ProductosPage extends StatefulWidget {
  @override
  _ProductosPageState createState() => _ProductosPageState();
}

class _ProductosPageState extends State<ProductosPage> {
  int cantidadProductos = 1;
  List<String> _tempSelectedGustos = [];
  List<ProductosEquivalenciasModel> _tempSelectedEquivalencias = [];
  TextEditingController _controllerGustosLibre = TextEditingController();

  bool _loadingBotonAgregar = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = ProviderInherited.productosBloc(context);

    return Scaffold(
      body: _listadoProductos(bloc),
    );
  }

  Widget _listadoProductos(ProductosBloc bloc) {
    return Scrollbar(
      child: StreamBuilder(
        stream: bloc.productosStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: LinearProgressIndicator(),
              ),
            );

          final List<ProductosCuentaModel> listaProductos = snapshot.data;

          return ListView.builder(
            itemCount: listaProductos.length,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              return productos(context, listaProductos, index);
            },
          );
        },
      ),
    );
  }

  Widget productos(
      BuildContext context, List<ProductosCuentaModel> listaProductos, int index) {
    final contadorProductos = ContadorProductos();
    return Card(
      child: Column(
        children: [
          ListTile(
            leading: IconButton(
              icon: Icon(
                Icons.add_circle_outline,
                size: 35.0,
                color: Colors.green,
              ),
              onPressed: () async {
                List<ProductosGustosModel> proGusModel = await DBProvider.db
                    .obtenerProductosGustosPorId(
                        listaProductos[index].productoid);
                List<ProductosCombosModel> proComModel = await DBProvider.db
                    .obtenerProductosCombosPorId(
                        listaProductos[index].productoid);
                List<ProductosEquivalenciasModel> proEquiModel =
                    await DBProvider.db.obtenerProductosEquivalenciasPorId(
                        listaProductos[index].productoid);

                if (proEquiModel.isNotEmpty) {
                  _tempSelectedEquivalencias.add(proEquiModel.first);
                }

                if (proGusModel.length > 0 && proComModel.length > 0) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProductosCombosPage(
                              proComModel: proComModel,
                              proGusModel: proGusModel,
                              producto: listaProductos[index],
                              contadorProductos:
                                  contadorProductos.cantidadProductos)));
                } else if (proGusModel.length > 0 &&
                    proComModel.length == 0 &&
                    proEquiModel.length == 0) {
                  //Aquí valida si el producto tiene gustos para mostrar el modal
                  gustosEquivalenciasDialog(context, listaProductos[index],
                      proGusModel, contadorProductos.cantidadProductos, []);
                } else if (proGusModel.length == 0 &&
                    proComModel.length == 0 &&
                    proEquiModel.length > 0) {
                  //Equivalencias solamente
                  gustosEquivalenciasDialog(context, listaProductos[index], [],
                      contadorProductos.cantidadProductos, proEquiModel);
                } else if (proGusModel.length > 0 &&
                    proComModel.length == 0 &&
                    proEquiModel.length > 0) {
                  //Equivalencias con gustosy sin combos
                  gustosEquivalenciasDialog(
                      context,
                      listaProductos[index],
                      proGusModel,
                      contadorProductos.cantidadProductos,
                      proEquiModel);
                } else {
                  //Aquí si el producto no tiene gustos se adiciona automáticamente
                  PostgresFunctions pos = PostgresFunctions();
                  final cabeceraProvider =
                      Provider.of<CabeceraService>(context, listen: false);

                  bool sigueActiva = await pos.agregarProducto(
                      listaProductos[index],
                      contadorProductos.cantidadProductos,
                      cabeceraProvider,
                      _tempSelectedGustos,
                      [],
                      _tempSelectedEquivalencias);
                  final snackBar = SnackBar(
                    content: Text('Producto adicionado'),
                    duration: Duration(milliseconds: 2000),
                  );

                  if (sigueActiva) {
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  } else {
                    Navigator.popUntil(context, (route) => route.isFirst);
                  }
                }
              },
            ),
            title: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${listaProductos[index].nombre}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )),
                Text(listaProductos[index].codigo)
              ],
            ),
            trailing: listaProductos[index].foto != ''
                ? GestureDetector(
                  child: ClipOval(
                    child: Hero(
                        tag: listaProductos[index].codigo,
                        child: Image.memory(
                        hex.decode(listaProductos[index].foto),
                        alignment: Alignment.centerRight,
                        fit: BoxFit.cover,
                        width: 80,
                        gaplessPlayback: true,
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                      return ProductoDetallePage(producto: listaProductos[index],);
                    }));
                  },
                )
                : Icon(Icons.fastfood),
          ),
          Row(
            children: [
              contadorProductos,
              SizedBox(
                width: 30.0,
              ),
              Text(
                'PV: \$ ${listaProductos[index].precio.round()}',
                style: TextStyle(fontSize: 18),
              )
            ],
          ),
        ],
      ),
    );
  }

  Future<void> gustosEquivalenciasDialog(
      BuildContext contextScaffold,
      ProductosCuentaModel producto,
      List<ProductosGustosModel> proGusModel,
      int contadorProductos,
      List<ProductosEquivalenciasModel> proEquiModel) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                proEquiModel.length > 0
                    ? Text("PRESENTACIONES",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold))
                    : SizedBox(),
                proEquiModel.length > 0
                    ? equivalenciasSection(proEquiModel)
                    : SizedBox(),
                proGusModel.length > 0
                    ? Text(
                        "GUSTOS",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      )
                    : SizedBox(),
                proGusModel.length > 0
                    ? gustosSection(proGusModel)
                    : SizedBox(),
                TextField(
                  controller: _controllerGustosLibre,
                  decoration: InputDecoration(
                      helperText: "Texto complementando los gustos",
                      hintText: "Gustos libre"),
                ),
                // SizedBox(height: 600.0,)
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                setState(() {
                  _tempSelectedGustos = [];
                  _tempSelectedEquivalencias = [];
                  _controllerGustosLibre.clear();
                });
                Navigator.pop(context, true);
              },
              child: Text("Cancelar",
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.red)),
                  padding: EdgeInsets.all(10.0)),
            ),
            TextButton(
              onPressed: () async {
                setState(() {
                  _loadingBotonAgregar = true;
                });

                final cabeceraProvider =
                    Provider.of<CabeceraService>(context, listen: false);
                PostgresFunctions pos = PostgresFunctions();

                if (_controllerGustosLibre.text != null &&
                    _controllerGustosLibre.text != "") {
                  _tempSelectedGustos.add(_controllerGustosLibre.text);
                }

                bool sigueActiva = await pos.agregarProducto(
                    producto,
                    contadorProductos,
                    cabeceraProvider,
                    _tempSelectedGustos,
                    [],
                    _tempSelectedEquivalencias);
                setState(() {
                  _tempSelectedGustos = [];
                  _tempSelectedEquivalencias = [];
                  _controllerGustosLibre.clear();
                  _loadingBotonAgregar = false;
                });

                if (sigueActiva) {
                  Navigator.pop(context, true);
                  final snackBar = SnackBar(
                    content: Text('Producto adicionado'),
                    duration: Duration(milliseconds: 2000),
                  );
                  ScaffoldMessenger.of(contextScaffold).showSnackBar(snackBar);
                } else {
                  Navigator.popUntil(context, (route) => route.isFirst);
                }
              },
              child: !_loadingBotonAgregar
                  ? Text("Agregar",
                      style: TextStyle(color: Colors.white, fontSize: 18.0))
                  : LinearProgressIndicator(),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.green)),
                  padding: EdgeInsets.all(10.0)),
            ),
          ],
        );
      },
    );
  }

  Widget gustosSection(List<ProductosGustosModel> proGusModel) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          width: 250.0,
          height: 320.0,
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 1.0)),
          child: Scrollbar(
            child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                      color: Colors.grey,
                    ),
                itemCount: proGusModel.length,
                itemBuilder: (BuildContext context, int index) {
                  String nombreGusto = proGusModel[index].nombre;
                  return SimpleDialogOption(
                    child: CheckboxListTile(
                      title: Text(proGusModel[index].nombre),
                      value: _tempSelectedGustos.contains(nombreGusto),
                      checkColor: Colors.white,
                      activeColor: Colors.blue,
                      onChanged: (bool value) {
                        if (value) {
                          if (!_tempSelectedGustos.contains(nombreGusto)) {
                            setState(() {
                              _tempSelectedGustos.add(nombreGusto);
                            });
                          }
                        } else {
                          if (_tempSelectedGustos.contains(nombreGusto)) {
                            setState(() {
                              _tempSelectedGustos.removeWhere(
                                  (String gusto) => gusto == nombreGusto);
                            });
                          }
                        }
                      },
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  Widget equivalenciasSection(List<ProductosEquivalenciasModel> proEquiModel) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          width: 250.0,
          height: 350.0,
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 1.0)),
          child: Scrollbar(
            child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                      color: Colors.grey,
                    ),
                itemCount: proEquiModel.length,
                itemBuilder: (BuildContext context, int index) {
                  int idEquivalencia = proEquiModel[index].equivalenciaid;
                  return SimpleDialogOption(
                    child: CheckboxListTile(
                      title: Text(proEquiModel[index].presentacion),
                      subtitle: Text("\$" +
                          proEquiModel[index].precioventaincluido.toString()),
                      value: _tempSelectedEquivalencias
                          .where((element) =>
                              element.equivalenciaid == idEquivalencia)
                          .length
                          .isOdd,
                      checkColor: Colors.white,
                      activeColor: Colors.blue,
                      onChanged: (bool value) {
                        if (value) {
                          if (!_tempSelectedEquivalencias
                              .where((element) =>
                                  element.equivalenciaid == idEquivalencia)
                              .length
                              .isOdd) {
                            setState(() {
                              _tempSelectedEquivalencias = [];
                              _tempSelectedEquivalencias
                                  .add(proEquiModel[index]);
                            });
                          }
                        } else {
                          if (_tempSelectedEquivalencias
                              .where((element) =>
                                  element.equivalenciaid == idEquivalencia)
                              .length
                              .isOdd) {
                            setState(() {
                              _tempSelectedEquivalencias.removeWhere(
                                  (element) =>
                                      element.equivalenciaid == idEquivalencia);
                            });
                          }
                        }
                      },
                    ),
                  );
                }),
          ),
        );
      },
    );
  }
}
