import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';

class ProductoDetallePage extends StatefulWidget {
  final ProductosCuentaModel producto;
  ProductoDetallePage({Key key, this.producto}) : super(key: key);

  @override
  State<ProductoDetallePage> createState() => _ProductoDetallePageState();
}

class _ProductoDetallePageState extends State<ProductoDetallePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: widget.producto.codigo,
            child: Image.memory(
              hex.decode(widget.producto.foto),
            ),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}