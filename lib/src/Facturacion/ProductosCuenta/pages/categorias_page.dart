import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/pages/productos_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/search/productos_search_delegate.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriasPage extends StatefulWidget {
  @override
  _CategoriasPageState createState() => _CategoriasPageState();
}

class _CategoriasPageState extends State<CategoriasPage> {
  int _selectedIndex;
  int _selectedIndexSub;

  List<CategoriasModel> subcategorias = [];
  @override
  void initState() {
    _selectedIndex = 0;
    _selectedIndexSub = 1;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final blocProductos = ProviderInherited.productosBloc(context);
    blocProductos.obtenerProductos(1);
  }

  final controller = PageController(
    initialPage: 0,
  );
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderInherited.categoriasBloc(context);
    final blocProductos = ProviderInherited.productosBloc(context);
    bloc.obtenerCategorias();

    return Scaffold(
      appBar: AppBar(
        title: TextField(
          readOnly: true,
          autofocus: true,
          onTap: () {
            showSearch(
              context: context,
              delegate: ProductosSearch(),
            );
          },
          decoration: InputDecoration(
              hintText: "Buscar productos...", icon: Icon(Icons.search)),
        ),
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(
              Icons.qr_code,
              color: Colors.black,
            ),
            onPressed: () async {
              PostgresFunctions postgresFunctions = PostgresFunctions();
              CabeceraService cabeceraProvider =
                  context.read<CabeceraService>();

              String barcode = await FlutterBarcodeScanner.scanBarcode(
                  "#2B061E", "Cancelar", true, ScanMode.DEFAULT);

              ProductosCuentaModel producto =
                  await DBProvider.db.getProductosPorCodigo(barcode);

              if (producto != null) {
                await postgresFunctions
                    .agregarProducto(producto, 1, cabeceraProvider, [], [], []);
                
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    backgroundColor: Colors.cyan[900],
                    content: Text(
                      'Producto agregado exitosamente',
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                      15.0,
                    )),
                  ),
                );
              }

              
            },
          )
        ],
      ),
      body: Row(
        children: [
          Container(
            width: size.width * 0.22,
            child: PageView(controller: controller, children: [
              _listadoCategorias(context, bloc, blocProductos),
              _listadoSubCategorias(context, blocProductos)
            ]),
          ),
          Container(
            width: 1,
            color: Colors.grey,
          ),
          Expanded(
            child: ProductosPage(),
          ),
        ],
      ),
    );
  }

  Widget _listadoCategorias(
      BuildContext context, CategoriasBloc bloc, ProductosBloc blocProductos) {
    return StreamBuilder(
      stream: bloc.categoriasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
              child: Text('Selecciona una categoría para ver los productos'));

        final List<CategoriasModel> listaCategorias = snapshot.data;

        return ListView.builder(
          itemCount: listaCategorias.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            return categorias(listaCategorias, index, blocProductos, context);
          },
        );
      },
    );
  }

  Widget _listadoSubCategorias(
      BuildContext context, ProductosBloc blocProductos) {
    if (subcategorias.length > 0) {
      return ListView.builder(
        itemCount: subcategorias.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return Padding(
              padding: const EdgeInsets.all(18.0),
              child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    size: 40.0,
                  ),
                  onPressed: () {
                    controller.jumpToPage(0);
                    blocProductos.obtenerProductos(1);
                    setState(() {
                      _selectedIndexSub = 1;
                      _selectedIndex = 0;
                    });
                  }),
            );
          } else {
            return ListTile(
              selected: index == _selectedIndexSub,
              selectedTileColor: Colors.grey[300],
              contentPadding: EdgeInsets.symmetric(horizontal: 4.0),
              title: Align(
                alignment: Alignment.center,
                child:
                    ImageListTile(imageString: subcategorias[index - 1].foto),
              ),
              subtitle: Text(
                '${subcategorias[index - 1].descripcion}',
                style: TextStyle(
                  fontSize: 9.0,
                  fontWeight: FontWeight.bold,
                ),
                overflow: TextOverflow.visible,
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
              onTap: () async {
                setState(() {
                  _selectedIndexSub = index;
                });
                await blocProductos
                    .obtenerProductos(subcategorias[index - 1].grupoid);
              },
            );
          }
        },
      );
    } else {
      return Text("Sin subcategorias");
    }
  }

  Widget categorias(List<CategoriasModel> listaCategorias, int index,
      ProductosBloc blocProductos, BuildContext context) {
    // String string = base64Decode(listaCategorias[index].foto.toString().trim());
    return ListTile(
      selected: index == _selectedIndex,
      selectedTileColor: Colors.grey[300],
      contentPadding: EdgeInsets.symmetric(horizontal: 4.0),
      title: Align(
        alignment: Alignment.center,
        child: ImageListTile(imageString: listaCategorias[index].foto),
      ),
      subtitle: Text(
        '${listaCategorias[index].descripcion}',
        style: TextStyle(
          fontSize: 9.0,
          fontWeight: FontWeight.bold,
        ),
        overflow: TextOverflow.visible,
        textAlign: TextAlign.center,
        maxLines: 2,
      ),
      onTap: () async {
        //Cambia el estado del ListTile seleccionado
        subcategorias = await DBProvider.db
            .getSubCategorias(listaCategorias[index].grupoid);

        if (subcategorias.length > 0) {
          if (subcategorias.length == 1) {
            setState(() {
              _selectedIndex = index;
            });
            await blocProductos.obtenerProductos(subcategorias.first.grupoid);
          } else {
            _listadoSubCategorias(context, blocProductos);
            controller.jumpToPage(2);
            blocProductos.obtenerProductos(subcategorias.first.grupoid);
            setState(() {
              _selectedIndex = index;
            });
          }
        } else {
          setState(() {
            _selectedIndex = index;
          });
          await blocProductos.obtenerProductos(listaCategorias[index].grupoid);
        }
      },
    );
  }
}

class ImageListTile extends StatelessWidget {
  final imageString;

  const ImageListTile({Key key, this.imageString}) : super(key: key);

  Widget build(BuildContext context) {
    if (imageString != '') {
      return Image.memory(
        hex.decode(imageString),
        gaplessPlayback: true,
        filterQuality: FilterQuality.low,
        cacheWidth: 100,
        cacheHeight: 100,
      );
    } else {
      return Icon(
        Icons.food_bank,
        size: 65.0,
      );
    }
  }
}
