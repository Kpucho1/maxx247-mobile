import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/widgets/contador_productos_widget.dart';

// ignore: must_be_immutable
class ProductosCombosPage extends StatefulWidget {
  List<ProductosGustosModel> proGusModel;
  List<ProductosCombosModel> proComModel;
  ProductosCuentaModel producto;
  int contadorProductos;

  ProductosCombosPage(
      {Key key,
      this.proComModel,
      this.proGusModel,
      this.producto,
      this.contadorProductos})
      : super(key: key);

  @override
  _ProductosCombosPageState createState() => _ProductosCombosPageState();
}

class _ProductosCombosPageState extends State<ProductosCombosPage> {
  List<String> _tempSelectedGustos = [];
  List<String> _tempSelectedGustosCombo = [];

  List<ProductosCombosModel> _tempSelectedCombos = [];
  List<ProductosEquivalenciasModel> _tempSelectedEquivalencias = [];

  int cantidadCombosSeleccionada = 0;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.producto.nombre),
          centerTitle: true,
          //  actions: [
          //     botonDialogGustosCombos(context),
          //  ],
        ),
        body: Column(
          children: [
            SizedBox(
              height: 5.0,
            ),
            Text("GUSTOS DEL COMBO",
                style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[800])),
            gustosSection(widget.proGusModel),
            Text("PRODUCTOS DEL COMBO",
                style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue[800])),
            Row(
              children: [
                Chip(
                  label: Text(
                    " Seleccionados: " + cantidadCombosSeleccionada.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  backgroundColor: cantidadCombosSeleccionada <
                          widget.producto.numeroMaximoCombos
                      ? Colors.green
                      : Colors.red,
                ),
                SizedBox(
                  width: size.width * 0.4,
                ),
                Chip(
                  label: Text(
                      "Maximo: " +
                          widget.producto.numeroMaximoCombos.toString(),
                      style: TextStyle(fontSize: 20.0)),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
            combosSection(widget.proComModel),
          ],
        ),
        persistentFooterButtons: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.red)),
                padding: EdgeInsets.all(10.0)),
            child: Text("CANCELAR",
                style: TextStyle(color: Colors.white, fontSize: 22.0)),
          ),
          SizedBox(
            width: 20.0,
          ),
          TextButton(
            onPressed: () async {
              final cabeceraProvider =
                  Provider.of<CabeceraService>(context, listen: false);
              PostgresFunctions pos = PostgresFunctions();

              bool sigueActiva = await pos.agregarProducto(
                  widget.producto,
                  widget.contadorProductos,
                  cabeceraProvider,
                  _tempSelectedGustos,
                  _tempSelectedCombos,
                  _tempSelectedEquivalencias);
              setState(() {
                _tempSelectedGustos = [];
              });

              if (sigueActiva) {
                Navigator.pop(context, true);
              } else {
                Navigator.popUntil(context, (route) => route.isFirst);
              }
            },
            style: TextButton.styleFrom(
                backgroundColor: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  side: BorderSide(color: Colors.green),
                ),
                padding: EdgeInsets.all(10.0)),
            child: Text("AGREGAR",
                style: TextStyle(color: Colors.white, fontSize: 22.0)),
          ),
        ],
      ),
    );
  }

  Widget combosSection(List<ProductosCombosModel> proComModel) {
    final size = MediaQuery.of(context).size;
    final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);

    return Container(
      color: Colors.grey[200],
      height: size.height * 0.37,
      child: Scrollbar(
        child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
            itemCount: proComModel.length,
            itemBuilder: (BuildContext context, int index) {
              int idCombo = proComModel[index].id;
              return SimpleDialogOption(
                child: CheckboxListTile(
                  title: Text(
                    proComModel[index].nombre,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Chip(
                        label: Text(formatCurrency
                            .format(proComModel[index].valoradicional)
                            .toString()),
                      ),
                      proComModel[index].gusto != null
                          ? Text("Gustos: " + proComModel[index].gusto)
                          : SizedBox(),
                      proComModel[index].cantidadseleccionada != null
                          ? Text("Cantidad: " +
                              proComModel[index]
                                  .cantidadseleccionada
                                  .toString())
                          : SizedBox()
                    ],
                  ),
                  value: _tempSelectedCombos
                      .where((element) => element.id == idCombo)
                      .length
                      .isOdd,
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  onChanged: (bool value) {
                    if (value) {
                      if (cantidadCombosSeleccionada <
                          widget.producto.numeroMaximoCombos) {
                        for (ProductosCombosModel model
                            in _tempSelectedCombos) {
                          if (model.id == idCombo) {
                            return;
                          }
                        }
                        _consultarGustosProductoComboDialog(
                            context, proComModel, index);
                      }
                    } else {
                      for (var i = 0; i < _tempSelectedCombos.length; i++) {
                        if (_tempSelectedCombos[i].id == idCombo) {
                          setState(() {
                            cantidadCombosSeleccionada -=
                                _tempSelectedCombos[i].cantidadseleccionada;
                            _tempSelectedCombos[i].gusto = null;
                            _tempSelectedCombos[i].cantidadseleccionada = null;
                            _tempSelectedCombos.removeAt(i);
                          });
                        }
                      }
                    }
                  },
                ),
              );
            }),
      ),
    );
  }

  _consultarGustosProductoComboDialog(
      BuildContext context, List<ProductosCombosModel> model, int index) async {
    List<ProductosGustosModel> gustosObjetoCombo = await DBProvider.db
        .obtenerProductosGustosPorId(model[index].productoidhijo);
    List<ProductosEquivalenciasModel> proEquiModel = await DBProvider.db
        .obtenerProductosEquivalenciasPorId(model[index].productoidhijo);

    final contadorProductos = ContadorProductos(
      limite: widget.producto.numeroMaximoCombos - cantidadCombosSeleccionada,
    );

    if (gustosObjetoCombo.length > 0) {
      return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Column(
              children: [
                Center(
                  child: contadorProductos,
                )
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                proEquiModel.length > 0 ? Text("Presentaciones") : SizedBox(),
                proEquiModel.length > 0
                    ? equivalenciasSectionDialog(proEquiModel)
                    : SizedBox(),
                gustosObjetoCombo.length > 0 ? Text("Gustos") : SizedBox(),
                gustosObjetoCombo.length > 0
                    ? gustosSectionDialog(gustosObjetoCombo)
                    : SizedBox(),
              ],
            ),
            actions: [
              TextButton(
                onPressed: () {
                  setState(() {
                    _tempSelectedGustosCombo = [];
                  });
                  Navigator.pop(context, true);
                },
                child: Text("Cancelar", style: TextStyle(color: Colors.white)),
                style: TextButton.styleFrom(
                    backgroundColor: Colors.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        side: BorderSide(color: Colors.red)),
                    padding: EdgeInsets.all(10.0)),
              ),
              TextButton(
                onPressed: () async {
                  setState(() {
                    _tempSelectedCombos.add(model[index]);

                    model[index].gusto = _tempSelectedGustosCombo.join(", ");
                    model[index].cantidadseleccionada =
                        contadorProductos.cantidadProductos;

                    _tempSelectedGustosCombo = [];

                    cantidadCombosSeleccionada +=
                        contadorProductos.cantidadProductos;
                  });

                  Navigator.pop(context);
                },
                child: Text("Agregar", style: TextStyle(color: Colors.white)),
                style: TextButton.styleFrom(
                    backgroundColor: Colors.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        side: BorderSide(color: Colors.green)),
                    padding: EdgeInsets.all(10.0)),
              ),
            ],
          );
        },
      );
    } else {
      setState(() {
        _tempSelectedCombos.add(model[index]);
        cantidadCombosSeleccionada++;
      });
    }
  }

  Widget gustosSectionDialog(List<ProductosGustosModel> gustosObjetoCombo) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        width: 250.0,
        height: 350.0,
        child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
            itemCount: gustosObjetoCombo.length,
            itemBuilder: (BuildContext context, int index) {
              String nombreGusto = gustosObjetoCombo[index].nombre;
              return SimpleDialogOption(
                child: CheckboxListTile(
                  title: Text(gustosObjetoCombo[index].nombre),
                  value: _tempSelectedGustosCombo.contains(nombreGusto),
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  onChanged: (bool value) {
                    if (value) {
                      if (!_tempSelectedGustosCombo.contains(nombreGusto)) {
                        setState(() {
                          // _tempSelectedGustosCombo = [];
                          _tempSelectedGustosCombo.add(nombreGusto);
                        });
                      }
                    } else {
                      if (_tempSelectedGustosCombo.contains(nombreGusto)) {
                        setState(() {
                          _tempSelectedGustosCombo.removeWhere(
                              (String gusto) => gusto == nombreGusto);
                        });
                      }
                    }
                  },
                ),
              );
            }),
      );
    });
  }

  Widget equivalenciasSectionDialog(
      List<ProductosEquivalenciasModel> equivalenciasObjetoCombo) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        width: 250.0,
        height: 350.0,
        child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
            itemCount: equivalenciasObjetoCombo.length,
            itemBuilder: (BuildContext context, int index) {
              int idEquivalencia =
                  equivalenciasObjetoCombo[index].equivalenciaid;

              return SimpleDialogOption(
                child: CheckboxListTile(
                  title: Text(equivalenciasObjetoCombo[index].presentacion),
                  value: _tempSelectedEquivalencias
                      .where(
                          (element) => element.equivalenciaid == idEquivalencia)
                      .length
                      .isOdd,
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  onChanged: (bool value) {
                    if (value) {
                      if (!_tempSelectedEquivalencias
                          .where((element) =>
                              element.equivalenciaid == idEquivalencia)
                          .length
                          .isOdd) {
                        setState(() {
                          _tempSelectedEquivalencias = [];
                          _tempSelectedEquivalencias
                              .add(equivalenciasObjetoCombo[index]);
                        });
                      }
                    } else {
                      if (_tempSelectedEquivalencias
                          .where((element) =>
                              element.equivalenciaid == idEquivalencia)
                          .length
                          .isOdd) {
                        setState(() {
                          _tempSelectedEquivalencias.removeWhere((element) =>
                              element.equivalenciaid == idEquivalencia);
                        });
                      }
                    }
                  },
                ),
              );
            }),
      );
    });
  }

  Widget gustosSection(List<ProductosGustosModel> proGusModel) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey[200],
      height: size.height * 0.35,
      // decoration: BoxDecoration(border: Border.all(color: Colors.grey, width: 1.0)),
      child: Scrollbar(
        child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
            itemCount: proGusModel.length,
            itemBuilder: (BuildContext context, int index) {
              String nombreGusto = proGusModel[index].nombre;
              return SimpleDialogOption(
                child: CheckboxListTile(
                  title: Text(
                    proGusModel[index].nombre,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                  value: _tempSelectedGustos.contains(nombreGusto),
                  checkColor: Colors.white,
                  activeColor: Colors.blue,
                  onChanged: (bool value) {
                    if (value) {
                      if (!_tempSelectedGustos.contains(nombreGusto)) {
                        setState(() {
                          _tempSelectedGustos.add(nombreGusto);
                        });
                      }
                    } else {
                      if (_tempSelectedGustos.contains(nombreGusto)) {
                        setState(() {
                          _tempSelectedGustos.removeWhere(
                              (String gusto) => gusto == nombreGusto);
                        });
                      }
                    }
                  },
                ),
              );
            }),
      ),
    );
  }
}
