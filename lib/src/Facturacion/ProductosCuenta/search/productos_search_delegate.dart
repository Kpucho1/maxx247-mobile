import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/pages/productos_combos_page.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/widgets/contador_productos_widget.dart';
import 'package:provider/provider.dart';

class ProductosSearch extends SearchDelegate {
  @override
  String get searchFieldLabel => 'Buscar productos...';
  List<String> _tempSelectedGustos = [];
  List<ProductosEquivalenciasModel> _tempSelectedEquivalencias = [];

  @override
  TextStyle get searchFieldStyle => TextStyle(
        color: Colors.grey,
      );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.red,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return Container();
    }
    final contadorProductos = ContadorProductos();

    return FutureBuilder(
      future: DBProvider.db.getProductosPorNombre(query),
      builder: (context, AsyncSnapshot<List<ProductosCuentaModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          final productos = snapshot.data;
          return ListView(
            children: productos.map((prod) {
              return Card(
                child: Column(
                  children: [
                    ListTile(
                      trailing: prod.foto != ''
                          ? ClipOval(
                              child: Image.memory(
                                hex.decode(prod.foto),
                                alignment: Alignment.centerRight,
                                fit: BoxFit.cover,
                                width: 55,
                                gaplessPlayback: true,
                              ),
                            )
                          : Icon(Icons.fastfood),
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${prod.nombre}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              )),
                          Text(prod.codigo)
                        ],
                      ),
                      leading: IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          size: 35.0,
                          color: Colors.green,
                        ),
                        onPressed: () async {
                          List<ProductosGustosModel> proGusModel =
                              await DBProvider.db
                                  .obtenerProductosGustosPorId(prod.productoid);
                          List<ProductosCombosModel> proComModel =
                              await DBProvider.db
                                  .obtenerProductosCombosPorId(prod.productoid);
                          List<ProductosEquivalenciasModel> proEquiModel =
                              await DBProvider.db
                                  .obtenerProductosEquivalenciasPorId(
                                      prod.productoid);

                          if (proGusModel.length > 0 &&
                              proComModel.length > 0) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductosCombosPage(
                                        proComModel: proComModel,
                                        proGusModel: proGusModel,
                                        producto: prod,
                                        contadorProductos: contadorProductos
                                            .cantidadProductos)));
                          } else if (proGusModel.length > 0 &&
                              proComModel.length == 0 &&
                              proEquiModel.length == 0) {
                            //Aquí valida si el producto tiene gustos para mostrar el modal
                            gustosEquivalenciasDialog(
                                context,
                                prod,
                                proGusModel,
                                contadorProductos.cantidadProductos, []);
                          } else if (proGusModel.length == 0 &&
                              proComModel.length == 0 &&
                              proEquiModel.length > 0) {
                            //Equivalencias solamente
                            gustosEquivalenciasDialog(
                                context,
                                prod,
                                [],
                                contadorProductos.cantidadProductos,
                                proEquiModel);
                          } else if (proGusModel.length > 0 &&
                              proComModel.length == 0 &&
                              proEquiModel.length > 0) {
                            //Equivalencias con gustosy sin combos
                            gustosEquivalenciasDialog(
                                context,
                                prod,
                                proGusModel,
                                contadorProductos.cantidadProductos,
                                proEquiModel);
                          } else {
                            PostgresFunctions pos = PostgresFunctions();
                            final cabeceraProvider =
                                Provider.of<CabeceraService>(context,
                                    listen: false);

                            pos.agregarProducto(
                                prod,
                                contadorProductos.cantidadProductos,
                                cabeceraProvider,
                                _tempSelectedGustos, [], []);

                            final snackBar = SnackBar(
                              content: Text('Producto adicionado'),
                              duration: Duration(milliseconds: 2000),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        },
                      ),
                    ),
                    Row(
                      children: [
                        contadorProductos,
                        SizedBox(
                          width: 30.0,
                        ),
                        Text(
                          'PV: \$ ${prod.costo.round()}',
                          style: TextStyle(fontSize: 18),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }

  Future<void> gustosEquivalenciasDialog(
      BuildContext contextScaffold,
      ProductosCuentaModel producto,
      List<ProductosGustosModel> proGusModel,
      int contadorProductos,
      List<ProductosEquivalenciasModel> proEquiModel) {
    return showDialog(
      context: contextScaffold,
      builder: (context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              proEquiModel.length > 0
                  ? Text("PRESENTACIONES",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold))
                  : SizedBox(),
              proEquiModel.length > 0
                  ? equivalenciasSection(proEquiModel)
                  : SizedBox(),
              proGusModel.length > 0
                  ? Text(
                      "GUSTOS",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    )
                  : SizedBox(),
              proGusModel.length > 0 ? gustosSection(proGusModel) : SizedBox(),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: Text("Cancelar",
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.red)),
                  padding: EdgeInsets.all(10.0)),
            ),
            TextButton(
              onPressed: () async {
                final cabeceraProvider =
                    Provider.of<CabeceraService>(context, listen: false);
                PostgresFunctions pos = PostgresFunctions();

                bool sigueActiva = await pos.agregarProducto(
                    producto,
                    contadorProductos,
                    cabeceraProvider,
                    _tempSelectedGustos,
                    [],
                    _tempSelectedEquivalencias);

                if (sigueActiva) {
                  Navigator.pop(context, true);
                  final snackBar = SnackBar(
                    content: Text('Producto adicionado'),
                    duration: Duration(milliseconds: 2000),
                  );
                  ScaffoldMessenger.of(contextScaffold).showSnackBar(snackBar);
                } else {
                  Navigator.popUntil(context, (route) => route.isFirst);
                }
              },
              child: Text("Agregar",
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.green)),
                  padding: EdgeInsets.all(10.0)),
            ),
          ],
        );
      },
    );
  }

  Widget gustosSection(List<ProductosGustosModel> proGusModel) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          width: 250.0,
          height: 350.0,
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 1.0)),
          child: Scrollbar(
            child: ListView.builder(
                itemCount: proGusModel.length,
                itemBuilder: (BuildContext context, int index) {
                  String nombreGusto = proGusModel[index].nombre;
                  return SimpleDialogOption(
                    child: CheckboxListTile(
                      title: Text("${proGusModel[index].nombre}"),
                      value: _tempSelectedGustos.contains(nombreGusto),
                      checkColor: Colors.white,
                      activeColor: Colors.blue,
                      onChanged: (bool value) {
                        if (value) {
                          if (!_tempSelectedGustos.contains(nombreGusto)) {
                            setState(() {
                              _tempSelectedGustos.add(nombreGusto);
                            });
                          }
                        } else {
                          if (_tempSelectedGustos.contains(nombreGusto)) {
                            setState(() {
                              _tempSelectedGustos.removeWhere(
                                  (String gusto) => gusto == nombreGusto);
                            });
                          }
                        }
                      },
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  Widget equivalenciasSection(List<ProductosEquivalenciasModel> proEquiModel) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          width: 250.0,
          height: 350.0,
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 1.0)),
          child: Scrollbar(
            child: ListView.builder(
                itemCount: proEquiModel.length,
                itemBuilder: (BuildContext context, int index) {
                  int idEquivalencia = proEquiModel[index].equivalenciaid;
                  return SimpleDialogOption(
                    child: CheckboxListTile(
                      title: Text(proEquiModel[index].presentacion),
                      value: _tempSelectedEquivalencias
                          .where((element) =>
                              element.equivalenciaid == idEquivalencia)
                          .length
                          .isOdd,
                      checkColor: Colors.white,
                      activeColor: Colors.blue,
                      onChanged: (bool value) {
                        if (value) {
                          if (_tempSelectedEquivalencias.length < 1) {
                            if (!_tempSelectedEquivalencias
                                .where((element) =>
                                    element.equivalenciaid == idEquivalencia)
                                .length
                                .isOdd) {
                              setState(() {
                                _tempSelectedEquivalencias
                                    .add(proEquiModel[index]);
                              });
                            }
                          }
                        } else {
                          if (_tempSelectedEquivalencias
                              .where((element) =>
                                  element.equivalenciaid == idEquivalencia)
                              .length
                              .isOdd) {
                            setState(() {
                              _tempSelectedEquivalencias.removeWhere(
                                  (element) =>
                                      element.equivalenciaid == idEquivalencia);
                            });
                          }
                        }
                      },
                    ),
                  );
                }),
          ),
        );
      },
    );
  }
}
