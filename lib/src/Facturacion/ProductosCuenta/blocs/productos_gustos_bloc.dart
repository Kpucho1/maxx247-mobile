import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';

class ProductosGustosBloc {
  static final ProductosGustosBloc _singletonProductosGustos =
      new ProductosGustosBloc._internal();

  factory ProductosGustosBloc() {
    return _singletonProductosGustos;
  }

  ProductosGustosBloc._internal() {
    obtenerProductosGustos();
  }

  final _productosGustosController =
      BehaviorSubject<List<ProductosGustosModel>>();
  final _productosGustosControllerPorId =
      BehaviorSubject<List<ProductosGustosModel>>();
  Stream<List<ProductosGustosModel>> get productosGustosStream =>
      _productosGustosController.stream;
  Stream<List<ProductosGustosModel>> get productosGustosPoridStream =>
      _productosGustosControllerPorId.stream;

  obtenerProductosGustos() async {
    _productosGustosController.sink
        .add(await DBProvider.db.obtenerProductosGustos());
  }

  agregarProductosGustos(ProductosGustosModel nuevoProductoGusto) async {
    await DBProvider.db.agregarProductosGustos(nuevoProductoGusto);
  }

  borrarProductosGustos() async {
    await DBProvider.db.borrarProductosGustos();
  }

  obtenerProductosGustosPorId(int productoid) async {
    _productosGustosControllerPorId.sink
        .add(await DBProvider.db.obtenerProductosGustosPorId(productoid));
  }

  List<ProductosGustosModel> get listaProductosGustos =>
      _productosGustosController.value;
  List<ProductosGustosModel> get productosGustosPorId =>
      _productosGustosControllerPorId.value;

  dispose() {
    _productosGustosController?.close();
    _productosGustosControllerPorId?.close();
  }
}
