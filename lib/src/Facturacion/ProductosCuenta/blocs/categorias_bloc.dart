import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';

class CategoriasBloc {
  final _categoriasController = BehaviorSubject<List<CategoriasModel>>();

  Stream<List<CategoriasModel>> get categoriasStream =>
      _categoriasController.stream;

  dispose() {
    _categoriasController?.close();
  }

  obtenerCategorias() async {
    _categoriasController.sink.add(await DBProvider.db.getTodasCategorias());
  }

  agregarCategorias(CategoriasModel nuevaCategoria) async {
    await DBProvider.db.agregarNuevaCategoria(nuevaCategoria);

    obtenerCategorias();
  }

  borrarCategorias() async {
    await DBProvider.db.deleteCategorias();

    obtenerCategorias();
  }
}
