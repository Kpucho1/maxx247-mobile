import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';

class ProductosBloc {
  final StreamController<List<ProductosCuentaModel>> _productosController =
      StreamController<List<ProductosCuentaModel>>.broadcast();
  // final _productosController = BehaviorSubject<List<ProductosModel>>();
  final _todosProductosController = BehaviorSubject<List<ProductosCuentaModel>>();

  Stream<List<ProductosCuentaModel>> get productosStream =>
      _productosController.stream;

  Stream<List<ProductosCuentaModel>> get todosLosProductos =>
      _todosProductosController.stream;

  dispose() {
    _productosController?.close();
    _todosProductosController?.close();
  }

  obtenerProductos(int grupo1id) async {
    _productosController.sink
        .add(await DBProvider.db.getProductosPorCategoria(grupo1id));
  }

  obtenerTodosProductos() async {
    _todosProductosController.sink.add(await DBProvider.db.getTodosProductos());
  }

  actualizarProductosListaPrecio(ProductosCuentaModel producto) async {
    await DBProvider.db.actualizarProductosListaPrecio(producto);
  }

  agregarProductos(ProductosCuentaModel nuevoProducto) async {
    await DBProvider.db.agregarNuevoProducto(nuevoProducto);
  }

  borrarProductos() async {
    await DBProvider.db.deleteProductos();
  }
}
