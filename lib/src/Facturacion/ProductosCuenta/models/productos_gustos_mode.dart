import 'dart:convert';

ProductosGustosModel productosGustosFromJson(String str) => ProductosGustosModel.fromJson(json.decode(str));

String productosGustosToJson(ProductosGustosModel data) => json.encode(data.toJson());

class ProductosGustosModel {
    ProductosGustosModel({
        this.id,
        this.gustoid,
        this.productoid,
        this.nombre,
    });

    int id;
    int gustoid;
    int productoid;
    String nombre;

    factory ProductosGustosModel.fromJson(Map<String, dynamic> json) => ProductosGustosModel(
        id: json["id"],
        gustoid: json["gustoid"],
        productoid: json["productoid"],
        nombre: json["nombre"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "gustoid": gustoid,
        "productoid": productoid,
        "nombre": nombre,
    };
}
