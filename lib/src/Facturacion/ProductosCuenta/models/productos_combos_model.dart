import 'dart:convert';

ProductosCombosModel productosCombosModelFromJson(String str) => ProductosCombosModel.fromJson(json.decode(str));

String productosCombosModelToJson(ProductosCombosModel data) => json.encode(data.toJson());

class ProductosCombosModel {
    ProductosCombosModel({
        this.id,
        this.productoid,
        this.productoidhijo,
        this.cantidadcombo,
        this.presentacion,
        this.preciocombo,
        this.valoradicional,
        this.tipocomboid,
        this.nombre,
        this.gusto,
        this.codigohijo,
        this.cantidadseleccionada,
        this.productoImprimeComanda,
        this.centrocostoid
    });

    int id;
    int productoid;
    int productoidhijo;
    int cantidadcombo;
    String presentacion;
    double preciocombo;
    double valoradicional;
    int tipocomboid;
    String nombre;
    String gusto;
    String codigohijo;
    int cantidadseleccionada;
    int productoImprimeComanda;
    int centrocostoid;

    factory ProductosCombosModel.fromJson(Map<String, dynamic> json) => ProductosCombosModel(
        id: json["id"],
        productoid: json["productoid"],
        productoidhijo: json["productoidhijo"],
        cantidadcombo: json["cantidadcombo"],
        presentacion: json["presentacion"],
        preciocombo: json["preciocombo"],
        valoradicional: json["valoradicional"],
        tipocomboid: json["tipocomboid"],
        nombre: json["nombre"],
        codigohijo: json["codigohijo"],
        productoImprimeComanda: json["productoImprimeComanda"],
        centrocostoid: json["centrocostoid"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "productoid": productoid,
        "productoidhijo": productoidhijo,
        "cantidadcombo": cantidadcombo,
        "presentacion": presentacion,
        "preciocombo": preciocombo,
        "valoradicional": valoradicional,
        "tipocomboid": tipocomboid,
        "nombre": nombre,
        "codigohijo": codigohijo,
        "productoimprimecomanda": productoImprimeComanda,
        "centrocostoid": centrocostoid
    };
}
