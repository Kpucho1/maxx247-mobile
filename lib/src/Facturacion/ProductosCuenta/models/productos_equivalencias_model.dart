import 'dart:convert';

ProductosEquivalenciasModel productosEquivalenciasModelFromJson(String str) => ProductosEquivalenciasModel.fromJson(json.decode(str));

String productosEquivalenciasModelToJson(ProductosEquivalenciasModel data) => json.encode(data.toJson());

class ProductosEquivalenciasModel {
    ProductosEquivalenciasModel({
        this.id,
        this.productoid,
        this.equivalenciaid,
        this.equivalencia,
        this.presentacion,
        this.porcentajeiva,
        this.porcentajeipo,
        this.tipoequivalencia,
        this.estado,
        this.principal,
        this.precioventaincluido,
    });

    int id;
    int productoid;
    int equivalenciaid;
    int equivalencia;
    String presentacion;
    int porcentajeiva;
    int porcentajeipo;
    int tipoequivalencia;
    int estado;
    int principal;
    int precioventaincluido;

    factory ProductosEquivalenciasModel.fromJson(Map<String, dynamic> json) => ProductosEquivalenciasModel(
        id: json["id"],
        productoid: json["productoid"],
        equivalenciaid: json["equivalenciaid"],
        equivalencia: json["equivalencia"],
        presentacion: json["presentacion"],
        porcentajeiva: json["porcentajeiva"],
        porcentajeipo: json["porcentajeipo"],
        tipoequivalencia: json["tipoequivalencia"],
        estado: json["estado"],
        principal: json["principal"],
        precioventaincluido: json["precioventaincluido"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "productoid": productoid,
        "equivalenciaid": equivalenciaid,
        "equivalencia": equivalencia,
        "presentacion": presentacion,
        "porcentajeiva": porcentajeiva,
        "porcentajeipo": porcentajeipo,
        "tipoequivalencia": tipoequivalencia,
        "estado": estado,
        "principal": principal,
        "precioventaincluido": precioventaincluido,
    };
}
