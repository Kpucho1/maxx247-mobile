import 'dart:convert';

ProductosCuentaModel productosModelFromJson(String str) =>
    ProductosCuentaModel.fromJson(json.decode(str));

String productosModelToJson(ProductosCuentaModel data) => json.encode(data.toJson());

class ProductosCuentaModel {
  ProductosCuentaModel({
    this.id,
    this.productoid,
    this.nombre,
    this.costo,
    this.grupo1id,
    this.centrocostoid,
    this.impuestoconsumo,
    this.codigo,
    this.precioivaventa,
    this.presentacion,
    this.foto,
    this.productoValorUnitario,
    this.productoImprimeComanda,
    this.productoEquivalencia,
    this.productoPorIva,
    this.productoPorIpo,
    this.esServicio,
    this.numeroMaximoCombos,
    this.ordenvista,
    this.estado, 
    this.precio,
    this.codigobarras1,
    this.codigobarras2,
    this.ts,
    this.pidePeso,
    this.porTiempo
  });

  int id;
  int productoid;
  String nombre;
  double costo;
  int grupo1id;
  int centrocostoid;
  double impuestoconsumo;
  String codigo;
  double precioivaventa;
  String presentacion;
  String foto;
  int productoValorUnitario;
  int productoImprimeComanda;
  int productoEquivalencia;
  double productoPorIva;
  double productoPorIpo;
  int esServicio;
  int numeroMaximoCombos;
  int ordenvista;
  int estado;
  double precio;
  String codigobarras1;
  String codigobarras2;
  String ts;
  int pidePeso;
  int porTiempo;

  factory ProductosCuentaModel.fromJson(Map<String, dynamic> json) => ProductosCuentaModel(
        id: json["id"],
        productoid: json["productoid"],
        nombre: json["nombre"],
        costo: json["costo"],
        grupo1id: json["grupo1id"],
        centrocostoid: json["centrocostoid"],
        impuestoconsumo: json["impuestoconsumo"],
        codigo: json["codigo"],
        precioivaventa: json["precioivaventa"],
        presentacion: json["presentacion"],
        foto: json["foto"],
        productoValorUnitario: json["productovalorunitario"],
        productoImprimeComanda: json["productoimprimecomanda"],
        productoEquivalencia: json["productoequivalencia"],
        productoPorIva: json["productoporiva"],
        productoPorIpo: json["productoporipo"],
        esServicio: json["esservicio"],
        numeroMaximoCombos: json["numeromaximocombos"],
        ordenvista: json["ordenvista"],
        estado: json["estado"],
        precio: json["precio"],
        codigobarras1: json["codigobarras1"],
        codigobarras2: json["codigobarras2"],
        ts: json["ts"],
        pidePeso: json["pidePeso"],
        porTiempo: json["porTiempo"]
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "productoid": productoid,
        "nombre": nombre,
        "costo": costo,
        "grupo1id": grupo1id,
        "centrocostoid": centrocostoid,
        "impuestoconsumo": impuestoconsumo,
        "codigo": codigo,
        "precioivaventa": precioivaventa,
        "presentacion": presentacion,
        "foto": foto,
        "productovalorunitario": productoValorUnitario,
        "productoimprimecomanda": productoImprimeComanda,
        "productoequivalencia": productoEquivalencia,
        "productoporiva": productoPorIva,
        "productoporipo": productoPorIpo,
        "esservicio": esServicio,
        "numeromaximocombos": numeroMaximoCombos,
        "ordenvista": ordenvista,
        "estado": estado,
        "precio": precio,
        "codigobarras1": codigobarras1,
        "codigobarras2": codigobarras2,
        "ts": ts,
        "pidepeso": pidePeso,
        "portiempo": porTiempo
      };
}
