import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/facturas_pendientes_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';

// ignore: must_be_immutable
class DetalleFacturaPage extends StatefulWidget {
  DetalleFacturaPage({Key key, this.factura}) : super(key: key);

  FacturaPendienteModel factura;

  @override
  _DetalleFacturaPageState createState() => _DetalleFacturaPageState();
}

class _DetalleFacturaPageState extends State<DetalleFacturaPage> {
  var _results;

  @override
  void initState() {
    super.initState();
    _results = consultarInformacion();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
          appBar: AppBar(
            title: Text(widget.factura.prefijo +
                "-" +
                widget.factura.numero.toString()),
            centerTitle: true,
            bottom: TabBar(
              indicatorColor: Color(0xffDD6E42),
              tabs: [
                Tab(
                  icon: Icon(Icons.shopping_bag),
                  text: 'Productos',
                ),
                // Tab(
                //   icon: Icon(Icons.file_present),
                //   text: 'Comandas',
                // )
              ],
            ),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              _listaProductos(),
              // _listaComandas(),
            ],
          )),
      length: 1, //2
    );
  }

  Widget _listaComandas() {
    return Column(
      children: [
        Expanded(
          child: FutureBuilder(
            future: _results,
            builder: (context, snapshot) {
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        )
      ],
    );
  }

  Widget _listaProductos() {
    return Column(
      children: [
        Expanded(
            child: FutureBuilder(
          future: _results,
          builder: (context, AsyncSnapshot<List> snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            final listaGeneral = snapshot.data;

            return ListView.builder(
              itemCount: listaGeneral[0].length,
              itemBuilder: (context, index) {
                return _cardProductos(context, index, listaGeneral[0]);
              },
            );
          },
        ))
      ],
    );
  }

  Widget _cardProductos(BuildContext context, int index,
      List<ProductoCuentaModel> listadoProductos) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.05,
        vertical: size.height * 0.002,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text('${listadoProductos[index].nombre}'),
              trailing: Text(
                  'Total: \$ ${listadoProductos[index].valortotal.round()} \n Cantidad: ${listadoProductos[index].cantidad}'),
            ),
          ],
        ),
      ),
    );
  }

  Future<List> consultarInformacion() async {
    final conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    List<ProductoCuentaModel> listaProductosModel = [];

    List listaGeneral = [];

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> listaProductos = await conexionNegocio.query(
          'SELECT fposdescripcion, CAST(fposcantidad AS REAL), CAST(fpossubunitario AS REAL) FROM datos.facturadetalle WHERE fposprefijo = @prefijocuenta AND fposnumero = @numerocuenta ORDER BY id ASC',
          substitutionValues: {
            "prefijocuenta": widget.factura.prefijo,
            "numerocuenta": widget.factura.numero,
          }); //Aquí consulta los productos de la cuenta de la mesa actual para renderizarlos

      for (var producto in listaProductos) {
        ProductoCuentaModel pro = ProductoCuentaModel();

        pro.nombre = producto[0];
        pro.cantidad = producto[1];
        pro.valortotal = producto[2];

        listaProductosModel.add(pro);
      }

      listaGeneral.add(listaProductosModel);

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }

    return listaGeneral;
  }
}
