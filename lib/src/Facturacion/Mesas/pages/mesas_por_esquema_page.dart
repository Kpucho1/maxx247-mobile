import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';
import 'package:maxxshop/src/widgets/lista_grid_widget.dart';

class MesasPorEsquemaPage extends StatefulWidget {
  final int esquemaId;


  int filtro;
  Function actualizarNumMesasVendedor;
  int vendedorid;
  MesasPorEsquemaPage({@required this.esquemaId, this.filtro, this.actualizarNumMesasVendedor, this.vendedorid});

  @override
  _MesasPorEsquemaPageState createState() => _MesasPorEsquemaPageState();
}

class _MesasPorEsquemaPageState extends State<MesasPorEsquemaPage> {
  final mesasRefreshBloc = MesasRefreshBloc();
  final blocEsquema = EsquemasBloc();

  EsquemaModel esquemaEnUso;

  @override
  void initState() {
    mesasRefreshBloc.obtenerMesasPorEsquema(widget.esquemaId);
    esquemaEnUso = blocEsquema.esquemaPorId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.filtro == 0 ? mesasRefreshBloc.obtenerMesasPorEsquema(widget.esquemaId) : mesasRefreshBloc.obtenerMesasPorEsquemaVendedor(widget.esquemaId);

    return Scaffold(
      body: Center(
        child: _listadoMesas(),
      ),
    );
  }

  Widget _listadoMesas() {
    return StreamBuilder<List<MesaModel>>(
      stream: mesasRefreshBloc.mesasPorEsquemaStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }

        final listadoMesasBD = snapshot.data;

        // widget.actualizarNumMesasVendedor(numero);

        // print("Mesas: "+listadoMesasBD.length.toString());

        return ListaGrid(
          listaMesas: listadoMesasBD,
          esquemaId: widget.esquemaId,
          esquemaEnUso: esquemaEnUso,
          actualizarNumMesasVendedor: widget.actualizarNumMesasVendedor,
        );
      },
    );
  }
}
