import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesa_individual_page.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';

class MesaCuentasActivasPage extends StatefulWidget {
  MesaCuentasActivasPage({Key key}) : super(key: key);

  @override
  _MesaCuentasActivasPageState createState() => _MesaCuentasActivasPageState();
}

class _MesaCuentasActivasPageState extends State<MesaCuentasActivasPage> {
  final _textController = new TextEditingController();
  // final timer = TimerConsultaBD.timerConsultaBD;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (ModalRoute.of(context).isCurrent) {
      setState(() {});
    }
  }

  Widget build(BuildContext context) {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    return WillPopScope(
      onWillPop: () {
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 2;
        });
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              'Cuentas Activas ${cabeceraProvider.mesaCabecera.nombreMesa}'),
          centerTitle: true,
        ),
        body: FutureBuilder(
          future: consultarCuentasActivas(cabeceraProvider),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            List<MesaCabecera> listadoCabecera = snapshot.data;

            return ListView.builder(
              itemCount: listadoCabecera.length,
              itemBuilder: (context, index) {
                return _cardCabeceras(
                  context,
                  index,
                  listadoCabecera,
                  cabeceraProvider,
                );
              },
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            dialogAdicionarCuenta(context);
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Future<Widget> dialogAdicionarCuenta(BuildContext context) async {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    PostgresFunctions postFunctions = PostgresFunctions();

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Adición de cuenta  la mesa'),
          content: _formNuevaCuenta(context),
          actions: [
            TextButton(
              child: Text(
                'Agregar',
                style: TextStyle(fontSize: 20.0),
              ),
              onPressed: () async {
                await postFunctions.crearCuentaEnBD(
                    cabeceraProvider.mesaCabecera.objetoid,
                    _textController.text,
                    context,
                    cabeceraProvider.mesaCabecera.nombreMesa,
                    1);
                _textController.text = "";
                Navigator.pop(context);
                setState(() {});
              },
            ),
            TextButton(
              child: Icon(
                Icons.remove_circle_outline,
                color: Colors.red,
              ),
              onPressed: () {
                _textController.text = "";
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Widget _formNuevaCuenta(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(vertical: size.width * 0.1),
      child: TextField(
        controller: _textController,
        autofocus: true,
        textAlign: TextAlign.center,
        autocorrect: false,
        enableSuggestions: false,
        decoration: InputDecoration(
          helperText: 'Alias de cuenta',
          helperStyle: TextStyle(fontSize: 16.0),
          icon: Icon(
            Icons.text_fields,
            size: 35.0,
            color: Color(0xff2B061E),
          ),
        ),
      ),
    );
  }

  Widget _cardCabeceras(BuildContext context, int index,
      List<MesaCabecera> listadoCabecera, CabeceraService cabeceraProvider) {
    final size = MediaQuery.of(context).size;

    final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);
    DateTime utcDateTime = DateTime.parse(listadoCabecera[index].ts);
    DateTime localTime = utcDateTime.toLocal();

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.05,
        vertical: size.height * 0.002,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text(listadoCabecera[index].prefijocuenta.toString() +
                  listadoCabecera[index].numerocuenta.toString() +
                  " / " +
                  listadoCabecera[index].nombreCuenta.toString()),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Apertura: " + localTime.toString()),
                  SizedBox(
                    height: 5.0,
                  ),
                  listadoCabecera[index].nombreCliente != ''
                      ? Text("Cliente: " +
                          listadoCabecera[index].nombreCliente.toString())
                      : Text("Sin cliente")
                ],
              ),
              trailing: Text(
                formatCurrency.format(listadoCabecera[index].totalapagar),
                style: TextStyle(fontSize: 18.0),
              ),
              onTap: () async {
                cabeceraProvider.mesaCabecera.prefijocuenta =
                    listadoCabecera[index].prefijocuenta;
                cabeceraProvider.mesaCabecera.numerocuenta =
                    listadoCabecera[index].numerocuenta;

                cabeceraProvider.mesaCabecera.nombreCliente =
                    listadoCabecera[index].nombreCliente;

                cabeceraProvider.mesaCabecera.nitCliente =
                    listadoCabecera[index].nitCliente;

                cabeceraProvider.mesaCabecera.nombreCuenta =
                    listadoCabecera[index].nombreCuenta;

                // TODO: Navega a la mesa individual
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MesaIndividualPage(),
                    ));
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<List<MesaCabecera>> consultarCuentasActivas(
      CabeceraService cabeceraProvider) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    List<MesaCabecera> listaCabeceras = [];

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> cabeceras = await conexionNegocio.query(
          "SELECT prefijocuenta, CAST(numerocuenta AS INTEGER), CAST(totalapagar AS INTEGER), CAST(ts AS TEXT), id, nombrecuenta, nombrecliente, direccioncliente, telefonocliente FROM datos.cuentasactivas WHERE mesahabitacionid = @objetoid AND estadocuenta <> 2 ORDER BY id ASC",
          substitutionValues: {
            "objetoid": cabeceraProvider.mesaCabecera.objetoid
          });

      for (var cabecera in cabeceras) {
        MesaCabecera cabeceraModel = MesaCabecera();

        cabeceraModel.prefijocuenta = cabecera[0];
        cabeceraModel.numerocuenta = cabecera[1];
        cabeceraModel.totalapagar = cabecera[2];
        cabeceraModel.ts = cabecera[3];
        cabeceraModel.idcuenta = cabecera[4];
        cabeceraModel.nombreCuenta = cabecera[5] != null ? cabecera[5] : '';
        cabeceraModel.nombreCliente =
            cabecera[6] != null ? cabecera[6].toString() : '';
        cabeceraModel.direccionCliente =
            cabecera[7] != null ? cabecera[7].toString() : '';
        cabeceraModel.telefonoCliente =
            cabecera[8] != null ? cabecera[8].toString() : '';

        listaCabeceras.add(cabeceraModel);
      }

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } catch (e) {
      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
      print(e);
    }

    return listaCabeceras;
  }
}
