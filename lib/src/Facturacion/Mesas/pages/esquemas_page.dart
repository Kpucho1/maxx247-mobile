import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesas_por_esquema_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
//import 'package:maxxshop/src/provider.dart';

class EsquemasPage extends StatefulWidget {
  int filtro;
  Function actualizarNumMesasVendedor;
  int vendedorid;
  EsquemasPage({this.filtro, this.actualizarNumMesasVendedor, this.vendedorid});
  @override
  _EsquemasPageState createState() => _EsquemasPageState();
}

class _EsquemasPageState extends State<EsquemasPage> {
  final mesasRefreshBloc = MesasRefreshBloc();
  final esquemasBloc = EsquemasBloc();
  int _selectedIndex;
  List<EsquemaModel> listaEsquemasDelStream;

  final prefs = PreferenciasUsuario();

  @override
  void initState() {
    _selectedIndex = 0;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    //final esquemasBloc = Provider.esquemasBloc(context);
    esquemasBloc.obtenerEsquemas();
    listaEsquemasDelStream = esquemasBloc.listaEsquemas;
    final bloc = EsquemasBloc();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      bloc.obtenerEsquemaActivo(1);
      mesasRefreshBloc.obtenerMesasPorEsquema(1);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        prefs.modoVistaMesas == 1 ? Container(
          height:
              listaEsquemasDelStream.length == 1 ? 0.0 : size.height * 0.075,
          child: _listadoEsquemas(esquemasBloc),
        ) : SizedBox(),
        Container(
          height: 1.0,
          color: Colors.grey,
        ),
        Expanded(
          child: StreamBuilder<Object>(
            stream: esquemasBloc.esquemaActivoStream,
            builder: (context, snapshot) {
              return MesasPorEsquemaPage(esquemaId: esquemasBloc.esquemaActivo, filtro: widget.filtro, actualizarNumMesasVendedor: widget.actualizarNumMesasVendedor, vendedorid: widget.vendedorid,);
            },
          ),
        ),
      ],
    );
  }

  Widget _listadoEsquemas(EsquemasBloc bloc) {
    return StreamBuilder(
      stream: bloc.esquemasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
            child: CircularProgressIndicator(),
          );

        final listadoEsquemasStream = snapshot.data;

        return ListView.builder(
          itemCount: listadoEsquemasStream.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            if (listadoEsquemasStream.length == 1) {
              return Container();
            } else {
              return esquemas(index, listadoEsquemasStream, bloc);
            }
          },
        );
      },
    );
  }

  Widget esquemas(
      int index, List<EsquemaModel> listaEsquemas, EsquemasBloc bloc) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 0.5),
      ),
      width: 145.0,
      child: ListTile(
        selected: index == _selectedIndex,
        selectedTileColor: Colors.grey[300],
        title: Text(
          '${listaEsquemas[index].nombre}',
          textAlign: TextAlign.center,
        ),
        onTap: () {
          setState(() {
            _selectedIndex = index;
          });

          bloc.obtenerEsquemaActivo(listaEsquemas[index].id);
          mesasRefreshBloc.obtenerMesasPorEsquema(listaEsquemas[index].id);
        },
      ),
    );
  }
}
