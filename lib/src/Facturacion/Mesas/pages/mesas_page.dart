import 'dart:async';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Home/models/sucursal_model.dart';
import 'package:maxxshop/src/Home/pages/ajustes_page.dart';
import 'package:maxxshop/src/Home/pages/menus_page.dart';

import 'package:maxxshop/src/Utils/timer.dart';
import 'package:maxxshop/src/db/conexion_db_timer.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/esquemas_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/despachos_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Conexiones/blocs/conexiones_bloc.dart';

class MesasPage extends StatefulWidget {
  @override
  _MesasPageState createState() => _MesasPageState();
}

class _MesasPageState extends State<MesasPage> with WidgetsBindingObserver {
  //

  final prefs = PreferenciasUsuario();
  final esquemasBloc = new EsquemasBloc();
  final conexionesBloc = new ConexionesBloc();
  final mesasRefreshBloc = new MesasRefreshBloc();
  TimerConsultaBD timer = TimerConsultaBD.timerConsultaBD;

  List<EsquemaModel> listadoEsquemas = [];

  int filtro = 0;

  @override
  void initState() {
    mesasRefreshBloc.obtenerTodasMesas();
    esquemasBloc.obtenerEsquemaActivo(1);
    super.initState();
  }

  @override
  void dispose() {
    timer.detenerTimer;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    listadoEsquemas = esquemasBloc.listaEsquemas;
    if (filtro == 0) {
      timer.iniciarTimer;
    }

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.filter_list_alt,
              size: 27.0,
            ),
            onPressed: () {
              //

              setState(() {
                filtro = filtro == 0 ? 1 : 0;
              });

              timer.detenerTimer;
            },
          ),
          title: Column(
            children: [
              Text("Mesas"),
            ],
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: [
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () async {
                timer.detenerTimer;

                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AjustesPage(),
                  ),
                ).then((_) {
                  timer.iniciarTimer;
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.logout),
              onPressed: () async{
                // timer?.cancel();
                prefs.timerHashCode = -1;
                await DBProvider.db.deleteMesas();
                // await DBProvider.db.deleteCategorias();
                // await DBProvider.db.deleteProductos();
                await DBProvider.db.borrarVendedor();
                await esquemasBloc.borrarEsquemas();
                // await DBProvider.db.borrarProductosGustos();
                // await DBProvider.db.borrarProductosEquivalencias();
                await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
                await ConexionTimerProvider.conexionTimer.cerrarConexion();

                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => MenusPage(),
                    ),
                    (route) => false);
              },
            ),
          ],
        ),
        body: EsquemasPage(
          filtro: filtro,
        ),
      );
  }

  // Future _dialogoConfirmarCierreConexion() {
  //   return showDialog(
  //     context: context,
  //     barrierDismissible: false,
  //     builder: (context) {
  //       return AlertDialog(
  //         title: Icon(
  //           Icons.warning,
  //           size: 40.0,
  //           color: Colors.red,
  //         ),
  //         content: Text(
  //             '¿Está seguro que desea cerrar la conexión al establecimiento?'),
  //         actions: [
  //           TextButton(
  //             onPressed: () async {
  //               // timer?.cancel();
  //               prefs.timerHashCode = -1;
  //               await DBProvider.db.deleteMesas();
  //               // await DBProvider.db.deleteCategorias();
  //               // await DBProvider.db.deleteProductos();
  //               await DBProvider.db.borrarVendedor();
  //               await esquemasBloc.borrarEsquemas();
  //               // await DBProvider.db.borrarProductosGustos();
  //               // await DBProvider.db.borrarProductosEquivalencias();
  //               await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
  //               await ConexionTimerProvider.conexionTimer.cerrarConexion();

  //               List<SucursalModel> sucursales = await DBProvider.db.obtenerSucursales();

  //               Navigator.of(context).pushAndRemoveUntil(
  //                   MaterialPageRoute(settings: RouteSettings(arguments: {
  //                     "sucursales": sucursales
  //                   }) , 
  //                     builder: (context) => MenusPage(),
  //                   ),
  //                   (route) => false);
  //             },
  //             child: Text(
  //               'Si',
  //               style: TextStyle(fontSize: 18.0),
  //             ),
  //           ),
  //           TextButton(
  //             onPressed: () => Navigator.of(context).pop(false),
  //             child: Text(
  //               'Cancelar',
  //               style: TextStyle(fontSize: 18.0),
  //             ),
  //           )
  //         ],
  //       );
  //     },
  //   );
  // }
}
