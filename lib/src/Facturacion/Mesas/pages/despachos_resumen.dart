import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/facturas_pendientes_model.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import "package:collection/collection.dart";


class DespachosResumenPage extends StatefulWidget {
  DespachosResumenPage({Key key, this.facturas, this.vendedores}) : super(key: key);

  List<FacturaPendienteModel> facturas;
  List<VendedorModel> vendedores;

  @override
  _DespachosResumenPageState createState() => _DespachosResumenPageState();
}

class _DespachosResumenPageState extends State<DespachosResumenPage> {

  Map<dynamic, dynamic> facturasOrganizadas;

  final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);


  @override
  Widget build(BuildContext context) {  
    return Scaffold(
      appBar: AppBar(
        title: Text("Resumen"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(height: 10.0,),
          totales(),
          SizedBox(height: 10.0,),
          listadoVendedores(),
        ],
      )
    );
  }

  Widget totales(){
    int totalFacturas = widget.facturas != null ? widget.facturas.map((e) => e.totalapagar).reduce((value, element) => value+element) : 0;

    return Card(
      child: ListTile(
        title: Center(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("# Facturas pendientes: "),
                  Text(widget.facturas.length.toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                ],
              ),
              SizedBox(height: 5.0,),
              Text(formatCurrency.format(totalFacturas),style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0) )


            ],
          ),
        ),
      ),
    );
  }

  Widget listadoVendedores(){
    facturasOrganizadas = groupBy(widget.facturas, (FacturaPendienteModel obj) => obj.vendedor2id);


    return Expanded(
      child: ListView.builder(
        itemCount: widget.vendedores.length,
        itemBuilder: (context, index) {
          return _cardVendedores(index, facturasOrganizadas[widget.vendedores[index].vendedorid]);
        },
      ),
    );
  }

  Widget _cardVendedores(int index, List<FacturaPendienteModel> facturasVendedor){
    int totalVendedor = facturasVendedor != null ? facturasVendedor.map((e) => e.totalapagar).reduce((value, element) => value+element) : 0;
    String nombreCompleto = widget.vendedores[index].nombre.toString().trimLeft();
    String primerLetra = nombreCompleto.length > 0  ? nombreCompleto[0] : '';

    return facturasVendedor != null ? Padding(
      padding: const EdgeInsets.all(8.0),
      child: ExpansionPanelList(
        expandedHeaderPadding:  const EdgeInsets.all(8.0) ,
        expansionCallback: (panelIndex, isExpanded) {
          setState(() {
            widget.vendedores[index].expanded = !isExpanded;
          });
        },
        children: [
          ExpansionPanel(
            headerBuilder: (context, isExpanded) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundColor: Color((primerLetra.hashCode * 0xFFFFFF).toInt()).withOpacity(1.0),
                  child: Text(primerLetra, style: TextStyle(color: Colors.white),),
                  radius: 28.0, 
                ),
                title: Text(widget.vendedores[index].nombre),
                trailing: Text(formatCurrency.format(totalVendedor).toString(), style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
              );
            },
            body: facturasVendedor != null ? 
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: facturasVendedor.map((facturas) {
                  return Column(
                    children: [
                      ListTile(
                        title: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(facturas.prefijo+" - "+facturas.numero.toString()),
                            Text(facturas.fecha.toLocal().toString()),
                            Text(formatCurrency.format(facturas.totalapagar).toString())
                          ],
                        ),
                        tileColor: Colors.grey[100],
                      ),
                      Divider()
                    ],
                  );
                }).toList(),
              ) : Text("Sin facturas asignadas", style: TextStyle(color: Colors.red)),
            canTapOnHeader: true,
            isExpanded: widget.vendedores[index].expanded
          )
        ],
      ),
    ) : SizedBox();

  }
}