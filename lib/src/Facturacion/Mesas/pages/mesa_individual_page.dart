import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/pages/comanda_pagar_cuenta_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesas_page.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:date_time_picker/date_time_picker.dart';

import 'package:maxxshop/src/Utils/timer_productos.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/Facturacion/Comanda/pages/comanda_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/pages/categorias_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesa_informacion_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';

// ignore: must_be_immutable
class MesaIndividualPage extends StatefulWidget {
  @override
  _MesaIndividualPageState createState() => _MesaIndividualPageState();
}

class _MesaIndividualPageState extends State<MesaIndividualPage>
    with SingleTickerProviderStateMixin {
  final _textController = new TextEditingController();
  TimerConsultaProductos timerProductos =
      TimerConsultaProductos.timerConsultaProductos;

  void update() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _textController?.dispose();
    // timerProductos.detenerTimer;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // timerProductos.iniciarTimer;

    return _crearTabsProductosCuenta();
  }

  Widget _crearTabsProductosCuenta() {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    return DefaultTabController(
      length: 3,
      initialIndex: cabeceraProvider.mesaCabecera.totalapagar > 0 ? 1 : 0,
      child: Scaffold(
        appBar: AppBar(
          title: _tituloCabeceraConTotal(),
          centerTitle: true,
          bottom: _crearTabBar(),
          actions: [
            IconButton(
              onPressed: () {
                dialogOpcionesMesa(context);
              },
              icon: Icon(
                Icons.info,
                size: 32.0,
              ),
            ),
          ],
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            CategoriasPage(),
            ComandaPage(
              callback: update,
            ),
            ComandaPagarCuentaPage()
          ],
        ),
      ),
    );
  }

  Future<void> dialogOpcionesMesa(BuildContext context) {
    initializeDateFormatting();
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    PostgresFunctions postgres = PostgresFunctions();

    return showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(
                "Opciones de la mesa",
                style: TextStyle(fontSize: 23.0),
              ),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: DateTimePicker(
                    initialValue:
                        cabeceraProvider.mesaCabecera.fechahoravencimiento ??
                            DateTime.now().toString(),
                    firstDate: DateTime(2021),
                    lastDate: DateTime(2100),
                    dateLabelText: "Fecha de entrega",
                    timeLabelText: "Hora",
                    type: DateTimePickerType.dateTime,
                    calendarTitle: "Selecciona fecha de entrega",
                    style: TextStyle(fontSize: 20.0),
                    use24HourFormat: false,
                    onChanged: (value) async {
                      await postgres.editarFechaHoraVencimientoCuenta(
                          cabeceraProvider, value);
                      cabeceraProvider.mesaCabecera.fechahoravencimiento =
                          value;
                    },
                  ),
                ),
                SimpleDialogOption(
                  child: TextButton(
                    child: Text("Información de la cuenta"),
                    onPressed: () async {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MesaInformacionPage()));
                    },
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue[300],
                      textStyle: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SimpleDialogOption(
                  child: TextButton(
                    child: Text("Editar alias de la cuenta"),
                    onPressed: () async {
                      Navigator.pop(context);
                      await dialogAdicionarCuenta(context);
                    },
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue[300],
                      textStyle: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SimpleDialogOption(
                  child: TextButton(
                    child: Text("Anular cuenta"),
                    onPressed: () async {
                      Navigator.pop(context);
                      await dialogAdicionarCuenta(context);
                    },
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue[300],
                      textStyle: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ));
  }

  Future<Widget> dialogAdicionarCuenta(BuildContext context) async {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    PostgresFunctions postFunctions = PostgresFunctions();

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Editar alias de la cuenta'),
          content: _formNuevaCuenta(context, cabeceraProvider),
          actions: [
            TextButton(
              child: Text(
                'Actualizar',
                style: TextStyle(fontSize: 20.0),
              ),
              onPressed: () async {
                await postFunctions.editarAliasCuentaCabecera(
                    cabeceraProvider, _textController.text);
                cabeceraProvider.mesaCabecera.nombreCuenta =
                    _textController.text;

                update();
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text(
                "Cancelar",
                style: TextStyle(color: Colors.red, fontSize: 20.0),
              ),
              onPressed: () {
                _textController.text = "";
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Future<Widget> dialogAnularCuenta(BuildContext context) async {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    PostgresFunctions postFunctions = PostgresFunctions();

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('¿Seguro quiere anular la cuenta?'),
          actions: [
            TextButton(
              child: Text(
                'Anular',
                style: TextStyle(fontSize: 20.0, color: Colors.red),
              ),
              onPressed: () async {
                await postFunctions.anularCuentaCabecera(
                    cabeceraProvider);

                Navigator.push(context, MaterialPageRoute(builder: (context) => MesasPage()));                
              },
            ),
            TextButton(
              child: Text(
                "Cancelar",
                style: TextStyle(color: Colors.red, fontSize: 20.0),
              ),
              onPressed: () {
                _textController.text = "";
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Widget _formNuevaCuenta(
      BuildContext context, CabeceraService cabeceraProvider) {
    final size = MediaQuery.of(context).size;

    _textController.clear();

    return Container(
      padding: EdgeInsets.symmetric(vertical: size.width * 0.1),
      child: TextField(
        controller: _textController,
        autofocus: true,
        textAlign: TextAlign.center,
        autocorrect: false,
        enableSuggestions: false,
        decoration: InputDecoration(
          helperText: 'Alias de cuenta',
          helperStyle: TextStyle(fontSize: 16.0),
          icon: Icon(
            Icons.text_fields,
            size: 35.0,
            color: Color(0xff2B061E),
          ),
        ),
      ),
    );
  }

  Widget _tituloCabeceraConTotal() {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    return Column(
      children: [
        Text(cabeceraProvider.mesaCabecera.nombreMesa +
            ' - ' +
            cabeceraProvider.mesaCabecera.nombreCuenta),
        Text(cabeceraProvider.mesaCabecera.nombreCliente)
      ],
    );
  }

  Widget _crearTabBar() {
    return TabBar(
      indicatorColor: Color(0xffDD6E42),
      tabs: [
        Tab(
          icon: Icon(Icons.shopping_bag),
          text: 'Productos',
        ),
        Tab(
          icon: Icon(Icons.monetization_on),
          text: 'Cuenta',
        ),
        Tab(
          icon: Icon(Icons.money),
          text: 'Pagar',
        )
      ],
    );
  }
}
