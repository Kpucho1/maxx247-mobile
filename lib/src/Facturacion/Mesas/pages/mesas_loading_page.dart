import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/provider.dart';

import 'package:maxxshop/src/Home/pages/home_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesas_page.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';

import 'package:maxxshop/src/Conexiones/blocs/conexiones_bloc.dart';

// ignore: must_be_immutable
class MesasLoadingPage extends StatefulWidget {
  @override
  _MesasLoadingPageState createState() => _MesasLoadingPageState();
}

class _MesasLoadingPageState extends State<MesasLoadingPage> {
  final conexionesBloc = new ConexionesBloc();

  @override
  void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _cargarInfoNegocio(context);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Image(
          image: AssetImage('assets/jar_loading.gif'),
        ),
      ),
    );
  }

  Future<void> _cargarInfoNegocio(BuildContext context) async {
    final esquemasBloc = EsquemasBloc();
    final blocCategorias = ProviderInherited.categoriasBloc(context);
    final blocProductos = ProviderInherited.productosBloc(context);
    final blocProductosGustos = ProviderInherited.productosGustosBloc(context);

    try {  

      final postFunctions = new PostgresFunctions();

      await postFunctions.schemasAndTables(blocCategorias, blocProductos, blocProductosGustos, esquemasBloc);

      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => MesasPage(),
        ),
        (_) => false,
      );
    } catch (e) {
      print(e);
      //print('de aca viene el error');
      _alertaSinConexion(context);
      conexionesBloc.borrarConexionActiva();
    }
  }

  Future _alertaSinConexion(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          'Error al intentar conectar. Por favor intenta de nuevo.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(),
                ),
                (route) => false),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}
