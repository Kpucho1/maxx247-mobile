import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/facturas_pendientes_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/despachos_detalle_factura.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/despachos_resumen.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:intl/intl.dart';

class DespachosPage extends StatefulWidget {
  @override
  _DespachosPageState createState() => _DespachosPageState();
}

class _DespachosPageState extends State<DespachosPage> {
  var _resultados;

  List<String> _tempSelectedFacturas = [];

  int _selectedIndex;

  int _cantidadFacturas = 0;

  List resultadosCopia = [];

  bool filtroFacturasSinAsignar =
      false; //false = filtro desactivado ; true = filtro activado

  @override
  void initState() {
    super.initState();
    _resultados = _consultarBD();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(_cantidadFacturas.toString() + ' facturas por despachar'),
        actions: [
          IconButton(
            icon: Icon(Icons.assignment),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DespachosResumenPage(
                          facturas: resultadosCopia[0],
                          vendedores: resultadosCopia[1])));
            },
          ),
          IconButton(
            icon: Icon(Icons.filter_list_alt),
            onPressed: () {
              setState(() {
                filtroFacturasSinAsignar =
                    filtroFacturasSinAsignar ? false : true;
                _resultados = _consultarBD();
              });
            },
          )
        ],
      ),
      body: _constructorDeSecciones(),
    );
  }

  Widget _constructorDeSecciones() {
    final size = MediaQuery.of(context).size;

    return Row(
      children: [
        Container(
          width: size.width * 0.6,
          child: _seccionFacturas(),
        ),
        Container(
          width: 1.0,
          color: Colors.grey,
        ),
        Expanded(
          child: _seccionVendedores(),
        ),
      ],
    );
  }

  Widget _seccionFacturas() {
    final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);

    return FutureBuilder(
        future: _resultados,
        builder: (context, AsyncSnapshot<List> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          final listaFacturas = snapshot.data;

          resultadosCopia = listaFacturas;

          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            physics: BouncingScrollPhysics(),
            itemCount: listaFacturas[0].length,
            itemBuilder: (context, index) {
              String prefijoNumero =
                  listaFacturas[0][index].prefijo.toString() +
                      "-" +
                      listaFacturas[0][index].numero.toString();
              List nombreVendedor =
                  listaFacturas[0][index].nombreVendedor2.split(RegExp("\\s+"));
              return CheckboxListTile(
                title: Row(children: [
                  Text(
                    prefijoNumero,
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Text("(" + nombreVendedor[0].toString() + ")",
                      style: TextStyle(fontSize: 16.0))
                ]),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    listaFacturas[0][index].nombreCliente.toString() != "null"
                        ? Text(listaFacturas[0][index].nombreCliente.toString(),
                            style: TextStyle(fontSize: 16.0))
                        : Text("Sin cliente",
                            style: TextStyle(
                              color: Colors.red,
                            )),
                    listaFacturas[0][index].direccionCliente.toString() !=
                            "null"
                        ? Text(
                            listaFacturas[0][index].direccionCliente.toString(),
                            style: TextStyle(fontSize: 16.0))
                        : Text("Sin direccion",
                            style: TextStyle(color: Colors.red)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(formatCurrency
                        .format(listaFacturas[0][index].totalapagar)),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text("Alias: " +
                        listaFacturas[0][index].nombreCuenta.toString()),
                    IconButton(
                      icon: Icon(
                        Icons.info,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetalleFacturaPage(
                                  factura: listaFacturas[0][index]),
                            ));
                      },
                    )
                  ],
                ),
                value: _tempSelectedFacturas.contains(prefijoNumero),
                checkColor: Colors.white,
                activeColor: Colors.blue,
                onChanged: (bool value) {
                  if (value) {
                    if (!_tempSelectedFacturas.contains(prefijoNumero)) {
                      if (listaFacturas[0][index].vendedor2id == 0 ||
                          listaFacturas[0][index].vendedor2id ==
                              listaFacturas[1][_selectedIndex].vendedorid) {
                        setState(() {
                          _tempSelectedFacturas.add(prefijoNumero);
                        });
                      }
                    }
                  } else {
                    if (_tempSelectedFacturas.contains(prefijoNumero)) {
                      setState(() {
                        _tempSelectedFacturas.removeWhere(
                            (String factura) => factura == prefijoNumero);
                      });
                    }
                  }
                },
              );
            },
          );
        });
  }

  Widget _seccionVendedores() {
    final formatCurrency = NumberFormat.simpleCurrency(decimalDigits: 0);

    return FutureBuilder(
        future: _resultados,
        builder: (context, AsyncSnapshot<List> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          final listaVendedores = snapshot.data;

          return Column(
            children: [
              Expanded(
                child: Container(
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.black,
                    ),
                    physics: BouncingScrollPhysics(),
                    itemCount: listaVendedores[1].length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(listaVendedores[1][index].nombre),
                        leading: Text("(" +
                            listaVendedores[1][index]
                                .numeroFacturasPendientes
                                .toString() +
                            ")"),
                        subtitle: Text(formatCurrency
                            .format(
                                listaVendedores[1][index].pendientesTotalaPagar)
                            .toString()),
                        selected: index == _selectedIndex,
                        selectedTileColor: Colors.grey[300],
                        onTap: () {
                          List<String> temp = [];
                          int newIndex = index != _selectedIndex ? index : -1;

                          if (listaVendedores[1][index]
                                      .numeroFacturasPendientes >
                                  0 &&
                              newIndex != -1) {
                            for (var factura in listaVendedores[0]) {
                              if (factura.vendedor2id ==
                                  listaVendedores[1][index].vendedorid) {
                                temp.add(factura.prefijo.toString() +
                                    "-" +
                                    factura.numero.toString());
                              }
                            }
                          }

                          setState(() {
                            _tempSelectedFacturas = newIndex != -1 ? temp : [];
                            _selectedIndex = newIndex;
                          });
                        },
                      );
                    },
                  ),
                ),
              ),
              GestureDetector(
                onTap: () async {
                  await _asignarFacturas(_selectedIndex, listaVendedores);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 70.0,
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.only(top: 22.0),
                      child: Text(
                        "Asignar",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Colors.orange),
                  ),
                ),
              ),
            ],
          );
        });
  }

  Future<void> _asignarFacturas(int vendedorIndex, List listasGeneral) async {
    final conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.transaction((cnt) async {
        if (!filtroFacturasSinAsignar) {
          await cnt.query(
              "UPDATE datos.facturacabecera SET fposvendedor2id = 0 WHERE fposvendedor2id = @vendedorid",
              substitutionValues: {
                "vendedorid": listasGeneral[1][vendedorIndex].vendedorid,
              });
        }

        for (var factura in _tempSelectedFacturas) {
          List prefijoNumero = factura.split("-");
          await cnt.query(
              "UPDATE datos.facturacabecera SET fposvendedor2id = @vendedorid WHERE fposprefijo = @prefijo AND fposnumero = @numero",
              substitutionValues: {
                "vendedorid": listasGeneral[1][vendedorIndex].vendedorid,
                "prefijo": prefijoNumero[0],
                "numero": prefijoNumero[1]
              });
        }
      });

      setState(() {
        _selectedIndex = -1;
        _tempSelectedFacturas = [];
        _resultados = _consultarBD();
      });

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } catch (e) {
      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    }
  }

  Future<List> _consultarBD() async {
    final conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    List resultadosGenerales = [];

    List<VendedorModel> listadoVendedores = [];

    List<FacturaPendienteModel> listaFacturasPendientes = [];

    List<List<dynamic>> resultadoFacturas;

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> resultadoVendedores = await conexionNegocio.query(
          "SELECT V.vendedorid, V.nombre, COUNT(F.fposvendedorid), CAST(COALESCE(SUM(F.fpostotalpagar), 0) AS INTEGER) FROM datos.vendedor V LEFT JOIN datos.facturacion_pendiente F ON V.vendedorid = F.fposvendedor2id WHERE V.hacedomicilio = 1 GROUP BY V.vendedorid, V.nombre ORDER BY V.nombre;");

      for (var item in resultadoVendedores) {
        final vendedor = VendedorModel();

        vendedor.vendedorid = item[0];
        vendedor.nombre = item[1];
        vendedor.numeroFacturasPendientes = item[2];
        vendedor.pendientesTotalaPagar = item[3];
        vendedor.expanded = true;

        listadoVendedores.add(vendedor);
      }

      if (filtroFacturasSinAsignar) {
        resultadoFacturas = await conexionNegocio.query(
            "SELECT F.fposprefijo, F.fposnumero, F.fposfecha, F.fposvendedorid, F.fposnomcli, F.fposdirecc, CAST(F.fpostotalpagar AS INTEGER), F.fposvendedor2id, COALESCE(V.nombre, ''), fposplaca FROM datos.facturacion_pendiente F LEFT JOIN datos.vendedor V ON F.fposvendedor2id = V.vendedorid  WHERE fposvendedor2id = 0 ORDER BY F.id DESC;");
      } else {
        resultadoFacturas = await conexionNegocio.query(
            "SELECT F.fposprefijo, F.fposnumero, F.fposfecha, F.fposvendedorid, F.fposnomcli, F.fposdirecc, CAST(F.fpostotalpagar AS INTEGER), F.fposvendedor2id, COALESCE(V.nombre, ''), fposplaca FROM datos.facturacion_pendiente F LEFT JOIN datos.vendedor V ON F.fposvendedor2id = V.vendedorid ORDER BY F.id DESC;");
      }

      for (var factura in resultadoFacturas) {
        final facturaPendiente = FacturaPendienteModel();

        facturaPendiente.prefijo = factura[0];
        facturaPendiente.numero = factura[1];
        facturaPendiente.fecha = factura[2];
        facturaPendiente.vendedorid = factura[3];
        facturaPendiente.nombreCliente = factura[4];
        facturaPendiente.direccionCliente = factura[5];
        facturaPendiente.totalapagar = factura[6];
        facturaPendiente.vendedor2id = factura[7];
        facturaPendiente.nombreVendedor2 = factura[8];
        facturaPendiente.nombreCuenta = factura[9];

        listaFacturasPendientes.add(facturaPendiente);
      }

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      resultadosGenerales.add(listaFacturasPendientes);
      resultadosGenerales.add(listadoVendedores);

      setState(() {
        _cantidadFacturas = listaFacturasPendientes.length;
      });

      return resultadosGenerales;
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }
  }
}
