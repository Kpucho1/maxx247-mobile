import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';

class MesaInformacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(cabeceraProvider.mesaCabecera.nombreMesa),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: consultarUltimoPedido(cabeceraProvider),
        builder: (context, AsyncSnapshot<String> snapshot) {
          
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          final data = snapshot.data;

          return Center(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    top: size.width * 0.01,
                    right: size.width * 0.1,
                    left: size.width * 0.1,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    shadowColor: Colors.black87,
                    elevation: 8.0,
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.timelapse),
                          title: Text(
                            "FECHA/HORA APERTURA CUENTA",
                            style: TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(
                            cabeceraProvider.mesaCabecera.ts.toString(),
                            style: TextStyle(fontSize: 16.0),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: size.width * 0.01,
                    right: size.width * 0.1,
                    left: size.width * 0.1,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    shadowColor: Colors.black87,
                    elevation: 8.0,
                    child: Column(
                      children: [
                        ListTile(
                            leading: Icon(Icons.timelapse),
                            title: Text(
                              "TIEMPO EN MINUTOS",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(data))
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: size.width * 0.01,
                    right: size.width * 0.1,
                    left: size.width * 0.1,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    shadowColor: Colors.black87,
                    elevation: 8.0,
                    child: Column(
                      children: [
                        ListTile(
                            leading: Icon(Icons.timelapse),
                            title: Text(
                              "FECHA/HORA ULTIMO PEDIDO",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(data))
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: size.width * 0.01,
                    right: size.width * 0.1,
                    left: size.width * 0.1,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    shadowColor: Colors.black87,
                    elevation: 8.0,
                    child: Column(
                      children: [
                        ListTile(
                            leading: Icon(Icons.timelapse),
                            title: Text(
                              "MINUTOS ULTIMO PEDIDO",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            subtitle: Text(data))
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: size.width * 0.01,
                    right: size.width * 0.1,
                    left: size.width * 0.1,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    shadowColor: Colors.black87,
                    elevation: 8.0,
                    child: Column(
                      children: [
                        ListTile(
                          leading: Icon(Icons.timelapse),
                          title: Text(
                            "PREFIJO Y NUMERO CUENTA",
                            style: TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.bold),
                          ),
                          subtitle: Text(cabeceraProvider.mesaCabecera.prefijocuenta
                                  .toString() +
                              cabeceraProvider.mesaCabecera.numerocuenta
                                  .toString()),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }
      ),
    );
  }

  Text tiempoMinutos(CabeceraService cabeceraProvider) {
    final fechaHoy = DateTime.now();
    DateTime fechaApertura =
    DateTime.parse(cabeceraProvider.mesaCabecera.ts.toString());
    return Text(fechaHoy.difference(fechaApertura).toString(),
        style: TextStyle(fontSize: 16.0));
  }

  Future<String> consultarUltimoPedido(CabeceraService cabeceraProvider) async {
    try {
      final ConexionesModel conexionActiva =
          await DBProvider.db.getConexionActiva();
          
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );
      
      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();
      
      List<List<dynamic>> result = await conexionNegocio.query(
          "SELECT ts FROM datos.cuentadetalle WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta ORDER BY id DESC LIMIT 1",
          substitutionValues: {
            "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
            "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta
          });
      
      await conexionNegocio.close();
      
      return result[0][0].toString();
    }catch (e) {
      print(e);
    }
  }
}
