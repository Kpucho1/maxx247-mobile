import 'dart:async';
import 'package:rxdart/subjects.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';

class MesasRefreshBloc {
  static final MesasRefreshBloc _singletonMesasRefresh =
      new MesasRefreshBloc._internal();

  factory MesasRefreshBloc() {
    return _singletonMesasRefresh;
  }

  MesasRefreshBloc._internal() {
    // obtenerTodasMesas();
  }

  final _mesasRefreshController = StreamController<List<MesaModel>>.broadcast();
  final _mesasPorEsquemaController =
      StreamController<List<MesaModel>>.broadcast();
  final _precioTotalCabeceraController = BehaviorSubject<double>();

  //Streams que recuperan la info
  Stream<List<MesaModel>> get mesasRefreshStream =>
      _mesasRefreshController.stream;

  Stream<List<MesaModel>> get mesasPorEsquemaStream =>
      _mesasPorEsquemaController.stream;

  Stream<double> get precioTotalCabecera =>
      _precioTotalCabeceraController.stream;

  double get totalCabecera => _precioTotalCabeceraController.value;

  dispose() {
    _mesasRefreshController?.close();
    _mesasPorEsquemaController?.close();
    _precioTotalCabeceraController?.close();
  }

  obtenerTodasMesas() async {
    _mesasRefreshController.sink.add(await DBProvider.db.getTodasMesas());
  }

  agregarMesas(MesaModel nuevaMesa) async {
    await DBProvider.db.nuevaMesa(nuevaMesa);
  }

  borrarMesas() async {
    await DBProvider.db.deleteMesas();

    obtenerTodasMesas();
  }

  //Obtención de mesas por esquemas
  obtenerMesasPorEsquema(int esquemaId) async {
    _mesasPorEsquemaController.sink
        .add(await DBProvider.db.obtenerMesaPorEsquemaId(esquemaId));
  }

  obtenerMesasPorEsquemaVendedor(int esquemaid) async{
    _mesasPorEsquemaController.sink.add(await DBProvider.db.obtenerMesaPorEsquemaVendedor(esquemaid));
  }

  borrarMesasSobrantes(int idPostgres) async {
    await DBProvider.db.deleteMesasSobrantes(idPostgres);

    obtenerTodasMesas();
  }

  actualizarMesas(MesaModel mesa, int objetoId) async {
    await DBProvider.db.actualizaMesas(mesa, objetoId);

    // obtenerTodasMesas();
  }

  actualizarEstadoMesa(int estado, int objetoid, int esquemaId) async {
    await DBProvider.db.actualizarEstadoMesa(estado, objetoid);

    obtenerMesasPorEsquema(esquemaId);
  }

  //Actualizar precios en la cabecera

  actualizarPrecioCabecera(double totalCabecera) {
    _precioTotalCabeceraController.sink.add(totalCabecera);
  }
}
