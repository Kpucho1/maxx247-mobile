import 'dart:async';
import 'package:rxdart/rxdart.dart';

class MesasBloc {
  /*List<String> estadosMesas = [
    'Vacia/Limpia',
    'Ocupada sin pedido',
    'Ocupada con pedido',
    'Petición de cuenta',
    'Ocupada y pagada',
    'Vacia/Sucia'
  ];*/

  final _mesasEstadoController = BehaviorSubject<int>();

  //Escucha y recuperación de datos
  Stream<int> get mesasEstadoStream => _mesasEstadoController.stream;

  //Inserción de datos
  Function(int) get changeEstadoMesa => _mesasEstadoController.sink.add;

  //obtencion del valor emitido en el stream
  int get estadoMesa => _mesasEstadoController.value;

  dispose() {
    _mesasEstadoController?.close();
  }
}
