import 'package:rxdart/rxdart.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';

class EsquemasBloc {
  static final EsquemasBloc _singletonEsquemas = new EsquemasBloc._internal();

  factory EsquemasBloc() {
    return _singletonEsquemas;
  }

  EsquemasBloc._internal() {
    obtenerEsquemas();
  }

  final _esquemaController = BehaviorSubject<List<EsquemaModel>>();
  final _esquemaActivoController = BehaviorSubject<int>();
  final _esquemaPorIdController = BehaviorSubject<EsquemaModel>();

  Stream<List<EsquemaModel>> get esquemasStream => _esquemaController.stream;
  Stream<int> get esquemaActivoStream => _esquemaActivoController.stream;
  Stream<EsquemaModel> get esquemaPorIdStream => _esquemaPorIdController.stream;

  obtenerEsquemas() async {
    _esquemaController.sink.add(await DBProvider.db.obtenerEsquemas());
  }

  agregarEsquema(EsquemaModel nuevoEsquema) async {
    await DBProvider.db.agregarEsquema(nuevoEsquema);
    print(nuevoEsquema);
  }

  obtenerEsquemaActivo(int esquemaId) async {
    _esquemaActivoController.sink.add(esquemaId);
    _esquemaPorIdController.sink
        .add(await DBProvider.db.obtenerEsquemaPorId(esquemaId));
  }

  borrarEsquemas() async {
    await DBProvider.db.borrarEsquemas();
  }

  List<EsquemaModel> get listaEsquemas => _esquemaController.value;
  int get esquemaActivo => _esquemaActivoController.value;
  EsquemaModel get esquemaPorId => _esquemaPorIdController.value;

  dispose() {
    _esquemaController?.close();
    _esquemaActivoController?.close();
    _esquemaPorIdController?.close();
  }
}
