import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';

class MesasProvider extends ChangeNotifier {
  List<MesaModel> _listadoMesas = [];

  UnmodifiableListView<MesaModel> get listadoMesas =>
      UnmodifiableListView(_listadoMesas);

  void agregarMesas(MesaModel mesa) {
    _listadoMesas.add(mesa);
    notifyListeners();
  }

  void removerMesas() {
    _listadoMesas.clear();
    notifyListeners();
  }
}
