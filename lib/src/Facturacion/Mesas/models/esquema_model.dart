import 'dart:convert';

EsquemaModel esquemaModelFromJson(String str) =>
    EsquemaModel.fromJson(json.decode(str));

String esquemaModelToJson(EsquemaModel data) => json.encode(data.toJson());

class EsquemaModel {
  EsquemaModel({
    this.id,
    this.esquemaid,
    this.nombre,
    this.colorvacialimpiafondo,
    this.colorvacialimpiatexto,
    this.colorocupadasinpedidofondo,
    this.colorocupadasinpedidotexto,
    this.colorocupadaconpedidotexto,
    this.colorocupadaconpedidofondo,
    this.colorsuciafondo,
    this.colorsuciatexto,
    this.colorpendienteporpagarfondo,
    this.colorpendienteporpagartexto,
    this.colorreparacionfondo,
    this.colorreaparaciontexto,
    this.colordisponiblefondo,
    this.colordisponbiletexto,
    this.colorocupadafondo,
    this.colorocupadatexto,
    this.colorantesdevencersefondo,
    this.colorantesdevencersetexto
  });

  int id;
  int esquemaid;
  String nombre;
  String colorvacialimpiafondo;
  String colorvacialimpiatexto;
  String colorocupadasinpedidofondo;
  String colorocupadasinpedidotexto;
  String colorocupadaconpedidotexto;
  String colorocupadaconpedidofondo;
  String colorsuciafondo;
  String colorsuciatexto;
  String colorpendienteporpagarfondo;
  String colorpendienteporpagartexto;
  String colorreparacionfondo;
  String colorreaparaciontexto;
  String colordisponiblefondo;
  String colordisponbiletexto;
  String colorocupadafondo;
  String colorocupadatexto;
  String colorantesdevencersefondo;
  String colorantesdevencersetexto;

  factory EsquemaModel.fromJson(Map<String, dynamic> json) => EsquemaModel(
        id: json["id"],
        esquemaid: json["esquemaid"],
        nombre: json["nombre"],
        colorvacialimpiafondo: json["colorvacialimpiafondo"],
        colorvacialimpiatexto: json["colorvacialimpiatexto"],
        colorocupadasinpedidofondo: json["colorocupadasinpedidofondo"],
        colorocupadasinpedidotexto: json["colorocupadasinpedidotexto"],
        colorocupadaconpedidotexto: json["colorocupadaconpedidotexto"],
        colorocupadaconpedidofondo: json["colorocupadaconpedidofondo"],
        colorsuciafondo: json["colorsuciafondo"],
        colorsuciatexto: json["colorsuciatexto"],
        colorpendienteporpagarfondo: json["colorpendienteporpagarfondo"],
        colorpendienteporpagartexto: json["colorpendienteporpagartexto"],
        colorreparacionfondo: json["colorreparacionfondo"],
        colorreaparaciontexto: json["colorreaparaciontexto"],
        colordisponiblefondo: json["colordisponiblefondo"],
        colordisponbiletexto: json["colordisponbiletexto"],
        colorocupadafondo: json["colorocupadafondo"],
        colorocupadatexto: json["colorocupadatexto"],
        colorantesdevencersefondo: json["colorantesdevencersefondo"],
        colorantesdevencersetexto: json["colorantesdevencersetexto"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "esquemaid": esquemaid,
        "nombre": nombre,
        "colorvacialimpiafondo": colorvacialimpiafondo,
        "colorvacialimpiatexto": colorvacialimpiatexto,
        "colorocupadasinpedidofondo": colorocupadasinpedidofondo,
        "colorocupadasinpedidotexto": colorocupadasinpedidotexto,
        "colorocupadaconpedidotexto": colorocupadaconpedidotexto,
        "colorocupadaconpedidofondo": colorocupadaconpedidofondo,
        "colorsuciafondo": colorsuciafondo,
        "colorsuciatexto": colorsuciatexto,
        "colorpendienteporpagarfondo": colorpendienteporpagarfondo,
        "colorpendienteporpagartexto": colorpendienteporpagartexto,
        "colorreparacionfondo": colorreparacionfondo,
        "colorreaparaciontexto": colorreaparaciontexto,
        "colordisponiblefondo": colordisponiblefondo,
        "colordisponbiletexto": colordisponbiletexto,
        "colorocupadafondo": colorocupadafondo,
        "colorocupadatexto": colorocupadatexto,
        "colorantesdevencersefondo": colorantesdevencersefondo,
        "colorantesdevencersetexto": colorantesdevencersetexto,
      };
}
