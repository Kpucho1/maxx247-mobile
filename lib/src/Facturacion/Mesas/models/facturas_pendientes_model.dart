import 'dart:convert';

FacturaPendienteModel facturaPendienteModelFromJson(String str) =>
    FacturaPendienteModel.fromJson(json.decode(str));

String facturaPendienteModelToJson(FacturaPendienteModel data) =>
    json.encode(data.toJson());

class FacturaPendienteModel {
  FacturaPendienteModel({
    this.prefijo,
    this.numero,
    this.fecha,
    this.vendedorid,
    this.nombreCliente,
    this.direccionCliente,
    this.totalapagar,
    this.vendedor2id,
    this.nombreVendedor2,
    this.nombreCuenta
  });

  String prefijo;
  int numero;
  DateTime fecha;
  int vendedorid;
  String nombreCliente;
  String direccionCliente;
  int totalapagar;
  int vendedor2id;
  String nombreVendedor2;
  String nombreCuenta;

  factory FacturaPendienteModel.fromJson(Map<String, dynamic> json) =>
      FacturaPendienteModel(
        prefijo: json["prefijo"],
        numero: json["numero"],
        fecha: json["fecha"],
        vendedorid: json["vendedorid"],
        nombreCliente: json["nombreCliente"],
        direccionCliente: json["direccionCliente"],
        totalapagar: json["totalapagar"]
      );

  Map<String, dynamic> toJson() => {
        "prefijo": prefijo,
        "numero": numero,
        "fecha": fecha,
        "vendedorid": vendedorid,
        "nombreCliente": nombreCliente,
        "direccionCliente": direccionCliente,
        "totalapagar": totalapagar
      };
}
