import 'dart:convert';

MesaModel mesaModelFromJson(String str) => MesaModel.fromJson(json.decode(str));

String mesaModelToJson(MesaModel data) => json.encode(data.toJson());

class MesaModel {
  MesaModel(
      {this.id,
      this.nombre,
      this.objetoid,
      this.estado,
      this.consumido,
      this.esquemaid,
      this.numeroCuentas,
      this.nombreVendedor,
      this.vendedorid

  });

  int id;
  String nombre;
  int objetoid;
  int estado = 1;
  dynamic consumido = 0;
  int esquemaid = 0;
  int numeroCuentas = 0;
  String nombreVendedor;
  int vendedorid;

  factory MesaModel.fromJson(Map<String, dynamic> json) => MesaModel(
      id: json["id"],
      nombre: json["nombre"],
      objetoid: json["objetoid"],
      estado: json["estado"],
      consumido: json["consumido"],
      esquemaid: json["esquemaid"],
      numeroCuentas: json["numeroCuentas"],
      nombreVendedor: json["nombreVendedor"],
      vendedorid: json["vendedorid"]

  );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "objetoid": objetoid,
        "estado": estado,
        "consumido": consumido,
        "esquemaid": esquemaid,
        "numeroCuentas": numeroCuentas,
        "nombreVendedor": nombreVendedor,
        "vendedorid": vendedorid,
      };
}
