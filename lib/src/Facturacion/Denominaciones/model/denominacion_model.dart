// To parse this JSON data, do
//
//     final denominacionModel = denominacionModelFromJson(jsonString);

import 'dart:convert';

DenominacionModel denominacionModelFromJson(String str) => DenominacionModel.fromJson(json.decode(str));

String denominacionModelToJson(DenominacionModel data) => json.encode(data.toJson());

class DenominacionModel {
    DenominacionModel({
        this.id,
        this.valor,
    });

    int id;
    int valor;

    factory DenominacionModel.fromJson(Map<String, dynamic> json) => DenominacionModel(
        id: json["id"],
        valor: json["valor"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "valor": valor,
    };
}
