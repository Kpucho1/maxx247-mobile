import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Denominaciones/model/denominacion_model.dart';
import 'package:maxxshop/src/Facturacion/Denominaciones/services/denominacion_postgres_service.dart';

class DenominacionesPage extends StatefulWidget {
  DenominacionesPage({Key key}) : super(key: key);

  @override
  State<DenominacionesPage> createState() => _DenominacionesPageState();
}

class _DenominacionesPageState extends State<DenominacionesPage> {
  TextEditingController _nuevaDenominacionController = TextEditingController();
  DenominacionPostgresService denominacionPostgresService = DenominacionPostgresService();

  @override
  void dispose() {
    _nuevaDenominacionController?.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Denominaciones"),
        centerTitle: true,
      ),
      body: _bodyDenominacion(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          await _dialogAgregarDenominacion(context);
        },
      ),
    );
  }

  Widget _bodyDenominacion(){

    return FutureBuilder(
      future: denominacionPostgresService.obtenerDenominaciones(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          List<DenominacionModel> listadoDenominaciones = snapshot.data;
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: listadoDenominaciones.length,
            itemBuilder: (context, index) {
              return Card(
                child: ListTile(
                  title: Text(listadoDenominaciones[index].valor.toString()),
                  trailing: IconButton(
                    icon: Icon(Icons.delete, color: Colors.red,),
                    onPressed: () async{
                      await denominacionPostgresService.eliminarDenominaciones(listadoDenominaciones[index].id);
                      setState(() {
                        
                      });
                    },
                  ),
                ), 
              );  
            },
          );
        }
      },
    );
  }

  Future<Widget> _dialogAgregarDenominacion(BuildContext context){
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: TextFormField(
            controller: _nuevaDenominacionController,
          ),
          actions: [
            TextButton(
              child: Text("Agregar"),
              onPressed: () async{
                DenominacionModel denominacion = DenominacionModel(
                  valor: int.tryParse(_nuevaDenominacionController.text)
                );

                await denominacionPostgresService.crearDenominaciones(denominacion);
                Navigator.pop(context);
                setState(() {
                  
                });
              },
            ),
            TextButton(
              child: Text("Cancelar"),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}