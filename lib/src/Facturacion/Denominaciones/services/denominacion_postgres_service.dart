import 'package:maxxshop/src/Facturacion/Denominaciones/model/denominacion_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class DenominacionPostgresService {
  DenominacionPostgresService._privateConstructor();

  static final DenominacionPostgresService _instancia =
      DenominacionPostgresService._privateConstructor();

  factory DenominacionPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<DenominacionModel>> obtenerDenominaciones() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    List<DenominacionModel> listadoDenominaciones = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT id, CAST(valor AS INTEGER) FROM datos.denominacion ORDER BY valor ASC");

      for (var item in _data) {
        DenominacionModel denominacionModel = DenominacionModel(
          id: item[0],
          valor: item[1],
        );

        listadoDenominaciones.add(denominacionModel);
      }
    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

    return listadoDenominaciones;
  }

  Future<void> crearDenominaciones(DenominacionModel denominacion) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("INSERT INTO datos.denominacion(valor) VALUES(@valor)", substitutionValues: {
        "valor": denominacion.valor
      });
    } catch (e) {
      print(e);
    }    

    await conexionNegocio.close();
  }

  Future<void> eliminarDenominaciones(int id) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    try {
      await conexionNegocio.query("DELETE FROM datos.denominacion WHERE id = @id", substitutionValues: {
        "id": id
      });
    } catch (e) {
      print(e);
    }
  }
}