import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/models/comanda_virtual_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/services/comanda_virtual_postgres_service.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/stream/comandas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/widgets/container_list_widget.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/widgets/tarjeta_comanda_widget.dart';

class ComandaVirtualPage extends StatefulWidget {
  ComandaVirtualPage({Key key}) : super(key: key);

  @override
  State<ComandaVirtualPage> createState() => _ComandaVirtualPageState();
}

class _ComandaVirtualPageState extends State<ComandaVirtualPage> {
  final  _comandasRefreshBloc = ComandasRefreshBloc();

  static Timer timer;


  dynamic _futureComandas;

  List<ComandaVirtualModel> _listadoNuevos = [];
  List<ComandaVirtualModel> _listadoHaciendo = [];
  List<ComandaVirtualModel> _listadoTerminado = [];

  ComandaVirtualPostgresService comandaVirtualPostgresService = ComandaVirtualPostgresService();



  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
    ]);
    super.initState();
    
    timer = Timer.periodic(Duration(seconds: 5), (Timer timer) async {
      await _comandasRefreshBloc.obtenerTodasComandas();
      print('hola');
    });
    

    // _futureComandas = comandaVirtualPostgresService.obtenerComandas();
  }


  @override
  void dispose(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    _listadoNuevos.clear();
    _listadoHaciendo.clear();
    _listadoTerminado.clear();
    timer?.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(),
      body: StreamBuilder(
        stream: _comandasRefreshBloc.comandaRefreshStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            List<ComandaVirtualModel> listadoComandas = snapshot.data;

            _listadoNuevos = listadoComandas.where((element) => element.estado == 0).toList();
            _listadoHaciendo = listadoComandas.where((element) => element.estado == 3).toList();
            _listadoTerminado = listadoComandas.where((element) => element.estado == 4).toList();
            return  _bodyEstructuraComandaVirtual();
            
          }
        }
      ),
    );
  }

  Widget _bodyEstructuraComandaVirtual(){
    return Container(
      color: Colors.blue[100],
      child: Center(
        child: ListView(
          
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          children: [
            DragTarget(
              builder: (context, List<dynamic> accepted, List<dynamic> rejected) {
                return ContainerListWidget(listado: _listadoNuevos, titulo: "Nuevos",);
              },
              onWillAccept: (data) {
                return data["estado"] == 3 || data["estado"] == 4;
              },
              onAccept: (data) async{
                ComandaVirtualModel _tarjetaCambioEstado = data["estado"] == 3 ? _listadoHaciendo.where((element) => element.id == data["id"]).first : _listadoTerminado.where((element) => element.id == data["id"]).first;
        
                setState(() {
        
                  _tarjetaCambioEstado.estado = 0;
        
                  _tarjetaCambioEstado.figura = TarjetaComandaWidget(datosComanda: _tarjetaCambioEstado,);
        
        
                  _listadoNuevos.add(_tarjetaCambioEstado);
        
                  data["estado"] == 3 ? _listadoHaciendo.removeWhere((element) => element.id == _tarjetaCambioEstado.id) : _listadoTerminado.removeWhere((element) => element.id == _tarjetaCambioEstado.id);
        
                });
        
                await comandaVirtualPostgresService.actualizarEstadoComanda(0, _tarjetaCambioEstado.prefijocomanda, _tarjetaCambioEstado.numerocomanda);
              },
            ),
            DragTarget(
              builder: (context, candidateData, rejectedData) {
                return ContainerListWidget(listado: _listadoHaciendo, titulo: "Haciendo",);
              },
              onWillAccept: (data) {
                return data["estado"] == 0 || data["estado"] == 4;
              },
              onAccept: (data) async{
                ComandaVirtualModel _tarjetaCambioEstado =  data["estado"] == 0 ? _listadoNuevos.where((element) => element.id == data["id"]).first : _listadoTerminado.where((element) => element.id == data["id"]).first;
        
                setState(() {
                  
                  _tarjetaCambioEstado.estado = 3;
        
                  _tarjetaCambioEstado.figura = TarjetaComandaWidget(datosComanda: _tarjetaCambioEstado,);
        
        
                  _listadoHaciendo.add(_tarjetaCambioEstado);
        
                  data["estado"] == 0 ? _listadoNuevos.removeWhere((element) => element.id == _tarjetaCambioEstado.id) : _listadoTerminado.removeWhere((element) => element.id == _tarjetaCambioEstado.id);
        
                });
                await comandaVirtualPostgresService.actualizarEstadoComanda(3, _tarjetaCambioEstado.prefijocomanda, _tarjetaCambioEstado.numerocomanda);
              },
            ),
            DragTarget(
              builder: (context, candidateData, rejectedData) {
                return ContainerListWidget(listado: _listadoTerminado, titulo: "Terminado",);
              },
              onWillAccept: (data){
                return data["estado"] == 0 || data["estado"] == 3;
              },
              onAccept: (data) async{
                ComandaVirtualModel _tarjetaCambioEstado = data["estado"] == 0  ? _listadoNuevos.where((element) => element.id == data["id"]).first : _listadoHaciendo.where((element) => element.id == data["id"]).first;
        
                setState(() {          
        
                  _tarjetaCambioEstado.estado = 4;
        
                  _tarjetaCambioEstado.figura = TarjetaComandaWidget(datosComanda: _tarjetaCambioEstado,);
        
                  _listadoTerminado.add(_tarjetaCambioEstado);
        
                  data["estado"] == 0 ? _listadoNuevos.removeWhere((element) => element.id == _tarjetaCambioEstado.id) : _listadoHaciendo.removeWhere((element) => element.id == _tarjetaCambioEstado.id);
                  
        
                });
                await comandaVirtualPostgresService.actualizarEstadoComanda(4, _tarjetaCambioEstado.prefijocomanda, _tarjetaCambioEstado.numerocomanda);
              },
            ),
          ],
        ),
      ),
    );
  }
}
