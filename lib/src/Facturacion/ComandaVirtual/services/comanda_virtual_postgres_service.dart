import 'dart:async';

import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/models/comanda_virtual_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/widgets/tarjeta_comanda_widget.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';


class ComandaVirtualPostgresService {
  ComandaVirtualPostgresService._privateConstructor();

  static final ComandaVirtualPostgresService _instancia =
      ComandaVirtualPostgresService._privateConstructor();

  factory ComandaVirtualPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Stream<List<ComandaVirtualModel>> obtenerComandasStream() async*{
    await Timer.periodic(Duration(seconds: 10), (_) async*{
      yield await obtenerComandas();
    });
  }

  Future<List<ComandaVirtualModel>> obtenerComandas() async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ComandaVirtualModel> listadoComandaVirtual = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT C.id, C.prefijocomanda, CAST(C.numerocomanda AS INTEGER), C.fechahora, C.fecha_confirma_recibido, C.fecha_confirma_despacho, C.fecha_confirma_terminado, C.estado, O.nombre  FROM datos.comandacabecera C, datos.objetos O WHERE C.estado <> 2 AND C.sucursalid = @sucursalid AND C.mesahabitacionid = O.objetoid", substitutionValues: {
        "sucursalid": prefs.sucursalid
      });

      for (var item in _data) {
        List<ProductoCuentaModel> listadoProductos = [];

        ComandaVirtualModel _comandaVirtualModelCabecera = ComandaVirtualModel(
          id: item[0],
          prefijocomanda: item[1],
          numerocomanda: item[2],
          fechaHora: item[3],
          fechaConfimaRecibido: item[4],
          fechaConfirmaDespacho: item[5],
          fechaConfirmaTerminado: item[6],
          estado: item[7],
          nombreMesa: item[8]
        );
        
        List<List<dynamic>> _productos = await conexionNegocio.query(
            'SELECT nombre, CAST(cantidad AS REAL), CAST(valortotal AS REAL), textogusto, productoid, presentacion FROM datos.comandadetalle WHERE prefijocomanda = @prefijocomanda AND numerocomanda = @numerocomanda AND estado = 0 AND sucursalid = @sucursalid',
            substitutionValues: {
              "prefijocomanda": item[1],
              "numerocomanda": item[2],
              "sucursalid": prefs.sucursalid,
            }); //Aquí consulta los productos de la cuenta de la mesa actual para renderizarlos

        for (var producto in _productos) {
          ProductoCuentaModel pro = ProductoCuentaModel();

          pro.nombre = producto[0];
          pro.cantidad = producto[1];
          pro.valortotal = producto[2];
          pro.gustos = producto[3];
          pro.productoid = producto[4];
          pro.presentacion = producto[5].toString();

          listadoProductos.add(pro);
        }

        _comandaVirtualModelCabecera.productos = listadoProductos;

        _comandaVirtualModelCabecera.figura = TarjetaComandaWidget(datosComanda: _comandaVirtualModelCabecera,);


        listadoComandaVirtual.add(_comandaVirtualModelCabecera);
      }
    } catch (e) {
      print(e);
      throw(e);
    }

    await conexionNegocio.close();

    return listadoComandaVirtual;
  }

  Future<void> actualizarEstadoComanda(int estado, String prefijo, int numero) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    String queryDinamico;
    
    try {
      switch (estado) {
        case 0:
          queryDinamico = ", confirma_recibido = 1 , fecha_confirma_recibido = @fecha";
          break;
        case 3:
          queryDinamico = ", confirma_terminado = 1 , fecha_confirma_terminado = @fecha";
          break;
        case 4:
          queryDinamico = ", confirma_despacho = 1 , fecha_confirma_despacho = @fecha";
          break;
        default:
      }

      await conexionNegocio.query("UPDATE datos.comandacabecera SET estado = @estado $queryDinamico WHERE prefijocomanda = @prefijocomanda AND numerocomanda = @numerocomanda", substitutionValues: {
        "estado": estado,
        "prefijocomanda": prefijo,
        "numerocomanda": numero,
        "fecha": DateTime.now().toLocal().toString(),
      });
    } catch (e) {
      print(e);
      throw e;
    }

    await conexionNegocio.close();
  }

}