// To parse this JSON data, do
//
//     final comandaVirtualModel = comandaVirtualModelFromJson(jsonString);

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';

ComandaVirtualModel comandaVirtualModelFromJson(String str) => ComandaVirtualModel.fromJson(json.decode(str));

String comandaVirtualModelToJson(ComandaVirtualModel data) => json.encode(data.toJson());

class ComandaVirtualModel {
    ComandaVirtualModel({
        this.id,
        this.prefijocomanda,
        this.numerocomanda,
        this.estado,
        this.figura,
        this.fechaHora,
        this.fechaConfimaRecibido,
        this.fechaConfirmaDespacho,
        this.fechaConfirmaTerminado,
        this.productos,
        this.nombreMesa,
        this.temporizador
    });

    int id;
    String prefijocomanda;
    int numerocomanda;
    int estado;
    Widget figura;
    DateTime fechaHora;
    DateTime fechaConfimaRecibido;
    DateTime fechaConfirmaDespacho;
    DateTime fechaConfirmaTerminado;
    List<ProductoCuentaModel> productos;
    String nombreMesa;
    Timer temporizador;

    factory ComandaVirtualModel.fromJson(Map<String, dynamic> json) => ComandaVirtualModel(
        id: json["id"],
        prefijocomanda: json["prefijocomanda"],
        numerocomanda: json["numerocomanda"],
        estado: json["estado"],
        figura: json["figura"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "prefijocomanda": prefijocomanda,
        "numerocomanda": numerocomanda,
        "estado": estado,
        "figura": figura,
    };
}
