import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/models/comanda_virtual_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/stream/stopwatch_comanda_virtual_stream.dart';

class TarjetaComandaWidget extends StatefulWidget {
  final ComandaVirtualModel datosComanda;
  const TarjetaComandaWidget({Key key, this.datosComanda}) : super(key: key);

  @override
  State<TarjetaComandaWidget> createState() => _TarjetaComandaWidgetState();
}

class _TarjetaComandaWidgetState extends State<TarjetaComandaWidget> {
  var timerSubscription;
  String  minutesStr = "00", secondsStr = "00";


  @override
  void initState() {
    _iniciarTimers();
    super.initState();
  }

  @override
  void dispose() {
    timerSubscription?.cancel();
    super.dispose();
  }

  _iniciarTimers(){
    DateTime fechaHoraActual = widget.datosComanda.fechaConfirmaTerminado != null ? widget.datosComanda.fechaConfirmaTerminado : widget.datosComanda.fechaConfirmaDespacho != null ? widget.datosComanda.fechaConfirmaDespacho : widget.datosComanda.fechaHora;

    int segundos = DateTime.now().toLocal().difference(fechaHoraActual).inSeconds;

    var timerStream = stopWatchStream();

    timerSubscription = timerStream.listen((int newTick) {
      setState(() {
        minutesStr = (((newTick + segundos) / 60))
            .floor()
            .toString()
            .padLeft(2, '0');
        secondsStr =
            ((newTick + segundos) % 60).floor().toString().padLeft(2, '0');
      });
    });
  }

  @override
  Widget build(BuildContext context) {   

    Color colorsEstado = int.parse(minutesStr) < 5 ? Colors.green : Colors.amber;

    return Draggable(
      affinity: Axis.horizontal,
      data: {
        "id": widget.datosComanda.id,
        "estado": widget.datosComanda.estado
      },
      child: Container(
        
        width: 300,
        child: Card(
          elevation: 10.0,
          child: ListTile(
            title: Container(
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: colorsEstado,
              ),
              child: Text(widget.datosComanda.nombreMesa, style: TextStyle(fontWeight: FontWeight.bold),)
            ),
            subtitle: Column(
              verticalDirection: VerticalDirection.down,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.datosComanda.productos.map((producto){
                return Column(
                  children: [
                    Text(producto.cantidad.toString()+" X "+producto.nombre + " - " + producto.presentacion),
                    Divider(color: Colors.grey)
                  ],
                );
              }).toList(),
            ),
            trailing: Text("$minutesStr:$secondsStr", style: TextStyle(leadingDistribution: TextLeadingDistribution.even,),),
          ),
        ),
      ),
      feedback: Container(
        
        width: 300,
        child: Card(
          child: ListTile(
            
          ),
        ),
      ),
    );
  }
}