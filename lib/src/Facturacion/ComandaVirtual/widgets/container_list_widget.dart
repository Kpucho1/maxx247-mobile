import 'package:flutter/material.dart';
import "package:collection/collection.dart";
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/models/comanda_virtual_model.dart';

class ContainerListWidget extends StatelessWidget {
  final String titulo;
  final List<ComandaVirtualModel> listado;
  const ContainerListWidget({Key key, this.listado, this.titulo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
        color: Colors.white70,

      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ElevatedButton(
            child: Text(this.titulo+" ("+this.listado.length.toString()+")"),
            onPressed: () async{
              await _dialogResumen(context);
            },
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              padding: EdgeInsets.symmetric(horizontal: 15),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: this.listado.map((e) => e.figura).toList(),
              ),
            )
          )
        ],
      ),
    );
  }

  Future<void> _dialogResumen(BuildContext context) async{
    List<ProductoCuentaModel> listadoProductos = [];

    for (var item in this.listado) {
      if (item.productos.length > 0) {
        listadoProductos.add(item.productos.map((e) => e).first); 
      }
    }

    Map<dynamic, dynamic> productosAgrupados = groupBy(listadoProductos, (ProductoCuentaModel obj) => obj.productoid);

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Resumen "+this.titulo),
          content: SingleChildScrollView(
            child: Column(
            mainAxisSize: MainAxisSize.min,
            children: productosAgrupados.values.map((producto) {
              double cantidad = 0;
              int cont = 0;

              for (var prod in producto) {
                cantidad += prod.cantidad;
              }
              return Card(
                child: ListTile(
                  title: Text(producto.first.nombre),
                  trailing: Text(cantidad.toInt().toString()),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: producto.map<Widget>((e) {
                      cont++;
                      return e.gustos != ""
                          ? Text(cont.toString() + "- " + e.gustos)
                          : SizedBox();
                    }).toList(),
                  ),
                ),
              );
            }).toList()),
          ),
        );
      },
    );
  }
}