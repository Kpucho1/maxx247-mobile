import 'dart:async';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/models/comanda_virtual_model.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/services/comanda_virtual_postgres_service.dart';

class ComandasRefreshBloc {
  static final ComandasRefreshBloc _singletonComandasRefresh =
      new ComandasRefreshBloc._internal();

  factory ComandasRefreshBloc() {
    return _singletonComandasRefresh;
  }

  ComandasRefreshBloc._internal() {
  }

  final _comandasRefreshController = StreamController<List<ComandaVirtualModel>>.broadcast();
  
  ComandaVirtualPostgresService comandaPostgresService = ComandaVirtualPostgresService();

  //Streams que recuperan la info
  Stream<List<ComandaVirtualModel>> get comandaRefreshStream =>
      _comandasRefreshController.stream;

  

  dispose() {
    _comandasRefreshController?.close();
   
  }

  obtenerTodasComandas() async {
    _comandasRefreshController.sink.add(await comandaPostgresService.obtenerComandas());
  }

  // //Obtención de mesas por esquemas
  // obtenerMesasPorEsquema(int esquemaId) async {
  //   _comandasRefreshController.sink
  //       .add(await DBProvider.db.obtenerMesaPorEsquemaId(esquemaId));
  // }

  // actualizarMesas(MesaModel mesa, int objetoId) async {
  //   await DBProvider.db.actualizaMesas(mesa, objetoId);
  // }

}
