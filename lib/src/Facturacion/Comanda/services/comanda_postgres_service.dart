import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';

//Manejo de peticiones para la comanda. Los returns de las funciones deben ir al bloc de la comanda
//para respectivo manejo de estados y eventos a la hora de renderizar la cuenta en la pantalla
//
class ComandaPostgresService {
  ComandaPostgresService._privateConstructor();

  final prefs = PreferenciasUsuario();

  static final ComandaPostgresService _instancia =
      ComandaPostgresService._privateConstructor();

  factory ComandaPostgresService() {
    return _instancia;
  }

  Future<void> actualizarProducto(
    int id,
    double cantidad,
    int productoid,
    int valorunitario,
    List<String> _tempSelectedGustos,
  ) async {
    //

    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    ProductosCuentaModel productoOriginal =
        await DBProvider.db.getProductosPorProductoId(productoid);

    int productoBase = 0;
    int productoValorIva = 0;
    int productoValorIpo = 0;

    if (productoOriginal.productoPorIva > 0) {
      productoBase =
          (valorunitario / ((productoOriginal.productoPorIva / 100) + 1))
              .round();
      productoValorIva =
          ((productoBase * cantidad) * (productoOriginal.productoPorIva / 100))
              .round();
    } else if (productoOriginal.productoPorIpo > 0) {
      productoBase =
          (valorunitario / ((productoOriginal.productoPorIpo / 100) + 1))
              .round();
      productoValorIpo =
          ((productoBase * cantidad) * (productoOriginal.productoPorIpo / 100))
              .round();
    } else {
      productoBase = valorunitario.round();
    }

    try {
      await conexionNegocio.query(
          "UPDATE datos.cuentadetalle SET productogusto = @productogusto, productocantidad = @productocantidad, productovalortotal = @productovalortotal, productobase = @productobase, productovaloriva = @productovaloriva, productovaloripo = @productovaloripo, productovalorunitario = @productovalorunitario, productoporipo = @productoporipo, productoporiva = @productoporiva WHERE cuentadetalleid = @id",
          substitutionValues: {
            "id": id,
            "productogusto": _tempSelectedGustos.join("/"),
            "productocantidad": cantidad,
            "productovalortotal": valorunitario * cantidad,
            "productobase": productoBase * cantidad,
            "productovaloriva": productoValorIva,
            "productovaloripo": productoValorIpo,
            "productovalorunitario": valorunitario,
            "productoporiva": productoOriginal.productoPorIva,
            "productoporipo": productoOriginal.productoPorIpo,
          });
    } catch (e) {
      print(e);
      throw Exception('Problema al intentar actualizar producto');
    }

    await conexionNegocio.close();
  }

  //========== Eliminar producto ============

  Future<void> eliminarProducto(int id) async {
    //

    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);
    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    // VendedorModel mesero = await DBProvider.db.obtenerVendedor();

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.cuentadetalle SET productoestado = 2, fechaanulacion = @fecha, usuarioanulacion = @usuario WHERE cuentadetalleid = @id",
          substitutionValues: {
            "id": id,
            "fecha": DateTime.now().toLocal(),
            "usuario": "Caja"
          });
    } catch (e) {
      print(e);
      throw Exception('Problema al eliminar producto');
    }

    await conexionNegocio.close();
  }

  //======== Funcion usada para renderizar los productos incluidos en la comanda, al igual que el total

  Future<Map> consultarDetalleCuenta(
    String prefijoCuenta,
    int numeroCuenta,
  ) async {
    //
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    double total = 0;
    int sinComanda = 0;
    List<List<dynamic>> listaProductos = [];
    List<ProductoCuentaModel> listaProductosModel = [];

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );
      double iva = 0;
      double ipo = 0;

      // VendedorModel mesero = await DBProvider.db.obtenerVendedor();

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      await conexionNegocio.transaction((cnt) async {
        listaProductos = await cnt.query(
            'SELECT productonombre, CAST(productocantidad AS REAL), CAST(productovalortotal AS REAL), productocodigo, cuentadetalle.centrocostoid, cuentadetalleid, prefijocomanda, CAST(numerocomanda AS INTEGER), productogusto, cuentadetalle.productoid, productoimprimecomanda, CAST(productovaloriva AS REAL), CAST(productovaloripo AS REAL), productopresentacion, CAST(productovalorunitario as INTEGER), impresora, centroscosto.nombre, cuentadetalle.vendedorid, cuentadetalle.puesto, vendedor.nombre FROM datos.cuentadetalle LEFT JOIN datos.centroscosto ON centroscosto.centrocostoid = cuentadetalle.centrocostoid AND centroscosto.sucursalid = @sucursalid LEFT JOIN datos.vendedor ON cuentadetalle.vendedorid = vendedor.vendedorid WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND productoestado = 0 AND cuentadetalle.sucursalid = @sucursalid ORDER BY cuentadetalle.id  ASC',
            substitutionValues: {
              "prefijocuenta": prefijoCuenta,
              "numerocuenta": numeroCuenta,
              "sucursalid": prefs.sucursalid,
            }); //Aquí consulta los productos de la cuenta de la mesa actual para renderizarlos

        for (var producto in listaProductos) {
          ProductoCuentaModel pro = ProductoCuentaModel();

          total += producto[2];
          iva += producto[11];
          ipo += producto[12];

          pro.nombre = producto[0];
          pro.cantidad = producto[1];
          pro.valortotal = producto[2];
          pro.codigo = producto[3];
          pro.centrocostoid = producto[4];
          pro.id = producto[5];
          pro.prefijocomanda = producto[6];
          pro.numerocomanda = producto[7];
          pro.gustos = producto[8];
          pro.productoid = producto[9];
          pro.imprimeComanda = producto[10];
          pro.presentacion = producto[13].toString();
          pro.valorunitario = producto[14];
          pro.ipimpresora = producto[15].toString();
          pro.nombreImpresora = producto[16].toString();
          pro.vendedorid = producto[17];
          pro.puesto = producto[18];
          pro.nombreVendedor = producto[19];

          //Se obtiene el numero de productos sin comandar
          if (pro.prefijocomanda == null || pro.numerocomanda == 0) {
            sinComanda++;
          }

          List<List<dynamic>> combos = await cnt.query(
              "SELECT productoid, COALESCE(cantidad::int, 1), productonombre, productogusto, productoimprimecomanda, centrocostoid FROM datos.cuentadetallecombos WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid AND cuentadetalleid = @cuentadetalleid",
              substitutionValues: {
                "prefijocuenta": prefijoCuenta,
                "numerocuenta": numeroCuenta,
                "sucursalid": prefs.sucursalid,
                "cuentadetalleid": pro.id,
              });

          if (combos.isNotEmpty && combos.length > 0) {
            List<ProductosCombosModel> listaCombos = [];
            for (var item in combos) {
              ProductosCombosModel productoCombo = ProductosCombosModel();

              productoCombo.productoidhijo = item[0];
              productoCombo.productoid = pro.productoid;
              productoCombo.cantidadcombo = item[1];
              productoCombo.nombre = item[2];
              productoCombo.gusto = item[3];
              productoCombo.productoImprimeComanda = item[4];
              productoCombo.centrocostoid = item[5];

              listaCombos.add(productoCombo);
            }

            pro.combos = listaCombos;
          }

          listaProductosModel.add(pro);
        }

        await cnt.query(
            "UPDATE datos.cuentacabecera SET totalapagar = @totalapagar, ivacuenta = @ivacuenta, impuestoconsumo = @impuestoconsumo WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
            substitutionValues: {
              "totalapagar": total,
              "ivacuenta": iva,
              "impuestoconsumo": ipo,
              "prefijocuenta": prefijoCuenta,
              "numerocuenta": numeroCuenta,
            });
      });

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } catch (e) {
      //
      print(e);
      throw Exception('Problema al consultar los detalles de la cuenta');
    }

    return {
      "TotalComanda": total ?? 0,
      "ProductosNoComandados": sinComanda,
      "ListaProductos":
          listaProductosModel.isNotEmpty ? listaProductosModel : [],
    };
  }

  Future agregarFormasPago(Map<String, dynamic> datos) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> maxId = await conexionNegocio
          .query("SELECT COALESCE(MAX(id), 0) FROM datos.cuentapago");

      print(maxId);

      await conexionNegocio.query(
          "INSERT INTO datos.cuentapago(id, prefijocuenta, numerocuenta, sucursalid, valor, facturada, valorpropina, documento, tipotarjetabanco) VALUES(@id, @prefijocuenta, @numerocuenta, @sucursalid, @valor, @facturada, @valorpropina, @documento, @tipotarjetabanco)",
          substitutionValues: {
            "prefijocuenta": datos["prefijocuenta"],
            "numerocuenta": datos["numerocuenta"],
            "valor": datos["valor"],
            "facturada": datos["facturada"],
            "valorpropina": datos["propina"],
            "documento": datos["documento"],
            "sucursalid": prefs.sucursalid,
            "tipotajetabanco": 1,
            "id": maxId.first[0] + 1,
            "tipotarjetabanco": datos["tipoTarjeta"]
          });

      await conexionNegocio.close();
    } catch (e) {
      print(e);
    }
  }

  Future<List<List<dynamic>>> retornarPagosTarjeta(
      String prefijoCuenta, int numeroCuenta, int tipoTarjeta) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> pagos = await conexionNegocio.query(
          "SELECT id, documento, valor, facturada, CAST(valorpropina AS INTEGER) FROM datos.cuentapago WHERE prefijocuenta = @prefijocuenta and numerocuenta = @numerocuenta AND tipotarjetabanco = @tipotarjetabanco",
          substitutionValues: {
            "prefijocuenta": prefijoCuenta,
            "numerocuenta": numeroCuenta,
            "tipotarjetabanco": tipoTarjeta,
          });

      await conexionNegocio.close();

      return pagos;
    } catch (e) {
      print(e);
    }
  }

  Future<void> eliminarPagoTarjeta(int id) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      await conexionNegocio.query("DELETE FROM datos.cuentapago WHERE id = @id",
          substitutionValues: {"id": id});

      await conexionNegocio.close();
    } catch (e) {
      print(e);
    }
  }

  // ignore: missing_return
  Future<DocumentoConsecutivoModel> crearFacturaPos(String prefijocuenta, int numerocuenta, Map<String, dynamic> formasPago) async{
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> consecutivo = await conexionNegocio.query("SELECT consecutivoid, prefijo, CAST(numeroactual AS integer), resoliciondian FROM datos.documentosconsecutivos WHERE sucursalid = @sucursalid AND tipodocumento = '1' LIMIT 1");


      await conexionNegocio.query("UPDATE datos.cuentacabecera SET prefijofactura = @prefijofactura, numerofactura = @numerofactura WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta", substitutionValues: {
        "prefijofactura": consecutivo.first[1],
        "numerofactura": consecutivo.first[2],
        "prefijocuenta": prefijocuenta,
        "numerocuenta": numerocuenta
      });

      await conexionNegocio.query("UPDATE datos.documentosconsecutivos SET numeroactual = @numeroactual WHERE consecutivoid = @consecutivoid", substitutionValues: {
        "numeroactual": consecutivo.first[2] + 1,
        "consecutivoid": consecutivo.first[0]
      });

      List<List<dynamic>> cabecera = await conexionNegocio.query("SELECT usuarioid, clienteid, direccioncliente, telefonocliente, ciudadcliente, centrocostoid, nombrecuenta, cast(subtotalcuenta as integer), cast(ivacuenta as integer), cast(descuentocuenta as integer), cast(propinacuenta as integer), cast(totalcuenta as integer), vendedorid, mesahabitacionid, esquemaid, cast(impuestoconsumo as integer), cast(totalapagar as integer), nombrecliente, nombrevendedor FROM datos.cuentacabecera WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta", substitutionValues:{
        "prefijocuenta": prefijocuenta,
        "numerocuenta": numerocuenta
      });

      List<List<dynamic>> detalle = await conexionNegocio.query("SELECT prefijocomanda, numerocomanda, productoid, productocodigo, productonombre, cast(productocantidad as integer), cast(productovalorunitario as integer), cast(productovalortotal as integer), productoservicio, productoimprimecomanda, productopresentacion, cast(productoequivalencia as integer), cast(productobase as integer), cast(productoporiva as integer), cast(productopordescuento as integer), cast(productovalordescuento as integer), cast(productovaloriva as integer), productocortesia, productogusto, cast(productoporipo as integer), vendedorid, cuentadetalleid, cast(productovaloripo as integer), centrocostoid FROM datos.cuentadetalle WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta", substitutionValues: {
        "prefijocuenta": prefijocuenta,
        "numerocuenta": numerocuenta,
      });

      await conexionNegocio.query("UPDATE datos.cuentacabecera SET estadocuenta = 1 WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid", substitutionValues: {
        "prefijocuenta": prefijocuenta,
        "numerocuenta": numerocuenta,
        "sucursalid": prefs.sucursalid
      });

      await conexionNegocio.query("UPDATE datos.objetos SET estado = 2 WHERE objetoid = @objetoid AND sucursalid = @sucursalid AND esquemaid = @esquemaid", substitutionValues: {
        "objetoid": cabecera.first[13],
        "sucursalid": prefs.sucursalid,
        "esqemaid": prefs.esquemaid
      });

      await conexionNegocio.query("INSERT INTO datos.facturacabecera(fposprefijo, fposnumero, clienteid, fposnomcli, fposdirecc, fpostelefo, fposciudad, fposcuenta, fposprefcu, fpossubfac, fposivafac, fpostotfac, esquemaid, fposvendedorid, fposobjetoid, fpossubbase, fpostotalpagar, fposnitcli, resoluciondian, nombreobjeto,numeroturno, sucursalid, fpospagefe, fpospagcre, fpospagche, fpospagbon, fpospagtarc, fpospagtard) VALUES(@fposprefijo, @fposnumero, @clienteid, @fposnomcli, @fposdirecc, @fpostelefo, @fposciudad, @fposcuenta, @fposprefcu, @fpossubfac, @fposivafac, @fpostotfac, @esquemaid, @fposvendedorid, @fposobjetoid, @fpossubbase, @fpostotalpagar, @fposnitcli, @resoluciondian, @nombreobjeto, @numeroturno, @sucursalid, @fpospagefe, @fpospagcre, @fpospagche, @fpospagbon, @fpospagtarc, @fpospagtard)", substitutionValues: {
        "fposprefijo": consecutivo.first[1],
        "fposnumero": consecutivo.first[2],
        "clienteid": cabecera.first[1],
        "fposnomcli": cabecera.first[17],
        "fposdirecc": cabecera.first[2],
        "fpostelefo": cabecera.first[3],
        "fposciudad": cabecera.first[4],
        "fposcuenta": numerocuenta,
        "fposprefcu": prefijocuenta,
        "fpossubfac": cabecera.first[7],
        "fposivafac": cabecera.first[8],
        "fpostotfac": cabecera.first[11],
        "esquemaid": prefs.esquemaid,
        "fposvendedorid": cabecera.first[12],
        "fposobjetoid": cabecera.first[13],
        "fpossubbase": cabecera.first[11],
        "fpostotalpagar": cabecera.first[16],
        "fposnitcli": 0,
        "resoluciondian": consecutivo.first[3],
        "nombreobjeto": "",
        "numeroturno": 0,
        "sucursalid": prefs.sucursalid,
        "fpospagefe": formasPago["efectivo"],
        "fpospagcre": formasPago["credito"],
        "fpospagche": formasPago["bancos"],
        "fpospagbon": formasPago["bonos"],
        "fpospagtarc": formasPago["tarjeta_credito"],
        "fpospagtard": formasPago["tarjeta_debito"]
      });

      for (var det in detalle) {
        await conexionNegocio.query("INSERT INTO datos.facturadetalle(fposprefijo, fposnumero, fposservicio, fposproducto, fposdescripcion, fposipototal, fposivatotal, fposdescuentototal, fposcantidad, fpospresentacion, fposequivalencia, fposvalorunitario, fpossubunitario, fpossubbase, fpospordescuento, productoid, fposvendedorid, observacion, fposcentrocostoid, sucursalid) VALUES(@fposprefijo, @fposnumero, @fposservicio, @fposproducto, @fposdescripcion, @fposipototal, @fposivatotal, @fposdescuentototal, @fposcantidad, @fpospresentacion, @fposequivalencia, @fposvalorunitario, @fpossubunitario, @fpossubbase, @fpospordescuento, @productoid, @fposvendedorid, @observacion, @fposcentrocostoid, @sucursalid)", substitutionValues: {
          "fposprefijo": consecutivo.first[1],
          "fposnumero": consecutivo.first[2],
          "fposservicio": det[9],
          "fposproducto": det[3],
          "fposdescripcion": det[4],
          "fposipototal": det[22],
          "fposivatotal": det[16],
          "fposdescuentototal": det[15],
          "fposcantidad": det[5],
          "fpospresentacion": det[11],
          "fposequivalencia": det[12],
          "fposvalorunitario": det[6],
          "fpossubunitario": det[7],
          "fpossubbase": 0,
          "fpospordescuento": det[14],
          "productoid": det[2],
          "fposvendedorid": 0,
          "observacion": "",
          "fposcentrocostoid": det[23],
          "sucursalid": prefs.sucursalid
        });
      }

      await conexionNegocio.close();


      DocumentoConsecutivoModel documentoConsecutivoModel = DocumentoConsecutivoModel(consecutivoid: consecutivo.first[0], prefijo: consecutivo.first[1], numeroactual: consecutivo.first[2], resoluciondian: consecutivo.first[3] ?? '');

      return documentoConsecutivoModel;


    } catch (e) {
      print(e);
    }
  }
}
