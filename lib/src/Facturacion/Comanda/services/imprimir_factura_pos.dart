

import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:image/image.dart' as img;
import 'package:qr_flutter/qr_flutter.dart';
import 'package:intl/intl.dart';

class ImprimirFacturaPos {
  final String prefijocuenta;
  final int numerocuenta;
  final String nombremesa;
  final Map<String, dynamic> formasPago;
  final Map<String, dynamic> cliente;
  final DocumentoConsecutivoModel consecutivo;

  ImprimirFacturaPos(
      {this.prefijocuenta,
      this.numerocuenta,
      this.nombremesa,
      this.formasPago,
      this.cliente, this.consecutivo});

  PreferenciasUsuario prefs = PreferenciasUsuario();

  Future<void> imprimir() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> cuentaCabecera = await conexionNegocio.query(
          "SELECT CAST(totalapagar AS INTEGER), CAST(totalapagar - ivacuenta - impuestoconsumo - propinacuenta AS INTEGER), CAST(ivacuenta AS INTEGER), CAST(impuestoconsumo AS INTEGER), CAST(propinacuenta AS INTEGER), CAST(totalapagar AS INTEGER), CAST(descuentocuenta AS INTEGER) FROM datos.cuentacabecera WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
          substitutionValues: {
            "prefijocuenta": this.prefijocuenta,
            "numerocuenta": this.numerocuenta,
          });

      List<List<dynamic>> cuentaDetalle = await conexionNegocio.query(
          "SELECT productocodigo, productonombre, productopresentacion, CAST(productocantidad as REAL), CAST(productovalortotal as INTEGER) FROM datos.cuentadetalle WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND productoestado <> 2",
          substitutionValues: {
            "prefijocuenta": this.prefijocuenta,
            "numerocuenta": this.numerocuenta,
          });

      List<List<dynamic>> datosEmpresa = await conexionNegocio.query(
          "SELECT nombre, nit, direccion, telefonos, ciudad, logoempresa, tiporegimen, email, centrocostosucursal FROM datos.empresa");


      List<List<dynamic>> impresora = await conexionNegocio.query(
          "SELECT matricula_mercantil FROM datos.empresa WHERE codigoempresaid = @sucursalid", substitutionValues: {
            "sucursalid": prefs.sucursalid
          });

      const PaperSize paper = PaperSize.mm80;

      final profile = await CapabilityProfile.load();

      final printer = NetworkPrinter(paper, profile);

      PosPrintResult res =
          await printer.connect(impresora.first[0], port: 9100);

      if (res == PosPrintResult.success) {
        await _processTicket(printer, cuentaCabecera.first, datosEmpresa.first,
            cuentaDetalle, consecutivo);

        printer.reset();
        printer.disconnect();
      }

      await conexionNegocio.close();
    } on PostgreSQLException catch (e) {
      print("Postgres: " + e.message);
    } on Exception catch (e) {
      print("Sistema: " + e.toString());
    }

    //Se conecta a la impresora
  }

  Future<void> _processTicket(
      NetworkPrinter ticket,
      List<dynamic> cuentaCabecera,
      List<dynamic> datosEmpresa,
      List<List<dynamic>> cuentaDetalle,
      DocumentoConsecutivoModel consecutivo) async {
    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    // if (datosEmpresa[5] != null && datosEmpresa[5] != []) {
    //   // final ByteData data = await rootBundle.load('assets/maxxcontrol_logo.png');
    //   // final Uint8List bytes = data.buffer.asUint8List();
    //   final img.Image image = img.decodeImage(datosEmpresa[5]);

    //   ticket.image(image);
    // }

    ticket.text(datosEmpresa[0],
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size2,
          width: PosTextSize.size2,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: true,
        ));

    ticket.emptyLines(1);

    ticket.text('Nit ' + datosEmpresa[1],
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.text(
        datosEmpresa[6] == 1 ? 'Responsable de iva' : 'No responsable de iva',
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    // ticket.text('Impuesto nacional al consumo',
    //     styles: PosStyles(
    //       bold: true,
    //       height: PosTextSize.size1,
    //       width: PosTextSize.size1,
    //       align: PosAlign.center,
    //       fontType: PosFontType.fontA,
    //       reverse: false,
    //       underline: false,
    //     ));

    ticket.text(datosEmpresa[2],
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.emptyLines(1);

    ticket.text(datosEmpresa[4],
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.text('TELEFONO ' + datosEmpresa[3],
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));


    ticket.hr(ch: '_');

    ticket.text('Mesa: ' + this.nombremesa,
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));
    
    ticket.text('Vendedor: CAJA' ,
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.text('Fecha: ' + DateTime.now().toLocal().toString(),
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.hr(ch: '-');

    ticket.row([
      PosColumn(
        text: 'Descripcion',
        width: 4,
        styles: PosStyles(align: PosAlign.left, underline: true),
      ),
      PosColumn(
        text: 'Presentacion',
        width: 3,
        styles: PosStyles(align: PosAlign.left, underline: true),
      ),
      PosColumn(
        text: 'Cant',
        width: 2,
        styles: PosStyles(align: PosAlign.right, underline: true),
      ),
      PosColumn(
        text: 'V/Total',
        width: 3,
        styles: PosStyles(align: PosAlign.right, underline: true),
      ),
    ]);

    ticket.hr(ch: '_');

    for (var detalle in cuentaDetalle) {
      ticket.text(detalle[1].toString().trim(), styles: PosStyles(align: PosAlign.left, width: PosTextSize.size1));
      ticket.row([
        PosColumn(
          text: '',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: detalle[2].toString(),
          width: 3,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: detalle[3].toString(),
          width: 2,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size1),
        ),
        PosColumn(
          text: detalle[4].toString(),
          width: 3,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size1),
        )
      ]);

      ticket.hr(ch: '-');
    }

    // ticket.hr(ch: '_');

    ticket.emptyLines(1);

    ticket.row([
      PosColumn(
        text: 'TOTAL FACTURA',
        width: 8,
        styles: PosStyles(
          align: PosAlign.left,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
      PosColumn(
        text: formatCurrency.format((cuentaCabecera[0])),
        width: 4,
        styles: PosStyles(
          align: PosAlign.right,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
    ]);

    ticket.row([
      PosColumn(
        text: 'SUBTOTAL',
        width: 8,
        styles: PosStyles(
          align: PosAlign.left,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
      PosColumn(
        text: formatCurrency.format(cuentaCabecera[1]),
        width: 4,
        styles: PosStyles(
          align: PosAlign.right,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
    ]);

    cuentaCabecera[2] != 0
        ? ticket.row([
            PosColumn(
              text: 'IVA',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formatCurrency.format(cuentaCabecera[2]),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : ticket.emptyLines(0);

    cuentaCabecera[3] != 0
        ? ticket.row([
            PosColumn(
              text: 'IPOCONSUMO',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formatCurrency.format(cuentaCabecera[3]),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : ticket.emptyLines(0);

    ticket.row([
      PosColumn(
        text: 'PROPINA SUGERIDA 5%',
        width: 8,
        styles: PosStyles(
          align: PosAlign.left,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
      PosColumn(
        text: '0',
        width: 4,
        styles: PosStyles(
          align: PosAlign.right,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
    ]);

    ticket.hr(ch: '_');

    ticket.text("TOTAL A PAGAR");
    ticket.emptyLines(1);

    ticket.text("\$ " + formatCurrency.format(cuentaCabecera[5]),
        styles: PosStyles(
            height: PosTextSize.size3,
            width: PosTextSize.size3,
            align: PosAlign.right));

    ticket.hr(ch: '_');

    ticket.text("FORMAS DE PAGO", styles: PosStyles(align: PosAlign.center));

    ticket.emptyLines(1);

    formasPago["efectivo"] > 0
        ? ticket.row([
            PosColumn(
              text: 'EFECTIVO',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["efectivo"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    formasPago["credito"] > 0
        ? ticket.row([
            PosColumn(
              text: 'CREDITO',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["credito"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    formasPago["tarjeta_debito"] > 0
        ? ticket.row([
            PosColumn(
              text: 'TARJETA DEBITO',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["tarjeta_debito"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    formasPago["tarjeta_credito"] > 0
        ? ticket.row([
            PosColumn(
              text: 'TARJETA CREDITO',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["tarjeta_credito"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    formasPago["separados"] > 0
        ? ticket.row([
            PosColumn(
              text: 'SEPARADOS',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["separados"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    formasPago["bonos"] > 0
        ? ticket.row([
            PosColumn(
              text: 'BONOS',
              width: 8,
              styles: PosStyles(
                align: PosAlign.left,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
            PosColumn(
              text: formasPago["bonos"].toString(),
              width: 4,
              styles: PosStyles(
                align: PosAlign.right,
                width: PosTextSize.size1,
                fontType: PosFontType.fontA,
              ),
            ),
          ])
        : null;

    ticket.row([
      PosColumn(
        text: 'CAMBIO',
        width: 8,
        styles: PosStyles(
          align: PosAlign.left,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
      PosColumn(
        text: '0',
        width: 4,
        styles: PosStyles(
          align: PosAlign.right,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
    ]);

    ticket.row([
      PosColumn(
        text: 'DESCUENTO',
        width: 8,
        styles: PosStyles(
          align: PosAlign.left,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
      PosColumn(
        text: '0',
        width: 4,
        styles: PosStyles(
          align: PosAlign.right,
          width: PosTextSize.size1,
          fontType: PosFontType.fontA,
        ),
      ),
    ]);

    ticket.hr(ch: '-');

    if (this.cliente["identificacion"] != null &&
        this.cliente["identificacion"] != "" &&
        this.cliente["identificacion"] != 0) {
      ticket.text("DATOS DEL CLIENTE",
          styles: PosStyles(align: PosAlign.center));

      ticket.hr(ch: '_');

      ticket.row([
        PosColumn(
          text: 'Nit / cc',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: this.cliente["identificacion"].toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      ticket.row([
        PosColumn(
          text: 'Nombre',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: this.cliente["nombre"].toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      ticket.row([
        PosColumn(
          text: 'Telefonos',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: this.cliente["telefono"].toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      ticket.row([
        PosColumn(
          text: 'Ciudad',
          width: 4,
          styles: PosStyles(
            align: PosAlign.left,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: this.cliente["ciudad"].toString(),
          width: 8,
          styles: PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
      ]);

      ticket.text('Direccion: '+ this.cliente["direccion"].toString(), styles: PosStyles(
            width: PosTextSize.size1,
            fontType: PosFontType.fontA,
          ));
    }

    ticket.emptyLines(1);

    // const String qrData =
    //     "NumFac: GCD10152 FecFac: 2020-07-14 HorFac: 05:54:13-05:00 NitFac: 10276515 DocAdq: 830050228 ValFac: 21055367.00 ValIva: 4000521.00 ValOtroIm: 0.00 ValTolFac: 25055888.00 CUFE: 7811f36b1057fbaacfc24613726067d1b5cc8fe1e02c63a69186cba86f80c401e81e569088c9ce399c19d9d16533b025 https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=7811f36b1057fbaacfc24613726067d1b5cc8fe1e02c63a69186cba86f80c401e81e569088c9ce399c19d9d16533b025";

    // const double qrSize = 250;
    // final uiImg = await QrPainter(
    //   data: qrData,
    //   version: QrVersions.auto,
    //   gapless: false,
    // ).toImageData(qrSize);

    // final dir = await getTemporaryDirectory();

    // final pathName = '${dir.path}/qr_tmp.png';

    // final qrFile = File(pathName);

    // final imgFile = await qrFile.writeAsBytes(uiImg.buffer.asUint8List());

    // final imga = img.decodeImage(imgFile.readAsBytesSync());

    // ticket.image(imga);

    // ticket.text(
    //     "7811f36b1057fbaacfc24613726067d1b5cc8fe1e02c63a69186cba86f80c401e81e569088c9ce399c19d9d16533b025",
    //     styles: PosStyles(fontType: PosFontType.fontB));

    // ticket.emptyLines(3);

    ticket.text('Sistema Pos No. ' + consecutivo.prefijo.toString() + consecutivo.numeroactual.toString(),
      styles: PosStyles(
        bold: true,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        align: PosAlign.center,
        fontType: PosFontType.fontA,
        reverse: false,
        underline: false,
      ));

    ticket.text(consecutivo.resoluciondian, styles: PosStyles(fontType: PosFontType.fontB));

    ticket.cut();
  }
}
