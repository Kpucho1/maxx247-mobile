import 'package:maxxshop/src/helpers/Adapters/I_Wrapper_Productos.dart';
import 'package:maxxshop/src/Facturacion/Comanda/services/comanda_postgres_service.dart';

//Clase de tipo adaptador para convertir los tipos de datos de las consultas desde la BD
//para ser usados en la renderizacion de la comanda. Se comunica directo con el bloc

class WrapperProductosComanda implements IWrapperProductosComanda {
  WrapperProductosComanda._privateConstructor();

  static final WrapperProductosComanda _instancia =
      WrapperProductosComanda._privateConstructor();

  factory WrapperProductosComanda() {
    return _instancia;
  }

  final ComandaPostgresService _comandaPostgresService =
      ComandaPostgresService();

  @override
  Future<Map<String, dynamic>> getProductosComanda(
    String prefijo,
    int cuenta,
  ) async {
    //

    final rawData = await _comandaPostgresService.consultarDetalleCuenta(
      prefijo,
      cuenta,
    );

    final totalComanda = rawData["TotalComanda"] as double;
    final listaProductos = rawData["ListaProductos"] as List;
    final productoSinComandar = rawData["ProductosNoComandados"];

    return listaProductos.isEmpty && totalComanda == 0.0
        ? {}
        : {
            "ListaProductos": listaProductos ?? [],
            "TotalComanda": totalComanda ?? 0.0,
            "ProductosNoComandados": productoSinComandar ?? 0,
          };
  }

  @override
  Future<Map<String, dynamic>> eliminarProductoComanda(
    int id,
    String prefijo,
    int cuenta,
  ) async {
    //
    //Ejecuta la función de eliminar el producto y nuevamente carga el listado de los productos
    //que permanecen en la comanda

    await _comandaPostgresService.eliminarProducto(id);

    return await getProductosComanda(prefijo, cuenta);
  }

  @override
  Future<Map<String, dynamic>> actualizarProductoComanda(
      int id,
      double cantidad,
      int productoId,
      int valorUnitario,
      String prefijo,
      int cuenta,
      List<String> _tempSelectedGustos) async {
    //

    await _comandaPostgresService.actualizarProducto(
      id,
      cantidad,
      productoId,
      valorUnitario,
      _tempSelectedGustos,
    );

    return await getProductosComanda(prefijo, cuenta);
  }
}
