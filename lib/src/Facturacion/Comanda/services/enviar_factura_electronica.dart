import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:http/http.dart' as http;

class EnviarFacturaElectronica {
  final String prefijocuenta;
  final int numerocuenta;
  final int cliente;

  EnviarFacturaElectronica(
      {@required this.prefijocuenta,
      @required this.numerocuenta,
      @required this.cliente});

  Future enviar() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final VendedorModel vendedor = await DBProvider.db.obtenerVendedor();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    try {
      List<Map<String, dynamic>> jsonDetalle = [];
      List<Map<String, dynamic>> clientes = [];

      ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

      List<List<dynamic>> consecutivoFactura = await conexionNegocio.query(
          "SELECT consecutivoid, prefijo, numeroactual, numerofinal FROM datos.documentosconsecutivos WHERE estado = 0 AND tipodocumento = '1'");

      if (consecutivoFactura.first[2] <= consecutivoFactura.first[3]) {
        List<List<dynamic>> cuentaCabecera = await conexionNegocio.query(
            "SELECT totalcuenta, subtotalcuenta, ivacuenta, impuestoconsumo, propinacuenta, totalapagar, descuentocuenta, subtotalgravado, subtotalexento, subtotalexcluido, subtotalnogravado, ingresosterceros FROM datos.cuentacabecera WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
            substitutionValues: {
              "prefijocuenta": this.prefijocuenta,
              "numerocuenta": this.numerocuenta.toString(),
            });

        List<List<dynamic>> datosEmpresa = await conexionNegocio.query(
            "SELECT nomsuc FROM datos.empresa WHERE codigoempresaid = @sucursalid",
            substitutionValues: {"sucursalid": vendedor.sucursalid});

        print(datosEmpresa);

        Map<String, dynamic> jsonCabecera = {
          "codigo_sucursal": 1,
          "prefijofactura": consecutivoFactura.first[1],
          "numerofactura": consecutivoFactura.first[2],
          "fecha_factura": DateTime.now().toLocal().toString(),
          "dias_plazo": 0,
          "xml": "",
          "cufe": "",
          "fecha_vencimiento": DateTime.now().toLocal().toString(),
          "numero_cuotas": 0,
          "tasa_interes": 0,
          "trm_dollar": 0,
          "trm_euro": 0,
          "total_factura": cuentaCabecera.first[0],
          "subtotal_factura": cuentaCabecera.first[1],
          "total_iva": cuentaCabecera.first[2],
          "total_ipo": cuentaCabecera.first[3],
          "total_propina": cuentaCabecera.first[4],
          "total_propina_efectivo": 0,
          "total_propina_tarjetas": 0,
          "total_propina_credito": 0,
          "total_apagar": cuentaCabecera.first[5],
          "total_descuento": cuentaCabecera.first[6],
          "total_gravado": cuentaCabecera.first[7],
          "total_exento": cuentaCabecera.first[8],
          "total_excluido": cuentaCabecera.first[9],
          "total_nogravado": cuentaCabecera.first[10],
          "total_ingreso_terceros": cuentaCabecera.first[10],
          "total_pagado_efectivo": 0,
          "total_pagado_tarjetadeb": 0,
          "total_pagado_tarjetacre": 0,
          "total_pagado_credito": 0,
          "total_pagado_consignacion": 0,
          "total_pagado_notadevolucion": 0,
          "total_pagado_bono": 0,
          "total_pagado_abono": 0,
          "total_suma_formaspago": 0,
          "prefijo_cierrecaja": "",
          "numero_cierrecaja": 0,
          "prefijo_despacho": "",
          "numero_despacho": 0,
          "codigo_lista_precio": 0,
          "codigo_objeto_relacionado": "",
          "codigo_transportadora": "4364",
          "codigo_cajero": "",
          "estado": 0,
          "codigo_usuario_add": "adm",
          "observacion": "OP 4364 OC 15092020 REMISION 4653"
        };

        List<List<dynamic>> cuentaDetalle = await conexionNegocio.query(
            "SELECT productocodigo, productonombre, productopresentacion, productocantidad, productobase, productovalorunitario, productoporiva, productoporipo, productopordescuento, productovalordescuento, productovaloriva, productovaloripo, productoservicio FROM datos.cuentadetalle WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
            substitutionValues: {
              "prefijocuenta": this.prefijocuenta,
              "numerocuenta": this.numerocuenta.toString(),
            });

        for (var detalle in cuentaDetalle) {
          jsonDetalle.add({
            "productocodigo": detalle[0],
            "producto_descripcion": detalle[1],
            "producto_referencia_fab": "",
            "producto_presentacion": detalle[2],
            "producto_serial": "",
            "producto_lote_numero": "",
            "producto_total_peso": 0,
            "producto_cantidad": detalle[3],
            "producto_preciounitario_lista": detalle[4] / detalle[3],
            "producto_valorunitario": detalle[5],
            "producto_subunitario": detalle[4],
            "producto_valorbase": detalle[4] / detalle[3],
            "producto_total_costo": 0,
            "producto_poriva": detalle[6],
            "producto_poripo": detalle[7],
            "producto_pordescuento1": detalle[8],
            "producto_pordescuento2": 0,
            "producto_valordescuento1": detalle[9],
            "producto_valordescuento2": 0,
            "producto_valoriva": detalle[10],
            "producto_valoripo": detalle[11],
            "producto_esservicio": detalle[12],
            "producto_esincluido": 0,
            "producto_escortesia": 0,
            "producto_ingresotercero": 0,
            "producto_codigo_bodega": 1,
            "prefijo_orden_compra": "",
            "numero_orden_compra": 0,
            "prefijo_remision": "",
            "numero_remision": 0,
            "prefijo_cuenta": "",
            "numero_cuenta": 0,
            "prefijo_comanda": "",
            "numero_comanda": 0,
            "ts": "",
            "codigo_usuario_add": ""
          });
        }

        List<List<dynamic>> cliente = await conexionNegocio.query(
            "SELECT nit, tipo_identificacion, nombre1, nombre2, apellido1, apellido2, razonsocial, direccion, celular, email FROM datos.clientes WHERE clienteid = @clienteid",
            substitutionValues: {"clienteid": this.cliente});

        clientes.add({
          "id": 0,
          "identificacion": cliente.first[0],
          "tipo_identificacion": cliente.first[1],
          "primer_nombre": cliente.first[2],
          "segundo_nombre": cliente.first[3],
          "primer_apellido": cliente.first[4],
          "segundo_apellido": cliente.first[5],
          "razon_social": cliente.first[6],
          "direccion": cliente.first[7],
          "celular": cliente.first[8],
          "codigo_dane_ciudad": "76147",
          "nombre_ciudad": "Cartago",
          "email": cliente.first[9]
        });

        final body = json.encode({
          "cabecera": jsonCabecera,
          "detalle": jsonDetalle.toList(),
          "clientes": clientes.toList()
        });

        final res = await http.post(
            'https://api.maxx247.com/api/webservice/invoice',
            body: body,
            headers: {
              "Authorization": datosEmpresa.first[0],
              "Content-Type": "application/json"
            });

        return res;
      }
    } on PostgreSQLException catch (e) {
      print(e);
    }
  }
}
