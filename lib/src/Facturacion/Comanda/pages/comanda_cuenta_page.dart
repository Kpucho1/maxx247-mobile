import 'package:flutter/material.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/comanda_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';

class ComandaCuentaPage extends StatefulWidget {
  @override
  _ComandaPageState createState() => _ComandaPageState();
}

class _ComandaPageState extends State<ComandaCuentaPage> {
  List<List<dynamic>> detalle;

  List<ProductoCuentaModel> listaProductos;

  // final NetworkPrinter printer = NetworkPrinter();

  double _totalComanda = 0.0;

  @override
  void initState() {
    //consultarDetalleCuenta();
    super.initState();
  }

  @override
  @override
  Widget build(BuildContext context) {
    final bloc = ProviderInherited.comandaBloc(context);

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: _listadoProductos(bloc),
          ),
          _valorTotalBajo(context),
        ],
      ),
    );
  }

  Widget _listadoProductos(ComandaBloc bloc) {
    return StreamBuilder<List<ProductoCuentaModel>>(
      stream: bloc.productosCuentaStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());

        listaProductos = snapshot.data;

        //print(listaProductos);

        return ListView.builder(
          itemCount: listaProductos.length,
          itemBuilder: (context, index) {
            return _cardProductos(context, index, listaProductos);
          },
        );
      },
    );
  }

  Widget _cardProductos(BuildContext context, int index,
      List<ProductoCuentaModel> listadoProductos) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.05,
        vertical: size.height * 0.002,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 4.0,
        child: ListTile(
          title: Text('${listadoProductos[index].nombre}'),
          subtitle: Text('Cantidad: ${listadoProductos[index].cantidad}'),
          trailing: Text('Total: \$ ${listadoProductos[index].valortotal}'),
        ),
      ),
    );
  }

  // Future<Ticket> processTicket(
  //     List<ProductoCuentaModel> productos, String centroCosto) async {
  //   Ticket ticket = Ticket(PaperSize.mm80);
  //   ticket.text('# TURNO 1',
  //       styles: PosStyles(
  //         bold: true,
  //         height: PosTextSize.size2,
  //         width: PosTextSize.size5,
  //         align: PosAlign.center,
  //         fontType: PosFontType.fontA,
  //         reverse: false,
  //         underline: true,
  //       ));
  //   ticket.emptyLines(1);
  //   ticket.text(centroCosto,
  //       styles: PosStyles(
  //         bold: true,
  //         height: PosTextSize.size1,
  //         width: PosTextSize.size3,
  //         align: PosAlign.center,
  //         fontType: PosFontType.fontA,
  //         reverse: false,
  //         underline: false,
  //       ));
  //   ticket.text(DateTime.now().toString(),
  //       styles: PosStyles(
  //         bold: true,
  //         height: PosTextSize.size1,
  //         width: PosTextSize.size2,
  //         align: PosAlign.center,
  //         fontType: PosFontType.fontA,
  //         reverse: false,
  //         underline: false,
  //       ));
  //   VendedorModel mesero = await DBProvider.db.obtenerVendedor();
  //   List<String> nombre = mesero.nombre.split(RegExp("\\s+"));

  //   ticket.text('Mesero: ' + nombre[0],
  //       styles: PosStyles(
  //         bold: true,
  //         height: PosTextSize.size1,
  //         width: PosTextSize.size3,
  //         align: PosAlign.center,
  //         fontType: PosFontType.fontA,
  //         reverse: false,
  //         underline: false,
  //       ));
  //   ticket.text('Mesa 50',
  //       styles: PosStyles(
  //         bold: true,
  //         height: PosTextSize.size1,
  //         width: PosTextSize.size3,
  //         align: PosAlign.center,
  //         fontType: PosFontType.fontA,
  //         reverse: true,
  //         underline: false,
  //       ));

  //   ticket.emptyLines(1);
  //   ticket.row([
  //     PosColumn(
  //       text: 'Descripcion',
  //       width: 10,
  //       styles: PosStyles(align: PosAlign.left, underline: true),
  //     ),
  //     PosColumn(
  //       text: 'Cantidad',
  //       width: 2,
  //       styles: PosStyles(align: PosAlign.left, underline: true),
  //     ),
  //   ]);
  //   ticket.hr();

  //   //Esta!!!
  //   for (var producto in productos) {
  //     ticket.row([
  //       PosColumn(
  //         text: producto.nombre,
  //         width: 10,
  //         styles: PosStyles(align: PosAlign.left),
  //       ),
  //       PosColumn(
  //         text: producto.cantidad.toString(),
  //         width: 2,
  //         styles: PosStyles(align: PosAlign.left),
  //       ),
  //     ]);
  //   }

  //   ticket.feed(2);
  //   ticket.cut();
  //   ticket.beep(n: 3, duration: PosBeepDuration.beep450ms);
  //   return ticket;
  // }

  Widget _valorTotalBajo(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.infinity,
        height: 80.0,
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          borderRadius: BorderRadius.circular(50.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Total: \$ $_totalComanda',
              textAlign: TextAlign.end,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Expanded(child: SizedBox()),
          ],
        ),
      ),
    );
  }
}
