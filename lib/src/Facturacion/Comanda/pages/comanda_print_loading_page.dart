// import 'package:flutter/foundation.dart';
import 'package:collection/collection.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Home/models/ajustes_model.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';

// ignore: must_be_immutable
class ComandaPrintLoadingPage extends StatefulWidget {
  List<ProductoCuentaModel> listaProductosModel;

  ComandaPrintLoadingPage({@required this.listaProductosModel});

  @override
  _ComandaPrintLoadingPageState createState() =>
      _ComandaPrintLoadingPageState();
}

class _ComandaPrintLoadingPageState extends State<ComandaPrintLoadingPage> {
  List<List<dynamic>> listaImpresoras = []; //listado de las impresoras
  final Connectivity _connectivity = Connectivity();
  final prefs = PreferenciasUsuario();


  @override
  void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) async {
      await _initConexionInternet(_connectivity);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              backgroundColor: Color(0xffDD6E42),
              strokeWidth: 1.5,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Imprimiendo. Por favor espere.'),
          ],
        ),
      ),
    );
  }

  Future<void> _initConexionInternet(Connectivity connectivity) async {
    ConnectivityResult result;

    try {
      result = await connectivity.checkConnectivity();
    } catch (e) {
      print(e.toString());
    }

    return _updateConnectionStatus(result);
  }

//Determina que rutas seguir según la informacion de conexión a internet
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _procesarComanda();
        break;
      case ConnectivityResult.none:
        Navigator.pop(context);
        break;
    }
  }

  Future<void> _procesarComanda() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    PosPrintResult res;

    VendedorModel mesero = await DBProvider.db.obtenerVendedor();

    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    bool controlImpresora = true;

    try {
      await conexionNegocio.transaction((cnx) async {
        listaImpresoras = await cnx.query(
            "SELECT centrocostoid, impresora, nombre, numerocopiascomanda, control_impresion_comanda FROM datos.centroscosto WHERE sucursalid = @sucursalid",
            substitutionValues: {
              "sucursalid": prefs.sucursalid,
            });

        List<List<dynamic>> numeroTurno = await cnx.query(
            "SELECT CAST(numeroturno AS INTEGER) FROM datos.empresa WHERE codigoempresaid = @sucursalid",
            substitutionValues: {
              "sucursalid": prefs.sucursalid,
            });

        List<List<dynamic>> comanda = await cnx.query(
            "SELECT prefijo, CAST(numeroactual AS INTEGER), id FROM datos.documentosconsecutivos WHERE tipodocumento = '3' AND sucursalid = @sucursalid LIMIT 1",
            substitutionValues: {"sucursalid": prefs.sucursalid});

        await cnx.query(
            "UPDATE datos.empresa SET numeroturno = @numeroturno WHERE codigoempresaid = @sucursalid",
            substitutionValues: {
              "numeroturno": numeroTurno[0][0] + 1,
              "sucursalid": prefs.sucursalid
            });

        await cnx.query(
            'UPDATE datos.documentosconsecutivos SET numeroactual = @numero WHERE id = @id',
            substitutionValues: {
              "numero": comanda[0][1] + 1,
              "id": comanda[0][2]
            });

        for (var i = 0; i < widget.listaProductosModel.length; i++) {
          List<List<dynamic>> nuevoCentroCostoid = await cnx.query(
              "SELECT centrocostoid FROM datos.productos_esquemas WHERE sucursalid = @sucursalid AND productoid = @productoid AND esquemaid = @esquemaid",
              substitutionValues: {
                "sucursalid": prefs.sucursalid,
                "esquemaid": prefs.esquemaid,
                "productoid": widget.listaProductosModel[i].productoid
              });

          if (nuevoCentroCostoid.length > 0) {
            widget.listaProductosModel[i].centrocostoid =
                nuevoCentroCostoid[0][0];
          }
        }

        for (var impresora in listaImpresoras) {
//

          List<ProductoCuentaModel> productosComandar = [];
          for (var i = 0; i < widget.listaProductosModel.length; i++) {
            if (widget.listaProductosModel[i].centrocostoid == impresora[0] &&
                widget.listaProductosModel[i].numerocomanda == 0 &&
                widget.listaProductosModel[i].imprimeComanda == 1) {
              productosComandar.add(widget.listaProductosModel[i]);
            }
          }

          if (productosComandar.isNotEmpty && impresora[4] == 0) {
            //Se revisa que el listado de productos a comandar no esté vacío y se imprime desde la app

            //Todo el trabajo de la impresora
            const PaperSize paper = PaperSize.mm80;

            final profile = await CapabilityProfile.load();

            final printer = NetworkPrinter(paper, profile);

            AjustesModel ajustes = await DBProvider.db.obtenerAjustes();

            res = await printer.connect(impresora[1].toString().trim(),
                port: 9100); //Se conecta a la impresora

            //Aquí se revisa que la impresora haya respondido correctamente
            if (res == PosPrintResult.success) {
              //Agupación de productos por código
              List<ProductoCuentaModel> productosAgrupadosPorVendedor = [];

              if (ajustes.agruparcomanda == 1) {
                List<List<dynamic>> productosAgrupados = await cnx.query(
                    "SELECT productonombre, CAST(SUM(productocantidad) AS REAL), productocodigo, cuentadetalle.centrocostoid,  productogusto, productopresentacion, impresora, centroscosto.nombre, cuentadetalle.vendedorid, vendedor.nombre FROM datos.cuentadetalle LEFT JOIN datos.centroscosto ON centroscosto.centrocostoid = cuentadetalle.centrocostoid AND centroscosto.sucursalid = @sucursalid LEFT JOIN datos.vendedor ON cuentadetalle.vendedorid = vendedor.vendedorid WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND productoestado = 0 AND cuentadetalle.sucursalid = @sucursalid AND cuentadetalle.prefijocomanda IS NULL AND cuentadetalle.numerocomanda = 0 GROUP BY productocodigo, productogusto, productopresentacion, productonombre, cuentadetalle.centrocostoid, impresora, centroscosto.nombre, cuentadetalle.vendedorid, vendedor.nombre",
                    substitutionValues: {
                      "prefijocuenta":
                          cabeceraProvider.mesaCabecera.prefijocuenta,
                      "numerocuenta":
                          cabeceraProvider.mesaCabecera.numerocuenta,
                      "sucursalid": prefs.sucursalid,
                    });

                for (var productos in productosAgrupados) {
                  ProductoCuentaModel prod = ProductoCuentaModel();
                  prod.nombre = productos[0];
                  prod.cantidad = productos[1];
                  prod.codigo = productos[2];
                  prod.centrocostoid = productos[3];
                  prod.gustos = productos[4];
                  prod.presentacion = productos[5];
                  prod.ipimpresora = productos[6];
                  prod.nombreImpresora = productos[7];
                  prod.vendedorid = productos[8];
                  prod.nombreVendedor = productos[9];

                  productosAgrupadosPorVendedor.add(prod);
                }
              }

              var productosEnvioComanda =
                  productosAgrupadosPorVendedor.length > 0
                      ? productosAgrupadosPorVendedor
                      : productosComandar;
              for (var i = 0; i < impresora[3]; i++) {
                //Hace un ciclo con el campo numerocopiascomanda
                await processTicket(
                    productosEnvioComanda,
                    impresora,
                    numeroTurno[0][0],
                    comanda,
                    mesero,
                    cabeceraProvider,
                    printer,
                    ajustes);
              }

              printer.reset();

              printer.disconnect();

              await cnx.query(
                  "INSERT INTO datos.comandacabecera (prefijocomanda, numerocomanda, prefijocuenta, numerocuenta, mesahabitacionid, vendedorid, impresora, centrodecostoid, numeroturno, sucursalid, esquemaid, fechahora) VALUES(@prefijocomanda, @numerocomanda, @prefijocuenta, @numerocuenta, @mesahabitacionid, @vendedorid, @impresora, @centrodecostoid, @numeroturno, @sucursalid, @esquemaid, @fechahora) ",
                  substitutionValues: {
                    "prefijocomanda": comanda[0][0],
                    "numerocomanda": comanda[0][1] + 1,
                    "prefijocuenta":
                        cabeceraProvider.mesaCabecera.prefijocuenta,
                    "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                    "mesahabitacionid": cabeceraProvider.mesaCabecera.objetoid,
                    "vendedorid": 0,
                    "impresora": impresora[1].toString().trim(),
                    "centrodecostoid": impresora[0],
                    "numeroturno": numeroTurno[0][0] + 1,
                    "sucursalid": prefs.sucursalid,
                    "esquemaid": prefs.esquemaid,
                    "fechahora": DateTime.now().toLocal(),
                  });

              for (var i = 0; i < productosComandar.length; i++) {
                await cnx.query(
                  "UPDATE datos.cuentadetalle SET prefijocomanda = @prefijocomanda, numerocomanda = @numerocomanda WHERE cuentadetalleid = @id",
                  substitutionValues: {
                    "prefijocomanda": comanda[0][0],
                    "numerocomanda": comanda[0][1] + 1,
                    "id": productosComandar[i].id
                  },
                );

                await cnx.query(
                    "INSERT INTO datos.comandadetalle (prefijocomanda, numerocomanda, productoid, nombre, cantidad, valortotal, textogusto, centrodecostoid, sucursalid, producto, prefijocuenta, numerocuenta, presentacion, puesto) VALUES (@prefijocomanda, @numerocomanda, @productoid, @nombre, @cantidad, @valortotal, @textogusto, @centrodecostoid, @sucursalid, @producto, @prefijocuenta, @numerocuenta, @presentacion, @puesto)",
                    substitutionValues: {
                      "prefijocomanda": comanda[0][0],
                      "numerocomanda": comanda[0][1] + 1,
                      "productoid": productosComandar[i].productoid,
                      "nombre": productosComandar[i].nombre,
                      "cantidad": productosComandar[i].cantidad,
                      "valortotal": productosComandar[i].valortotal,
                      "textogusto": productosComandar[i].gustos,
                      "centrodecostoid": impresora[0],
                      "sucursalid": prefs.sucursalid,
                      "producto": productosComandar[i].codigo,
                      "prefijocuenta":
                          cabeceraProvider.mesaCabecera.prefijocuenta,
                      "numerocuenta":
                          cabeceraProvider.mesaCabecera.numerocuenta,
                      "presentacion": productosComandar[i].presentacion,
                      "puesto": productosComandar[i].puesto,
                    });
              }
            } else {
              cnx.cancelTransaction();
              return;
            }
          } else if (productosComandar.isNotEmpty && impresora[4] == 1) {
            //Este es el codigo para que maxxcontrol haga la impresión a una USB
            controlImpresora = false;
            for (var i = 0; i < productosComandar.length; i++) {
              await cnx.query(
                "UPDATE datos.cuentadetalle SET prefijocomanda = @prefijocomanda, numerocomanda = @numerocomanda WHERE cuentadetalleid = @id",
                substitutionValues: {
                  "prefijocomanda": comanda[0][0],
                  "numerocomanda": comanda[0][1] + 1,
                  "id": productosComandar[i].id
                },
              );

              await cnx.query(
                  "INSERT INTO datos.comandadetalle (prefijocomanda, numerocomanda, productoid, nombre, cantidad, valortotal, textogusto, centrodecostoid, sucursalid, producto, prefijocuenta, numerocuenta, presentacion, puesto) VALUES (@prefijocomanda, @numerocomanda, @productoid, @nombre, @cantidad, @valortotal, @textogusto, @centrodecostoid, @sucursalid, @producto, @prefijocuenta, @numerocuenta, @presentacion, @puesto)",
                  substitutionValues: {
                    "prefijocomanda": comanda[0][0],
                    "numerocomanda": comanda[0][1] + 1,
                    "productoid": productosComandar[i].productoid,
                    "nombre": productosComandar[i].nombre,
                    "cantidad": productosComandar[i].cantidad,
                    "valortotal": productosComandar[i].valortotal,
                    "textogusto": productosComandar[i].gustos,
                    "centrodecostoid": impresora[0],
                    "sucursalid": prefs.sucursalid,
                    "producto": productosComandar[i].codigo,
                    "prefijocuenta":
                        cabeceraProvider.mesaCabecera.prefijocuenta,
                    "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                    "presentacion": productosComandar[i].presentacion,
                    "puesto": productosComandar[i].puesto
                  });
            }
            await cnx.query(
                "INSERT INTO datos.comandacabecera (prefijocomanda, numerocomanda, prefijocuenta, numerocuenta, mesahabitacionid, vendedorid, impresora, centrodecostoid, numeroturno, sucursalid, esquemaid, fechahora, estado) VALUES(@prefijocomanda, @numerocomanda, @prefijocuenta, @numerocuenta, @mesahabitacionid, @vendedorid, @impresora, @centrodecostoid, @numeroturno, @sucursalid, @esquemaid, @fechahora, @estado) ",
                substitutionValues: {
                  "prefijocomanda": comanda[0][0],
                  "numerocomanda": comanda[0][1] + 1,
                  "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                  "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                  "mesahabitacionid": cabeceraProvider.mesaCabecera.objetoid,
                  "vendedorid": mesero.vendedorid,
                  "impresora": "",
                  "centrodecostoid": impresora[0],
                  "numeroturno": numeroTurno[0][0] + 1,
                  "sucursalid": prefs.sucursalid,
                  "esquemaid": mesero.esquemaid,
                  "fechahora": DateTime.now().toLocal(),
                  "estado": 4,
                });
          }
        }
      });
      await conexionNegocio.close();
      if (!mounted) return;

      if ((res != null && mounted) || !controlImpresora) {
        if (res == PosPrintResult.success || !controlImpresora) {
          Navigator.pop(context);
        } else if (res != PosPrintResult.success &&
            res != PosPrintResult.printInProgress) {
          _respuestaImpresora(res);
        }
      } else if (res == null && mounted || controlImpresora) {
        Navigator.pop(context,
            'Productos sin impresora asignada\n\nElimine el producto y vuelva a agregarlo.\n\nSi el error persiste, comuníquese con el encargado del establecimiento.\n\nEs posible que no haya una impresora asociada.');
      }
    } catch (e) {
      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
      print(e);
    }
  }

  //===========================

  void _respuestaImpresora(
    PosPrintResult res,
    /*String ipImpresora, String nombre*/
  ) {
    if (res == PosPrintResult.printInProgress) {
      print("imprimiendo");
    } else if (res == PosPrintResult.printerNotSelected ||
        res == PosPrintResult.ticketEmpty ||
        res == PosPrintResult.timeout) {
      if (!mounted) return;
      if (Navigator.canPop(context)) {
        Navigator.pop(context,
            'Error al intentar imprimir algunos productos. Verifica la impresora correspondiente a los produtos no comandados.');
      }
    } else if (res == PosPrintResult.success) {
      print("correcto");
      if (!mounted) return;
      // if (Navigator.canPop(context)) {
      //   Navigator.pop(context);
      // }
    } else if (res == PosPrintResult.ticketEmpty) {
      print("Error generando el tiquete en");
      if (!mounted) return;
      if (Navigator.canPop(context)) {
        Navigator.pop(context,
            'Error al intentar imprimir algunos productos. Verifica la impresora correspondiente a los produtos no comandados.');
      }
      // } else if (res.value == PosPrintResult.timeout.value) {
      //   print("No se pudo conectar a la impresora de");
      //   if (Navigator.canPop(context)) {
      //     Navigator.pop(context, 'No se pudo conectar a la impresora de $nombre');
      //   }
      // }
    }
  }

  Future<void> cabeceraTicket(
      List<dynamic> centroCosto,
      int numeroTurno,
      List<List<dynamic>> comanda,
      VendedorModel mesero,
      CabeceraService cabeceraProvider,
      NetworkPrinter ticket) async {
    ticket.text('# TURNO ' + numeroTurno.toString(),
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size2,
          width: PosTextSize.size2,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: true,
        ));
    ticket.emptyLines(1);
    ticket.text(centroCosto[2].toString().trim(),
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size3,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));
    ticket.hr(ch: '_');

    ticket.text(DateTime.now().toString(),
        styles: PosStyles(
          bold: true,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          align: PosAlign.center,
          fontType: PosFontType.fontA,
          reverse: false,
          underline: false,
        ));

    ticket.row([
      PosColumn(
        text: 'Comanda: ',
        width: 4,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
      PosColumn(
        text: comanda[0][0].toString() +
            (comanda[0][1] + 1).toString() +
            "/" +
            cabeceraProvider.mesaCabecera.prefijocuenta.toString() +
            cabeceraProvider.mesaCabecera.numerocuenta.toString(),
        width: 8,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
    ]);

    // List<String> nombre = mesero.nombre.split(RegExp("\\s+"));

    ticket.row([
      PosColumn(
        text: 'Mesero: ',
        width: 6,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
      PosColumn(
        text: "",
        width: 6,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
    ]);

    ticket.row([
      PosColumn(
        text: 'Mesa: ',
        width: 5,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
      PosColumn(
        text: cabeceraProvider.mesaCabecera.nombreMesa,
        width: 7,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size3),
      ),
    ]);

    ticket.row([
      PosColumn(
        text: 'Alias: ',
        width: 6,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
      PosColumn(
        text: cabeceraProvider.mesaCabecera.nombreCuenta != null
            ? cabeceraProvider.mesaCabecera.nombreCuenta
            : "Sin alias",
        width: 6,
        styles: PosStyles(
            align: PosAlign.left, underline: false, width: PosTextSize.size2),
      ),
    ]);

    if (cabeceraProvider.mesaCabecera.fechahoravencimiento != null &&
        cabeceraProvider.mesaCabecera.fechahoravencimiento != "" &&
        DateTime.now().toLocal().isBefore(DateTime.parse(
            cabeceraProvider.mesaCabecera.fechahoravencimiento))) {
      ticket.row([
        PosColumn(
          text: 'Entrega: ',
          width: 6,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size2),
        ),
        PosColumn(
          text:
              DateTime.parse(cabeceraProvider.mesaCabecera.fechahoravencimiento)
                  .toLocal()
                  .toString()
                  .substring(0, 16),
          width: 6,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size1),
        ),
      ]);
    }

    if (cabeceraProvider.mesaCabecera.nombreCliente != null &&
        cabeceraProvider.mesaCabecera.nitCliente != null &&
        cabeceraProvider.mesaCabecera.nombreCliente != "" &&
        cabeceraProvider.mesaCabecera.nitCliente != "") {
      ticket.hr(ch: "_");

      ticket.row([
        PosColumn(
          text: 'Cliente:',
          width: 4,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size2),
        ),
        PosColumn(
          text: cabeceraProvider.mesaCabecera.nombreCliente,
          width: 8,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size1),
        ),
      ]);
      ticket.row([
        PosColumn(
          text: 'Id:',
          width: 4,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size2),
        ),
        PosColumn(
          text: cabeceraProvider.mesaCabecera.nitCliente,
          width: 8,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size1),
        ),
      ]);
      // ticket.row([
      //   PosColumn(
      //     text: 'Dir:',
      //     width: 4,
      //     styles: PosStyles(
      //         align: PosAlign.left, underline: false, width: PosTextSize.size2),
      //   ),
      //   PosColumn(
      //     text: cabeceraProvider.mesaCabecera.direccionCliente,
      //     width: 8,
      //     styles: PosStyles(
      //         align: PosAlign.left, underline: false, width: PosTextSize.size1, ),
      //   ),
      // ]);
      ticket.text(cabeceraProvider.mesaCabecera.direccionCliente);

      ticket.row([
        PosColumn(
          text: 'Tel:',
          width: 4,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size2),
        ),
        PosColumn(
          text: cabeceraProvider.mesaCabecera.telefonoCliente,
          width: 8,
          styles: PosStyles(
            align: PosAlign.left,
            underline: false,
            width: PosTextSize.size1,
          ),
        ),
      ]);

      ticket.row([
        PosColumn(
          text: 'Ciu:',
          width: 4,
          styles: PosStyles(
              align: PosAlign.left, underline: false, width: PosTextSize.size2),
        ),
        PosColumn(
          text: cabeceraProvider.mesaCabecera.ciudadCliente,
          width: 8,
          styles: PosStyles(
            align: PosAlign.left,
            underline: false,
            width: PosTextSize.size1,
          ),
        ),
      ]);
    }

    ticket.emptyLines(1);
    ticket.row([
      PosColumn(
        text: 'Descripcion',
        width: 10,
        styles: PosStyles(align: PosAlign.left, underline: true),
      ),
      PosColumn(
        text: 'cant',
        width: 2,
        styles: PosStyles(align: PosAlign.right, underline: true),
      ),
    ]);
    ticket.hr();
  }

  Future<void> processTickerPorCombo(
      List<ProductosCombosModel> productos,
      List<dynamic> centroCosto,
      int numeroTurno,
      List<List<dynamic>> comanda,
      VendedorModel mesero,
      CabeceraService cabeceraProvider,
      NetworkPrinter ticket) async {
    await cabeceraTicket(
        centroCosto, numeroTurno, comanda, mesero, cabeceraProvider, ticket);

    productos.sort((a, b) => a.nombre.compareTo(b.nombre));

    for (var producto in productos) {
      ticket.row([
        PosColumn(
          text: producto.nombre + "(" + producto.presentacion + ")",
          width: 10,
          styles: PosStyles(
            align: PosAlign.left,
            height: PosTextSize.size2,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: producto.cantidadcombo.round().toString(),
          width: 2,
          styles: PosStyles(
              align: PosAlign.left, width: PosTextSize.size4, bold: true),
        ),
      ]);
      if (producto.gusto != null && producto.gusto.length > 2) {
        ticket.emptyLines(1);

        ticket.text("GUSTOS: " + producto.gusto,
            styles: PosStyles(align: PosAlign.left));
      }
      ticket.hr(ch: "_");
    }

    ticket.feed(2);
    ticket.cut();
  }

  Future<void> processTicket(
      List<ProductoCuentaModel> productos,
      List<dynamic> centroCosto,
      int numeroTurno,
      List<List<dynamic>> comanda,
      VendedorModel mesero,
      CabeceraService cabeceraProvider,
      NetworkPrinter ticket,
      AjustesModel ajustes) async {
    //Aqui procesa la comanda normalmente

    List<ProductosCombosModel> listaCombosAComandar = [];

    numeroTurno += 1;

    await cabeceraTicket(
        centroCosto, numeroTurno, comanda, mesero, cabeceraProvider, ticket);

    //Esta!!!
    for (var producto in productos) {
      ticket.text(producto.nombre, styles: PosStyles(align: PosAlign.left, width:
                ajustes.fontsize == 1 ? PosTextSize.size2 : PosTextSize.size1,));
      ticket.row([
        PosColumn(
          text: '',
          width: 10,
          styles: PosStyles(
            align: PosAlign.left,
            width:
                ajustes.fontsize == 1 ? PosTextSize.size2 : PosTextSize.size1,
            fontType: PosFontType.fontA,
          ),
        ),
        PosColumn(
          text: producto.cantidad.round().toString(),
          width: 2,
          styles: PosStyles(
              align: PosAlign.right,
              width:
                  ajustes.fontsize == 1 ? PosTextSize.size2 : PosTextSize.size1,
              bold: true),
        ),
      ]);

      if (producto.presentacion != 'Unidad' && producto.presentacion != '') {
        ticket.emptyLines(1);

        ticket.text("PRESENTACION: " + producto.presentacion,
            styles: PosStyles(align: PosAlign.left));
      }

      if (producto.gustos != null && producto.gustos.length > 2) {
        ticket.emptyLines(1);

        ticket.text("GUSTOS: " + producto.gustos,
            styles: PosStyles(align: PosAlign.left));
      }

      if (producto?.combos != null) {
        ticket.emptyLines(1);
        ticket.text("COMBO: \n");
        for (var item in producto.combos) {
          ticket.row([
            PosColumn(
              text: item.cantidadcombo.toString() +
                  " - " +
                  item.nombre +
                  "  (" +
                  item.gusto +
                  ")",
              width: 12,
              styles: PosStyles(align: PosAlign.left),
            ),
          ]);

          item.presentacion = producto.nombre;

          if (item.centrocostoid != centroCosto[0]) {
            listaCombosAComandar.add(item);
          }
        }
      }
      ticket.hr(ch: "_");
    }

    ticket.feed(2);
    ticket.cut();
    //ticket.beep(n: 3, duration: PosBeepDuration.beep450ms);

    if (listaCombosAComandar.length > 0) {
      for (var impresora in listaImpresoras) {
        List<ProductosCombosModel> productosAComandar = [];

        for (var productoCombo in listaCombosAComandar) {
          if (productoCombo.centrocostoid == impresora[0]) {
            productosAComandar.add(productoCombo);
          }
        }

        if (productosAComandar.length > 0) {
          const PaperSize paper = PaperSize.mm80;

          final profile = await CapabilityProfile.load();

          final printer = NetworkPrinter(paper, profile);

          final res = await printer.connect(impresora[1].toString().trim(),
              port: 9100); //Se conecta a la impresora

          if (res == PosPrintResult.success) {
            await processTickerPorCombo(productosAComandar, impresora,
                numeroTurno, comanda, mesero, cabeceraProvider, printer);

            printer.disconnect();
          }
        }
      }
    }
  }
}
