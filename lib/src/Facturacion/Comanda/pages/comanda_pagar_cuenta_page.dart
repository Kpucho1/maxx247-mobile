import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/pages/comanda_detalles_tarjeta_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/pages/imprimir_factura_loading_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/services/enviar_factura_electronica.dart';
import 'package:maxxshop/src/Facturacion/Denominaciones/model/denominacion_model.dart';
import 'package:maxxshop/src/Facturacion/Denominaciones/services/denominacion_postgres_service.dart';

class ComandaPagarCuentaPage extends StatefulWidget {
  ComandaPagarCuentaPage({Key key}) : super(key: key);

  @override
  _ComandaPagarCuentaPageState createState() => _ComandaPagarCuentaPageState();
}

class _ComandaPagarCuentaPageState extends State<ComandaPagarCuentaPage> {
  TextEditingController _totalEfectivoController = TextEditingController();
  TextEditingController _totalCreditoController = TextEditingController();
  TextEditingController _totalTarjetaDebitoController = TextEditingController();
  TextEditingController _totalTarjetaCreditoController =
      TextEditingController();
  TextEditingController _totalSeparadosController = TextEditingController();
  TextEditingController _totalBancosController = TextEditingController();
  TextEditingController _totalBonosController = TextEditingController();

  int totalAPagar = 0;
  DenominacionModel _denominacionSeleccionada = DenominacionModel(id: 0);

  @override
  void initState() {
    super.initState();
    totalAPagar = context.read<CabeceraService>().mesaCabecera.totalapagar;

    _totalEfectivoController.text = totalAPagar.toString();
    _totalCreditoController.text = "0";
    _totalTarjetaDebitoController.text = "0";
    _totalTarjetaCreditoController.text = "0";
    _totalSeparadosController.text = "0";
    _totalBancosController.text = "0";
    _totalBonosController.text = "0";
  }

  @override
  void dispose() {
    super.dispose();
    _totalEfectivoController.dispose();
    _totalCreditoController.dispose();
    _totalTarjetaDebitoController.dispose();
    _totalTarjetaCreditoController.dispose();
    _totalSeparadosController.dispose();
    _totalBancosController.dispose();
    _totalBonosController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: KeyboardDismisser(
        child: WillPopScope(
          child: Scaffold(
            body: SingleChildScrollView(
              child: _formularioPagarCuenta(),
              physics: BouncingScrollPhysics(),
            ),
          ),
          onWillPop: () {
            Navigator.pop(context);
            return;
          },
        ),
      ),
    );
  }

  void _actualizarPrecios(String value) {
    _totalEfectivoController.text = value != '0' || value != ''
        ? (totalAPagar - int.parse(value ?? '0')).toString()
        : totalAPagar.toString();
  }

  _formularioPagarCuenta() {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.1,
        vertical: size.height * 0.05,
      ),
      child: Form(
        child: Column(
          children: [
            Text(
              "Total \$" + totalAPagar.toString(),
              style: TextStyle(fontSize: 24.0, color: Colors.red[300]),
            ),
            SizedBox(
              height: 20.0,
            ),
            _totalEfectivo(),
            SizedBox(
              height: 15.0,
            ),
            _totalCredito(),
            SizedBox(
              height: 15.0,
            ),
            _totalTarjetaDebito(),
            SizedBox(
              height: 15.0,
            ),
            _totalTarjetaCredito(),
            SizedBox(
              height: 15.0,
            ),
            _totalSeparados(),
            SizedBox(
              height: 15.0,
            ),
            _totalBancos(),
            SizedBox(
              height: 15.0,
            ),
            _totalBonos(),
            SizedBox(
              height: 25.0,
            ),
            _botonFacturaPos(),
            SizedBox(
              height: 10.0,
            ),
            _botonFacturaElectronica(),
          ],
        ),
      ),
    );
  }

  Widget _totalEfectivo() {
    return FocusScope(
      child: TextFormField(
        controller: _totalEfectivoController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Efectivo',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
      ),
    );
  }

  Widget _totalCredito() {
    return FocusScope(
      child: TextFormField(
        controller: _totalCreditoController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Credito',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
        onChanged: (value) {
          _actualizarPrecios(value);
        },
      ),
    );
  }

  Widget _totalTarjetaDebito() {
    return FocusScope(
      child: TextFormField(
        controller: _totalTarjetaDebitoController,
        onTap: () {
          MesaCabecera datosCabecera =
              context.read<CabeceraService>().mesaCabecera;

          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ComandaDetalleTarjePage(
                          numeroCuenta: datosCabecera.numerocuenta,
                          prefijoCuenta: datosCabecera.prefijocuenta, tipoTarjeta: 1,)))
              .then((value) {
            if ((int.parse(_totalEfectivoController.text) - value) <=
                datosCabecera.totalapagar) {
              _totalEfectivoController.text =
                  (int.parse(_totalEfectivoController.text) - value).toString();
            }
            _totalTarjetaDebitoController.text =
                value > 0 ? value.toString() : '0';
          });
        },
        readOnly: true,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Tarjeta Debito',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
      ),
    );
  }

  Widget _totalTarjetaCredito() {
    return FocusScope(
      child: TextFormField(
        onTap: () {
          MesaCabecera datosCabecera =
              context.read<CabeceraService>().mesaCabecera;

          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ComandaDetalleTarjePage(
                          numeroCuenta: datosCabecera.numerocuenta,
                          prefijoCuenta: datosCabecera.prefijocuenta, tipoTarjeta: 2, )))
              .then((value) {
            if ((int.parse(_totalEfectivoController.text) - value) <=
                datosCabecera.totalapagar) {
              _totalEfectivoController.text =
                  (int.parse(_totalEfectivoController.text) - value).toString();
            }
            _totalTarjetaCreditoController.text =
                value > 0 ? value.toString() : '0';
          });
        },
        readOnly: true,
        controller: _totalTarjetaCreditoController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Tarjeta Credito',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
      ),
    );
  }

  Widget _totalSeparados() {
    return FocusScope(
      child: TextFormField(
        controller: _totalSeparadosController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Separados',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
        onChanged: (value) {
          _actualizarPrecios(value);
        },
      ),
    );
  }

  Widget _totalBancos() {
    return FocusScope(
      child: TextFormField(
        controller: _totalBancosController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Bancos',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
        onChanged: (value) {
          _actualizarPrecios(value);
        },
      ),
    );
  }

  Widget _totalBonos() {
    return FocusScope(
      child: TextFormField(
        controller: _totalBonosController,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Total Bonos',
            suffixIcon: Icon(Icons.money),
            labelStyle: TextStyle(
              color: Colors.black,
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(width: 1, style: BorderStyle.none))),
        onChanged: (value) {
          _actualizarPrecios(value);
        },
      ),
    );
  }

  Widget _botonFacturaPos() {
    final size = MediaQuery.of(context).size;

    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        primary: Colors.blue,
        padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.2, vertical: size.height * 0.01),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      icon: Icon(
        Icons.monetization_on,
        color: Colors.white,
      ),
      label: Text('Factura Pos'),
      onPressed: () async {
        if (int.parse(_totalEfectivoController.text) > 0) {
          if ( await _dialogDenominaciones()) {
            Map<String, dynamic> formasPago = {
                "efectivo": int.parse(_totalEfectivoController.text),
                "credito": int.parse(_totalCreditoController.text),
                "tarjeta_debito": int.parse(_totalTarjetaDebitoController.text),
                "tarjeta_credito": int.parse(_totalTarjetaCreditoController.text),
                "separados": int.parse(_totalSeparadosController.text),
                "bancos": int.parse(_totalBancosController.text),
                "bonos": int.parse(_totalBonosController.text),
                "cambio": _denominacionSeleccionada.id != 0 ? _denominacionSeleccionada.valor - int.parse(_totalEfectivoController.text) : 0
              };
              
            Navigator.push(context, MaterialPageRoute(builder: (context) => ImprimirFacturaLoadingPage(formasPago: formasPago,)));
          }
          
        }

       
      },
    );
  }

  Future<bool> _dialogDenominaciones() async{
    DenominacionPostgresService denominacionPostgresService = DenominacionPostgresService();
    return showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text("Denominaciones"),
              content: FutureBuilder(
                future: denominacionPostgresService.obtenerDenominaciones(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  else{
                    List<DenominacionModel> listadoDenominaciones = snapshot.data;
                    return Container(
                      height: 200.0,
                      width: 200.0,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: listadoDenominaciones.length,
                        itemBuilder: (context, index) {
                          return Card(
                            child: ListTile(
                              title: Text(listadoDenominaciones[index].valor.toString()),
                              leading: _denominacionSeleccionada.id == listadoDenominaciones[index].id ? Icon(Icons.check, color: Colors.green,) : SizedBox(),
                              onTap: (){
                                setState(() {
                                  _denominacionSeleccionada = listadoDenominaciones[index];
                                });
                              },
                            ),
                          );
                        },
                      ),
                    );
                  }
                },
              ),
              actions: [
                TextButton(
                  child: Text("Cancelar"),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                TextButton(
                  child: Text("Continuar"),
                  onPressed: () {
                     Navigator.pop(context, true);
                  },
                )
              ],
            );
          }
        );
      },
    );
  }

  Widget _botonFacturaElectronica() {
    final size = MediaQuery.of(context).size;

    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        primary: Colors.blue,
        padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.2, vertical: size.height * 0.01),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      icon: Icon(
        Icons.monetization_on,
        color: Colors.white,
      ),
      label: Text('Factura Electrónica'),
      onPressed: () async {
        MesaCabecera datosCabecera =
            context.read<CabeceraService>().mesaCabecera;

        if (datosCabecera.clienteid != 0) {
          EnviarFacturaElectronica enviarFacturaService =
              EnviarFacturaElectronica(
                  numerocuenta: datosCabecera.numerocuenta,
                  prefijocuenta: datosCabecera.prefijocuenta,
                  cliente: datosCabecera.clienteid);

          Response res = await enviarFacturaService.enviar();

          await _dialogRespuestaFacturaElectronica(context, res);
        } else {
          await _dialogClienteSinSeleccionar(context);
        }
      },
    );
  }

  Future _dialogRespuestaFacturaElectronica(
      BuildContext context, Response res) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Respuesta Factura electrónica"),
        content: SingleChildScrollView(child: Text(res.body)),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            child: Text("Cerrar",
                style: TextStyle(color: Colors.white, fontSize: 18.0)),
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.red)),
                padding: EdgeInsets.all(10.0)),
          ),
        ],
      ),
    );
  }

  Future _dialogClienteSinSeleccionar(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Error"),
        content: SingleChildScrollView(
            child: Text("No se ha seleccionado un cliente")),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            child: Text("Cerrar",
                style: TextStyle(color: Colors.white, fontSize: 18.0)),
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.red)),
                padding: EdgeInsets.all(10.0)),
          ),
        ],
      ),
    );
  }
}