import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/services/comanda_postgres_service.dart';
import 'package:maxxshop/src/Facturacion/Comanda/services/imprimir_factura_pos.dart';
import 'package:maxxshop/src/Empresa/models/documentosconsecutivos_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesas_page.dart';
import 'package:provider/src/provider.dart';

class ImprimirFacturaLoadingPage extends StatefulWidget {
  final Map<String, dynamic> formasPago;
  ImprimirFacturaLoadingPage({Key  key, this.formasPago}) : super(key: key);

  @override
  State<ImprimirFacturaLoadingPage> createState() => _ImprimirFacturaLoadingPageState();
}

class _ImprimirFacturaLoadingPageState extends State<ImprimirFacturaLoadingPage> {
  @override
  void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _procesarFactura(context);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              backgroundColor: Color(0xffDD6E42),
              strokeWidth: 1.5,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Imprimiendo factura. Por favor espere.'),
          ],
        ),
      ),
    );
  }

  Future<void> _procesarFactura(BuildContext context) async {

    try {  
       MesaCabecera datosCabecera = context.read<CabeceraService>().mesaCabecera;


        Map<String, dynamic> datosCliente = {
          "nombre": datosCabecera.nombreCliente,
          "direccion": datosCabecera.direccionCliente,
          "identificacion": datosCabecera.nitCliente,
          "telefono": datosCabecera.telefonoCliente,
          "ciudad": datosCabecera.ciudadCliente,
        };

        ComandaPostgresService comandaPostgresService = ComandaPostgresService();

        DocumentoConsecutivoModel consecutivo = await comandaPostgresService.crearFacturaPos(datosCabecera.prefijocuenta, datosCabecera.numerocuenta, widget.formasPago);

        ImprimirFacturaPos pos = ImprimirFacturaPos(
            prefijocuenta: datosCabecera.prefijocuenta,
            numerocuenta: datosCabecera.numerocuenta,
            nombremesa: datosCabecera.nombreMesa,
            formasPago: widget.formasPago,
            cliente: datosCliente,
            consecutivo: consecutivo
        );
        await pos.imprimir();
      
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MesasPage(),
        ),
      ).then((value) => Navigator.pop(context));
    } catch (e) {
      //print('de aca viene el error');
      _alertaSinConexion(context);
    }
  }

  Future _alertaSinConexion(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          'Error al intentar conectar. Por favor intenta de nuevo.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}