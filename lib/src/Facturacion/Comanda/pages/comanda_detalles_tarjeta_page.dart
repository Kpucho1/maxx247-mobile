import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

import 'package:maxxshop/src/Facturacion/Comanda/services/comanda_postgres_service.dart';

class ComandaDetalleTarjePage extends StatefulWidget {
  final String prefijoCuenta;
  final int numeroCuenta;
  final int tipoTarjeta;

  ComandaDetalleTarjePage({this.prefijoCuenta, this.numeroCuenta, this.tipoTarjeta});

  @override
  _ComandaDetalleTarjePageState createState() =>
      _ComandaDetalleTarjePageState();
}

class _ComandaDetalleTarjePageState extends State<ComandaDetalleTarjePage> {
  TextEditingController _numeroAprobacionController = TextEditingController();
  TextEditingController _totalPropinaController = TextEditingController();
  TextEditingController _totalPagadoController = TextEditingController();

  ComandaPostgresService postgresService = ComandaPostgresService();

  int totalTransaccion = 0;

  var _future;
  @override
  void initState() {
    super.initState();

    _future = postgresService.retornarPagosTarjeta(
        widget.prefijoCuenta, widget.numeroCuenta, widget.tipoTarjeta);

    _numeroAprobacionController.text = "";
    _totalPropinaController.text = "0";
    _totalPagadoController.text = "0";
  }

  @override
  void dispose() {
    super.dispose();
    _numeroAprobacionController.dispose();
    _totalPropinaController.dispose();
    _totalPagadoController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: KeyboardDismisser(
        child: WillPopScope(
          child: Scaffold(
            appBar: AppBar(
              title: Text("Pago con tarjeta"),
              centerTitle: true,
            ),
            body: SingleChildScrollView(
              child: _formularioPagarCuenta(),
              physics: BouncingScrollPhysics(),
            ),
          ),
          onWillPop: () {
            Navigator.pop(context, totalTransaccion);
            return;
          },
        ),
      ),
    );
  }

  _formularioPagarCuenta() {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.1,
        vertical: size.height * 0.05,
      ),
      child: Form(
        child: Column(
          children: [
            _numeroAprobacion(),
            SizedBox(
              height: 15.0,
            ),
            _totalPropina(),
            SizedBox(
              height: 15.0,
            ),
            _totalPagado(),
            SizedBox(
              height: 15.0,
            ),
            SizedBox(
              height: 25.0,
            ),
            _botonAgregarFormaPago(),
            SizedBox(
              height: 15.0,
            ),
            Container(height: 250, child: _listadoPagosAdicionados())
          ],
        ),
      ),
    );
  }

  Widget _numeroAprobacion() {
    return TextFormField(
      controller: _numeroAprobacionController,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: '# Aprobacion',
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, style: BorderStyle.none))),
    );
  }

  Widget _totalPropina() {
    return TextFormField(
      controller: _totalPropinaController,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: 'Propina',
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, style: BorderStyle.none))),
    );
  }

  Widget _totalPagado() {
    return TextFormField(
      controller: _totalPagadoController,
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          labelText: 'Pagado',
          labelStyle: TextStyle(
            color: Colors.black,
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(width: 1, style: BorderStyle.none))),
    );
  }

  Widget _botonAgregarFormaPago() {
    final size = MediaQuery.of(context).size;

    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        primary: Colors.blue,
        padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.2, vertical: size.height * 0.01),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      icon: Icon(
        Icons.add,
        color: Colors.white,
      ),
      label: Text('Agregar'),
      onPressed: () async {

        if (_totalPagadoController.text == '' || _totalPagadoController.text == '0') {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.cyan[900],
              content: Text(
                'No se ha especificado un valor',
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                15.0,
              )),
            ),
          );
        }
        else{
          Map<String, dynamic> datos = {
            "prefijocuenta": widget.prefijoCuenta,
            "numerocuenta": widget.numeroCuenta,
            "valor": int.parse(_totalPagadoController.text),
            "facturada": int.parse(_totalPagadoController.text) +
                int.parse(_totalPropinaController.text),
            "propina": int.parse(_totalPropinaController.text),
            "documento": _numeroAprobacionController.text,
            "tipoTarjeta": widget.tipoTarjeta,
          };

          totalTransaccion += datos["facturada"];

          await postgresService.agregarFormasPago(datos);

          setState(() {
            _future = postgresService.retornarPagosTarjeta(
                widget.prefijoCuenta, widget.numeroCuenta, widget.tipoTarjeta);
            _numeroAprobacionController.clear();
            _totalPagadoController.text = '0';
            _totalPropinaController.text = '0';
          });
        }
        
      },
    );
  }

  Widget _listadoPagosAdicionados() {
    return FutureBuilder(
      future: _future,
      builder: (context, AsyncSnapshot<List<List<dynamic>>> snapshot) {
        final data = snapshot.data;

        if (!snapshot.hasData) {
          return Center(
            child: Text("No se han agregado pagos"),
          );
        } else {
          return ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return Card(
                child: ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.delete),
                    color: Colors.red,
                    onPressed: () async {
                      await postgresService.eliminarPagoTarjeta(data[index][0]);

                      totalTransaccion -= data[index][3];

                      setState(() {
                        _future = postgresService.retornarPagosTarjeta(
                            widget.prefijoCuenta, widget.numeroCuenta, widget.tipoTarjeta);
                      });
                    },
                  ),
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("# de aprobación: " + data[index][1]),
                      Text("Pagado: " + data[index][3].toString()),
                      Text("Propina: " + data[index][4].toString())
                    ],
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }
}