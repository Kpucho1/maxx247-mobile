import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/Facturacion/Comanda/fragments/comanda_fragment.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/fragments/botones_inferiores_comanda_fragment.dart';
import 'package:maxxshop/src/Facturacion/Comanda/bloc/operaciones_comanda/operaciones_comanda_bloc.dart';

// ignore: must_be_immutable
class ComandaPage extends StatefulWidget {
  Function callback;

  ComandaPage({this.callback});
  _ComandaPageState createState() => _ComandaPageState();
}

class _ComandaPageState extends State<ComandaPage> {
  bool ignoring = false;
  int productosSinComandar = 0;

  //Al ser este el widget contenedor de la ComandaFragment,
  //se encargará de enviar el evento de carga de productos al bloc

  @override
  void initState() {
    super.initState();

    final numeroCuenta =
        context.read<CabeceraService>().mesaCabecera.numerocuenta;
    final prefijoCuenta =
        context.read<CabeceraService>().mesaCabecera.prefijocuenta;

    context.read<OperacionesComandaBloc>().add(OnCargarProductosComanda(
          numeroCuenta: numeroCuenta,
          prefijoCuenta: prefijoCuenta,
        ));
  }

  @override
  Widget build(BuildContext context) {
    //La construccion de la lista de productos de la comanda pasa a ComandaFragment y los botones
    //pasan a BotonesInferioresFragment

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: ComandaFragment(),
          ),
          LimitedBox(
            maxHeight: 75.0,
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                color: Colors.grey,
                width: 0.5,
              )),
              child: BotonesInferioresFragment(callback: widget.callback),
            ),
          ),
        ],
      ),
    );
  }
}
