import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'operaciones_impresora_event.dart';
part 'operaciones_impresora_state.dart';

class OperacionesImpresoraBloc extends Bloc<OperacionesImpresoraEvent, OperacionesImpresoraState> {
  OperacionesImpresoraBloc() : super(OperacionesImpresoraInitial());

  @override
  Stream<OperacionesImpresoraState> mapEventToState(
    OperacionesImpresoraEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
