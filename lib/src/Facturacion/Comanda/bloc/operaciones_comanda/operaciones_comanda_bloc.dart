import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:maxxshop/src/helpers/Adapters/I_Wrapper_Productos.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';

part 'operaciones_comanda_event.dart';
part 'operaciones_comanda_state.dart';

class OperacionesComandaBloc
    extends Bloc<OperacionesComandaEvent, OperacionesComandaState> {
  final IWrapperProductosComanda wrapperProductos;

  OperacionesComandaBloc({this.wrapperProductos})
      : super(OperacionesComandaInitial());

  @override
  Stream<OperacionesComandaState> mapEventToState(
    OperacionesComandaEvent event,
  ) async* {
    //
    final currentState = state;

    if (event is OnCargarProductosComanda) {
      try {
        //
        if (currentState is OperacionesComandaInitial) {
          //

          final res = await this
              .wrapperProductos
              .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }

        if (currentState is CargaListaProductosComandaState) {
          // final res = await this
          //     .wrapperProductos
          //     .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          // yield res.isEmpty
          //     ? ListaVaciaComandaState()
          //     : currentState.copyWith(
          //         totalCuenta: res["TotalComanda"],
          //         lista: res["ListaProductos"],
          //         productosNoComandados: res["ProductosNoComandados"],
          //       );

          yield OperacionesComandaInitial();

          final res = await this
              .wrapperProductos
              .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }

        if (currentState is ListaVaciaComandaState) {
          yield OperacionesComandaInitial();

          final res = await this
              .wrapperProductos
              .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }

        if (currentState is ErrorCargaProductosComandaState) {
          yield OperacionesComandaInitial();

          final res = await this
              .wrapperProductos
              .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          print(res.isEmpty);

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }

        if (currentState is ErrorEliminarProductoDeComanda) {
          yield OperacionesComandaInitial();

          final res = await this
              .wrapperProductos
              .getProductosComanda(event.prefijoCuenta, event.numeroCuenta);

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }
      } catch (_) {
        //
        yield ErrorCargaProductosComandaState();
      }
    }

    //=========== Eventos y estados referentes a la eliminación de productos ===========

    if (event is OnEliminarProducto) {
      try {
        if (currentState is CargaListaProductosComandaState) {
          //
          yield EliminarProductoLoadInProgress();

          final res = await wrapperProductos.eliminarProductoComanda(
            event.productoId,
            event.prefijoCuenta,
            event.numeroCuenta,
          );

          //Se usa para ejecutar una accion en el bloc listener
          yield EliminadoProductoDeComanda();

          yield res.isEmpty
              ? ListaVaciaComandaState()
              : CargaListaProductosComandaState(
                  total: res["TotalComanda"],
                  lista: res["ListaProductos"],
                  productosNoComandados: res["ProductosNoComandados"],
                );
        }
      } catch (e) {
        //
        print(e);
        yield ErrorEliminarProductoDeComanda();
      }
    }

    //=========== Eventos y estados referentes a la actualizacion de productos ===========

    if (event is OnActualizarProducto) {
      try {
        //

        if (currentState is CargaListaProductosComandaState) {
          yield ActualizarProductoLoadInProgress();

          final res = await wrapperProductos.actualizarProductoComanda(
            event.id,
            event.cantidad,
            event.productoId,
            event.valorUnitario,
            event.prefijoCuenta,
            event.numeroCuenta,
            event.tempSelectedGustos,
          );

          yield ActualizadoProductoDeComanda();

          yield CargaListaProductosComandaState(
            total: res["TotalComanda"],
            lista: res["ListaProductos"],
            productosNoComandados: res["ProductosNoComandados"],
          );
        }
      } catch (e) {
        print(e);
        yield ErrorActualizarProductoDeComanda();
      }
    }
  }
}
