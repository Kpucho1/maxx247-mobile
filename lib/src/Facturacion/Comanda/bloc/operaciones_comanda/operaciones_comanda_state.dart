part of 'operaciones_comanda_bloc.dart';

@immutable
abstract class OperacionesComandaState {}

class OperacionesComandaInitial extends OperacionesComandaState {}

class ErrorCargaProductosComandaState extends OperacionesComandaState {}

class CargaListaProductosComandaState extends OperacionesComandaState {
  final double totalComanda;
  final int productosSinComandar;
  final List<ProductoCuentaModel> listaProductosComanda;

  CargaListaProductosComandaState({
    double total,
    int productosNoComandados,
    List<ProductoCuentaModel> lista,
  })  : this.totalComanda = total ?? 0.0,
        this.productosSinComandar = productosNoComandados ?? 0,
        this.listaProductosComanda = lista ?? [];

  CargaListaProductosComandaState copyWith({
    double totalCuenta,
    int productosNoComandados,
    List<ProductoCuentaModel> lista,
  }) =>
      new CargaListaProductosComandaState(
        total: totalCuenta ?? this.totalComanda,
        productosNoComandados:
            productosNoComandados ?? this.productosSinComandar,
        lista: lista ?? [],
      );
}

class ListaVaciaComandaState extends OperacionesComandaState {}

//======= Estados eliminacion de productos ===========

class EliminarProductoLoadInProgress extends OperacionesComandaState {}

class EliminadoProductoDeComanda extends OperacionesComandaState {}

class ErrorEliminarProductoDeComanda extends OperacionesComandaState {}

//======= Estados actualizacion de productos ===========

class ActualizarProductoLoadInProgress extends OperacionesComandaState {}

class ActualizadoProductoDeComanda extends OperacionesComandaState {}

class ErrorActualizarProductoDeComanda extends OperacionesComandaState {}
