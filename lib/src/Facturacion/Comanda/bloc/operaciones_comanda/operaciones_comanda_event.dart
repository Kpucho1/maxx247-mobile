part of 'operaciones_comanda_bloc.dart';

@immutable
abstract class OperacionesComandaEvent {}

class OnCargarProductosComanda extends OperacionesComandaEvent {
  final int numeroCuenta;
  final String prefijoCuenta;

  OnCargarProductosComanda({this.numeroCuenta, this.prefijoCuenta});
}

class OnEliminarProducto extends OperacionesComandaEvent {
  final int productoId;
  final int numeroCuenta;
  final String prefijoCuenta;

  OnEliminarProducto({this.productoId, this.numeroCuenta, this.prefijoCuenta});
}

class OnActualizarProducto extends OperacionesComandaEvent {
  final int id;
  final int numeroCuenta;
  final String prefijoCuenta;
  final double cantidad;
  final int productoId;
  final int valorUnitario;
  final List<String> tempSelectedGustos;

  OnActualizarProducto({
    this.id,
    this.numeroCuenta,
    this.prefijoCuenta,
    this.cantidad,
    this.productoId,
    this.valorUnitario,
    this.tempSelectedGustos,
  });
}
