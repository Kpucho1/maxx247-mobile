
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/widgets/contador_productos_widget.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/Comanda/bloc/operaciones_comanda/operaciones_comanda_bloc.dart';

class ComandaFragment extends StatelessWidget {
  //

  @override
  Widget build(BuildContext context) {
    //

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: _AppBarComandaFragment(),
      ),
      body: _CuerpoComandaFragment(),
    );
  }
}

class _AppBarComandaFragment extends StatelessWidget {
  //

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OperacionesComandaBloc, OperacionesComandaState>(
      builder: (context, state) {
        Widget _appBar;

        if (state is OperacionesComandaInitial) {
          //

          _appBar = AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            centerTitle: false,
            title: Text(
              'Cargando productos...',
              style: TextStyle(
                color: Color(0xff243447),
              ),
            ),
          );
        } else if (state is ErrorCargaProductosComandaState) {
          //

          _appBar = AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            centerTitle: false,
            title: Text(
              '¡Error al cargar los productos!',
              style: TextStyle(
                color: Colors.red,
              ),
            ),
          );
        } else if (state is CargaListaProductosComandaState) {
          //
          final formatCurrency =
              NumberFormat.currency(decimalDigits: 0, symbol: "");

          final totalComanda = formatCurrency.format(state.totalComanda);

          _appBar = AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            centerTitle: false,
            title: Text(
              'Total cuenta: \$ $totalComanda',
              style: TextStyle(
                color: Color(0xff243447),
              ),
            ),
          );
        } else if (state is ListaVaciaComandaState) {
          //

          _appBar = AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            centerTitle: false,
            title: Text(''),
          );
        } else if (state is EliminarProductoLoadInProgress) {
          //
          _appBar = Container();
        } else if (state is ActualizarProductoLoadInProgress) {
          //

          _appBar = Container();
        }

        return _appBar;
      },
    );
  }
}

//Widget que se encarga de renderizar la vista de la comanda según los diferentes estados del bloc
//Los estados pueden variar desde carga inicial, lista con productos, lista vacía o error al obtener
//los productos
class _CuerpoComandaFragment extends StatelessWidget {
  //
  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return BlocListener<OperacionesComandaBloc, OperacionesComandaState>(
      listener: (context, state) {
        //Muestra un snackbar para confirmar que el producto fue eliminado con éxito

        if (state is EliminadoProductoDeComanda) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.cyan[900],
              content: Text(
                'Producto eliminado exitosamente',
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                15.0,
              )),
            ),
          );
        }

        if (state is ActualizadoProductoDeComanda) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.cyan[900],
              content: Text(
                'Producto actualizado exitosamente',
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                15.0,
              )),
            ),
          );
        }
      },
      child: BlocBuilder<OperacionesComandaBloc, OperacionesComandaState>(
        builder: (context, state) {
          Widget _widget;

          if (state is OperacionesComandaInitial) {
            //
            _widget = Center(
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
              ),
            );
          } else if (state is ErrorCargaProductosComandaState) {
            //

            final ButtonStyle outlineButtonStyle = OutlinedButton.styleFrom(
              primary: Color(0xff243447),
              minimumSize: Size(88, 36),
              padding: EdgeInsets.symmetric(horizontal: 26),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
            ).copyWith(
              side: MaterialStateProperty.resolveWith<BorderSide>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed))
                    return BorderSide(
                      color: Theme.of(context).colorScheme.primary,
                      width: 1,
                    );
                  return null; // Defer to the widget's default.
                },
              ),
            );

            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 40.0),
                  LimitedBox(
                    maxHeight: 200,
                    maxWidth: 200,
                    child: SvgPicture.asset('assets/error_conexion.svg'),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    'Ocurrió un error en la conexión.\n\nAsegúrese que la conexión a la red se encuentre disponible e intente de nuevo.\n\nIP Servidor: ${prefs.ipServidor}',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 30.0),
                  OutlinedButton(
                    child: Text('Volver a cargar'),
                    style: outlineButtonStyle,
                    onPressed: () {
                      //
                      final numeroCuenta = context
                          .read<CabeceraService>()
                          .mesaCabecera
                          .numerocuenta;
                      final prefijoCuenta = context
                          .read<CabeceraService>()
                          .mesaCabecera
                          .prefijocuenta;

                      context
                          .read<OperacionesComandaBloc>()
                          .add(OnCargarProductosComanda(
                            numeroCuenta: numeroCuenta,
                            prefijoCuenta: prefijoCuenta,
                          ));
                    },
                  ),
                ],
              ),
            );
          } else if (state is CargaListaProductosComandaState) {
            //
            final listaProductos = state.listaProductosComanda;

            _widget = ListView.builder(
              itemCount: listaProductos.length,
              itemBuilder: (context, index) {
                //

                return _ProductosComanda(
                  producto: listaProductos[index],
                  index: index,
                );
              },
            );
          } else if (state is ListaVaciaComandaState) {
            //

            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 40.0),
                  LimitedBox(
                    maxHeight: 200,
                    maxWidth: 200,
                    child: SvgPicture.asset('assets/carrito_vacio.svg'),
                  ),
                  SizedBox(height: 15.0),
                  Text(
                    'No hay productos.\n\nAgregue productos para verlos acá y comandarlos.',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ),
            );
          } else if (state is EliminarProductoLoadInProgress) {
            //
            _widget = Center(
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
              ),
            );
          } else if (state is ActualizarProductoLoadInProgress) {
            //
            _widget = Center(
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
              ),
            );
          }

          return _widget;
        },
      ),
    );
  }
}

// ignore: must_be_immutable
class _ProductosComanda extends StatelessWidget {
  final ProductoCuentaModel producto;
  final int index;

  List<String> _tempSelectedGustos = [];

  _ProductosComanda({@required this.producto, @required this.index});

  @override
  Widget build(BuildContext context) {
    return _cardProductos(context, this.index, this.producto);
  }

  Widget _cardProductos(
      BuildContext context, int index, ProductoCuentaModel producto) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.05,
        vertical: size.height * 0.002,
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        shadowColor: Colors.black87,
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text('${producto.nombre}'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  producto.gustos != null && producto.gustos != ""
                      ? Column(
                          children: [
                            Text(
                              "GUSTOS: " + producto.gustos,
                              style: TextStyle(fontSize: 14.0),
                            ),
                            SizedBox(
                              height: 10.0,
                            )
                          ],
                        )
                      : SizedBox(),
                  producto.combos != null
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: producto.combos.map<Widget>((combo) {
                            return Text(combo.cantidadcombo.toString() +
                                " - " +
                                combo.nombre +
                                " (" +
                                combo.gusto.toString() +
                                ")");
                          }).toList())
                      : SizedBox(),
                  producto.centrocostoid != 0
                      ? Text(
                          producto.nombreImpresora + " " + producto.ipimpresora,
                          style: TextStyle(color: Colors.grey[400]),
                        )
                      : SizedBox(),
                  producto.puesto > 0
                      ? Text("Puesto: " + producto.puesto.toString(),
                          style: TextStyle(color: Colors.grey[400]))
                      : SizedBox(),
                  producto.nombreVendedor != null
                      ? Text("Vendedor: " + producto.nombreVendedor,
                          style: TextStyle(color: Colors.grey[400]))
                      : SizedBox()
                ],
              ),
              trailing: Text(
                  'Total: \$ ${producto.valortotal.round()} \n Cantidad: ${producto.cantidad.round()} \n ${producto.presentacion}'),
              leading: (producto.numerocomanda == 0 &&
                          producto.prefijocomanda == null) &&
                      producto.imprimeComanda == 1
                  ? Icon(Icons.error, color: Colors.red)
                  : Icon(Icons.check, color: Colors.green),
              onTap: () async {
                //

                List<ProductosGustosModel> proGusModel = await DBProvider.db
                      .obtenerProductosGustosPorId(producto.productoid);

                  if (producto.numerocomanda == 0) {
                    await _dialogoEditarProductos(
                        context, producto, index, proGusModel);
                  }
              },
            ),
          ],
        ),
      ),
    );
  }

  Future _dialogoEditarProductos(
      BuildContext context,
      ProductoCuentaModel producto,
      int index,
      List<ProductosGustosModel> proGusModel) {
    return showDialog(
      context: context,
      builder: (context) {
        final contadorProductos = ContadorProductos();
        _tempSelectedGustos =
            producto.gustos != null ? producto.gustos.split("/") : [];
        contadorProductos.cantidadProductos = producto.cantidad.round();
        return AlertDialog(
          title: Column(
            children: [
              Text(
                '${producto.nombre}',
                textAlign: TextAlign.center,
              ),
              Center(
                child: contadorProductos,
              )
            ],
          ),
          scrollable: true,
          content: StatefulBuilder(
            builder: (context, setState) {
              return Container(
                width: 50.0,
                height: 300.0,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1.0)),
                child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                          color: Colors.grey,
                        ),
                    itemCount: proGusModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      String nombreGusto = proGusModel[index].nombre;
                      return SimpleDialogOption(
                        child: CheckboxListTile(
                          title: Text(proGusModel[index].nombre),
                          value: _tempSelectedGustos.contains(nombreGusto),
                          checkColor: Colors.white,
                          activeColor: Colors.blue,
                          onChanged: (bool value) {
                            if (value) {
                              if (!_tempSelectedGustos.contains(nombreGusto)) {
                                setState(() {
                                  _tempSelectedGustos.add(nombreGusto);
                                });
                              }
                            } else {
                              if (_tempSelectedGustos.contains(nombreGusto)) {
                                setState(() {
                                  _tempSelectedGustos.removeWhere(
                                      (String gusto) => gusto == nombreGusto);
                                });
                              }
                            }
                          },
                        ),
                      );
                    }),
              );
            },
          ),
          actions: [
            TextButton(
              child: Text(
                "Eliminar",
                style: TextStyle(color: Colors.white),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.red)),
                  padding: EdgeInsets.all(10.0)),
              onPressed: () {
                final numeroCuenta =
                    context.read<CabeceraService>().mesaCabecera.numerocuenta;
                final prefijoCuenta =
                    context.read<CabeceraService>().mesaCabecera.prefijocuenta;

                context.read<OperacionesComandaBloc>().add(
                      OnEliminarProducto(
                        productoId: producto.id,
                        numeroCuenta: numeroCuenta,
                        prefijoCuenta: prefijoCuenta,
                      ),
                    );

                context.read<CabeceraService>().mesaCabecera.totalapagar -= producto.valortotal.toInt();


                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text('Actualizar', style: TextStyle(color: Colors.white)),
              onPressed: () {
                final numeroCuenta =
                    context.read<CabeceraService>().mesaCabecera.numerocuenta;
                final prefijoCuenta =
                    context.read<CabeceraService>().mesaCabecera.prefijocuenta;

                context.read<OperacionesComandaBloc>().add(
                      OnActualizarProducto(
                        id: producto.id,
                        cantidad:
                            contadorProductos.cantidadProductos.ceilToDouble(),
                        numeroCuenta: numeroCuenta,
                        prefijoCuenta: prefijoCuenta,
                        productoId: producto.productoid,
                        valorUnitario: producto.valorunitario,
                        tempSelectedGustos: _tempSelectedGustos,
                      ),
                    );

                Navigator.pop(context);
              },
              style: TextButton.styleFrom(
                  backgroundColor: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    side: BorderSide(color: Colors.blue),
                  ),
                  padding: EdgeInsets.all(10.0)),
            ),
          ],
        );
      },
    );
  }
}
