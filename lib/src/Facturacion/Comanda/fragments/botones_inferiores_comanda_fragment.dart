import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/pages/comanda_print_loading_page.dart';
import 'package:maxxshop/src/Facturacion/Comanda/bloc/operaciones_comanda/operaciones_comanda_bloc.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Facturacion/Clientes/pages/mesa_cliente_page.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class BotonesInferioresFragment extends StatelessWidget {
  final Function callback;

  const BotonesInferioresFragment({Key key, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: _CuerpoBotonesInferiores(callback: this.callback),
    );
  }
}

class _CuerpoBotonesInferiores extends StatelessWidget {
  final Function callback;

  const _CuerpoBotonesInferiores({Key key, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: BlocBuilder<OperacionesComandaBloc, OperacionesComandaState>(
        builder: (context, state) {
          Widget _cuerpoInterno;

          if (state is OperacionesComandaInitial) {
            //

            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
              ],
            );
          } else if (state is ListaVaciaComandaState) {
            //

            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
              ],
            );
          } else if (state is CargaListaProductosComandaState) {
            //

            if (state.productosSinComandar > 0) {
              _cuerpoInterno = Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _BotonComandar(listaProductos: state.listaProductosComanda),
                  _BotonClientes(callback: this.callback),
                  _BotonResumen(listaProducto: state.listaProductosComanda),
                ],
              );
            } else {
              _cuerpoInterno = Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _BotonClientes(callback: this.callback),
                  _BotonResumen(listaProducto: state.listaProductosComanda),
                ],
              );
            }
          } else if (state is ErrorCargaProductosComandaState) {
            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
              ],
            );
          } else if (state is ErrorEliminarProductoDeComanda) {
            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
              ],
            );
          } else if (state is EliminarProductoLoadInProgress) {
            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
                _BotonResumen(),
              ],
            );
          } else if (state is ActualizarProductoLoadInProgress) {
            _cuerpoInterno = Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BotonClientes(callback: this.callback),
                _BotonResumen(),
              ],
            );
          }

          return _cuerpoInterno;
        },
      ),
    );
  }
}

class _BotonComandar extends StatefulWidget {
  //
  final List<ProductoCuentaModel> listaProductos;
  _BotonComandar({Key key, this.listaProductos}) : super(key: key);

  @override
  __BotonComandarState createState() => __BotonComandarState();
}

//======== Boton de comandar maneja su propio estado para que solo se permita oprimir una vez
//======== por cada conjunto de productos no comandados. =============

class __BotonComandarState extends State<_BotonComandar> {
  bool ignoreHit = false;

  final ButtonStyle _estiloBotonComandar = ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
    backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
    padding: MaterialStateProperty.all<EdgeInsets>(
        EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0)),
    shape: MaterialStateProperty.all<OutlinedBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
    ),
  );

  //======== El widget IgnorePointer permite deshabilitar el botón cuando ha sido
  // oprimido una vez y según los escenarios en los que deba activarse y desactivarse ===========

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignoreHit,
      child: ElevatedButton(
        style: _estiloBotonComandar,
        child: Text(
          'Comandar',
          style: TextStyle(fontSize: 16.0),
        ),
        onPressed: () async {
          setState(() {
            ignoreHit = true;
          });

          

          await Navigator.push(
            context,
            MaterialPageRoute(
              settings: RouteSettings(name: 'comandaPage'),
              builder: (context) => ComandaPrintLoadingPage(
                listaProductosModel: widget.listaProductos,
              ),
            ),
          ).then((value) async {
            if (value != null) {
              await _alertaErrorEnImpresion(context, value);
            }
          });

          final numeroCuenta =
              context.read<CabeceraService>().mesaCabecera.numerocuenta;
          final prefijoCuenta =
              context.read<CabeceraService>().mesaCabecera.prefijocuenta;

          context.read<OperacionesComandaBloc>().add(OnCargarProductosComanda(
                numeroCuenta: numeroCuenta,
                prefijoCuenta: prefijoCuenta,
              ));
        },
      ),
    );
  }

  Future _alertaErrorEnImpresion(
    BuildContext context,
    String mensajeError,
  ) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      useRootNavigator: true,
      builder: (context) => AlertDialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        scrollable: true,
        content: Text(
          '$mensajeError',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.justify,
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    ).then((_) {
      //Reactiva el botón de comandar para volver a intentar imprimir
      setState(() {
        ignoreHit = false;
      });
    });
  }
}

class _BotonClientes extends StatelessWidget {
  //
  final Function callback;
  final double paddingHorizontal;

  _BotonClientes({Key key, this.paddingHorizontal, this.callback})
      : super(key: key);

  final ButtonStyle _estiloBotonClientes = ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
    backgroundColor: MaterialStateProperty.resolveWith<Color>(
      (states) {
        Color backgroundColor;

        if (states.contains(MaterialState.pressed)) {
          backgroundColor = Color(0xffc51f5d).withOpacity(0.5);
        }

        return backgroundColor;
      },
    ),
    padding: MaterialStateProperty.all<EdgeInsets>(
      EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
    ),
    overlayColor: MaterialStateProperty.resolveWith<Color>(
      (states) {
        Color backgroundColor;

        if (states.contains(MaterialState.pressed)) {
          backgroundColor = Color(0xffc51f5d).withOpacity(0.5);
        }

        return backgroundColor;
      },
    ),
    side: MaterialStateProperty.all<BorderSide>(
        BorderSide(color: Color(0xffc51f5d))),
    shape: MaterialStateProperty.all<OutlinedBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: _estiloBotonClientes,
      child: Text(
        'Clientes',
        style: TextStyle(fontSize: 16.0),
      ),
      onPressed: () async {
        //

        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MesaClientePage(callback: this.callback),
          ),
        );
      },
    );
  }
}

class _BotonResumen extends StatelessWidget {
  //
  final List<ProductoCuentaModel> listaProducto;
  _BotonResumen({Key key, this.listaProducto}) : super(key: key);

  final ButtonStyle _estiloBotonResumen = ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
    backgroundColor: MaterialStateProperty.resolveWith<Color>(
      (states) {
        Color backgroundColor;

        if (states.contains(MaterialState.pressed)) {
          backgroundColor = Color(0xffc51f5d).withOpacity(0.5);
        }

        return backgroundColor;
      },
    ),
    padding: MaterialStateProperty.all<EdgeInsets>(
      EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
    ),
    overlayColor: MaterialStateProperty.resolveWith<Color>(
      (states) {
        Color backgroundColor;

        if (states.contains(MaterialState.pressed)) {
          backgroundColor = Color(0xffc51f5d).withOpacity(0.5);
        }

        return backgroundColor;
      },
    ),
    side: MaterialStateProperty.all<BorderSide>(
      BorderSide(
        color: Color(0xffc51f5d),
      ),
    ),
    shape: MaterialStateProperty.all<OutlinedBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: _estiloBotonResumen,
      child: Text(
        'Resumen',
        style: TextStyle(fontSize: 16.0),
      ),
      onPressed: () async {
        //

        Map<dynamic, dynamic> productosAgrupados = groupBy(
            this.listaProducto, (ProductoCuentaModel obj) => obj.codigo);

        await _dialogResumen(context, productosAgrupados);
      },
    );
  }

  Future _dialogResumen(
      BuildContext context, Map<dynamic, dynamic> productos) async {
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    VendedorModel mesero = await DBProvider.db.obtenerVendedor();

    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> esquemaPropina = await conexionNegocio.query(
        "SELECT CAST(porcentajepropina AS INTEGER) FROM datos.esquemas WHERE esquemaid = @esquemaid",
        substitutionValues: {"esquemaid": mesero.esquemaid});

    List<List<dynamic>> total = await conexionNegocio.query(
        "SELECT CAST(totalapagar AS INTEGER), CAST(subtotalcuenta AS INTEGER) FROM datos.cuentacabecera WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta",
        substitutionValues: {
          "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
          "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta
        });

    return showDialog(
      context: context,
      builder: (context) => DefaultTabController(
        initialIndex: 0,
        length: 2,
        child: AlertDialog(
          title: TabBar(
            labelColor: Colors.black,
            indicatorColor: Color(0xffDD6E42),
            tabs: [
              Tab(
                icon: Icon(
                  Icons.shopping_bag,
                  color: Colors.black,
                ),
                text: 'Productos',
              ),
              Tab(
                icon: Icon(Icons.monetization_on, color: Colors.black),
                text: 'Cuenta',
              )
            ],
          ),
          content: Container(
            width: 300,
            height: 300,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                _resumenProductos(productos),
                _resumenTotalAPagar(esquemaPropina.first, total.first),
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: Text("Cerrar",
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.red)),
                  padding: EdgeInsets.all(10.0)),
            ),
          ],
        ),
      ),
    );
  }

  Widget _resumenProductos(Map<dynamic, dynamic> productos) {
    return SingleChildScrollView(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: productos.values.map((producto) {
            double cantidad = 0;
            int cont = 0;

            for (var prod in producto) {
              cantidad += prod.cantidad;
            }
            return Card(
              child: ListTile(
                title: Text(producto.first.nombre),
                trailing: Text(cantidad.toInt().toString()),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: producto.map<Widget>((e) {
                    cont++;
                    return e.gustos != ""
                        ? Text(cont.toString() + "- " + e.gustos)
                        : SizedBox();
                  }).toList(),
                ),
              ),
            );
          }).toList()),
    );
  }

  Widget _resumenTotalAPagar(List<dynamic> propina, List<dynamic> total) {
    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    double propinaFinal = (total[0] * (propina[0] / 100));
    return SingleChildScrollView(
      child: Column(
        children: [
          TextFormField(
            textAlign: TextAlign.right,
            readOnly: true,
            initialValue: '\$ ' + total[0].toString(),
            style: TextStyle(fontSize: 20),
            decoration: InputDecoration(
                hintText: 'Subtotal',
                labelText: 'Subtotal',
                labelStyle: TextStyle(
                  fontSize: 18,
                )),
          ),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            textAlign: TextAlign.right,
            readOnly: true,
            initialValue: '\$ ' + propinaFinal.toString(),
            style: TextStyle(fontSize: 20),
            decoration: InputDecoration(
                hintText: 'Propina',
                labelText: 'Propina',
                labelStyle: TextStyle(
                  fontSize: 18,
                )),
          ),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            textAlign: TextAlign.right,
            readOnly: true,
            initialValue: '\$ ' + (total[0] + propinaFinal).toString(),
            style: TextStyle(fontSize: 20),
            decoration: InputDecoration(
                hintText: 'Total',
                labelText: 'Total a pagar',
                labelStyle: TextStyle(
                  fontSize: 18,
                )),
          )
        ],
      ),
    );
  }
}
