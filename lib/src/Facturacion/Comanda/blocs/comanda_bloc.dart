import 'dart:async';

import 'package:rxdart/subjects.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/producto_cuenta_model.dart';

class ComandaBloc {
  final _comandaController = BehaviorSubject<List<ProductoCuentaModel>>();

  Stream<List<ProductoCuentaModel>> get productosCuentaStream =>
      _comandaController.stream;

  obtenerProductos(List<ProductoCuentaModel> listaProductos) {
    _comandaController.sink.add(listaProductos);
  }

  List<ProductoCuentaModel> get listadoProductos => _comandaController.value;

  dispose() {
    _comandaController?.close();
  }
}
