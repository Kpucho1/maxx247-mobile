import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';

class CabeceraService with ChangeNotifier {
  MesaCabecera _mesaCabecera;

  MesaCabecera get mesaCabecera => this._mesaCabecera;
  bool get existeMesaCabecera => this._mesaCabecera != null ? true : false;

  set mesaCabecera(MesaCabecera cabecera) {
    this._mesaCabecera = cabecera;
    notifyListeners();
  }
}
