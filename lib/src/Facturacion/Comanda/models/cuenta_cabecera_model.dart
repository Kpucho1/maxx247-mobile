import 'dart:convert';

MesaCabecera mesaCabeceraFromJson(String str) => MesaCabecera.fromJson(json.decode(str));

String mesaCabeceraToJson(MesaCabecera data) => json.encode(data.toJson());

class MesaCabecera {
    MesaCabecera({
        this.idcuenta,
        this.prefijocuenta,
        this.numerocuenta,
        this.ts,
        this.totalapagar,
        this.nombreMesa,
        this.objetoid,
        this.nombreCuenta,
        this.nombreCliente,
        this.direccionCliente,
        this.nitCliente,
        this.telefonoCliente,
        this.clienteid,
        this.fechahoravencimiento,
        this.ciudadCliente
    });

    int idcuenta;
    String prefijocuenta;
    int numerocuenta;
    String ts;
    int totalapagar;
    String nombreMesa;
    int objetoid;
    String nombreCuenta;
    String nombreCliente;
    String direccionCliente;
    String nitCliente;
    String telefonoCliente;
    int clienteid;
    String ciudadCliente;
    String fechahoravencimiento;


    factory MesaCabecera.fromJson(Map<String, dynamic> json) => MesaCabecera(
        idcuenta: json["idcuenta"],
        prefijocuenta: json["prefijocuenta"],
        numerocuenta: json["numerocuenta"],
        ts: json["ts"],
        totalapagar: json["totalapagar"],
        nombreMesa: json["nombreMesa"],
        objetoid: json["objetoid"],
        nombreCuenta: json["nombreCuenta"],
        nombreCliente: json["nombreCliente"],
        direccionCliente: json["direccionCliente"],
        nitCliente: json["nitCliente"],
        telefonoCliente: json["telefonoCliente"],
        clienteid: json["clienteid"],
    );

    Map<String, dynamic> toJson() => {
        "idcuenta": idcuenta,
        "prefijocuenta": prefijocuenta,
        "numerocuenta": numerocuenta,
        "ts": ts,
        "totalapagar": totalapagar,
        "nombreMesa": nombreMesa,
        "objetoid": objetoid,
        "numeroCuenta": nombreCuenta,
        "nombreCliente": nombreCliente,
        "direccionCliente": direccionCliente,
        "telefonoCliente": telefonoCliente,
        "nitCliente": nitCliente,
        "clienteid": clienteid
    };
}
