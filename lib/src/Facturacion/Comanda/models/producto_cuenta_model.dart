import 'dart:convert';

import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';

ProductoCuentaModel welcomeFromJson(String str) =>
    ProductoCuentaModel.fromJson(json.decode(str));

String welcomeToJson(ProductoCuentaModel data) => json.encode(data.toJson());

class ProductoCuentaModel {
  ProductoCuentaModel({
    this.codigo,
    this.nombre,
    this.cantidad,
    this.valortotal,
    this.centrocostoid,
    this.id,
    this.prefijocomanda,
    this.numerocomanda,
    this.gustos,
    this.productoid,
    this.imprimeComanda,
    this.combos,
    this.presentacion,
    this.numeroturno,
    this.valorunitario,
    this.vendedorid,
    this.puesto,
    this.nombreVendedor,
  });

  String codigo;
  String nombre;
  double cantidad;
  double valortotal;
  int centrocostoid;
  int id;
  String prefijocomanda;
  int numerocomanda;
  String gustos;
  int productoid;
  int imprimeComanda;
  List<ProductosCombosModel> combos;
  String presentacion;
  int numeroturno = 0;
  int valorunitario;
  String ipimpresora;
  String nombreImpresora;
  int vendedorid;
  int puesto;
  String nombreVendedor;

  factory ProductoCuentaModel.fromJson(Map<String, dynamic> json) =>
      ProductoCuentaModel(
        codigo: json["codigo"],
        nombre: json["nombre"],
        cantidad: json["cantidad"].toDouble(),
        valortotal: json["valortotal"],
        centrocostoid: json["centrocostoid"],
        id: json["id"],
        prefijocomanda: json["prefijocomanda"],
        numerocomanda: json["numerocomanda"],
        gustos: json["gustos"],
        productoid: json["productoid"],
      );

  Map<String, dynamic> toJson() => {
        "codigo": codigo,
        "nombre": nombre,
        "cantidad": cantidad,
        "valortotal": valortotal,
        "centrocostoid": centrocostoid,
        "id": id,
        "prefijocomanda": prefijocomanda,
        "numerocomanda": numerocomanda,
        "gustos": gustos,
        "productoid": productoid
      };
}
