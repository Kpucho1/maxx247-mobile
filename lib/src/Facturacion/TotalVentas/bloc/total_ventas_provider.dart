import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/facturas_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/propina_model.dart';

import 'package:maxxshop/src/Facturacion/TotalVentas/models/consecutivo_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/producto_util_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/participacion_grupo_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/services/repositorio_total_ventas.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/vendedor_totalVentas_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/bar_chart_totalVentas_model.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';

//Provider para el manejo de estado de lo relacionado al total de ventas
class TotalVentasProvider with ChangeNotifier {
  //
  final prefs = PreferenciasUsuario();
  final repositorioTotalVentas = RepositorioTotalVentas();
  //
  //Fechas del filtro para reportes
  String _fechaInicial;
  String _fechaFinal;

  String get fechaInicial => this._fechaInicial;
  String get fechaFinal => this._fechaFinal;

  Future<void> establecerFechaInicial(String fechaInicial) async {
    this._fechaInicial = fechaInicial;
    notifyListeners();
  }

  Future<void> establecerFechaFinal(String fechaFinal) async {
    this._fechaFinal = fechaFinal;
    notifyListeners();
  }

  //Filtro por vendedor para generar reporte en total ventas
  List<VendedorTotalVentasModel> _listaVendedoresTotalVentas;

  List<VendedorTotalVentasModel> get listaVendedoresTotalVentas =>
      this._listaVendedoresTotalVentas;

  Future<void> establecerListaVendedoresTotalVentas(int sucursalId) async {
    this._listaVendedoresTotalVentas = await repositorioTotalVentas
        .obtenerVendedores(sucursalId, prefs.licencia);

    notifyListeners();
  }

  //Establece vendedor seleccionado
  VendedorTotalVentasModel _vendedorSeleccionado;

  VendedorTotalVentasModel get vendedorSeleccionado =>
      this._vendedorSeleccionado;

  void establecerVendedorSeleccionado(VendedorTotalVentasModel vendedor) {
    this._vendedorSeleccionado = vendedor;
    notifyListeners();
  }

  //Establece los consecutivos
  List<ConsecutivoModel> _listaConsecutivos;

  List<ConsecutivoModel> get listaConsecutivos => this._listaConsecutivos;

  Future<void> obtenerConsecutivos(
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    //
    this._listaConsecutivos = await repositorioTotalVentas.obtenerConsecutivos(
        prefs.licencia, fechaInicial, fechaFinal, sucursalId);

    notifyListeners();
  }

  //Establece las propinas
  PropinasModel _propinas;

  PropinasModel get propinas => this._propinas;

  Future<void> obtenerPropinas(
      String fechaInicial, String fechaFinal, int sucursalId) async {
    this._propinas = await repositorioTotalVentas.obtenerPropinas(
        prefs.licencia, fechaInicial, fechaFinal, sucursalId);

    notifyListeners();
  }

  //Establece las facturas
  List<FacturaModel> _listadoFacturas;

  List<FacturaModel> get listadoFacturas => this._listadoFacturas;

  Future<void> obtenerListadoFacturas(
      String fechaInicial, String fechaFinal, int sucursalId) async {
    this._listadoFacturas = await repositorioTotalVentas.obtenerFacturas(
        prefs.licencia, fechaInicial, fechaFinal, sucursalId);

    notifyListeners();
  }

  //Establece el map para generar los gráficos de formas de pago y facturas
  Map<String, double> _mapaFormasDePagoGlobal;
  Map<String, double> _mapaFormasDePagoHoy;
  Map<String, dynamic> _mapaFacturasFormasDePago;

  double _totalVentasHoy;
  int _totalVentasGlobal;
  String _ultimoActualizado;

  Map<String, double> get mapaFormasDePago => this._mapaFormasDePagoGlobal;
  Map<String, double> get mapaFormasDePagoHoy => this._mapaFormasDePagoHoy;
  Map<String, dynamic> get mapaFacturasFormasDePago =>
      this._mapaFacturasFormasDePago;

  double get totalVentasHoy => this._totalVentasHoy;
  int get totalVentasGlobal => this._totalVentasGlobal;
  String get ultimoActualizado => this._ultimoActualizado;

  //Formas de pago y facturas de GLOBAL para usar con los filtros

  Future<void> establecerMapaFormasDePagoGlobal(
    String fechaInicial,
    String fechaFinal,
    int vendedorId,
    int sucursalId,
  ) async {
    //
    final res = await repositorioTotalVentas.obtenerTotalVentasGlobal(
        prefs.licencia, fechaInicial, fechaFinal, sucursalId, vendedorId);

    this._mapaFormasDePagoGlobal = res["FormasDePago"];
    this._mapaFacturasFormasDePago = res["Facturas"];
    this._totalVentasGlobal = res["TotalVentas"];

    notifyListeners();
  }

  //Formas de pago y facturas de HOY

  Future<void> establecerMapaFormasDePagoHoy(int sucursalId) async {
    final Map resultadoFormasPagoHoy = await repositorioTotalVentas
        .obtenerTotalVentasHoy(sucursalId, prefs.licencia);

    this._mapaFormasDePagoHoy = resultadoFormasPagoHoy["FormasDePago"];
    this._totalVentasHoy = resultadoFormasPagoHoy["TotalVentasHoy"];
    this._ultimoActualizado = resultadoFormasPagoHoy["Actualizado"];

    notifyListeners();
  }

  //Establece el listado de objetos para generar las gráficas de barras

  List<BarChartTotalVentasModel> _listadoBarChartTotalVentas;

  List<BarChartTotalVentasModel> get listadoBarChartTotalVentas =>
      this._listadoBarChartTotalVentas;

  Future<void> establecerListaSeriesBarChartTotalVentas(
      String conexionId,
      String fechaInicial,
      String fechaFinal,
      int vendedorId,
      int sucursalId,
      String uid) async {
    //TODO: Obtener data para la gráfica de barras

    notifyListeners();
  }

  //Obtiene lo relacionado a la utilidad por producto

  List<ProductoUtilModel> _listadoProductos;

  List<ProductoUtilModel> get listadoProductos => this._listadoProductos;

  Future<void> obtenerUtilidadProductos(
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
    int vendedorId,
  ) async {
    //
    this._listadoProductos =
        await repositorioTotalVentas.obtenerUtilidadesProductos(
            prefs.licencia, fechaInicial, fechaFinal, sucursalId, vendedorId);

    notifyListeners();
  }

  //Obtiene lo relacionado a las cortesias

  List<ProductoUtilModel> _listadoCortesias;

  List<ProductoUtilModel> get listadoCortesias => this._listadoCortesias;

  Future<void> obtenerCortesias(
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
    int vendedorId,
  ) async {
    //
    this._listadoCortesias = await repositorioTotalVentas.obtenerCortesias(
        prefs.licencia, fechaInicial, fechaFinal, sucursalId, vendedorId);

    notifyListeners();
  }

  //Obtiene la info relacionada a la participacion de los grupos

  List<ParticipacionGrupoModel> _listaParticipacionGrupos;
  Map<String, double> _mapaParticipacionGrupos;

  List<ParticipacionGrupoModel> get listaParticipacionGrupos =>
      this._listaParticipacionGrupos;

  Map<String, double> get mapaParticipacionGrupos =>
      this._mapaParticipacionGrupos;

  Future<void> obtenerParticipacionGrupos(
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    //
    final Map<dynamic, dynamic> res =
        await repositorioTotalVentas.obtenerParticipacionGrupos(
      prefs.licencia,
      fechaInicial,
      fechaFinal,
      sucursalId,
    );

    this._listaParticipacionGrupos = res["ListaParticipacion"];
    this._mapaParticipacionGrupos = res["MapaParticipacion"];

    notifyListeners();
  }
}
