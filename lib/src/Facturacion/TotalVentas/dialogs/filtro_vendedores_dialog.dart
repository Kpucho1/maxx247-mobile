import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/vendedor_totalVentas_model.dart';

class FiltroVendedores extends StatefulWidget {
  @override
  _FiltroVendedoresState createState() => _FiltroVendedoresState();
}

class _FiltroVendedoresState extends State<FiltroVendedores> {
  final prefs = PreferenciasUsuario();
  //
  //Establece el id de la conexion y obtiene la lista de vendedores
  @override
  void initState() {
    context
        .read<TotalVentasProvider>()
        .establecerListaVendedoresTotalVentas(prefs.sucursalid ?? 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: _appBarFiltroVendedores(),
      body: _cuerpoFiltroVendedores(),
      floatingActionButton: _botonFlotanteTodosLosVendedores(),
    );
  }

  Widget _appBarFiltroVendedores() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      centerTitle: true,
      title: Text(
        'Filtrar por vendedor',
        style: TextStyle(
          color: Colors.black,
        ),
      ),
      leading: IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.red,
          size: 30.0,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  Widget _cuerpoFiltroVendedores() {
    final listadoVendedores = context.select(
        (TotalVentasProvider value) => value.listaVendedoresTotalVentas);

    return listadoVendedores != null
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount:
                listadoVendedores.length != null ? listadoVendedores.length : 0,
            itemBuilder: (context, index) {
              return _vendedores(listadoVendedores[index]);
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _vendedores(VendedorTotalVentasModel vendedor) {
    return ListTile(
      leading: Icon(Icons.keyboard_arrow_right),
      title: Text(vendedor.nombre),
      onTap: () async {
        final _totalVentasProvider = context.read<TotalVentasProvider>();
        //Establece el vendedor para luego mostrarlo en la pagina principal
        _totalVentasProvider.establecerVendedorSeleccionado(vendedor);

        //Obtiene las fechas para hacer la consulta
        final fechaProvider = context.read<TotalVentasProvider>();

        final vendedorSeleccionado = _totalVentasProvider.vendedorSeleccionado;

        await _totalVentasProvider.establecerMapaFormasDePagoGlobal(
          fechaProvider.fechaInicial,
          fechaProvider.fechaFinal,
          vendedorSeleccionado.id,
          prefs.sucursalid,
        );

        Navigator.pop(context);
      },
    );
  }

  Widget _botonFlotanteTodosLosVendedores() {
    return FloatingActionButton(
      child: Text('Todos'),
      onPressed: () async {
        final _totalVentasProvider = context.read<TotalVentasProvider>();
        //Establece el vendedorid en cero(0) para filtrar por todos los vendedores
        final _todosVendedores = VendedorTotalVentasModel();

        _todosVendedores.id = 0;
        _todosVendedores.nombre = 'Todos los vendedores';

        _totalVentasProvider.establecerVendedorSeleccionado(_todosVendedores);

        final vendedor =
            context.read<TotalVentasProvider>().vendedorSeleccionado;

        await _totalVentasProvider.establecerMapaFormasDePagoGlobal(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          vendedor.id,
          prefs.sucursalid,
        );

        Navigator.pop(context);
      },
    );
  }
}
