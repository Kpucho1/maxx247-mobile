import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';

class FiltroFechaHora extends StatefulWidget {
  @override
  _FiltroFechaHoraState createState() => _FiltroFechaHoraState();
}

class _FiltroFechaHoraState extends State<FiltroFechaHora> {
  final prefs = PreferenciasUsuario();
  DateTime _fechaInicialSeleccionada = DateTime.now();
  DateTime _fechaFinalSeleccionada = DateTime.now();
  TimeOfDay _horaInicialSeleccionada = TimeOfDay(hour: 00, minute: 01);
  TimeOfDay _horaFinalSeleccionada = TimeOfDay(hour: 23, minute: 59);

  final scaffoldKeyFiltroFecha = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKeyFiltroFecha,
      appBar: _appBarFiltroFechaHora(context),
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: _cuerpoFiltroFechaHora(),
      ),
    );
  }

  Widget _appBarFiltroFechaHora(BuildContext context) {
    return AppBar(
      title: Text(
        'Filtrar fecha y hora',
        style: TextStyle(
          color: Colors.black,
        ),
      ),
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.close,
          color: Colors.red,
          size: 30.0,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      backgroundColor: Colors.white,
      elevation: 0.0,
    );
  }

  Widget _cuerpoFiltroFechaHora() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _tagsOpcionesDeTiempo(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        _seleccionarFechaInicial(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.001),
        // _seleccionarHoraInicial(),
        // SizedBox(height: MediaQuery.of(context).size.height * 0.01),
        _seleccionarFechaFinal(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        // _seleccionarHoraFinal(),
        // SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        _botonConfirmarFechas(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
      ],
    );
  }

  Widget _tagsOpcionesDeTiempo() {
    final _screenSize = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: _screenSize.height < 850 ? 6.0 : 8.0),
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _botonUltimosCincoDias(),
            _botonUltimosQuinceDias(),
            _botonUltimosTreintaDias(),
          ],
        ),
      ),
    );
  }

  Widget _botonUltimosCincoDias() {
    return TextButton(
      style: TextButton.styleFrom(
          backgroundColor: Colors.blueGrey,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0))),
      child: Text(
        'Últimos 5 días',
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,
      ),
      onPressed: () => _establecerUltimosDias(5),
    );
  }

  Widget _botonUltimosQuinceDias() {
    return TextButton(
      style: TextButton.styleFrom(
          backgroundColor: Colors.blueGrey,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0))),
      child: Text(
        'Últimos 15 días',
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,
      ),
      onPressed: () => _establecerUltimosDias(15),
    );
  }

  Widget _botonUltimosTreintaDias() {
    return TextButton(
      style: TextButton.styleFrom(
          backgroundColor: Colors.blueGrey,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0))),
      child: Text(
        'Últimos 30 días',
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,
      ),
      onPressed: () => _establecerUltimosDias(30),
    );
  }

  _establecerUltimosDias(int dias) async {
    final _totalVentasProvider = context.read<TotalVentasProvider>();

    _fechaFinalSeleccionada = DateTime.now();
    _fechaInicialSeleccionada = _fechaFinalSeleccionada.subtract(Duration(
      days: dias,
      hours: _fechaFinalSeleccionada.hour,
      minutes: _fechaFinalSeleccionada.minute,
    ));

    final snackBar = SnackBar(
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Cargando información. Un momento por favor.'),
          SizedBox(height: 5.0),
          CircularProgressIndicator(),
        ],
      ),
      backgroundColor: Colors.cyan[900],
    );

    scaffoldKeyFiltroFecha.currentState.showSnackBar(snackBar);

    await _totalVentasProvider
        .establecerFechaInicial(_fechaInicialSeleccionada.toString());
    await _totalVentasProvider
        .establecerFechaFinal(_fechaFinalSeleccionada.toString());

    final vendedor = _totalVentasProvider.vendedorSeleccionado;

    await context.read<TotalVentasProvider>().establecerMapaFormasDePagoGlobal(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          vendedor.id,
          prefs.sucursalid ?? 1,
        );

    await context.read<TotalVentasProvider>().obtenerConsecutivos(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
        );

    await context.read<TotalVentasProvider>().obtenerPropinas(
        _totalVentasProvider.fechaInicial,
        _totalVentasProvider.fechaFinal,
        prefs.sucursalid);

    await context.read<TotalVentasProvider>().obtenerListadoFacturas(
        _totalVentasProvider.fechaInicial,
        _totalVentasProvider.fechaFinal,
        prefs.sucursalid);

    await context.read<TotalVentasProvider>().obtenerUtilidadProductos(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
          vendedor.id,
        );

    await context.read<TotalVentasProvider>().obtenerCortesias(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
          vendedor.id,
        );

    await context.read<TotalVentasProvider>().obtenerParticipacionGrupos(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
        );

    // await context
    //     .read<TotalVentasProvider>()
    //     .establecerListaSeriesBarChartTotalVentas(
    //       conexionId,
    //       fechas.fechaInicial,
    //       fechas.fechaFinal,
    //       vendedor.id,
    //       prefs.sucursalid,
    //       uid,
    //     );

    Navigator.pop(context);
  }

  Widget _seleccionarFechaInicial() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _botonParaSeleccionarFechaInicial(),
        _textoFechaInicialSeleccionada(),
      ],
    );
  }

  Widget _botonParaSeleccionarFechaInicial() {
    return RaisedButton.icon(
      color: Colors.cyan[100],
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      icon: Icon(CupertinoIcons.calendar_today),
      label: Text(
        'Fecha de inicio',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () => _pickerSeleccionarFechaInicial(context),
    );
  }

  Widget _textoFechaInicialSeleccionada() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.width * 0.01,
      ),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        '${_fechaInicialSeleccionada.toLocal()}'.split(' ')[0],
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  _pickerSeleccionarFechaInicial(BuildContext context) async {
    final DateTime seleccionada = await showDatePicker(
      context: context,
      initialDate: _fechaInicialSeleccionada,
      firstDate: DateTime(2010),
      lastDate: DateTime.now(),
      helpText: 'Seleccione la fecha de inicio',
      cancelText: 'Cancelar',
      confirmText: 'Seleccionar',
      errorFormatText: 'Ingrese una fecha válida',
      errorInvalidText: 'Ingrese una fecha en el rango permitido',
    );

    if (seleccionada != null && seleccionada != _fechaInicialSeleccionada)
      setState(() {
        _fechaInicialSeleccionada = seleccionada;
      });
  }

  Widget _seleccionarFechaFinal() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _botonParaSeleccionarFechaFinal(),
        _textoFechaFinalSeleccionada(),
      ],
    );
  }

  Widget _botonParaSeleccionarFechaFinal() {
    return RaisedButton.icon(
      color: Colors.cyan[100],
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.125),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      icon: Icon(CupertinoIcons.calendar_today),
      label: Text(
        'Fecha de fin',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () => _pickerSeleccionarFechaFinal(context),
    );
  }

  Widget _textoFechaFinalSeleccionada() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.width * 0.01,
      ),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        '${_fechaFinalSeleccionada.toLocal()}'.split(' ')[0],
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  _pickerSeleccionarFechaFinal(BuildContext context) async {
    final DateTime seleccionada = await showDatePicker(
      context: context,
      initialDate: _fechaFinalSeleccionada,
      firstDate: DateTime(2010),
      lastDate: DateTime.now(),
      helpText: 'Seleccione la fecha de inicio',
      cancelText: 'Cancelar',
      confirmText: 'Seleccionar',
      errorFormatText: 'Ingrese una fecha válida',
      errorInvalidText: 'Ingrese una fecha en el rango permitido',
    );

    if (seleccionada != null && seleccionada != _fechaFinalSeleccionada)
      setState(() {
        _fechaFinalSeleccionada = seleccionada;
      });
  }

  Widget _seleccionarHoraInicial() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _botonParaSeleccionarHoraInicial(),
        _textoHoraInicialSeleccionada(),
      ],
    );
  }

  Widget _botonParaSeleccionarHoraInicial() {
    return RaisedButton.icon(
      color: Colors.cyan[100],
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.128),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      icon: Icon(CupertinoIcons.clock),
      label: Text(
        'Hora inicial',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () => _pickerSeleccionarHoraInicial(),
    );
  }

  Widget _textoHoraInicialSeleccionada() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.08,
        vertical: MediaQuery.of(context).size.width * 0.01,
      ),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        '${_horaInicialSeleccionada.format(context)}',
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  _pickerSeleccionarHoraInicial() async {
    final TimeOfDay horaSeleccionada = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 00, minute: 00),
      cancelText: 'Cancelar',
      confirmText: 'Seleccionar',
      helpText: 'Seleccione hora inicial',
    );

    if (horaSeleccionada != null ||
        horaSeleccionada != _horaInicialSeleccionada) {
      print(horaSeleccionada.format(context));
      setState(() {
        _horaInicialSeleccionada = horaSeleccionada;
      });
    }
  }

  Widget _seleccionarHoraFinal() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _botonParaSeleccionarHoraFinal(),
        _textoHoraFinalSeleccionada(),
      ],
    );
  }

  Widget _botonParaSeleccionarHoraFinal() {
    return RaisedButton.icon(
      color: Colors.cyan[100],
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.139),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      icon: Icon(CupertinoIcons.clock),
      label: Text(
        'Hora final',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () => _pickerSeleccionarHoraFinal(),
    );
  }

  Widget _textoHoraFinalSeleccionada() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.07,
        vertical: MediaQuery.of(context).size.width * 0.01,
      ),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        '${_horaFinalSeleccionada.format(context)}',
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  _pickerSeleccionarHoraFinal() async {
    final TimeOfDay horaSeleccionada = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 00, minute: 00),
      cancelText: 'Cancelar',
      confirmText: 'Seleccionar',
      helpText: 'Seleccione hora inicial',
    );

    if (horaSeleccionada != null ||
        horaSeleccionada != _horaInicialSeleccionada) {
      print(horaSeleccionada.format(context));
      setState(() {
        _horaFinalSeleccionada = horaSeleccionada;
      });
    }
  }

  Widget _botonConfirmarFechas() {
    return OutlineButton.icon(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.3,
      ),
      borderSide: BorderSide(color: Colors.green, width: 2.0),
      highlightElevation: 0.0,
      highlightColor: Colors.green[300],
      highlightedBorderColor: Colors.green,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      icon: Icon(
        Icons.check,
        color: Colors.green[900],
      ),
      label: Text(
        'Confirmar',
        style: TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: () => _confirmarFechas(),
    );
  }

  void _confirmarFechas() async {
    final _totalVentasProvider = context.read<TotalVentasProvider>();

    //TODO: Agregar provider para horas seleccionadas

    //Establece las fechas en el estado
    await _totalVentasProvider.establecerFechaInicial(
        _fechaInicialSeleccionada.toString().split(' ')[0]);
    await _totalVentasProvider
        .establecerFechaFinal(_fechaFinalSeleccionada.toString().split(' ')[0]);

    final vendedor = _totalVentasProvider.vendedorSeleccionado;

    final snackBar = SnackBar(
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      duration: Duration(seconds: 10),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Cargando información. Un momento por favor.'),
          SizedBox(height: 5.0),
          CircularProgressIndicator(),
        ],
      ),
      backgroundColor: Colors.cyan[900],
    );

    scaffoldKeyFiltroFecha.currentState.showSnackBar(snackBar);

    //Realiza condicion segun la fecha inicial sea igual o no al presente día
    if (_totalVentasProvider.fechaInicial ==
        DateTime.now().toString().split(' ')[0]) {
      //
      final _fechaFinal = DateTime.now();
      final _fechaInicial = DateTime.now().subtract(
          Duration(hours: _fechaFinal.hour, minutes: _fechaFinal.minute));

      await _totalVentasProvider.establecerMapaFormasDePagoGlobal(
        _fechaInicial.toString(),
        _fechaFinal.toString(),
        vendedor.id,
        prefs.sucursalid ?? 1,
      );

      await context.read<TotalVentasProvider>().obtenerConsecutivos(
            _fechaInicial.toString(),
            _fechaFinal.toString(),
            prefs.sucursalid ?? 1,
          );

      await context.read<TotalVentasProvider>().obtenerPropinas(
          _fechaInicial.toString(),
          _fechaFinal.toString(),
          prefs.sucursalid);

      await context.read<TotalVentasProvider>().obtenerListadoFacturas(
          _fechaInicial.toString(),
          _fechaFinal.toString(),
          prefs.sucursalid);

      await _totalVentasProvider.obtenerUtilidadProductos(
        _fechaInicial.toString(),
        _fechaFinal.toString(),
        prefs.sucursalid ?? 1,
        vendedor.id,
      );

      await _totalVentasProvider.obtenerCortesias(
        _fechaInicial.toString(),
        _fechaFinal.toString(),
        prefs.sucursalid ?? 1,
        vendedor.id,
      );

      await context.read<TotalVentasProvider>().obtenerParticipacionGrupos(
            _fechaInicial.toString(),
            _fechaFinal.toString(),
            prefs.sucursalid ?? 1,
          );

      //   await obtenerFormasDePagoProvider
      //       .establecerListaSeriesBarChartTotalVentas(
      //     conexionId,
      //     _fechaInicial.toString(),
      //     _fechaFinal.toString(),
      //     vendedor.id,
      //     prefs.sucursalid,
      //     uid,
      //   );
    } else {
      //
      if (_totalVentasProvider.fechaFinal ==
          DateTime.now().toString().split(' ')[0]) {
        await _totalVentasProvider.establecerMapaFormasDePagoGlobal(
          _totalVentasProvider.fechaInicial,
          DateTime.now().toString(),
          vendedor.id,
          prefs.sucursalid,
        );

        await context.read<TotalVentasProvider>().obtenerConsecutivos(
              _totalVentasProvider.fechaInicial,
              DateTime.now().toString(),
              prefs.sucursalid ?? 1,
            );

        await context.read<TotalVentasProvider>().obtenerPropinas(
            _totalVentasProvider.fechaInicial,
            DateTime.now().toString(),
            prefs.sucursalid);

        await context.read<TotalVentasProvider>().obtenerListadoFacturas(
            _totalVentasProvider.fechaInicial,
            DateTime.now().toString(),
            prefs.sucursalid);

        await context.read<TotalVentasProvider>().obtenerUtilidadProductos(
              _totalVentasProvider.fechaInicial,
              DateTime.now().toString(),
              prefs.sucursalid ?? 1,
              vendedor.id,
            );

        await context.read<TotalVentasProvider>().obtenerCortesias(
              _totalVentasProvider.fechaInicial,
              DateTime.now().toString(),
              prefs.sucursalid ?? 1,
              vendedor.id,
            );

        await context.read<TotalVentasProvider>().obtenerParticipacionGrupos(
              _totalVentasProvider.fechaInicial,
              DateTime.now().toString(),
              prefs.sucursalid ?? 1,
            );

        // await obtenerFormasDePagoProvider
        //     .establecerListaSeriesBarChartTotalVentas(
        //   conexionId,
        //   fechaProvider.fechaInicial,
        //   fechaProvider.fechaFinal,
        //   vendedor.id,
        //   prefs.sucursalid,
        //   uid,
        // );
      } else {
        final _fechaFinal = _totalVentasProvider.fechaFinal + ' ' + '23:59:59';

        await _totalVentasProvider.establecerMapaFormasDePagoGlobal(
          _totalVentasProvider.fechaInicial,
          _fechaFinal,
          vendedor.id,
          prefs.sucursalid,
        );

        await context.read<TotalVentasProvider>().obtenerConsecutivos(
              _totalVentasProvider.fechaInicial,
              _fechaFinal,
              prefs.sucursalid ?? 1,
            );

        await context.read<TotalVentasProvider>().obtenerPropinas(
            _totalVentasProvider.fechaInicial,
            _fechaFinal,
            prefs.sucursalid);

        await context.read<TotalVentasProvider>().obtenerListadoFacturas(
            _totalVentasProvider.fechaInicial,
            _fechaFinal,
            prefs.sucursalid);

        await context.read<TotalVentasProvider>().obtenerUtilidadProductos(
              _totalVentasProvider.fechaInicial,
              _fechaFinal,
              prefs.sucursalid ?? 1,
              vendedor.id,
            );

        await context.read<TotalVentasProvider>().obtenerCortesias(
              _totalVentasProvider.fechaInicial,
              _fechaFinal,
              prefs.sucursalid ?? 1,
              vendedor.id,
            );

        await context.read<TotalVentasProvider>().obtenerParticipacionGrupos(
              _totalVentasProvider.fechaInicial,
              _fechaFinal,
              prefs.sucursalid ?? 1,
            );

        // await obtenerFormasDePagoProvider
        //     .establecerListaSeriesBarChartTotalVentas(
        //   conexionId,
        //   fechaProvider.fechaInicial,
        //   fechaProvider.fechaFinal,
        //   vendedor.id,
        //   prefs.sucursalid,
        //   uid,
        // );
      }
    }

    Navigator.pop(context);
  }
}
