import 'package:flutter/material.dart';

class FiltroCajeroCaja extends StatefulWidget {
  @override
  _FiltroCajeroCajaState createState() => _FiltroCajeroCajaState();
}

class _FiltroCajeroCajaState extends State<FiltroCajeroCaja> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: _appBarFiltroVendedores(),
    );
  }

  Widget _appBarFiltroVendedores() {
    return AppBar(
      centerTitle: true,
      title: Text(
        'Filtrar por cajero',
        style: TextStyle(
          color: Colors.black,
        ),
      ),
      leading: IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.red,
          size: 30.0,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
    );
  }
}
