import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:maxxshop/src/Facturacion/TotalVentas/models/consecutivo_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/facturas_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/participacion_grupo_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/producto_util_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/propina_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/vendedor_totalVentas_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';


class RepositorioTotalVentas {
  //Una única instancia es creada (singleton)
  RepositorioTotalVentas._privateConstructor();

  static final RepositorioTotalVentas _instancia =
      RepositorioTotalVentas._privateConstructor();

  factory RepositorioTotalVentas() {
    return _instancia;
  }
  //Grafica de barras
  //https://api.maxxcontrol.net/api/emp/totalsales/salesperday/<fechaHoraIInicial>/<fechaHorafinal>/<sucursalid>/<vendedorid>

  // Tabla porcentajes de utilidad
  // /emp/totalsales/groups/<fechaHoraInicial>/<fechaHoraFinal>/<sucursalid>

  final _url = 'https://api.maxxcontrol.net/api';

  Future<Map> obtenerTotalVentasHoy(int idSucursal, String licencia) async {
    final url = '$_url/emp/totalsales/today/$idSucursal';

    Map<String, double> _formasPago = Map();
    double _totalVentasHoy;
    String _ultimoActualizado;

    try {
      final res = await http.get(url, headers: {"licencia": licencia});

      final _decodedData = json.decode(res.body);
      final _metodosPago = _decodedData["formaspago"];

      _formasPago = {
        "Efectivo": double.tryParse(_metodosPago["efectivo"].toString()),
        "Tarj. Crédito":
            double.tryParse(_metodosPago["tarjetacredito"].toString()),
        "Tarj. Débito":
            double.tryParse(_metodosPago["tarjetadebito"].toString()),
        "Abonos": double.tryParse(_metodosPago["abono"].toString()),
      };

      _totalVentasHoy = double.tryParse(_decodedData["total"].toString());

      _ultimoActualizado = _decodedData["actualizado"];
    } catch (e) {
      print(e);
      print(e.runtimeType);
      print("Error al obtener total de ventas de Hoy");
      _formasPago = {};
      _totalVentasHoy = 0.0;
    }

    return {
      "FormasDePago": _formasPago ?? {},
      "TotalVentasHoy": _totalVentasHoy ?? 0.0,
      "Actualizado": _ultimoActualizado ?? '',
    };
  }

  Future<Map> obtenerTotalVentasGlobal(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
    int vendedorId,
  ) async {
    //
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    Map<String, double> _formasPago = Map();
    Map<String, dynamic> _facturasFormasPago = Map();
    int _totalVentasHoy = 0;

    try {
      List<List<dynamic>> _data;
      List<List<dynamic>> _numeroFacturas;

      if (vendedorId != 0) {
        _data = await conexionNegocio.query("SELECT COALESCE(CAST(SUM(fpospagefe) AS INTEGER), 0) AS efectivo, COALESCE(CAST(SUM(fpospagtarc) AS INTEGER), 0) AS tarjetacredito, COALESCE(CAST(SUM(fpospagtard) AS INTEGER), 0) AS tarjetadebito, COALESCE(CAST(SUM(fpospagabo) AS INTEGER), 0) AS abono, COALESCE(CAST(SUM(fpospagcre) AS INTEGER), 0) AS credito, COALESCE(CAST(SUM(fpospagbon) AS INTEGER), 0) AS bono, COALESCE(CAST(SUM(fpospagche) AS INTEGER), 0) AS cheque FROM datos.facturacabecera WHERE fposfecha BETWEEN @fechainicial AND @fechafinal AND sucursalid = @sucursalid AND vendedorid = @vendedorid AND fposestado = 0", substitutionValues: {
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal,
          "sucursalid": sucursalId,
          "vendedorid": vendedorId
        });

        _numeroFacturas = await conexionNegocio.query("SELECT COUNT(fpospagefe) filter(where fpospagefe > 0) AS numeroefectivo, COUNT(fpospagtarc) filter(where fpospagtarc > 0)  as numerotarcredito, COUNT(fpospagtard) filter(where fpospagtard > 0) as numerotardebito, COUNT(fpospagabo) filter(where fpospagabo > 0) as numeroabono, COUNT(fpospagcre) filter(where fpospagcre > 0) as numerocredito, COUNT(fpospagbon) filter(where fpospagbon > 0) as numerobono, COUNT(fpospagche) filter(where fpospagche > 0) as numerocheque FROM datos.facturacabecera WHERE fposfecha BETWEEN @fechainicial AND @fechafinal AND sucursalid = @sucursalid AND vendedorid = @vendedorid", substitutionValues: {
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal,
          "sucursalid": sucursalId,
          "vendedorid": vendedorId
        });
      }
      else{
        _data = await conexionNegocio.query("SELECT COALESCE(CAST(SUM(fpospagefe) AS INTEGER), 0) AS efectivo, COALESCE(CAST(SUM(fpospagtarc) AS INTEGER), 0) AS tarjetacredito, COALESCE(CAST(SUM(fpospagtard) AS INTEGER), 0) AS tarjetadebito, COALESCE(CAST(SUM(fpospagabo) AS INTEGER), 0) AS abono, COALESCE(CAST(SUM(fpospagcre) AS INTEGER), 0) AS credito, COALESCE(CAST(SUM(fpospagbon) AS INTEGER), 0) AS bono, COALESCE(CAST(SUM(fpospagche) AS INTEGER), 0) AS cheque FROM datos.facturacabecera WHERE fposfecha BETWEEN @fechainicial AND @fechafinal AND sucursalid = @sucursalid AND fposestado = 0", substitutionValues: {
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal,
          "sucursalid": sucursalId,
        });

        _numeroFacturas = await conexionNegocio.query("SELECT COUNT(fpospagefe) filter(where fpospagefe > 0) AS numeroefectivo, COUNT(fpospagtarc) filter(where fpospagtarc > 0)  as numerotarcredito, COUNT(fpospagtard) filter(where fpospagtard > 0) as numerotardebito, COUNT(fpospagabo) filter(where fpospagabo > 0) as numeroabono, COUNT(fpospagcre) filter(where fpospagcre > 0) as numerocredito, COUNT(fpospagbon) filter(where fpospagbon > 0) as numerobono, COUNT(fpospagche) filter(where fpospagche > 0) as numerocheque FROM datos.facturacabecera WHERE fposfecha BETWEEN @fechainicial AND @fechafinal AND sucursalid = @sucursalid", substitutionValues: {
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal,
          "sucursalid": sucursalId,
        });
      }

      final _metodosPago = _data.first;
      final _facturas = _numeroFacturas.first;


      _formasPago = {
        "Efectivo":
            double.tryParse(_metodosPago[0].toString()),
        "Tarj. Crédito":
            double.tryParse(_metodosPago[1].toString()),
        "Tarj. Débito":
            double.tryParse(_metodosPago[2].toString()),
        "Abonos": double.tryParse(_metodosPago[3].toString()),
        "Créditos":
            double.tryParse(_metodosPago[4].toString()),
        "Bonos": double.tryParse(_metodosPago[5].toString()),
        "Cheques": double.tryParse(_metodosPago[6].toString())
      };

      _facturasFormasPago = {
        "Efectivo": _facturas[0],
        "Tarj. Crédito": _facturas[1],
        "Tarj. Débito": _facturas[2],
        "Abonos": _facturas[3],
        "Créditos": _facturas[4],
        "Bonos": _facturas[5],
        "Cheques": _facturas[6],
      };

      _formasPago.forEach((key, value) {
        _totalVentasHoy += value.toInt();
      });
      //
    } catch (e) {
      //
      print(e);
      print(e.runtimeType);
      print('Error al obtener total de ventas global');
      _formasPago = {};
      _totalVentasHoy = 0;
    }
    await conexionNegocio.close();

    return {
      "FormasDePago": _formasPago ?? {},
      "Facturas": _facturasFormasPago ?? {},
      "TotalVentas": _totalVentasHoy ?? 0.0,
    };
  }

  Future<List<ConsecutivoModel>> obtenerConsecutivos(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    List<ConsecutivoModel> _listadoConsecutivos = [];

    try {
      
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT MIN(C.fposnumero) AS desde, MAX(C.fposnumero) AS hasta, D.prefijo  FROM datos.documentosconsecutivos D, datos.facturacabecera C WHERE  (C.fposfecha BETWEEN '$fechaInicial' AND '$fechaFinal') AND C.sucursalid = @sucursalid  AND D.prefijo = C.fposprefijo GROUP BY D.prefijo", substitutionValues: {
        "sucursalid": sucursalId
      });

      for (var consec in _data) {
        final nuevoConsecutivo = ConsecutivoModel(
          prefijo: consec[2],
          desde: consec[0],
          hasta: consec[1],
        );

        _listadoConsecutivos.add(nuevoConsecutivo);
      }
      //
    } catch (e) {
      //
      print(e);
      print(e.runtimeType);
      print('Error al obtener consecutivos');
    }
    await conexionNegocio.close();

    return _listadoConsecutivos ?? [];
  }

  Future<PropinasModel> obtenerPropinas(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    PropinasModel propinas = PropinasModel();

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT COALESCE(CAST(SUM(fpospropin) AS INTEGER), 0) AS totalpropina, COALESCE(CAST(SUM(fpospropinacre) AS INTEGER), 0) AS propinacredito, COALESCE(CAST(SUM(fpospropinaefe) AS INTEGER), 0) AS propinaefectivo, COALESCE(CAST(SUM(fpospropinatar) AS INTEGER), 0) AS propinatarjeta FROM datos.facturacabecera WHERE sucursalid = @sucursalid AND fposfecha BETWEEN @fechainicial AND @fechafinal", substitutionValues: {
        "sucursalid": sucursalId,
        "fechainicial": fechaInicial,
        "fechafinal": fechaFinal
      });


      propinas.propinacredito = _data.first[1];
      propinas.propinaefectivo = _data.first[2];
      propinas.propinatarjeta = _data.first[3];
      propinas.totalpropina = _data.first[0];

      //
    } catch (e) {
      //
      print(e);
      print(e.runtimeType);
      print('Error al obtener propinas');
    }
    await conexionNegocio.close();

    return propinas ?? null;
  }

  Future<List<ProductoUtilModel>> obtenerUtilidadesProductos(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
    int vendedorId,
  ) async {
    //
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ProductoUtilModel> _listadoProductos = [];
    List<List<dynamic>> _data;

    try {
      if (vendedorId != 0) {
        _data = await conexionNegocio.query("SELECT productoid, fposproducto, SUM(CAST(fposvalorunitario AS INTEGER)) AS total, fposdescripcion, SUM(CAST(fposcantidad AS INTEGER)) AS cantidad, fpospresentacion FROM datos.facturacabecera AS cab, datos.facturadetalle AS det WHERE cab.sucursalid = @sucursalid AND cab.vendedorid = @vendedorid AND cab.fposprefijo = det.fposprefijo AND cab.fposnumero = det.fposnumero AND fposfecha BETWEEN @fechainicial AND @fechafinal AND fposestado = 0 AND fposescortesia = 0 AND fposincluido = 0 AND fposingresotercero = 0 GROUP BY productoid, fposdescripcion, fposproducto, fpospresentacion ORDER BY total DESC", substitutionValues: {
          "sucursalid": sucursalId,
          "vendedorid": vendedorId,
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal
        });
      }
      else{
        _data = await conexionNegocio.query("SELECT productoid, fposproducto, SUM(CAST(fposvalorunitario AS INTEGER)) AS total, fposdescripcion, SUM(CAST(fposcantidad AS INTEGER)) AS cantidad, fpospresentacion FROM datos.facturacabecera AS cab, datos.facturadetalle AS det WHERE cab.sucursalid = @sucursalid  AND cab.fposprefijo = det.fposprefijo AND cab.fposnumero = det.fposnumero AND fposfecha BETWEEN @fechainicial AND @fechafinal AND fposestado = 0 AND fposescortesia = 0 AND fposincluido = 0 AND fposingresotercero = 0  GROUP BY productoid, fposdescripcion, fposproducto, fpospresentacion ORDER BY total DESC", substitutionValues: {
          "sucursalid": sucursalId,
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal
        });
      }

      for (var prod in _data) {
        final producto = ProductoUtilModel(
            id: prod[0],
            descripcion: prod[3],
            total: prod[2],
            cantidad: prod[4],
            presentacion: prod[5],
            producto: prod[1]);

        _listadoProductos.add(producto);
      }
    } catch (e) {
      //
      print(e);
      print(e.runtimeType);
      print('Error al obtener productos');
      _listadoProductos = [];
    }

    await conexionNegocio.close();

    return _listadoProductos ?? [];
  }

  Future<List<ProductoUtilModel>> obtenerCortesias(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
    int vendedorId,
  ) async {
    //
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ProductoUtilModel> _listadoCortesias = [];
    List<List<dynamic>> _data;

    try {
      if (vendedorId != 0) {
        _data = await conexionNegocio.query("SELECT productoid, fposproducto, SUM(CAST(fposvalorunitario AS INTEGER)) AS total, fposdescripcion, SUM(CAST(fposcantidad AS INTEGER)) AS cantidad, fpospresentacion FROM datos.facturacabecera AS cab, datos.facturadetalle AS det WHERE cab.sucursalid = @sucursalid AND cab.vendedorid = @vendedorid AND cab.fposprefijo = det.fposprefijo AND cab.fposnumero = det.fposnumero AND fposfecha BETWEEN @fechainicial AND @fechafinal AND fposestado = 0 AND fposescortesia = 1 AND fposincluido = 0 AND fposingresotercero = 0 GROUP BY productoid, fposdescripcion, fposproducto, fpospresentacion ORDER BY total DESC", substitutionValues: {
          "sucursalid": sucursalId,
          "vendedorid": vendedorId,
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal
        });
      }
      else{
        _data = await conexionNegocio.query("SELECT productoid, fposproducto, SUM(CAST(fposvalorunitario AS INTEGER)) AS total, fposdescripcion, SUM(CAST(fposcantidad AS INTEGER)) AS cantidad, fpospresentacion FROM datos.facturacabecera AS cab, datos.facturadetalle AS det WHERE cab.sucursalid = @sucursalid  AND cab.fposprefijo = det.fposprefijo AND cab.fposnumero = det.fposnumero AND fposfecha BETWEEN @fechainicial AND @fechafinal AND fposestado = 0 AND fposescortesia = 1 AND fposincluido = 0 AND fposingresotercero = 0  GROUP BY productoid, fposdescripcion, fposproducto, fpospresentacion ORDER BY total DESC", substitutionValues: {
          "sucursalid": sucursalId,
          "fechainicial": fechaInicial,
          "fechafinal": fechaFinal
        });
      }

      for (var prod in _data) {
        final producto = ProductoUtilModel(
            id: int.tryParse(prod[0]),
            descripcion: prod[3],
            total: prod[2],
            cantidad: prod[4],
            presentacion: prod[5],
            producto: prod[1]);

        _listadoCortesias.add(producto);
      }
    } catch (e) {
      //
      print(e);
      print(e.runtimeType);
      print('Error al obtener productos');
      _listadoCortesias = [];
    }
    await conexionNegocio.close();

    return _listadoCortesias ?? [];
  }

  Future<Map> obtenerParticipacionGrupos(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ParticipacionGrupoModel> _listadoParticipacionGrupos = [];
    Map<String, double> _mapaParticipacionGrupos = {};
    int total = 0;

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT G.codigogrupo, G.descripcion, SUM(CAST(D.fpossubbase AS INTEGER)) AS valorbase, SUM(CAST(D.fposvalorunitario * D.fposcantidad AS INTEGER)) AS valor_subtotal FROM datos.facturadetalle D, datos.productos P, datos.grupos1 G, datos.facturacabecera cab WHERE D.productoid = P.productoid AND P.grupo1id = G.grupoid AND (fposfecha BETWEEN @fechainicial AND @fechafinal) AND cab.sucursalid = @sucursalid AND fposestado = 0 AND fposescortesia = 0 AND fposincluido = 0 AND fposingresotercero = 0 AND cab.fposprefijo = D.fposprefijo AND cab.fposnumero = D.fposnumero GROUP BY G.descripcion, G.grupoid, G.codigogrupo ORDER BY valor_subtotal  DESC", substitutionValues: {
        "sucursalid": sucursalId,
        "fechainicial": fechaInicial,
        "fechafinal": fechaFinal
      });

      for (var grupo in _data) {
        final nuevoGrupo = ParticipacionGrupoModel(
          codigoGrupo: grupo[0],
          descripcion: grupo[1],
          valorBase: grupo[2],
          valorSubtotal: grupo[3],
        );

        total += grupo[3];

        _listadoParticipacionGrupos.add(nuevoGrupo);
      }

      for (ParticipacionGrupoModel cat in _listadoParticipacionGrupos) {
        cat.porcentajeParticipacion = (cat.valorBase * 100) / total;
      }

      _mapaParticipacionGrupos = Map.fromIterable(
        _listadoParticipacionGrupos,
        key: (element) => element.descripcion,
        value: (element) => element.porcentajeParticipacion,
      );

      //
    } catch (e) {
      print(e);
      print(e.runtimeType);
      _listadoParticipacionGrupos = [];
      _mapaParticipacionGrupos = {};
    }
    await conexionNegocio.close();

    return {
      "ListaParticipacion": _listadoParticipacionGrupos ?? [],
      "MapaParticipacion": _mapaParticipacionGrupos ?? {},
    };
  }

  Future<List<VendedorTotalVentasModel>> obtenerVendedores(
      int sucursalId, String licencia) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<VendedorTotalVentasModel> _listaVendedores = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT nombre, vendedorid FROM datos.vendedor WHERE sucursalid = @sucursalid", substitutionValues: {
        "sucursalid": sucursalId
      });

      for (var vendedor in _data) {
        final nuevoVendedor = VendedorTotalVentasModel(
          id: vendedor[1],
          nombre: vendedor[0],
        );

        _listaVendedores.add(nuevoVendedor);
      }
    } catch (e) {
      print(e);
      print(e.runtimeType);
      _listaVendedores = [];
    }
    await conexionNegocio.close();

    return _listaVendedores ?? [];
  }

  Future<List<FacturaModel>> obtenerFacturas(
    String licencia,
    String fechaInicial,
    String fechaFinal,
    int sucursalId,
  ) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<FacturaModel> listadoFacturas = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT fposprefijo AS prefijo, CAST(fposnumero AS INTEGER), CAST(fpostotalpagar AS INTEGER), fposfecha AS fecha FROM datos.facturacabecera WHERE sucursalid = @sucursalid AND fposfecha BETWEEN @fechainicial AND @fechafinal AND fposestado <> 2", substitutionValues: {
        "sucursalid": sucursalId,
        "fechainicial": fechaInicial,
        "fechafinal": fechaFinal
      });

      for (var item in _data) {
        FacturaModel factura = FacturaModel();

        factura.prefijofactura = item[0];
        factura.numerofactura = item[1];
        factura.totalapagar = item[2];
        factura.fecha = item[3];

        listadoFacturas.add(factura);
      }

      //
    } catch (e) {
      //
      throw(e);
    }
    await conexionNegocio.close();

    return listadoFacturas ?? null;
  }

  Future<void> anularFacturaPos(String prefijo, int numero) async{
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query("UPDATE datos.facturacabecera SET fposestado = 2 WHERE fposprefijo = @fposprefijo AND fposnumero = @fposnumero", substitutionValues: {
        "fposprefijo": prefijo,
        "fposnumero": numero,
      });

    } catch (e) {
      throw(e);
    }

    await conexionNegocio.close();
  }
}
