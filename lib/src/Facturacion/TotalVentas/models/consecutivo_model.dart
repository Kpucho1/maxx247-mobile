import 'dart:convert';

ConsecutivoModel consecutivoModelFromJson(String str) =>
    ConsecutivoModel.fromJson(json.decode(str));

String consecutivoModelToJson(ConsecutivoModel data) =>
    json.encode(data.toJson());

class ConsecutivoModel {
  ConsecutivoModel({
    this.prefijo,
    this.desde,
    this.hasta,
  });

  String prefijo;
  int desde;
  int hasta;

  factory ConsecutivoModel.fromJson(Map<String, dynamic> json) =>
      ConsecutivoModel(
        prefijo: json["prefijo"],
        desde: json["desde"],
        hasta: json["hasta"],
      );

  Map<String, dynamic> toJson() => {
        "prefijo": prefijo,
        "desde": desde,
        "hasta": hasta,
      };
}
