import 'dart:convert';
import 'package:charts_flutter/flutter.dart' as charts;

BarChartTotalVentasModel barChartTotalVentasModelFromJson(String str) =>
    BarChartTotalVentasModel.fromJson(json.decode(str));

String barChartTotalVentasModelToJson(BarChartTotalVentasModel data) =>
    json.encode(data.toJson());

class BarChartTotalVentasModel {
  BarChartTotalVentasModel({
    this.dia,
    this.mes,
    this.anio,
    this.fecha,
    this.totalVentas,
    this.color,
  });

  String dia;
  String mes;
  String anio;
  String fecha;
  int totalVentas;
  charts.Color color;

  factory BarChartTotalVentasModel.fromJson(Map<String, dynamic> json) =>
      BarChartTotalVentasModel(
        dia: json["dia"],
        mes: json["mes"],
        anio: json["anio"],
        fecha: json["fecha"],
        totalVentas: json["totalVentas"],
      );

  Map<String, dynamic> toJson() => {
        "dia": dia,
        "mes": mes,
        "anio": anio,
        "fecha": fecha,
        "totalVentas": totalVentas,
      };
}
