import 'dart:convert';

ProductoUtilModel productoUtilModelFromJson(String str) =>
    ProductoUtilModel.fromJson(json.decode(str));

String productoUtilModelToJson(ProductoUtilModel data) =>
    json.encode(data.toJson());

class ProductoUtilModel {
  ProductoUtilModel({
    this.id,
    this.producto,
    this.descripcion,
    this.total,
    this.cantidad,
    this.presentacion,
  });

  int id;
  String producto;
  String descripcion;
  int total;
  int cantidad;
  String presentacion;

  factory ProductoUtilModel.fromJson(Map<String, dynamic> json) =>
      ProductoUtilModel(
        id: json["id"],
        producto: json["producto"],
        descripcion: json["descripcion"],
        total: json["total"],
        cantidad: json["cantidad"],
        presentacion: json["presentacion"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "producto": producto,
        "descripcion": descripcion,
        "total": total,
        "cantidad": cantidad,
        "presentacion": presentacion,
      };
}
