import 'dart:convert';

VendedorTotalVentasModel vendedorTotalVentasModelFromJson(String str) =>
    VendedorTotalVentasModel.fromJson(json.decode(str));

String vendedorTotalVentasModelToJson(VendedorTotalVentasModel data) =>
    json.encode(data.toJson());

class VendedorTotalVentasModel {
  VendedorTotalVentasModel({
    this.id,
    this.nombre,
  });

  int id;
  String nombre;

  factory VendedorTotalVentasModel.fromJson(Map<String, dynamic> json) =>
      VendedorTotalVentasModel(
        id: json["id"],
        nombre: json["nombre"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
      };
}
