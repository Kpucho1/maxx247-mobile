import 'dart:convert';

ParticipacionGrupoModel participacionGrupoModelFromJson(String str) =>
    ParticipacionGrupoModel.fromJson(json.decode(str));

String participacionGrupoModelToJson(ParticipacionGrupoModel data) =>
    json.encode(data.toJson());

class ParticipacionGrupoModel {
  ParticipacionGrupoModel({
    this.codigoGrupo,
    this.descripcion,
    this.valorBase,
    this.valorSubtotal,
    this.porcentajeParticipacion,
  });

  String codigoGrupo;
  String descripcion;
  int valorBase;
  int valorSubtotal;
  double porcentajeParticipacion;

  factory ParticipacionGrupoModel.fromJson(Map<String, dynamic> json) =>
      ParticipacionGrupoModel(
        codigoGrupo: json["codigoGrupo"],
        descripcion: json["descripcion"],
        valorBase: json["valorBase"],
        valorSubtotal: json["valorSubtotal"],
        porcentajeParticipacion: json["porcentajeParticipacion"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "codigoGrupo": codigoGrupo,
        "descripcion": descripcion,
        "valorBase": valorBase,
        "valorSubtotal": valorSubtotal,
        "porcentajeParticipacion": porcentajeParticipacion,
      };
}
