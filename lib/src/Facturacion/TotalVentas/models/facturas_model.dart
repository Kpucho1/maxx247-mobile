// To parse this JSON data, do
//
//     final facturaModel = facturaModelFromJson(jsonString);

import 'dart:convert';

FacturaModel facturaModelFromJson(String str) => FacturaModel.fromJson(json.decode(str));

String facturaModelToJson(FacturaModel data) => json.encode(data.toJson());

class FacturaModel {
    FacturaModel({
        this.id,
        this.prefijofactura,
        this.numerofactura,
        this.totalapagar,
        this.fecha,
    });

    int id;
    String prefijofactura;
    int numerofactura;
    int totalapagar;
    DateTime fecha;

    factory FacturaModel.fromJson(Map<String, dynamic> json) => FacturaModel(
        id: json["id"],
        prefijofactura: json["prefijofactura"],
        numerofactura: json["numerofactura"],
        totalapagar: json["totalapagar"],
        fecha: json["fecha"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "prefijofactura": prefijofactura,
        "numerofactura": numerofactura,
        "totalapagar": totalapagar,
        "fecha": fecha,
    };
}
