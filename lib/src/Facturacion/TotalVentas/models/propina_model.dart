import 'dart:convert';

PropinasModel propinasModelFromJson(String str) =>
    PropinasModel.fromJson(json.decode(str));

String propinasModelToJson(PropinasModel data) => json.encode(data.toJson());

class PropinasModel {
  PropinasModel({
    this.totalpropina,
    this.propinaefectivo,
    this.propinacredito,
    this.propinatarjeta,
  });

  int totalpropina;
  int propinaefectivo;
  int propinacredito;
  int propinatarjeta;

  factory PropinasModel.fromJson(Map<String, dynamic> json) => PropinasModel(
        totalpropina: json["totalpropina"],
        propinaefectivo: json["propinaefectivo"],
        propinacredito: json["propinacredito"],
        propinatarjeta: json["propinatarjeta"],
      );

  Map<String, dynamic> toJson() => {
        "totalpropina": totalpropina,
        "propinaefectivo": propinaefectivo,
        "propinacredito": propinacredito,
        "propinatarjeta": propinatarjeta,
      };
}
