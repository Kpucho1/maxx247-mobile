import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:connectivity/connectivity.dart';

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/total_ventas_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/vendedor_totalVentas_model.dart';

class TotalVentasLoadingPage extends StatefulWidget {
  @override
  _TotalVentasLoadingPageState createState() => _TotalVentasLoadingPageState();
}

class _TotalVentasLoadingPageState extends State<TotalVentasLoadingPage> {
  final Connectivity _connectivity = Connectivity();
  final prefs = PreferenciasUsuario();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      await _initConexionInternet();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.transparent,
      body: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
        ),
      ),
    );
  }

  Future<void> _initConexionInternet() async {
    ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return Future.value(null);
    }

    return _actualizarEstadoDeLaConexion(result);
  }

  //Funcion que ejecuta código según sea el estado de la conexión a internet
  Future<void> _actualizarEstadoDeLaConexion(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _conectarTotalVentas();
        break;
      case ConnectivityResult.none:
        Navigator.pop(context, 'Conexión de red no disponible');
        break;
    }
  }

  void _conectarTotalVentas() async {
    //Fechas para calcular hoy por defecto
    final _fechaFinal = DateTime.now().toLocal();
    final _fechaInicio = DateTime.now()
        .subtract(
            Duration(hours: _fechaFinal.hour, minutes: _fechaFinal.minute))
        .toLocal();

    //Establece las fechas en el estado
    final fechasProvider =
        Provider.of<TotalVentasProvider>(context, listen: false);
    fechasProvider.establecerFechaInicial(_fechaInicio.toString());
    fechasProvider.establecerFechaFinal(_fechaFinal.toString());

    //Establece "Todos los vendedores" como primer filtro
    final _todosVendedores = VendedorTotalVentasModel();

    _todosVendedores.id = 0;
    _todosVendedores.nombre = 'Todos los vendedores';

    final vendedorProvider = context.read<TotalVentasProvider>();
    vendedorProvider.establecerVendedorSeleccionado(_todosVendedores);

    if (prefs.sucursalid == null) {
      prefs.sucursalid = 1;
    }

    final _totalVentasProvider = context.read<TotalVentasProvider>();
    final _fechaInicial = context.read<TotalVentasProvider>().fechaInicial;
    final _fechaFinalFunc = context.read<TotalVentasProvider>().fechaFinal;

    //Para la grafica y la tabla mostrar totales y facturas
    await context.read<TotalVentasProvider>().establecerMapaFormasDePagoGlobal(
          _fechaInicial,
          _fechaFinalFunc,
          _totalVentasProvider.vendedorSeleccionado.id,
          prefs.sucursalid ?? 1,
        );

    await context.read<TotalVentasProvider>().obtenerConsecutivos(
          _fechaInicial,
          _fechaFinalFunc,
          prefs.sucursalid ?? 1,
        );

    await context.read<TotalVentasProvider>().obtenerPropinas(
        _fechaInicial, _fechaFinalFunc, prefs.sucursalid ?? 1);
    
    await context.read<TotalVentasProvider>().obtenerListadoFacturas(
        _fechaInicial, _fechaFinalFunc, prefs.sucursalid ?? 1);

    await context.read<TotalVentasProvider>().obtenerUtilidadProductos(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
          _totalVentasProvider.vendedorSeleccionado.id ?? 0,
        );

    await context.read<TotalVentasProvider>().obtenerCortesias(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
          _totalVentasProvider.vendedorSeleccionado.id ?? 0,
        );

    await context.read<TotalVentasProvider>().obtenerParticipacionGrupos(
          _totalVentasProvider.fechaInicial,
          _totalVentasProvider.fechaFinal,
          prefs.sucursalid ?? 1,
        );

    //Para la grafica de barras
    // await _totalVentasProvider.establecerListaSeriesBarChartTotalVentas(
    //   _conexionActivaProvider,
    //   _fechaInicial,
    //   _fechaFinalFunc,
    //   _totalVentasProvider.vendedorSeleccionado.id,
    //   prefs.sucursalid,
    //   prefs.uid,
    // );

    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => TotalVentasPage(),
      ),
    );
  }
}
