import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/facturas_model.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/services/repositorio_total_ventas.dart';
import 'package:provider/src/provider.dart';
import 'dart:math';


class FacturasPage extends StatefulWidget {
  const FacturasPage({Key key}) : super(key: key);

  @override
  _FacturasPageState createState() => _FacturasPageState();
}

class _FacturasPageState extends State<FacturasPage> {
  TextEditingController _codigoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBarFacturas(),
      body: _cuerpoFacturas(),
    );
  }

  @override
  void dispose() {
    _codigoController?.dispose();
    super.dispose();
  }

  Widget _appBarFacturas() {
    final Size _screenSize = MediaQuery.of(context).size;

    return AppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Text(
        'Facturas generadas',
        style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: _screenSize.height < 850 ? 18.0 : 20.0),
      ),
      elevation: 0.5,
      actions: [
        // _botonInformativoProductos(),
      ],
    );
  }

  Widget _cuerpoFacturas() {
    final _listadoFacturas =
        context.select((TotalVentasProvider value) => value.listadoFacturas);

    return _listadoFacturas != null
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: _listadoFacturas != null ? _listadoFacturas.length : 0,
            itemBuilder: (context, index) {
              return _facturas(_listadoFacturas[index]);
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _facturas(FacturaModel factura) {

    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        children: [
          ListTile(
            title: Text(factura.prefijofactura+" - "+factura.numerofactura.toString()),
            trailing: Text("\$ "+formatCurrency.format(factura.totalapagar)),
            subtitle: Text(factura.fecha.toLocal().toString()),
            onTap: () async{
              _dialogAnularFactura(factura.prefijofactura, factura.numerofactura);
            },
          ),
        ],
      ),
    );
  }

  Future _dialogAnularFactura(String prefijo, int numero){
    var number = Random();
    int codigo = number.nextInt(9999);
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("¿Está seguro que desea anular la factura? Digite el número para continuar: "+ codigo.toString()),
          content: TextFormField(
            controller: _codigoController,
            decoration: InputDecoration(
              hintText: "Codigo"
            ),
          ),
          actions: [
            ElevatedButton(
              child: Text("Cancelar"),
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
                padding: EdgeInsets.symmetric(horizontal: 5),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              onPressed: () async{
                Navigator.pop(context);

              },
            ),
            ElevatedButton(
              child: Text("Anular"),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                padding: EdgeInsets.symmetric(horizontal: 5),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
              onPressed: () async{
                RepositorioTotalVentas repositorioTotalVentas = RepositorioTotalVentas();
                if (_codigoController.text == codigo.toString()) {
                  await repositorioTotalVentas.anularFacturaPos(prefijo, numero);
                  Navigator.pop(context);
                }
                else{
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      behavior: SnackBarBehavior.floating,
                      duration: Duration(seconds: 5),
                      backgroundColor: Colors.red,
                      content: Text(
                        'Código incorrecto',
                        style: TextStyle(
                          fontSize: 14.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  );
                }

              },
            )
          ],
        );
      },
    );
  }
}
