import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';

// ignore: must_be_immutable
class HoyPage extends StatefulWidget {
  @override
  _HoyPageState createState() => _HoyPageState();
}

class _HoyPageState extends State<HoyPage> with WidgetsBindingObserver {
  final prefs = PreferenciasUsuario();
  final _scaffoldKeyHoy = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    _obtenerInfo();
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //maxx_1088008751_1

      final snackBar = SnackBar(
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        content: Text(
          'Actualizando...',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
        backgroundColor: Colors.cyan[600],
        duration: Duration(seconds: 3),
        onVisible: () async {
          //
          await context
              .read<TotalVentasProvider>()
              .establecerMapaFormasDePagoHoy(
                prefs.sucursalid ?? 1,
              );
        },
      );

      _scaffoldKeyHoy.currentState.showSnackBar(snackBar);
    }

    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  _obtenerInfo() async {
    //Se usa función para poder usar async/await en el Initstate

    await context.read<TotalVentasProvider>().establecerMapaFormasDePagoHoy(
          prefs.sucursalid ?? 1,
        );
  }

  //Datos para la gráfica
  Map<String, double> _formasDePago;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKeyHoy,
      appBar: _appBarHoyPage(),
      backgroundColor: Color(0xffFEFEFA),
      body: _cuerpoEstadisticasRecientes(),
    );
  }

  Widget _appBarHoyPage() {
    final _screenSize = MediaQuery.of(context).size;

    final _totalVentasHoy =
        context.select((TotalVentasProvider value) => value.totalVentasHoy);
    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    return AppBar(
      centerTitle: false,
      title: Text(
        'Hoy, \$ ${_totalVentasHoy != null ? formatCurrency.format(_totalVentasHoy) : 0.0}',
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: _screenSize.height < 850 ? 21.0 : 24.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: Color(0xffFEFEFA),
      elevation: 0.0,
      leading: _botonRefreshHoy(),
      actions: [
        _botonDeNotificaciones(context),
      ],
    );
  }

  Widget _botonDeNotificaciones(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return IconButton(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      splashRadius: 0.5,
      icon: Icon(
        CupertinoIcons.bell,
        color: Theme.of(context).accentColor,
        size: _screenSize.height < 850 ? 27.0 : 29.0,
      ),
      onPressed: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => null,
          ),
        );
      },
    );
  }

  Widget _botonRefreshHoy() {
    return IconButton(
      iconSize: 27.0,
      highlightColor: Colors.transparent,
      icon: Icon(
        CupertinoIcons.refresh,
        color: Theme.of(context).accentColor,
        size: 25.0,
      ),
      onPressed: () {
        //maxx_1088008751_1

        final snackBar = SnackBar(
          behavior: SnackBarBehavior.floating,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          content: Text(
            'Actualizando...',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
          backgroundColor: Colors.cyan[600],
          duration: Duration(seconds: 3),
          onVisible: () async {
            final _totalVentasProvider = context.read<TotalVentasProvider>();

            await _totalVentasProvider.establecerMapaFormasDePagoHoy(
              prefs.sucursalid ?? 1,
            );

            // await _totalVentasProvider.establecerTotalDeVentasHoy(
            //   prefs.conexionId,
            //   _fechaInicial.toString(),
            //   _fechaFinal.toString(),
            //   0,
            //   prefs.sucursalid,
            //   prefs.uid,
            // );
          },
        );

        _scaffoldKeyHoy.currentState.showSnackBar(snackBar);
      },
    );
  }

  Widget _cuerpoEstadisticasRecientes() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _tarjetaGraficaCircularTextoActualizado(),
      ],
    );
  }

  Widget _tarjetaGraficaCircularTextoActualizado() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              _graficaCircularFormasDePago(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _textoTiempoActualizacion(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _graficaCircularFormasDePago() {
    _formasDePago = context
        .select((TotalVentasProvider value) => value.mapaFormasDePagoHoy);

    return _formasDePago == null ||
            _formasDePago.isEmpty ||
            _formasDePago["Efectivo"] == 0 &&
                _formasDePago["Tarj. Débito"] == 0 &&
                _formasDePago["Tarj. Crédito"] == 0 &&
                _formasDePago["Abonos"] == 0
        ? Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: Column(
                children: [
                  Expanded(child: SvgPicture.asset('assets/sin_registros.svg')),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text('Sin registros aún para mostrar',
                      style: TextStyle(fontSize: 16.0)),
                ],
              ),
            ),
          )
        : PieChart(
            dataMap: _formasDePago,
            chartValuesOptions: ChartValuesOptions(
              showChartValuesInPercentage: true,
            ),
            chartType: ChartType.ring,
            chartRadius: MediaQuery.of(context).size.width * 0.45,
          );
  }

  Widget _textoTiempoActualizacion() {
    final _ultimoActualizado =
        context.select((TotalVentasProvider value) => value.ultimoActualizado);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Actualizado',
          textAlign: TextAlign.center,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.01,
        ),
        Text(_ultimoActualizado ?? 'sin registro'),
      ],
    );
  }
}
