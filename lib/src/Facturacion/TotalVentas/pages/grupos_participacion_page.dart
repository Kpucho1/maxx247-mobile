import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_svg/flutter_svg.dart';

import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/participacion_grupo_model.dart';

class ParticipacionGruposPage extends StatefulWidget {
  @override
  _ParticipacionGruposPageState createState() =>
      _ParticipacionGruposPageState();
}

class _ParticipacionGruposPageState extends State<ParticipacionGruposPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffFEFEFA),
      appBar: _appBarParticipacionGrupos(),
      body: _cuerpoParticipacionGrupos(),
    );
  }

  Widget _appBarParticipacionGrupos() {
    final Size _screenSize = MediaQuery.of(context).size;

    return AppBar(
      elevation: 0.5,
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Text(
        'Participación por grupos',
        style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: _screenSize.height < 850 ? 18.0 : 20.0),
      ),
      actions: [
        _botonInformativoGrupos(),
      ],
    );
  }

  Widget _botonInformativoGrupos() {
    final Size _screenSize = MediaQuery.of(context).size;

    return IconButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      icon: Icon(
        CupertinoIcons.info,
        color: Theme.of(context).iconTheme.color,
        size: _screenSize.height < 850 ? 27.0 : 30.0,
      ),
      onPressed: () {
        return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              title: Icon(
                CupertinoIcons.doc_chart,
                size: 50.0,
              ),
              content: Text(
                'Aquí encontrarás cuales son los grupos de productos que más aportaron al valor del total de ventas en el rango de fechas seleccionado.',
                textAlign: TextAlign.justify,
              ),
              actions: [
                TextButton(
                  child: Text(
                    'Cerrar',
                    style: TextStyle(
                      color: Theme.of(context).iconTheme.color,
                      fontSize: _screenSize.height < 850 ? 15.0 : 17.0,
                    ),
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Widget _cuerpoParticipacionGrupos() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 5.0,
        ),
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            children: [
              _graficaParticipaciones(context),
              SizedBox(
                height: 6.0,
              ),
              Text('Porcentaje (%) de participación de cada grupo'),
              SizedBox(
                height: 7.0,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        _textoDetallesParticipacion(),
        SizedBox(
          height: 5.0,
        ),
        Expanded(child: _listaDetallesDeParticipacion()),
      ],
    );
  }

  Widget _textoDetallesParticipacion() {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: _screenSize.height * 0.035,
      width: double.infinity,
      child: Text(
        'Detalles de participación',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: _screenSize.height < 850 ? 18.0 : 20.0,
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _graficaParticipaciones(BuildContext context) {
    List<ParticipacionGrupoModel> _listaParticipacion = [];

    _listaParticipacion = context
        .select((TotalVentasProvider value) => value.listaParticipacionGrupos);

    List<charts.Series<ParticipacionGrupoModel, String>> series = [
      charts.Series(
        id: 'Porcentajes de participación',
        data: _listaParticipacion,
        domainFn: (ParticipacionGrupoModel datum, _) => datum.descripcion,
        measureFn: (ParticipacionGrupoModel datum, _) =>
            datum.porcentajeParticipacion,
        colorFn: (_, __) =>
            charts.ColorUtil.fromDartColor(Theme.of(context).iconTheme.color),
      ),
    ];

    return _listaParticipacion == null || _listaParticipacion == []
        ? Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: Column(
                children: [
                  Expanded(child: SvgPicture.asset('assets/sin_registros.svg')),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    'No se encontraron registros',
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ),
            ),
          )
        : Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            height: 280.0,
            child: charts.BarChart(
              series,
              animate: true,
              vertical: false,
            ),
          );
  }

  Widget _listaDetallesDeParticipacion() {
    List<ParticipacionGrupoModel> _listaParticipacion = [];

    _listaParticipacion = context
        .select((TotalVentasProvider value) => value.listaParticipacionGrupos);

    return _listaParticipacion != null || _listaParticipacion != []
        ? ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount:
                _listaParticipacion != null ? _listaParticipacion.length : 0,
            itemBuilder: (context, index) {
              return _grupos(_listaParticipacion[index]);
            },
          )
        : Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: Column(
                children: [
                  Expanded(child: SvgPicture.asset('assets/sin_registros.svg')),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    'No se encontraron registros',
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ),
            ),
          );
  }

  Widget _grupos(ParticipacionGrupoModel grupo) {

    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");


    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: ListTile(
        leading: Text(grupo.codigoGrupo ?? ''),
        title: Text(grupo.descripcion ?? ''),
        subtitle: Text(
          'Base: \$ ${formatCurrency.format(grupo.valorBase) ?? '0'} / Total \$ ${formatCurrency.format(grupo.valorSubtotal) ?? '0'}' ??
              '',
          
        ),
        
      ),
    );
  }
}
