import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/producto_util_model.dart';

class CortesiasPage extends StatefulWidget {
  @override
  _CortesiasPageState createState() => _CortesiasPageState();
}

class _CortesiasPageState extends State<CortesiasPage> {
  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffFEFEFA),
      appBar: _appBarProductos(),
      body: _cuerpoProductos(),
    );
  }

  Widget _appBarProductos() {
    final Size _screenSize = MediaQuery.of(context).size;

    return AppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Text(
        'Cortesias ofrecidas',
        style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: _screenSize.height < 850 ? 18.0 : 20.0),
      ),
      elevation: 0.5,
      actions: [
        _botonInformativoProductos(),
      ],
    );
  }

  Widget _botonInformativoProductos() {
    final Size _screenSize = MediaQuery.of(context).size;

    return IconButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      icon: Icon(
        CupertinoIcons.info,
        color: Theme.of(context).iconTheme.color,
        size: _screenSize.height < 850 ? 27.0 : 30.0,
      ),
      onPressed: () {
        return showDialog(
          context: context,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    '¿Qué encontrarás aquí?',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: _screenSize.height < 850 ? 18.0 : 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 35.0),
                    child: Text(
                      'En esta vista podrás ver las cortesías que se ofrecieron en tu negocio.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: _screenSize.height < 850 ? 15.0 : 17.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  TextButton(
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Theme.of(context).iconTheme.color,
                        fontSize: _screenSize.height < 850 ? 15.0 : 17.0,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget _cuerpoProductos() {
    final _listadoProductos =
        context.select((TotalVentasProvider value) => value.listadoCortesias);

    return _listadoProductos != null
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: _listadoProductos != null ? _listadoProductos.length : 0,
            itemBuilder: (context, index) {
              return _productos(_listadoProductos[index]);
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _productos(ProductoUtilModel producto) {
    final Size _screenSize = MediaQuery.of(context).size;

    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        children: [
          ListTile(
            leading: Text(producto.id.toString() ?? ''),
            title: Text(producto.descripcion ?? ''),
            subtitle: Text(
              'Cantidad: ${producto.cantidad.toString()} / ${producto.presentacion ?? 'Unidad'}' ??
                  '',
              style: TextStyle(
                fontSize: _screenSize.height < 850 ? 14.0 : 15.0,
              ),
            ),
            trailing: Text(
              '\$ ${formatCurrency.format(producto.total).toString()}' ?? '',
              style: TextStyle(
                fontSize: _screenSize.height < 850 ? 14.0 : 15.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
