import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/models/bar_chart_totalVentas_model.dart';

class EstadisticasPage extends StatefulWidget {
  @override
  _EstadisticasPageState createState() => _EstadisticasPageState();
}

class _EstadisticasPageState extends State<EstadisticasPage> {
  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: _appBarEstadisticas(),
      body: _cuerpoEstadisticas(),
    );
  }

  Widget _appBarEstadisticas() {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      title: Text(
        'Estadísticas de ventas',
        style: TextStyle(color: Colors.black),
      ),
      centerTitle: false,
      elevation: 0.0,
    );
  }

  Widget _cuerpoEstadisticas() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _cardGraficaDeBarras(),
      ],
    );
  }

  Widget _cardGraficaDeBarras() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.3,
        child: Card(
          elevation: 2.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: _graficaDeBarrasTotalVentas(),
          ),
        ),
      ),
    );
  }

  Widget _graficaDeBarrasTotalVentas() {
    final _listadoDataSeries = context.select(
        (TotalVentasProvider value) => value.listadoBarChartTotalVentas);

    List<charts.Series<BarChartTotalVentasModel, String>> series = [
      charts.Series(
        id: 'Total Ventas',
        data: _listadoDataSeries,
        domainFn: (BarChartTotalVentasModel series, _) => series.fecha,
        measureFn: (BarChartTotalVentasModel series, _) => series.totalVentas,
        colorFn: (_, __) =>
            charts.ColorUtil.fromDartColor(Theme.of(context).accentColor),
      ),
    ];

    return charts.BarChart(
      series,
      animate: true,
    );
  }
}
