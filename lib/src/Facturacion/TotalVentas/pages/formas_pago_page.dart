import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';

class FormasDePagoPage extends StatefulWidget {
  @override
  _FormasDePagoPageState createState() => _FormasDePagoPageState();
}

class _FormasDePagoPageState extends State<FormasDePagoPage> {
  final prefs = PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBarFormasDePago(),
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffFEFEFA),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: _cuerpoFormasDePagoPage(context),
      ),
    );
  }

  Widget _appBarFormasDePago() {
    final Size _screenSize = MediaQuery.of(context).size;

    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      title: Text(
        'Ingresos por formas de pago',
        style: TextStyle(
            color: Colors.black,
            fontSize: _screenSize.height < 850 ? 18.0 : 20.0),
      ),
      centerTitle: false,
      elevation: 0.5,
      actions: [
        _botonInformativoFormasDePago(),
      ],
    );
  }

  Widget _botonInformativoFormasDePago() {
    final Size _screenSize = MediaQuery.of(context).size;

    return IconButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      icon: Icon(
        CupertinoIcons.info,
        color: Theme.of(context).iconTheme.color,
        size: _screenSize.height < 850 ? 27.0 : 30.0,
      ),
      onPressed: () {
        return showDialog(
          context: context,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    '¿Qué encontrarás aquí?',
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: _screenSize.height < 850 ? 18.0 : 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      'En esta vista podrás ver el total de ingresos, comparar los ingresos por formas de pago y los consecutivos de facturación.\n\nMás adelante también se mostrará lo relacionado a impuestos, propinas y ganancias netas.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: _screenSize.height < 850 ? 15.0 : 17.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  TextButton(
                    child: Text(
                      'Cerrar',
                      style: TextStyle(
                        color: Theme.of(context).iconTheme.color,
                        fontSize: _screenSize.height < 850 ? 15.0 : 17.0,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget _cuerpoFormasDePagoPage(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _tarjetaGraficaCircular(context),
        _totalVentasFormasDePago(),
        _tablaFormasDePago(),
        _tituloConsecutivos(),
        _tablaConsecutivos(),
        _tituloPropinas(),
        _tablaPropinas()
      ],
    );
  }

  Widget _tarjetaGraficaCircular(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              _graficaCircularFormasDePago(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _graficaCircularFormasDePago(BuildContext context) {
    Map<String, double> _formasDePago = <String, double>{};
    _formasDePago =
        context.select((TotalVentasProvider value) => value.mapaFormasDePago);

    print('Efectivo ${_formasDePago["Efectivo"]}');

    return _formasDePago == null ||
            _formasDePago.isEmpty ||
            _formasDePago["Efectivo"] == 0 &&
                _formasDePago["Tarj. Débito"] == 0 &&
                _formasDePago["Tarj. Crédito"] == 0 &&
                _formasDePago["Abonos"] == 0 &&
                _formasDePago["Créditos"] == 0 &&
                _formasDePago["Bonos"] == 0 &&
                _formasDePago["Cheques"] == 0
        ? Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.22,
              child: Column(
                children: [
                  Expanded(child: SvgPicture.asset('assets/sin_registros.svg')),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    'No se encontraron registros',
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ),
            ),
          )
        : PieChart(
            dataMap: _formasDePago,
            centerText: _formasDePago == null ||
                    _formasDePago.isEmpty ||
                    _formasDePago["Efectivo"] == 0 &&
                        _formasDePago["Tarj. Débito"] == 0 &&
                        _formasDePago["Tarj. Crédito"] == 0 &&
                        _formasDePago["Abonos"] == 0 &&
                        _formasDePago["Créditos"] == 0 &&
                        _formasDePago["Bonos"] == 0 &&
                        _formasDePago["Cheques"] == 0
                ? 'No se encontraron registros'
                : '',
            chartValuesOptions: ChartValuesOptions(
              showChartValuesInPercentage: true,
              chartValueBackgroundColor: Colors.transparent,
            ),
            chartType: ChartType.ring,
            chartRadius: MediaQuery.of(context).size.width * 0.45,
          );
  }

  Widget _totalVentasFormasDePago() {
    final _totalVentas =
        context.select((TotalVentasProvider value) => value.totalVentasGlobal);
    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: _screenSize.height * 0.035,
      width: double.infinity,
      child: Text(
        'Total ingresos: \$ ${formatCurrency.format(_totalVentas)}',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: _screenSize.height < 850 ? 18.0 : 20.0,
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _tablaFormasDePago() {
    final Size _screenSize = MediaQuery.of(context).size;

    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    Map<String, double> _formasDePago = <String, double>{};
    _formasDePago =
        context.select((TotalVentasProvider value) => value.mapaFormasDePago);

    Map<String, dynamic> _numeroFacturasFormasDePago = <String, dynamic>{};
    _numeroFacturasFormasDePago = context
        .select((TotalVentasProvider value) => value.mapaFacturasFormasDePago);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: DataTable(
          columnSpacing: _screenSize.height < 850 ? 24.0 : 33.0,
          columns: [
            DataColumn(
              label: Text('Forma de pago'),
            ),
            DataColumn(
              numeric: true,
              label: Text('# Facturas'),
            ),
            DataColumn(
              numeric: true,
              label: Text('Monto \$'),
            ),
          ],
          rows: [
            DataRow(
              cells: [
                DataCell(
                  Text('Efectivo'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Efectivo"] != null
                        ? _numeroFacturasFormasDePago["Efectivo"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Efectivo"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Efectivo"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Tarj. Crédito'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Tarj. Crédito"] != null
                        ? _numeroFacturasFormasDePago["Tarj. Crédito"]
                            .toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Tarj. Crédito"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Tarj. Crédito"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Tarj. Débito'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Tarj. Débito"] != null
                        ? _numeroFacturasFormasDePago["Tarj. Débito"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Tarj. Débito"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Tarj. Débito"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Abonos'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Abonos"] != null
                        ? _numeroFacturasFormasDePago["Abonos"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Abonos"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Abonos"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Créditos'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Créditos"] != null
                        ? _numeroFacturasFormasDePago["Créditos"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Créditos"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Créditos"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Bonos'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Bonos"] != null
                        ? _numeroFacturasFormasDePago["Bonos"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Bonos"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Bonos"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Cheques'),
                ),
                DataCell(
                  Text(
                    _numeroFacturasFormasDePago["Cheques"] != null
                        ? _numeroFacturasFormasDePago["Cheques"].toString()
                        : '0',
                  ),
                ),
                DataCell(
                  Text(
                    _formasDePago["Cheques"] != null
                        ? '\$ ${formatCurrency.format(_formasDePago["Cheques"])}'
                        : '\$ 0',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _tituloConsecutivos() {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: _screenSize.height * 0.035,
      width: double.infinity,
      child: Text(
        'Consecutivos',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 20.0,
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _tablaConsecutivos() {
    final Size _screenSize = MediaQuery.of(context).size;

    final listadoConsecutivos =
        context.select((TotalVentasProvider value) => value.listaConsecutivos);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: DataTable(
          columnSpacing: _screenSize.height < 850 ? 33.0 : 36.0,
          columns: [
            DataColumn(
              label: Text('Prefijo'),
            ),
            DataColumn(
              numeric: true,
              label: Text('Desde'),
            ),
            DataColumn(
              numeric: true,
              label: Text('Hasta'),
            ),
          ],
          rows: listadoConsecutivos.map((consecutivo) {
            return DataRow(
              cells: [
                DataCell(
                  Text('${consecutivo.prefijo}' ?? '000'),
                ),
                DataCell(
                  Text('${consecutivo.desde}' ?? '000'),
                ),
                DataCell(
                  Text('${consecutivo.hasta}' ?? '000'),
                ),
              ],
            );
          }).toList(),
        ),
      ),
    );
  }

  Widget _tituloPropinas() {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: _screenSize.height * 0.035,
      width: double.infinity,
      child: Text(
        'Propinas',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 20.0,
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _tablaPropinas() {
    final Size _screenSize = MediaQuery.of(context).size;

    final propinas =
        context.select((TotalVentasProvider value) => value.propinas);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: DataTable(
          columnSpacing: _screenSize.height < 850 ? 33.0 : 36.0,
          columns: [
            DataColumn(
              label: Text('Forma pago'),
            ),
            DataColumn(
              numeric: true,
              label: Text('Total'),
            ),
          ],
          rows: [
            DataRow(
              cells: [
                DataCell(
                  Text('Efectivo'),
                ),
                DataCell(
                  Text('${propinas.propinaefectivo}' ?? '0'),
                )
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Tarjeta'),
                ),
                DataCell(
                  Text('${propinas.propinatarjeta}' ?? '0'),
                )
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text('Credito'),
                ),
                DataCell(
                  Text('${propinas.propinacredito}' ?? '0'),
                )
              ],
            ),
            DataRow(
              cells: [
                DataCell(
                  Text(
                    'Total',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                DataCell(
                  Text('${propinas.totalpropina}' ?? '0',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
