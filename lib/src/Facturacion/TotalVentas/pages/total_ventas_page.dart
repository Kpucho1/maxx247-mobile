import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:maxxshop/src/Facturacion/menu_facturacion_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/cortesias_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/facturas_page.dart';
import 'package:provider/provider.dart';

import 'package:maxxshop/src/Facturacion/TotalVentas/pages/productos_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/formas_pago_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/bloc/total_ventas_provider.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/grupos_participacion_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/dialogs/filtro_fecha_hora_dialog.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/dialogs/filtro_vendedores_dialog.dart';


class TotalVentasPage extends StatefulWidget {
  @override
  _TotalVentasPageState createState() => _TotalVentasPageState();
}

class _TotalVentasPageState extends State<TotalVentasPage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(context, MaterialPageRoute(builder: (context) => MenuFacturacionPage()));
        return true;
      },
      child: Scaffold(
        appBar: _appBarTotalVentas(),
        backgroundColor: Colors.white,
        body: _cuerpoTotalDeVentas(context),
        bottomNavigationBar: _bottomNavBarTotalVentas(),
      ),
    );
  }

  Widget _appBarTotalVentas() {
    final _screenSize = MediaQuery.of(context).size;

    return AppBar(
      automaticallyImplyLeading: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: 20.0,
        ),
        color: Theme.of(context).accentColor,
        splashColor: Colors.transparent,
        splashRadius: 0.5,
        onPressed: () async{
          Navigator.push(context, MaterialPageRoute(builder: (context) => MenuFacturacionPage()));
        } ,
      ),
      centerTitle: false,
      title: Text(
        'Total ventas',
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: _screenSize.height < 850 ? 23.0 : 24.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: Colors.white,
      elevation: 0.0,
      actions: [
        _botonFiltrarPorFechaYHora(),
        _botonFiltrarPorVendedores(),
        // _botonFiltrarPorCajero(),
      ],
    );
  }

  Widget _botonFiltrarPorFechaYHora() {
    final _screenSize = MediaQuery.of(context).size;

    return IconButton(
      splashColor: Colors.transparent,
      splashRadius: 0.5,
      icon: Icon(
        CupertinoIcons.calendar,
        size: _screenSize.height < 850 ? 25.0 : 28.0,
        color: Theme.of(context).iconTheme.color,
      ),
      onPressed: () => _mostrarFiltrosDeFechaYHora(),
    );
  }

  Widget _botonFiltrarPorVendedores() {
    final _screenSize = MediaQuery.of(context).size;

    return IconButton(
      splashColor: Colors.transparent,
      splashRadius: 0.5,
      icon: Icon(
        CupertinoIcons.person_2,
        size: _screenSize.height < 850 ? 26.0 : 30.0,
        color: Theme.of(context).iconTheme.color,
      ),
      onPressed: () => _mostrarFiltroVendedores(),
    );
  }

  // Widget _botonFiltrarPorCajero() {
  //   final _screenSize = MediaQuery.of(context).size;

  //   return IconButton(
  //     icon: Icon(
  //       Icons.table_view,
  //       size: _screenSize.height < 850 ? 26.0 : 30.0,
  //       color: Theme.of(context).accentColor,
  //     ),
  //     onPressed: () => _mostrarFiltroCajero(),
  //   );
  // }

  Widget _cuerpoTotalDeVentas(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _tarjetaFiltrosAplicados(context),
        Expanded(child: _cambiarPaginas(_currentIndex)),
      ],
    );
  }

  Widget _cambiarPaginas(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return FormasDePagoPage();
      case 1:
        return ProductosPage();
      case 2:
        return ParticipacionGruposPage();
      case 3:
        return CortesiasPage();
      case 4:
        return FacturasPage();
      default:
        return FormasDePagoPage();
    }
  }

  Widget _tarjetaFiltrosAplicados(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      child: Card(
        elevation: 2.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _fechasSeleccionadas(context),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.005,
              ),
              _vendedorSeleccionado(context),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.005,
              ),
              // _cajeroSeleccionado(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _fechasSeleccionadas(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    final fechasProvider =
        Provider.of<TotalVentasProvider>(context, listen: true);

    DateTime _fechaComodinInicial = DateTime.now();
    DateTime _fechaComodinFinal = DateTime.now();

    _fechaComodinInicial = _fechaComodinFinal.subtract(Duration(days: 30));

    final _estiloFechas = TextStyle(
      fontSize: _screenSize.height < 850 ? 15.0 : 16.0,
      fontWeight: FontWeight.bold,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Desde: ${fechasProvider.fechaInicial == null ? _fechaComodinInicial.toString().split(' ')[0] : fechasProvider.fechaInicial.split(" ")[0]}',
          style: _estiloFechas,
        ),
        Text('--', style: _estiloFechas),
        Text(
          'Hasta: ${fechasProvider.fechaFinal == null ? _fechaComodinFinal.toString().split(' ')[0] : fechasProvider.fechaFinal.split(" ")[0]}',
          style: _estiloFechas,
        ),
      ],
    );
  }

  Widget _vendedorSeleccionado(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    final vendedorProvider =
        Provider.of<TotalVentasProvider>(context, listen: true);

    final _estiloVendedor = TextStyle(
      fontSize: _screenSize.height < 850 ? 15.0 : 16.0,
      fontWeight: FontWeight.bold,
    );

    final _todosLosVendedores = 'Todos los vendedores';

    return Text(
      'Vendedor: ${vendedorProvider.vendedorSeleccionado == null ? _todosLosVendedores : vendedorProvider.vendedorSeleccionado.nombre}',
      style: _estiloVendedor,
    );
  }

  // Widget _cajeroSeleccionado() {
  //   final _estiloCajero =
  //       TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);

  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //     children: [
  //       Text('Cajero: Todos los cajeros', style: _estiloCajero),
  //     ],
  //   );
  // }

  Widget _bottomNavBarTotalVentas() {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Color(0xffc51f5d),
        textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(
                color: Color.fromRGBO(36, 52, 71, 1.0),
              ),
            ),
      ),
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white /*Color.fromRGBO(36, 52, 71, 1.0)*/,
        currentIndex: _currentIndex,
        onTap: (value) {
          setState(() {
            _currentIndex = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.money_dollar),
            label: 'Ingresos',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.bag),
            label: 'Productos',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.doc_chart),
            label: 'Grupos',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.gift),
            label: 'Cortesias',
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.doc_checkmark),
            label: 'Facturas',
          ),
        ],
      ),
    );
  }

  Future _mostrarFiltrosDeFechaYHora() {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return FiltroFechaHora();
      },
    );
  }

  Future _mostrarFiltroVendedores() {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return FiltroVendedores();
      },
    );
  }

  // Future _mostrarFiltroCajero() {
  //   return showModalBottomSheet(
  //     context: context,
  //     builder: (context) {
  //       return FiltroCajeroCaja();
  //     },
  //   );
  // }
}
