import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/db/conexion_db_negocio.dart';

class CierreCajaPostgresService {
  CierreCajaPostgresService._privateConstructor();

  static final CierreCajaPostgresService _instancia =
      CierreCajaPostgresService._privateConstructor();

  factory CierreCajaPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<void> ejecutarCierreCaja() async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {

      double efe, efe2, tac, tad, che, pro, cre, tots, tosu, toeg, torc, abo, bon = 0;

      List<List<dynamic>> totales = await conexionNegocio.query("SELECT fposfecha,fposestado,fpospagefe,fpospagtarc,fpospagtard,fpospagche,fpospropin,fpospagcre,fpospagabo,fpospagbon FROM datos.facturacabecera WHERE sucursalid = @sucursalid AND fposnumerocierre = 0 AND fposestado <> 2 ORDER BY fposfecha");

      for (var row in totales) {
        efe += row[2];
        tac += row[3];
        tad += row[4];
        che += row[5];
        pro += row[6];
        cre += row[7];
        abo += row[8];
        bon += row[9];

        tots = efe + tac + tad + che + cre + bon;
      }
     
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }

  Future<void> _calcularEgresos(){
    
  }
}