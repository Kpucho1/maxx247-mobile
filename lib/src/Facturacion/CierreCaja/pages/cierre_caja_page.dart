import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CierreCajaPage extends StatefulWidget {
  CierreCajaPage({Key key}) : super(key: key);

  @override
  State<CierreCajaPage> createState() => _CierreCajaPageState();
}

class _CierreCajaPageState extends State<CierreCajaPage> {
  TextEditingController _efectivoController = TextEditingController(text: "0");
  TextEditingController _tarjetaDebitoController = TextEditingController(text: "0");
  TextEditingController _tarjetaCreditoController = TextEditingController(text: "0");
  TextEditingController _consignacionController = TextEditingController(text: "0");
  TextEditingController _creditoController = TextEditingController(text: "0");  
  TextEditingController _bonosController = TextEditingController(text: "0");

  double _totalFormasPago = 0;

  @override
  void dispose() {
    _efectivoController?.dispose();
    _tarjetaDebitoController?.dispose();
    _tarjetaCreditoController?.dispose();
    _consignacionController?.dispose();
    _creditoController?.dispose();
    _bonosController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cierre de caja"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Aceptar", style: TextStyle(color: Colors.white),),
            onPressed: (){

            },
          )
        ],
      ),
      body: _formularioCierreCaja(),
    );
  }

  _actualizarTotal(){
    setState(() {
      _totalFormasPago = double.tryParse(_efectivoController.text) + double.tryParse(_tarjetaDebitoController.text) + double.tryParse(_tarjetaCreditoController.text) + double.tryParse(_consignacionController.text) + double.tryParse(_creditoController.text) + double.tryParse(_bonosController.text);
    });
  }

  Widget _formularioCierreCaja() {

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _efectivoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _tarjetaDebitoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _tarjetaCreditoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _consignacionInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _creditosInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _bonosInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              Text("Total: \$ "+_totalFormasPago.toString(), style: TextStyle(fontSize: 26),),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              SvgPicture.asset(
                  'assets/cierre_caja.svg',
                  alignment: Alignment.bottomCenter,
                  height: 170,
              )
              
            ],
          ),
        ),
      ),
    );
  }

  Widget _efectivoInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _efectivoController,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Efectivo',
      ),
    );
  }

  Widget _tarjetaDebitoInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      controller: _tarjetaDebitoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Tarjetas Débito',
      ),
    );
  }

  Widget _tarjetaCreditoInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      controller: _tarjetaCreditoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Tarjetas Crédito',
      ),
    );
  }

  Widget _consignacionInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      controller: _consignacionController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Bancos / Consignacion',
      ),
    );
  }

  Widget _creditosInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      controller: _creditoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Créditos',
      ),
    );
  }

  Widget _bonosInput(){
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      onEditingComplete: (){
        _actualizarTotal();
      },
      textAlign: TextAlign.end,
      controller: _bonosController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Bonos',
      ),
    );
  }
}