import 'package:maxxshop/src/Facturacion/Proveedores/models/proveedor_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class ProveedoresPostgresService {
  ProveedoresPostgresService._privateConstructor();

  static final ProveedoresPostgresService _instancia =
      ProveedoresPostgresService._privateConstructor();

  factory ProveedoresPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<int> crearProveedor(ProveedorModel proveedor) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    
    int id = 0;
    
    try {
      
      List<List<dynamic>> maxProveedorid = await conexionNegocio.query("SELECT MAX(proveedorid) + 1 FROM datos.proveedores");

      await conexionNegocio.query("INSERT INTO datos.proveedores(proveedorid, nit, nombre1, nombre2, apellido1, apellido2, razonsocial, nombrecompleto, direccion, telefono, email, ciudad, celular) VALUES(@proveedorid, @nit, @nombre1, @nombre2, @apellido1, @apellido2, @razonsocial, @nombrecompleto, @direccion, @telefono, @email, @ciudad, @celular)", substitutionValues: {
        "proveedorid": maxProveedorid.first.first,
        "nit": proveedor.nit,
        "nombre1": proveedor.primerNombre,
        "nombre2": proveedor.segundoNombre,
        "apellido1": proveedor.primerApellido,
        "apellido2": proveedor.segundoApellido,
        "razonsocial": proveedor.razonSocial,
        "nombrecompleto": proveedor.nombreCompleto,
        "direccion": proveedor.direccion,
        "telefono": proveedor.telefono,
        "email": proveedor.email,
        "ciudad": proveedor.ciudad,
        "celular": proveedor.celular
      });

      id = maxProveedorid.first.first;
    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

    return id;

  }

  Future<List<ProveedorModel>> obtenerProveedores() async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ProveedorModel> _listadoProveedores = [];

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT proveedorid, nit, nombre1, nombre2, apellido1, apellido2, razonsocial, nombrecompleto, direccion, telefono, ciudad, email, celular FROM datos.proveedores");

      for (var item in _data) {
        ProveedorModel proveedorModel = ProveedorModel();

        proveedorModel.proveedorid = item[0];
        proveedorModel.nit = item[1];
        proveedorModel.primerNombre = item[2];
        proveedorModel.segundoNombre = item[3];
        proveedorModel.primerApellido = item[4];
        proveedorModel.segundoApellido = item[5];
        proveedorModel.razonSocial = item[6];
        proveedorModel.nombreCompleto = item[7];
        proveedorModel.direccion = item[8];
        proveedorModel.telefono = item[9];
        proveedorModel.ciudad = item[10];
        proveedorModel.celular = item[11];

        _listadoProveedores.add(proveedorModel);

      }

    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

    return _listadoProveedores;
    
  }

  Future<ProveedorModel> obtenerProveedoresPorCedula(String nit) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();


    ProveedorModel proveedorModel = ProveedorModel();

    try {
      List<List<dynamic>> _data = await conexionNegocio.query("SELECT proveedorid, nit, nombre1, nombre2, apellido1, apellido2, razonsocial, nombrecompleto, direccion, telefono, ciudad, email, celular FROM datos.proveedores WHERE nit LIKE @nit", substitutionValues: {
        "nit": "%"+nit+"%"
      });


      if (_data.isNotEmpty) {
        proveedorModel.proveedorid = _data.first[0];
        proveedorModel.nit = _data.first[1];
        proveedorModel.primerNombre = _data.first[2];
        proveedorModel.segundoNombre = _data.first[3];
        proveedorModel.primerApellido = _data.first[4];
        proveedorModel.segundoApellido = _data.first[5];
        proveedorModel.razonSocial = _data.first[6];
        proveedorModel.nombreCompleto = _data.first[7];
        proveedorModel.direccion = _data.first[8];
        proveedorModel.telefono = _data.first[9];
        proveedorModel.ciudad = _data.first[10];
        proveedorModel.celular = _data.first[11];
      }

    } catch (e) {
      print(e);
    }

    await conexionNegocio.close();

    return proveedorModel ?? null;
    
  }

}