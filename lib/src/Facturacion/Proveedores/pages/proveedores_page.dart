import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/models/proveedor_model.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/pages/crear_proveedor_page.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/services/proveedores_postgres_service.dart';
import 'package:maxxshop/src/widgets/image_list_tile.dart';

class ProveedoresPage extends StatefulWidget {
  ProveedoresPage({Key key}) : super(key: key);

  @override
  State<ProveedoresPage> createState() => _ProveedoresPageState();
}

class _ProveedoresPageState extends State<ProveedoresPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Proveedores"),
        centerTitle: true,
      ),
      body: _bodyProveedores(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => CrearProveedorPage())).then((value){
            Navigator.pop(context, value);
          });
        },
      ),
    );
  }

  _bodyProveedores(){
    ProveedoresPostgresService proveedoresPostgresService = ProveedoresPostgresService();
    return FutureBuilder(
      future: proveedoresPostgresService.obtenerProveedores(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else{
          List<ProveedorModel> _listadoProveedores = snapshot.data;
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: _listadoProveedores.length,
            itemBuilder: (context, index) {
              String nombre = (_listadoProveedores[index].nombreCompleto != null && _listadoProveedores[index].nombreCompleto != '' ? _listadoProveedores[index].nombreCompleto : _listadoProveedores[index].razonSocial) ?? 'N';

              return Card(
                child: ListTile(
                  title: Text(nombre ?? ''),
                  subtitle: Text(_listadoProveedores[index].nit ?? 0),
                  leading: ImageListTile(primerLetra: nombre[0] ?? 'N', imageString: '',),
                  onTap: () => Navigator.pop(context, _listadoProveedores[index]),
                ),
              );
            },
          );
        }
      },
    );
  }
}