import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/models/proveedor_model.dart';
import 'package:maxxshop/src/Facturacion/Proveedores/services/proveedores_postgres_service.dart';

class CrearProveedorPage extends StatefulWidget {
  CrearProveedorPage({Key key}) : super(key: key);

  @override
  State<CrearProveedorPage> createState() => _CrearProveedorPageState();
}

class _CrearProveedorPageState extends State<CrearProveedorPage> {

  
  TextEditingController _nitController = TextEditingController();
  TextEditingController _primerNombreController = TextEditingController();
  TextEditingController _segundoNombreController = TextEditingController();
  TextEditingController _primerApellidoController = TextEditingController();
  TextEditingController _segundoApellidoController = TextEditingController();
  TextEditingController _razonSocialController = TextEditingController(); 
  TextEditingController _direccionController = TextEditingController();
  TextEditingController _telefonoController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _ciudadController = TextEditingController();
  TextEditingController _celularController = TextEditingController();


  @override
  void dispose() {
    _nitController?.dispose();
    _primerNombreController?.dispose();
    _segundoNombreController?.dispose();
    _primerApellidoController?.dispose();
    _segundoApellidoController?.dispose();
    _razonSocialController?.dispose();
    _direccionController?.dispose();
    _telefonoController?.dispose();
    _emailController?.dispose();
    _ciudadController?.dispose();
    _celularController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Crear proveedor"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Crear"),
            onPressed: () async{
              ProveedoresPostgresService proveedoresPostgresService = ProveedoresPostgresService();

              ProveedorModel proveedorModel = ProveedorModel(
                nit: _nitController.text,
                primerNombre: _primerNombreController.text,
                segundoNombre: _segundoNombreController.text,
                primerApellido: _primerApellidoController.text,
                segundoApellido: _segundoApellidoController.text,
                razonSocial: _razonSocialController.text,
                direccion: _direccionController.text,
                telefono: _telefonoController.text,
                email: _emailController.text
              );

              proveedorModel.proveedorid = await proveedoresPostgresService.crearProveedor(proveedorModel);

              Navigator.pop(context, proveedorModel);
            },
          )
        ]
      ),
      body: _formularioCrearProveedor()
    );
  }

  _formularioCrearProveedor(){
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _nitInput(),
              SizedBox(height: 10,),
              _primerNombreInput(),
              SizedBox(height: 10,),
              _segundoNombreInput(),
              SizedBox(height: 10,),
              _primerApellidoInput(),
              SizedBox(height: 10,),
              _segundoApellidoInput(),
              SizedBox(height: 10,),
              _razonSocialInput(),
              SizedBox(height: 10,),
              _direccionInput(),
              SizedBox(height: 10,),
              _telefonoInput(),
              SizedBox(height: 10,),
              _emailInput()
              
            ],
          ),
        ),
      ),
    );
  }

  Widget _nitInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nitController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Nit*',
      ),
    );
  }

  Widget _primerNombreInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _primerNombreController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Primer nombre',
      ),
    );
  }

  Widget _segundoNombreInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _segundoNombreController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Segundo Nombre',
      ),
    );
  }

  Widget _primerApellidoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _primerApellidoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Primer Apellido',
      ),
    );
  }

  Widget _segundoApellidoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _segundoApellidoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Segundo Apellido',
      ),
    );
  }

  Widget _razonSocialInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _razonSocialController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Razon Social (opcional)',
      ),
    );
  }

  Widget _direccionInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _direccionController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Direccion',
      ),
    );
  }

  Widget _telefonoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _telefonoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Telefono',
      ),
    );
  }

  Widget _emailInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _emailController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Email',
      ),
    );
  }
}