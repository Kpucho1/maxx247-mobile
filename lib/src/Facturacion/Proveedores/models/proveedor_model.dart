// To parse this JSON data, do
//
//     final proveedorModel = proveedorModelFromJson(jsonString);

import 'dart:convert';

ProveedorModel proveedorModelFromJson(String str) => ProveedorModel.fromJson(json.decode(str));

String proveedorModelToJson(ProveedorModel data) => json.encode(data.toJson());

class ProveedorModel {
    ProveedorModel({
        this.id,
        this.proveedorid,
        this.nit,
        this.primerNombre,
        this.segundoNombre,
        this.primerApellido,
        this.segundoApellido,
        this.razonSocial,
        this.nombreCompleto,
        this.direccion,
        this.telefono,
        this.email,
        this.ciudad,
        this.celular,
    });

    int id;
    int proveedorid;
    String nit;
    String primerNombre;
    String segundoNombre;
    String primerApellido;
    String segundoApellido;
    String razonSocial;
    String nombreCompleto;
    String direccion;
    String telefono;
    String email;
    String ciudad;
    String celular;

    factory ProveedorModel.fromJson(Map<String, dynamic> json) => ProveedorModel(
        id: json["id"],
        proveedorid: json["proveedorid"],
        nit: json["nit"],
        primerNombre: json["primerNombre"],
        segundoNombre: json["segundoNombre"],
        primerApellido: json["primerApellido"],
        segundoApellido: json["segundoApellido"],
        razonSocial: json["razonSocial"],
        nombreCompleto: json["nombreCompleto"],
        direccion: json["direccion"],
        telefono: json["telefono"],
        email: json["email"],
        ciudad: json["ciudad"],
        celular: json["celular"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "proveedorid": proveedorid,
        "nit": nit,
        "primerNombre": primerNombre,
        "segundoNombre": segundoNombre,
        "primerApellido": primerApellido,
        "segundoApellido": segundoApellido,
        "razonSocial": razonSocial,
        "nombreCompleto": nombreCompleto,
        "direccion": direccion,
        "telefono": telefono,
        "email": email,
        "ciudad": ciudad,
        "celular": celular,
    };
}
