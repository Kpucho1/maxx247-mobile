import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maxxshop/src/Facturacion/CierreCaja/pages/cierre_caja_page.dart';
import 'package:maxxshop/src/Facturacion/ComandaVirtual/pages/comanda_virtual_page.dart';
import 'package:maxxshop/src/Facturacion/Egresos/pages/egreso_page.dart';
import 'package:maxxshop/src/Facturacion/Denominaciones/pages/denominaciones_page.dart';
import 'package:maxxshop/src/Facturacion/Mesas/pages/mesas_loading_page.dart';
import 'package:maxxshop/src/Facturacion/TotalVentas/pages/total_ventas_loading_page.dart';
import 'package:maxxshop/src/Utils/modulo_creacion_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';

class MenuFacturacionPage extends StatelessWidget {
  const MenuFacturacionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Facturacion"),
        centerTitle: true,
      ),
      body:  SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            _imagenDecoracion(),
            TablaOpcionesMenu(),
          ]
        ),
      ),
    );
  }

  Widget _imagenDecoracion() {
    return Container(
      child: SafeArea(
        child: Center(
          child: SvgPicture.asset(
            'assets/facturacion.svg',
            height: 150.0,
          ),
        ),
      ),
    );
  }
}

class TablaOpcionesMenu extends StatelessWidget {
  const TablaOpcionesMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.attach_money_outlined,
                  texto: 'POS',
                ),
                onTap: () async {
                  final prefs = PreferenciasUsuario();

                  if(prefs.modoVistaMesas == null){
                    prefs.modoVistaMesas = 1;
                  }

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MesasLoadingPage(),
                      ));
                },
              ),
              InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ModuloCreacionPage(),
                      ));
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.document_scanner,
                  texto: 'FE',
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TotalVentasLoadingPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.list,
                texto: 'Total ventas',
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CierreCajaPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.calculate,
                texto: 'Cierres caja',
              ),
            ),
          ]),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EgresoPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.outbox_sharp,
                texto: 'Egresos',
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DenominacionesPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.money,
                texto: 'Denominaciones',
              ),
            ),
          ]),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
           TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ComandaVirtualPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.computer,
                texto: 'Comanda virtual',
              ),
            ),
            SizedBox()
          ]),
        ],
      ),
    );
  }
}