import 'package:flutter/cupertino.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/comanda_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_gustos_bloc.dart';
import 'package:maxxshop/src/Usuario/blocs/datos_personales_bloc.dart';
export 'package:maxxshop/src/Usuario/blocs/datos_personales_bloc.dart';
import 'package:maxxshop/src/Usuario/blocs/login_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_bloc.dart';
export 'package:maxxshop/src/Usuario/blocs/login_bloc.dart';

class ProviderInherited extends InheritedWidget {
  final loginBlocInstancia = LoginBloc();
  final datosPersonalesBloc = DatosPersonalesBloc();
  final _categoriasBlocInstancia = CategoriasBloc();
  final _productosBloc = ProductosBloc();
  final _mesasBloc = MesasBloc();
  final _comandaBloc = ComandaBloc();
  final _productosGustosBloc = ProductosGustosBloc();
  //final _esquemasBloc = EsquemasBloc();

  static ProviderInherited _instancia;

  factory ProviderInherited({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = new ProviderInherited._internal(key: key, child: child);
    }

    return _instancia;
  }

  ProviderInherited._internal({Key key, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => true;

  static DatosPersonalesBloc of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        .datosPersonalesBloc;
  }

  static LoginBloc loginBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        .loginBlocInstancia;
  }

  static CategoriasBloc categoriasBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        ._categoriasBlocInstancia;
  }

  static ProductosBloc productosBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        ._productosBloc;
  }

  static MesasBloc mesasBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        ._mesasBloc;
  }

  static ComandaBloc comandaBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        ._comandaBloc;
  }

  static ProductosGustosBloc productosGustosBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<ProviderInherited>()
        ._productosGustosBloc;
  }

  /*static EsquemasBloc esquemasBloc(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ProviderInherited>()._esquemasBloc;
  }*/

}
