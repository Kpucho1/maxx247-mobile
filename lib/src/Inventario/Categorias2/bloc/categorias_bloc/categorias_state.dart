part of 'categorias_bloc.dart';

@immutable
abstract class Categorias2State extends Equatable {
  @override
  List<Object> get props => [];
}

//Estados para la creacion de nuevas categorias

class CrearCategoria2LoadingState extends Categorias2State {}

class CategoriaCreada2ExitoState extends Categorias2State {}

class ErrorCrearCategoria2State extends Categorias2State {}

//Estados para la actualizacion/modificacion de categorias

class ActualizarCategoria2State extends Categorias2State {}

class ErrorActualizarCategoria2State extends Categorias2State {}

//Estados para la eliminación de categorías

class EliminarCategoria2State extends Categorias2State {}

class ErrorEliminarCategoria2State extends Categorias2State {}

//Estados referentes al listado de categorias a mostrar al usuario

class InitialCategorias2State extends Categorias2State {}

class ErrorObtenerCategorias2State extends Categorias2State {}

class ObtenerCategorias2State extends Categorias2State {
  //

  final List<CategoriasModel> listadoCategorias;

  ObtenerCategorias2State({List<CategoriasModel> lista, int categoria})
      : this.listadoCategorias = lista ?? [];

  ObtenerCategorias2State copyWith({
    List<CategoriasModel> listadoCategorias,
  }) =>
      ObtenerCategorias2State(
        lista: listadoCategorias ?? this.listadoCategorias,
      );

  @override
  List<Object> get props => [listadoCategorias];
}
