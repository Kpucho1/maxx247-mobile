part of 'categorias_bloc.dart';

@immutable
abstract class Categorias2Event {}

class OnObtenerCategorias2 extends Categorias2Event {}

class OnCrearCategoria2 extends Categorias2Event {
  final CategoriasModel nuevaCategoria;

  OnCrearCategoria2({this.nuevaCategoria});
}

class OnActualizarCategoria2 extends Categorias2Event {
  final CategoriasModel categoria;

  OnActualizarCategoria2({this.categoria});
}

class OnEliminarCategoria2 extends Categorias2Event {
  final int grupoId;

  OnEliminarCategoria2({this.grupoId});
}
