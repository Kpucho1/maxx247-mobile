import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:maxxshop/src/Inventario/Categorias2/models/categorias_model.dart';
import 'package:maxxshop/src/Inventario/Categorias2/services/categorias_postgres_service.dart';

part 'categorias_event.dart';
part 'categorias_state.dart';

class Categorias2Bloc extends Bloc<Categorias2Event, Categorias2State> {
  Categorias2Bloc() : super(InitialCategorias2State());

  final _categoriasPostgres = Categorias2PostgresService();


  @override
  Stream<Categorias2State> mapEventToState(
    Categorias2Event event,
  ) async* {
    //
    final currentState = state;

    if (event is OnObtenerCategorias2) {
      //
      try {
        if (currentState is InitialCategorias2State) {
          //
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield ObtenerCategorias2State(lista: lista);
        }

        if (currentState is ErrorObtenerCategorias2State) {
          //
          yield InitialCategorias2State();

          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield ObtenerCategorias2State(lista: lista);
        }

        if (currentState is ErrorCrearCategoria2State) {
          yield InitialCategorias2State();

          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield ObtenerCategorias2State(lista: lista);
        }
        //
      } catch (_) {
        //
        yield ErrorObtenerCategorias2State();
      }
    }

    if (event is OnCrearCategoria2) {
      try {
        if (currentState is ObtenerCategorias2State) {
          //
          yield CrearCategoria2LoadingState();

          await _categoriasPostgres.crearNuevaCategoria(event.nuevaCategoria);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield CategoriaCreada2ExitoState();
          Future.delayed(Duration(seconds: 2));
          yield ObtenerCategorias2State(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorCrearCategoria2State();
      }
    }

    if (event is OnActualizarCategoria2) {
      try {
        //
        if (currentState is ObtenerCategorias2State) {
          yield ActualizarCategoria2State();

          await _categoriasPostgres.actualizarCategoria(event.categoria);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield ObtenerCategorias2State(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorActualizarCategoria2State();
      }
    }

    if (event is OnEliminarCategoria2) {
      try {
        if (currentState is ObtenerCategorias2State) {
          yield EliminarCategoria2State();

          await _categoriasPostgres.eliminarCategoria(event.grupoId);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield ObtenerCategorias2State(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorEliminarCategoria2State();
      }
    }
  }
}
