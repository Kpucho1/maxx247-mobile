part of 'seleccionar_categoria_bloc.dart';

abstract class SeleccionarCategoria2State extends Equatable {
  const SeleccionarCategoria2State();

  @override
  List<Object> get props => [];
}

class SeleccionarCategoriaInitial extends SeleccionarCategoria2State {}

class SeleccionarCategoriaLoadingState extends SeleccionarCategoria2State {}

class ErrorObtenerCategoriaSeleccionadaState extends SeleccionarCategoria2State {
}

class ObtenerCategoriaSeleccionadaState extends SeleccionarCategoria2State {
  final int categoriaSeleccionada;
  final List<CategoriasModel> listadoCategorias;

  ObtenerCategoriaSeleccionadaState(
      {int categoriaSeleccionada, List<CategoriasModel> lista})
      : this.categoriaSeleccionada = categoriaSeleccionada ?? null,
        this.listadoCategorias = lista ?? [];

  ObtenerCategoriaSeleccionadaState copyWith({
    int categoria,
    List<CategoriasModel> listaCategorias,
  }) =>
      ObtenerCategoriaSeleccionadaState(
        categoriaSeleccionada: categoria ?? this.categoriaSeleccionada,
        lista: listaCategorias ?? this.listadoCategorias,
      );

  @override
  List<Object> get props => [categoriaSeleccionada, listadoCategorias];
}
