import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:maxxshop/src/Inventario/Categorias2/models/categorias_model.dart';
import 'package:maxxshop/src/Inventario/Categorias2/services/categorias_postgres_service.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';

part 'seleccionar_categoria_event.dart';
part 'seleccionar_categoria_state.dart';

class SeleccionarCategoria2Bloc
    extends Bloc<SeleccionarCategoria2Event, SeleccionarCategoria2State> {
  //Crea una suscripcion al bloc de categorias para reaccionar a eventos
  //como el agregado, actualizado o eliminado de alguna categoría
  //
  final Categorias2Bloc blocCategorias;
  StreamSubscription subscriptionBlocCategorias;

  final _categoriasPostgres = Categorias2PostgresService();

  SeleccionarCategoria2Bloc({this.blocCategorias})
      : super(SeleccionarCategoriaInitial()) {
    subscriptionBlocCategorias = blocCategorias.listen((state) {
      if (state is ObtenerCategorias2State) {
        //Carga el dropdown luego que las categorias en general han empezado a ser cargadas
        //
        add(OnCargarCategorias2DropDown());
      }
    });
  }

  @override
  Future<void> close() {
    subscriptionBlocCategorias?.cancel();
    return super.close();
  }

  @override
  Stream<SeleccionarCategoria2State> mapEventToState(
    SeleccionarCategoria2Event event,
  ) async* {
    final currentState = state;

    if (event is OnCargarCategorias2DropDown) {
      //
      try {
        if (currentState is SeleccionarCategoriaInitial) {
          final lista =
              await _categoriasPostgres.obtenerTodasCategoriasDropDown();

          yield ObtenerCategoriaSeleccionadaState(lista: lista);
        }

        if (currentState is ObtenerCategoriaSeleccionadaState) {
          final lista =
              await _categoriasPostgres.obtenerTodasCategoriasDropDown();

          yield currentState.copyWith(listaCategorias: lista);
        }

        if (currentState is ErrorObtenerCategoriaSeleccionadaState) {
          yield SeleccionarCategoriaLoadingState();

          final lista =
              await _categoriasPostgres.obtenerTodasCategoriasDropDown();

          yield ObtenerCategoriaSeleccionadaState(lista: lista);
        }
        //
      } on TimeoutException catch (_) {
        //
        yield ErrorObtenerCategoriaSeleccionadaState();
        add(OnCargarCategorias2DropDown());
        //
      } catch (_) {
        yield ErrorObtenerCategoriaSeleccionadaState();
      }
    }

    if (event is OnSeleccionarCategoria) {
      if (currentState is ObtenerCategoriaSeleccionadaState) {
        yield currentState.copyWith(categoria: event.categoriaSeleccionada);
      }
    }
  }
}
