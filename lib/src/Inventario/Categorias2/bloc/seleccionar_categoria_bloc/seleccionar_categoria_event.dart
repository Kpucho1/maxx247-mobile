part of 'seleccionar_categoria_bloc.dart';

abstract class SeleccionarCategoria2Event extends Equatable {
  const SeleccionarCategoria2Event();

  @override
  List<Object> get props => [];
}

class OnCargarCategorias2DropDown extends SeleccionarCategoria2Event {}

class OnSeleccionarCategoria extends SeleccionarCategoria2Event {
  final int categoriaSeleccionada;

  OnSeleccionarCategoria({this.categoriaSeleccionada});
}

class CargarCategoriaSeleccionada extends SeleccionarCategoria2Event {
  final int categoria;

  CargarCategoriaSeleccionada({this.categoria});
}
