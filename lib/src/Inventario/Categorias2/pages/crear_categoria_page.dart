import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';
import 'package:maxxshop/src/widgets/boton_subir_foto.dart';

import 'package:maxxshop/src/Inventario/Categorias2/models/categorias_model.dart';

class CrearCategoria2Page extends StatefulWidget {
  CrearCategoria2Page({Key key}) : super(key: key);

  @override
  _CrearCategoriaPageState createState() => _CrearCategoriaPageState();
}

class _CrearCategoriaPageState extends State<CrearCategoria2Page> {
  TextEditingController _codigoCategoriaController = TextEditingController();
  TextEditingController _nombreCategoriaController = TextEditingController();

  final botonAgregarFoto = BotonSubirFoto();

  @override
  void dispose() {
    _codigoCategoriaController?.dispose();
    _nombreCategoriaController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        actions: _buildActions(),
        title: Text(
          "Crear categoría",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: _formularioCrearCategoria(),
    );
  }

  List<Widget> _buildActions() {
    return <Widget>[
      TextButton(
        child: Text("Guardar"),
        onPressed: () async {
          List<int> imagenHex = botonAgregarFoto.imagenSeleccionada != null
              ? botonAgregarFoto.imagenSeleccionada.readAsBytesSync()
              : [];

          CategoriasModel nuevaCategoria = CategoriasModel(
              codigogrupo: _codigoCategoriaController.text,
              descripcion: _nombreCategoriaController.text,
              foto: hex.encode(imagenHex));

          context
              .read<Categorias2Bloc>()
              .add(OnCrearCategoria2(nuevaCategoria: nuevaCategoria));

          Navigator.pop(context);
        },
      )
    ];
  }


  Widget _formularioCrearCategoria() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              botonAgregarFoto
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código de la categoría*',
      ),
    );
  }

  Widget _nombreCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre de la categoría*',
      ),
    );
  }

}
