import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/widgets/image_list_tile.dart';
import 'package:maxxshop/src/Inventario/Categorias2/pages/crear_categoria_page.dart';
import 'package:maxxshop/src/Inventario/Categorias2/pages/editar_categoria_page.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';

class Categorias2Page extends StatefulWidget {
  @override
  _Categorias2PageState createState() => _Categorias2PageState();
}

class _Categorias2PageState extends State<Categorias2Page> {
  @override
  void initState() {
    super.initState();
    context.read<Categorias2Bloc>().add(OnObtenerCategorias2());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              //TODO: Search delegate de categorías
            },
          )
        ],
        title: Text(
          "Categorias 2",
          style: TextStyle(color: Colors.black),
        ),
      ),
      floatingActionButton: _botonFlotante(),
      body: _listadoCategorias(),
    );
  }

  Widget _botonFlotante() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CrearCategoria2Page(),
          ),
        ).then(
            (_) => context.read<Categorias2Bloc>().add(OnObtenerCategorias2()));
      },
      child: Icon(
        Icons.add,
        size: 27.0,
      ),
    );
  }

  Widget _listadoCategorias() {
    return BlocListener<Categorias2Bloc, Categorias2State>(
      listener: (context, state) {
        if (state is ErrorObtenerCategorias2State) {
          //Se muestra un snackbar informando el error
          //y dando la posibilidad de cargar de nuevo las categorias
          //
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              duration: Duration(seconds: 5),
              backgroundColor: Colors.red,
              content: Text(
                'Error al intentar obtener las categorías. Intente de nuevo por favor',
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          );
        } else if (state is CategoriaCreada2ExitoState) {
          //Muestra snackbar de éxito al crear la categoría
          //
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              duration: Duration(seconds: 5),
              backgroundColor: Colors.cyan[900],
              content: Text(
                'Categoría creada con éxito',
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          );
        }
      },
      child: BlocBuilder<Categorias2Bloc, Categorias2State>(
        builder: (context, state) {
          Widget _widget;
          //
          if (state is InitialCategorias2State) {
            //
            _widget = Center(
                child: CircularProgressIndicator(
              strokeWidth: 1.5,
            ));
            //
          } else if (state is ErrorObtenerCategorias2State) {
            //
            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 150,
                    width: 400,
                    child: SvgPicture.asset('assets/error_conexion.svg'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Ocurrió un error en la conexión.',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Revise que la conexión de red se encuentre disponible e intente de nuevo.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Se recomienda también verificar el número de dispositivos conectados a la red, ya que al haber muchos dispositivos conectados, la red se puede ver saturada, no disponible ó su velocidad puede reducirse considerablemente.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  OutlinedButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(EdgeInsets.symmetric(
                      horizontal: 50.0,
                      vertical: 10.0,
                      )),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
                    ),
                    child: Text('Volver a cargar'),
                    onPressed: () => context
                        .read<Categorias2Bloc>()
                        .add(OnObtenerCategorias2()),
                  ),
                ],
              ),
            );
            //
          } else if (state is ObtenerCategorias2State) {
            //
            final _listaCategorias = state.listadoCategorias;

            _widget = ListView.builder(
              itemCount: _listaCategorias.length,
              itemBuilder: (context, index) {
                String nombreCategoria = _listaCategorias[index].descripcion;

                return Card(
                  child: ListTile(
                    leading: ImageListTile(
                      imageString: _listaCategorias[index].foto,
                      primerLetra: nombreCategoria[0],
                    ), //Avatar de la categoria
                    title: Text(nombreCategoria), //Nombre de la categoria
                    subtitle: Text(
                        "3 productos"), //cantidad de productos en la categoria
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EditarCategoria2Page(
                            categoria: _listaCategorias[index],
                          ),
                        ),
                      ).then(
                        (_) => context
                            .read<Categorias2Bloc>()
                            .add(OnObtenerCategorias2()),
                      );
                    },
                  ),
                );
              },
            );
          } else if (state is CrearCategoria2LoadingState) {
            _widget = Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 1.5,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text('Creando categoría. Un momento por favor.'),
              ],
            );
          } else if (state is CategoriaCreada2ExitoState) {
            _widget = Center(
              child: Icon(
                Icons.check_circle_outline,
                color: Colors.green[800],
                size: 100.0,
              ),
            );
          } else if (state is ErrorCrearCategoria2State) {
            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 150,
                    width: 400,
                    child: SvgPicture.asset('assets/error_conexion.svg'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Ocurrió un error al crear la nueva categoría.',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Revise que la conexión de red se encuentre disponible e intente de nuevo.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Se recomienda también verificar el número de dispositivos conectados a la red, ya que al haber muchos dispositivos conectados, la red se puede ver saturada, no disponible ó su velocidad puede reducirse considerablemente.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  OutlinedButton(
                   
                    child: Text('Regresar a categorías'),
                    onPressed: () => context
                        .read<Categorias2Bloc>()
                        .add(OnObtenerCategorias2()),
                  ),
                ],
              ),
            );
            //
          } else if (state is ActualizarCategoria2State) {
            //
            _widget = Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 1.5,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text('Actualizando categoría. Un momento por favor.'),
              ],
            );
          } else if (state is ErrorActualizarCategoria2State) {
            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 150,
                    width: 400,
                    child: SvgPicture.asset('assets/error_conexion.svg'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Ocurrió un error al actualizar la categoría.',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Revise que la conexión de red se encuentre disponible e intente de nuevo.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Se recomienda también verificar el número de dispositivos conectados a la red, ya que al haber muchos dispositivos conectados, la red se puede ver saturada, no disponible ó su velocidad puede reducirse considerablemente.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  OutlinedButton(
                    
                    child: Text('Regresar a categorías'),
                    onPressed: () => context
                        .read<Categorias2Bloc>()
                        .add(OnObtenerCategorias2()),
                  ),
                ],
              ),
            );
          } else if (state is EliminarCategoria2State) {
            //
            _widget = Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 1.5,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text('Eliminando categoría. Un momento por favor.'),
              ],
            );
          } else if (state is ErrorActualizarCategoria2State) {
            _widget = Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 150,
                    width: 400,
                    child: SvgPicture.asset('assets/error_conexion.svg'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Ocurrió un error al eliminar la categoría.',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Revise que la conexión de red se encuentre disponible e intente de nuevo.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Se recomienda también verificar el número de dispositivos conectados a la red, ya que al haber muchos dispositivos conectados, la red se puede ver saturada, no disponible ó su velocidad puede reducirse considerablemente.',
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  OutlinedButton(
                    
                    child: Text('Regresar a categorías'),
                    onPressed: () => context
                        .read<Categorias2Bloc>()
                        .add(OnObtenerCategorias2()),
                  ),
                ],
              ),
            );
          }

          return _widget;
        },
      ),
    );
  }
}
