import 'dart:io';
import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:maxxshop/src/widgets/botones_redondeados.dart';
import 'package:maxxshop/src/Inventario/Categorias2/models/categorias_model.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';

class EditarCategoria2Page extends StatefulWidget {
  final CategoriasModel categoria;

  EditarCategoria2Page({Key key, this.categoria}) : super(key: key);

  @override
  _EditarCategoria2PageState createState() => _EditarCategoria2PageState();
}

class _EditarCategoria2PageState extends State<EditarCategoria2Page> {
  TextEditingController _codigoCategoriaController = TextEditingController();
  TextEditingController _nombreCategoriaController = TextEditingController();

  PickedFile _image;
  File imagenSeleccionada;

  ImagePicker imagenPicker = ImagePicker();

  @override
  void dispose() {
    _codigoCategoriaController?.dispose();
    _nombreCategoriaController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _codigoCategoriaController.text = widget.categoria.codigogrupo;
    _nombreCategoriaController.text = widget.categoria.descripcion;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        actions: _buildActions(),
        title: Text(
          "Editar categoría",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: _formularioCrearCategoria(),
    );
  }

  List<Widget> _buildActions() {
    return <Widget>[
      TextButton(
        child: Text("Guardar"),
        onPressed: () async {
          List<int> imagenHex = imagenSeleccionada != null
              ? imagenSeleccionada.readAsBytesSync()
              : [];

          final imagen = imagenHex != []
              ? hex.encode(imagenHex)
              : (widget.categoria.foto != "" ? widget.categoria.foto : "");

          CategoriasModel categoria = CategoriasModel(
              grupoid: widget.categoria.grupoid,
              codigogrupo: _codigoCategoriaController.text,
              descripcion: _nombreCategoriaController.text,
              foto: imagen);

          context
              .read<Categorias2Bloc>()
              .add(OnActualizarCategoria2(categoria: categoria));

          Navigator.pop(context);
        },
      )
    ];
  }

  _imgFromCamera() async {
    PickedFile image = await imagenPicker.getImage(
        source: ImageSource.camera, imageQuality: 50);

    if (image != null) {
      setState(() {
        _image = image;
        imagenSeleccionada = File(_image.path);
      });
    }
  }

  _imgFromGallery() async {
    PickedFile image = await imagenPicker.getImage(
        source: ImageSource.gallery, imageQuality: 50);

    if (image != null) {
      setState(() {
        _image = image;
        imagenSeleccionada = File(_image.path);
      });
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Agregar de la galería'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camara'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _formularioCrearCategoria() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _botonAgregarFoto(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _botonEliminarCategoria()
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código de la categoría*',
      ),
    );
  }

  Widget _nombreCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre de la categoría*',
      ),
    );
  }

  Widget _botonAgregarFoto() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.camera,
                  texto: 'Foto',
                ),
                onTap: () async {
                  _showPicker(context);
                },
              ),
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: widget.categoria.foto != ""
                    ? Image.memory(
                        hex.decode(widget.categoria.foto),
                        filterQuality: FilterQuality.low,
                        gaplessPlayback: true,
                        cacheHeight: 100,
                        cacheWidth: 100,
                      )
                    : (imagenSeleccionada == null
                        ? Text('No se ha seleccionado una imagen')
                        : Image.file(
                            imagenSeleccionada,
                            filterQuality: FilterQuality.low,
                          )),
                onTap: () async {
                  _showPicker(context);
                },
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _botonEliminarCategoria() {
    return OutlinedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 10.0,
        )),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: Text(
        'Eliminar',
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: 14.0,
        ),
      ),
      onPressed: () {
        context
            .read<Categorias2Bloc>()
            .add(OnEliminarCategoria2(grupoId: widget.categoria.grupoid));

        Navigator.pop(context);
      },
    );
  }
}
