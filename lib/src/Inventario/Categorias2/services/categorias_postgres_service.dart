import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/Inventario/Categorias2/models/categorias_model.dart';

class Categorias2PostgresService {
  Categorias2PostgresService._privateConstructor();

  static final Categorias2PostgresService _instancia =
      Categorias2PostgresService._privateConstructor();

  factory Categorias2PostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<CategoriasModel>> obtenerTodasCategorias() async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> categorias = [];

    List<CategoriasModel> listadoCategorias = [];

    try {
      categorias = await conexionNegocio.query(
          "SELECT grupoid, codigogrupo, descripcion, COALESCE(convert_from(foto, 'UTF8'), ''), ordenvista, grupoidpadre FROM datos.grupos2 WHERE esventa = 0 ORDER BY ordenvista ASC");

      for (var categoria in categorias) {
        final nuevaCategoria = new CategoriasModel();

        nuevaCategoria.grupoid = categoria[0];
        nuevaCategoria.codigogrupo = categoria[1];
        nuevaCategoria.descripcion = categoria[2];
        nuevaCategoria.foto = categoria[3].toString();
        nuevaCategoria.ordenvista = categoria[4];
        nuevaCategoria.grupopadreid = categoria[5];

        listadoCategorias.add(nuevaCategoria);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error al obtener las categorias de productos');
    }
    await conexionNegocio.close();

    return listadoCategorias ?? [];
  }

  Future<List<CategoriasModel>> obtenerTodasCategoriasDropDown() async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> categorias = [];

    List<CategoriasModel> listadoCategorias = [];

    try {
      categorias = await conexionNegocio.query(
          "SELECT grupoid, descripcion FROM datos.grupos2 WHERE esventa = 0 ORDER BY ordenvista ASC");

      for (var categoria in categorias) {
        final nuevaCategoria = new CategoriasModel();

        nuevaCategoria.grupoid = categoria[0];
        nuevaCategoria.descripcion = categoria[1];

        listadoCategorias.add(nuevaCategoria);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error al obtener las categorias del dropdown');
    }
    await conexionNegocio.close();

    return listadoCategorias ?? [];
  }

  Future<void> crearNuevaCategoria(CategoriasModel nuevaCategoria) async {
   ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> grupoid = await conexionNegocio
          .query("SELECT MAX(grupoid) + 1 FROM datos.grupos1");

      await conexionNegocio.query(
          "INSERT INTO datos.grupos2(grupoid, codigogrupo, descripcion, esventa, foto) VALUES(@grupoid, @codigogrupo, @descripcion, @esventa, @foto)",
          substitutionValues: {
            "grupoid": grupoid.first.first,
            "codigogrupo": nuevaCategoria.codigogrupo,
            "descripcion": nuevaCategoria.descripcion,
            "esventa": 0,
            "foto": nuevaCategoria.foto,
          });
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Problema al crear categoría');
    }

    await conexionNegocio.close();
  }

  Future<void> actualizarCategoria(CategoriasModel categoria) async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.grupos2 SET codigogrupo = @codigogrupo, descripcion = @descripcion, esventa = @esventa, foto = @foto WHERE grupoid = @grupoid",
          substitutionValues: {
            "grupoid": categoria.grupoid,
            "codigogrupo": categoria.codigogrupo,
            "descripcion": categoria.descripcion,
            "esventa": 0,
            "foto": categoria.foto,
          });
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Problema al actualizar la categoria');
    }

    await conexionNegocio.close();
  }

  Future<void> eliminarCategoria(int grupoid) async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "DELETE FROM datos.grupos2 WHERE grupoid = @grupoid",
          substitutionValues: {
            "grupoid": grupoid,
          });
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('No se eliminó la categoría');
    }

    await conexionNegocio.close();
  }
}
