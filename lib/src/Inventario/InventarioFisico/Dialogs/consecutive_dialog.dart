import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/services/postgres_service.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';


class ConsecutivesDialog extends StatefulWidget {
  ConsecutivesDialog({Key key}) : super(key: key);

  @override
  _ConsecutivesDialogState createState() => _ConsecutivesDialogState();
}

class _ConsecutivesDialogState extends State<ConsecutivesDialog> {
  HomePostgresService postgres = HomePostgresService();

  var _resultadosBd;

  final prefs = PreferenciasUsuario();


  @override
  void initState() {
    _resultadosBd = postgres.consultarFisicoCabecera();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _resultadosBd,
      builder: (context, AsyncSnapshot<List<List<dynamic>>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        List<List<dynamic>> listaPrefijos =  snapshot.data;



        List<Widget> consecutivos = listaPrefijos.map((consecutivo) {
            return SimpleDialogOption(
              child: Text(consecutivo[0]+" "+consecutivo[1].toString(), style: TextStyle(fontSize: 18.0)),
              onPressed: () {
                prefs.prefijoFisico = consecutivo[0];
                prefs.numeroFisico = consecutivo[1].toString();
                Navigator.pop(context);
              },
            );
        }).toList();

        consecutivos.insert(0, SimpleDialogOption(
              child: Text("Ninguno", style: TextStyle(color: Colors.red, fontSize: 18.0),),
              onPressed: () {
                prefs.prefijoFisico = null;
                prefs.numeroFisico = null;
                Navigator.pop(context);
              },
        ));

        return SimpleDialog(
          title: Text("CONSECUTIVOS"),
          children: consecutivos
        );
      },
    );
  }
}