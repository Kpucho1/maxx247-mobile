import 'package:flutter/material.dart';


class ProductExistsDialog extends StatefulWidget {
  ProductExistsDialog({Key key}) : super(key: key);

  @override
  _ProductExistsDialogState createState() => _ProductExistsDialogState();
}

class _ProductExistsDialogState extends State<ProductExistsDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actions: [
        TextButton(
          child: Text("Volver"),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
      title: Text("El producto no existe en el inventario"),
    );
  }
}