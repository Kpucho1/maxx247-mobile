import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Inventario/Productos/models/productos_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class HomePostgresService {
  final prefs = PreferenciasUsuario();

  Future<List<List<dynamic>>> consultarFisicoCabecera() async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> results = await conexionNegocio.query(
          "SELECT prefijoentrada, CAST(numeroentrada AS INTEGER), fechaentrada, tipoentrada FROM datos.fisicocabecera WHERE estado = 0");

      await conexionNegocio.close();

      return results;
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
    }
  }

  Future<bool> existeProducto(String codigo) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> producto;

    try {
      producto = await conexionNegocio.query(
          "SELECT productoid FROM datos.productos WHERE producto = @codigo OR sucursales = @codigo",
          substitutionValues: {"codigo": codigo});
    } catch (e) {
      print(e);

      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();

    return producto.length > 0;
  }

  Future<List<ProductosModel>> insertarRegistroDetalle(String codigo) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<ProductosModel> listadoProductos = [];

    try {
      List<List<dynamic>> producto = await conexionNegocio.query(
          "SELECT productoid, nombre FROM datos.productos WHERE producto = @codigo OR sucursales = @codigo",
          substitutionValues: {"codigo": codigo});

      await conexionNegocio.query(
          "INSERT INTO datos.fisicodetalle(sucursalid, prefijoentrada, numeroentrada, codigoproducto, productoid, cantidad, nombreproducto, estado) VALUES(@sucursalid, @prefijoentrada, @numeroentrada, @codigoproducto, @productoid, @cantidad, @nombreproducto, @estado)",
          substitutionValues: {
            "sucursalid": prefs.sucursalid,
            "prefijoentrada": prefs.prefijoFisico,
            "numeroentrada": prefs.numeroFisico,
            "codigoproducto": codigo,
            "cantidad": 1,
            "productoid": producto.first[0],
            "nombreproducto": producto.first[1],
            "estado": 0
          });

      List<List<dynamic>> results = await conexionNegocio.query(
          "SELECT codigoproducto, id FROM datos.fisicodetalle WHERE prefijoentrada = @prefijoentrada AND numeroentrada = @numeroentrada AND sucursalid = @sucursalid ORDER BY id DESC",
          substitutionValues: {
            "sucursalid": 1,
            "prefijoentrada": prefs.prefijoFisico,
            "numeroentrada": prefs.numeroFisico,
          });

      for (var codigo in results) {
        listadoProductos.add(ProductosModel(codigo: codigo[0], id: codigo[1]));
      }

      await conexionNegocio.close();
    } catch (e) {
      print(e);

      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
    }

    return listadoProductos ?? [];
  }

  Future<List<ProductosModel>> eliminarRegistroDetalle(int id) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    List<ProductosModel> listadoProductos = [];

    try {
      await conexionNegocio.query(
          "DELETE FROM datos.fisicodetalle WHERE id = @id",
          substitutionValues: {"id": id});

      List<List<dynamic>> results = await conexionNegocio.query(
          "SELECT codigoproducto, id FROM datos.fisicodetalle WHERE prefijoentrada = @prefijoentrada AND numeroentrada = @numeroentrada AND sucursalid = @sucursalid ORDER BY id DESC",
          substitutionValues: {
            "sucursalid": 1,
            "prefijoentrada": prefs.prefijoFisico,
            "numeroentrada": prefs.numeroFisico,
          });

      for (var codigo in results) {
        listadoProductos.add(ProductosModel(codigo: codigo[0], id: codigo[1]));
      }

      await conexionNegocio.close();
    } catch (e) {
      print(e);

      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
    }

    return listadoProductos ?? [];
  }
}
