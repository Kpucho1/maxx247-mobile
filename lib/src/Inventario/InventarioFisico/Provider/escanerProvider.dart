import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/services/postgres_service.dart';
import 'package:maxxshop/src/Inventario/Productos/models/productos_model.dart';

class EscanerProvider with ChangeNotifier {
  List<ProductosModel> _listadoCodigos;

  List<ProductosModel> get listadoCodigos => this._listadoCodigos;

  Future<void> insertarObtenerCodigos(String codigo) async {
    HomePostgresService postgres = HomePostgresService();

    this._listadoCodigos = await postgres.insertarRegistroDetalle(codigo);

    notifyListeners();
  }

  Future<void> eliminarObtenerRegistros(int id) async {
    HomePostgresService postgres = HomePostgresService();

    this._listadoCodigos = await postgres.eliminarRegistroDetalle(id);

    notifyListeners();
  }
}
