import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/Dialogs/consecutive_dialog.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/Dialogs/product_exists_dialog.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/Provider/escanerProvider.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/services/postgres_service.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/widgets/botones_redondeados.dart';
import 'package:provider/provider.dart';


class HomePageInventarioFisico extends StatefulWidget {
  HomePageInventarioFisico({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePageInventarioFisico> {
  final prefs = PreferenciasUsuario();

  TextEditingController _controllerInputManual = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return Container(
       child: Scaffold(
         appBar: AppBar(
           title: Text("Añadir inventario fisico"),
           centerTitle: true,
           automaticallyImplyLeading: true,
         ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            prefs.prefijoFisico != "" && prefs.numeroFisico != "" ? Text("Prefijo y numero actual: "+prefs.prefijoFisico+" "+prefs.numeroFisico, style: TextStyle(fontSize: 22.0),) : Text("Sin prefijo y número seleccionado", style: TextStyle(fontSize: 22.0, color: Colors.red)),
            SizedBox(height: _screenSize.height*0.01,),
            _botonesRedondeados(),
            prefs.prefijoFisico != "" && prefs.numeroFisico != "" ? _inputCodigoManual() : Container(),
            SizedBox(height: _screenSize.height*0.01,),
            prefs.prefijoFisico != "" && prefs.numeroFisico != "" ? Text("Códigos Escaneados", style: TextStyle(fontSize: 22.0)) : Container(),
            prefs.prefijoFisico != "" && prefs.numeroFisico != "" ? Container(child: _listaCodigosEscaneados(), width: double.infinity, height: _screenSize.height * 0.2, padding: EdgeInsets.symmetric(horizontal: 20.0,), decoration: BoxDecoration(border: Border.all()), margin: const EdgeInsets.all(20.0),) : Container()
          ],
        ),
       ),
    );
  }

  Widget _listaCodigosEscaneados(){
    final listadoCodigosEscaneados = context.select((EscanerProvider value) => value.listadoCodigos);

    return listadoCodigosEscaneados != null ? 
      ListView.separated(
          separatorBuilder: (context, index) => Divider(color: Colors.black,),
          itemCount: listadoCodigosEscaneados.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(listadoCodigosEscaneados[index].codigo),
              trailing: IconButton(
                icon: Icon(Icons.delete, color: Colors.red,),
                onPressed: () async{
                  await context.read<EscanerProvider>().eliminarObtenerRegistros(listadoCodigosEscaneados[index].id);
                },
              ),
            );   
          },
        ) 
      : Container();
    
    
  }

  Future<String> scanQR() async {

    print(prefs.prefijoFisico);
    if (prefs.prefijoFisico != "" && prefs.numeroFisico != "") {
      String barcodeScanRes  = await FlutterBarcodeScanner.scanBarcode(
        "#2B061E", "Cancelar", true, ScanMode.BARCODE);

      print("escaneado: " + barcodeScanRes.toString());

      return !mounted ? "" : barcodeScanRes ;
    }
    else{
      await _dialogSinPrefijoNumero();
    }
  }

  _dialogSinPrefijoNumero() async{
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("No se ha especificado un prefijo y número de inventario."),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("Volver", style: TextStyle(color: Colors.red, fontSize: 22.0),),
          )
        ],
      )
    );
  }

  Widget _inputCodigoManual(){
    final _screenSize = MediaQuery.of(context).size;
    
    return Container(
      width: _screenSize.width * 0.6,
      height: _screenSize.width * 0.26,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextField(
            controller: _controllerInputManual,
            decoration: InputDecoration(
              labelText: "Codigo producto"
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                child: Text("Agregar"),
                onPressed: () async{
                  HomePostgresService postgres = HomePostgresService();

                  bool existeProducto = await postgres.existeProducto(_controllerInputManual.text);

                  if (existeProducto) {
                    await context.read<EscanerProvider>().insertarObtenerCodigos(_controllerInputManual.text);
                    _controllerInputManual.clear();
                  }
                  else{
                    showDialog(
                      context: context,
                      builder: (context) => ProductExistsDialog(),
                    );
                  }
                },
              ),
              IconButton(
                icon: Icon(Icons.close, color: Colors.red),
                onPressed: () {
                  _controllerInputManual.clear();
                },
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _botonesRedondeados() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.text_snippet,
                  texto: 'Seleccionar consecutivo',
                ),
                onTap: () async{
                  showDialog(
                    context: context,
                    builder: (context) => ConsecutivesDialog()
                  ).then((value){
                    setState(() { });
                  });
                  
                  
                },
              ),
              prefs.prefijoFisico != "" && prefs.numeroFisico != "" ? InkWell(
                onTap: () async {
                  HomePostgresService postgres = HomePostgresService();

                  String res =  await scanQR();

                  if (res != "-1") {
                    bool existeProducto = await postgres.existeProducto(res);

                    if (existeProducto) {
                      await context.read<EscanerProvider>().insertarObtenerCodigos(res);
                    }
                    else{
                      showDialog(
                        context: context,
                        builder: (context) =>  ProductExistsDialog()
                      );
                    }
                  }
                  
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.qr_code,
                  texto: 'Escanear codigo de barras',
                ),
              ) : Container(),
            ],
          ),
        ],
      ),
    );
  }
}