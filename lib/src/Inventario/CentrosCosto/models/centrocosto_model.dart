// To parse this JSON data, do
//
//     final centroCostoModel = centroCostoModelFromJson(jsonString);

import 'dart:convert';

CentroCostoModel centroCostoModelFromJson(String str) => CentroCostoModel.fromJson(json.decode(str));

String centroCostoModelToJson(CentroCostoModel data) => json.encode(data.toJson());

class CentroCostoModel {
    CentroCostoModel({
        this.sucursalid,
        this.nombre,
        this.centrocostoid,
        this.codigo,
        this.impresora,
        this.cantidadProductos,
    });

    int sucursalid;
    String nombre;
    int centrocostoid;
    String codigo;
    String impresora;
    int cantidadProductos;

    factory CentroCostoModel.fromJson(Map<String, dynamic> json) => CentroCostoModel(
        sucursalid: json["sucursalid"],
        nombre: json["nombre"],
        centrocostoid: json["centrocostoid"],
        codigo: json["codigo"],
        impresora: json["impresora"]
    );

    Map<String, dynamic> toJson() => {
        "sucursalid": sucursalid,
        "nombre": nombre,
        "centrocostoid": centrocostoid,
        "codigo": codigo,
        "impresora": impresora
    };
}
