import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/services/centros_costo_service.dart';

class EditarCentroCostoPage extends StatefulWidget {
  final CentroCostoModel centroCosto;
  EditarCentroCostoPage(this.centroCosto);

  @override
  _EditarCentroCostoPageState createState() => _EditarCentroCostoPageState();
}

class _EditarCentroCostoPageState extends State<EditarCentroCostoPage> {

  TextEditingController _codigoController = TextEditingController();
  TextEditingController _nombreController = TextEditingController();
  TextEditingController _impresoraController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _codigoController.text = widget.centroCosto.codigo;
    _nombreController.text = widget.centroCosto.nombre;
    _impresoraController.text = widget.centroCosto.impresora;
  }

  @override
  void dispose() {
    super.dispose();
    _codigoController?.dispose();
    _nombreController?.dispose();
    _impresoraController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Editar centro costo"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Actualizar", style: TextStyle(color: Colors.white)),
            onPressed: () async{
              CentrosCostoPostgresService centrosCostoPostgresService  = CentrosCostoPostgresService();

              CentroCostoModel centroCosto = CentroCostoModel();

              centroCosto.nombre = _nombreController.text;
              centroCosto.codigo = _codigoController.text;
              centroCosto.impresora = _impresoraController.text;
              centroCosto.centrocostoid = widget.centroCosto.centrocostoid;

              await centrosCostoPostgresService.actualizarCentrosCosto(centroCosto);

              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _formularioCrearCentroCosto()
    );
  }


  Widget _formularioCrearCentroCosto() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _impresoraInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),

              _botonEliminarCentroCosto()
              // _seccionSwitches()
            ],
          ),
        ),
      ),
    );
  }

  Widget _botonEliminarCentroCosto() {
    return OutlinedButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.symmetric(
        horizontal: 50.0,
        vertical: 10.0,
        )),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
      child: Text(
        'Eliminar',
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontSize: 14.0,
        ),
      ),
      onPressed: () async{
        CentrosCostoPostgresService centrosCostoPostgresService  = CentrosCostoPostgresService();

        await centrosCostoPostgresService.eliminarCentroCosto(widget.centroCosto.centrocostoid);

        Navigator.pop(context);
      },
    );
  }

  Widget _codigoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código del centro de costo*',
      ),
    );
  }

  Widget _nombreInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre del centro de costo*',
      ),
    );
  }

  Widget _impresoraInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _impresoraController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Direccion ip de la impresora',
      ),
    );
  }
  
}