import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/services/centros_costo_service.dart';

class CrearCentroCostoPage extends StatefulWidget {
  CrearCentroCostoPage({Key key}) : super(key: key);

  @override
  _CrearCentroCostoPageState createState() => _CrearCentroCostoPageState();
}

class _CrearCentroCostoPageState extends State<CrearCentroCostoPage> {
  TextEditingController _codigoController = TextEditingController();
  TextEditingController _nombreController = TextEditingController();
  TextEditingController _impresoraController = TextEditingController();

  bool _impresoraIp = false;
  bool _impresoraBluetooth = false;

  @override
  void dispose() {
    super.dispose();
    _codigoController?.dispose();
    _nombreController?.dispose();
    _impresoraController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nuevo centro costo"),
        centerTitle: true,
        actions: [
          TextButton(
            child: Text("Guardar", style: TextStyle(color: Colors.white)),
            onPressed: () async{
              CentrosCostoPostgresService centrosCostoPostgresService  = CentrosCostoPostgresService();

              CentroCostoModel nuevoCentroCosto = CentroCostoModel();

              nuevoCentroCosto.nombre = _nombreController.text;
              nuevoCentroCosto.codigo = _codigoController.text;
              nuevoCentroCosto.impresora = _impresoraController.text;

              await centrosCostoPostgresService.crearCentroCosto(nuevoCentroCosto);

              Navigator.pop(context);
            },
          )
        ],
      ),
      body: _formularioCrearCentroCosto(),
    );
  }

  Widget _formularioCrearCentroCosto() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _impresoraInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _seccionSwitches()
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código del centro de costo*',
      ),
    );
  }

  Widget _nombreInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre del centro de costo*',
      ),
    );
  }

  Widget _impresoraInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _impresoraController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Direccion ip de la impresora',
      ),
    );
  }

  Widget _seccionSwitches(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _switchImpresoraIp(),
        _switchImpresoraBluetooth(),
      ],
    );
  }

  Widget _switchImpresoraIp() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _impresoraIp = value;
            });
          },
          value: _impresoraIp,
          
        ),
        Text("Es impresora ip")
      ]
    );
  }

  Widget _switchImpresoraBluetooth() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _impresoraBluetooth = value;
            });
          },
          value: _impresoraBluetooth,
          
        ),
        Text("Es impresora bluetooth")
      ]
    );
  }
}