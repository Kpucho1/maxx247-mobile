import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/pages/crear_centrocosto_page.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/pages/editar_centrocosto_page.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/services/centros_costo_service.dart';

class CentrosCostoPage extends StatefulWidget {
  CentrosCostoPage({Key key}) : super(key: key);

  @override
  _CentrosCostoPageState createState() => _CentrosCostoPageState();
}

class _CentrosCostoPageState extends State<CentrosCostoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Centros de costo"),
        centerTitle: true,
      ),      
      body: _cuerpoCentrosCosto(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          await Navigator.push(context, MaterialPageRoute(builder: (context) => CrearCentroCostoPage())).then((value) {
            setState(() {});
          });
        },
      ),
    );
  }

  Widget _cuerpoCentrosCosto(context){
    CentrosCostoPostgresService centrosCostoPostgresService = CentrosCostoPostgresService();

    return FutureBuilder(
      future: centrosCostoPostgresService.obtenerCentrosCosto(),
      builder: (context, AsyncSnapshot<List<CentroCostoModel>> snapshot){
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator()
          );
        }
        else{
          List<CentroCostoModel> _listadoCentrosCosto = snapshot.data;

          return ListView.separated(
            separatorBuilder: (context, index) => Divider(),
            itemCount: _listadoCentrosCosto.length,
            itemBuilder: (context, index){
              return ListTile(
                title: Text( _listadoCentrosCosto[index].nombre),
                trailing: Text(_listadoCentrosCosto[index].impresora),
                subtitle: Text("Productos: "+_listadoCentrosCosto[index].cantidadProductos.toString()),
                onTap: () async{
                  await Navigator.push(context, MaterialPageRoute(settings: RouteSettings(arguments: {
                    "model": _listadoCentrosCosto[index]
                  }), builder: (context) => EditarCentroCostoPage(_listadoCentrosCosto[index]))).then((value) {
                    setState(() {});
                  });
                }
              );
            },
          );
        }
      },
    );
  }
}