import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class CentrosCostoPostgresService{

  CentrosCostoPostgresService._privateConstructor();

  static final CentrosCostoPostgresService _instancia =
      CentrosCostoPostgresService._privateConstructor();

  factory CentrosCostoPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();


  Future<List<CentroCostoModel>> obtenerCentrosCosto() async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> centroscosto = [];

    List<CentroCostoModel> listadoCentrosCosto = [];

    try {
      centroscosto = await conexionNegocio.query(
            "SELECT C.nombre, C.codigo, C.sucursalid, C.centrocostoid, C.impresora, count(P.id) FROM datos.centroscosto C, datos.productos P WHERE C.sucursalid = @sucursalid AND P.centrodecostoid = C.centrocostoid GROUP BY C.nombre, C.codigo, C.sucursalid, C.centrocostoid, C.impresora",
            substitutionValues: {
              "sucursalid": prefs.sucursalid
            });
      

      for (var centro in centroscosto) {
        final nuevoCentroCosto = CentroCostoModel();

        nuevoCentroCosto.nombre = centro[0];
        nuevoCentroCosto.codigo = centro[1];
        nuevoCentroCosto.sucursalid = centro[2];
        nuevoCentroCosto.centrocostoid = centro[3];
        nuevoCentroCosto.impresora = centro[4];
        nuevoCentroCosto.cantidadProductos = centro[5];
        
        listadoCentrosCosto.add(nuevoCentroCosto);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error obteniendo los centros de costo');
    }

    await conexionNegocio.close();

    return listadoCentrosCosto;
  }

  Future<void> crearCentroCosto(CentroCostoModel nuevoCentroCosto) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    try {
      List<List<dynamic>> maxCentroCostoId = await conexionNegocio.query(
            "SELECT MAX(centrocostoid) + 1 FROM datos.centroscosto");

      await conexionNegocio.query(
            "INSERT INTO datos.centroscosto(centrocostoid, sucursalid, nombre, codigo, impresora) VALUES (@centrocostoid, @sucursalid, @nombre, @codigo, @impresora)",
            substitutionValues: {
              "centrocostoid": maxCentroCostoId.first.first,
              "sucursalid": prefs.sucursalid,
              "nombre": nuevoCentroCosto.nombre,
              "codigo": nuevoCentroCosto.codigo,
              "impresora": nuevoCentroCosto.impresora
            });
      
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error creando los centros de costo');
    }

    await conexionNegocio.close();

  }

  Future<void> actualizarCentrosCosto(CentroCostoModel centroCosto) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    try {

      await conexionNegocio.query(
            "UPDATE datos.centroscosto SET nombre = @nombre, codigo = @codigo, impresora = @impresora WHERE centrocostoid = @centrocostoid",
            substitutionValues: {
              "centrocostoid": centroCosto.centrocostoid,
              "nombre": centroCosto.nombre,
              "codigo": centroCosto.codigo,
              "impresora": centroCosto.impresora
            });
      
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error actualizando los centros de costo');
    }

    await conexionNegocio.close();

  }

  Future<void> eliminarCentroCosto(int centrocostoid) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    try {

      await conexionNegocio.query(
            "DELETE FROM datos.centroscosto WHERE centrocostoid = @centrocostoid",
            substitutionValues: {
              "centrocostoid": centrocostoid,
            });
      
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error actualizando los centros de costo');
    }

    await conexionNegocio.close();

  }
}