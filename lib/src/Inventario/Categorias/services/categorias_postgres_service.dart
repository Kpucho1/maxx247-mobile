import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

import 'package:maxxshop/src/db/conexion_db_negocio.dart';

class CategoriasPostgresService {
  CategoriasPostgresService._privateConstructor();

  static final CategoriasPostgresService _instancia =
      CategoriasPostgresService._privateConstructor();

  factory CategoriasPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<CategoriasModel>> obtenerTodasCategorias() async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> categorias = [];

    List<CategoriasModel> listadoCategorias = [];

    try {
      categorias = await conexionNegocio.query(
          "SELECT grupoid, codigogrupo, descripcion, COALESCE(convert_from(foto, 'UTF8'), ''), ordenvista, grupoidpadre FROM datos.grupos1 WHERE esventa = 0 ORDER BY ordenvista ASC");

      for (var categoria in categorias) {
        final nuevaCategoria = new CategoriasModel();

        nuevaCategoria.grupoid = categoria[0];
        nuevaCategoria.codigogrupo = categoria[1];
        nuevaCategoria.descripcion = categoria[2];
        nuevaCategoria.foto = categoria[3].toString();
        nuevaCategoria.ordenvista = categoria[4];
        nuevaCategoria.grupopadreid = categoria[5];

        listadoCategorias.add(nuevaCategoria);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error al obtener las categorias de productos');
    }
    await conexionNegocio.close();

    return listadoCategorias ?? [];
  }

  Future<List<CategoriasModel>> obtenerTodasCategoriasDropDown() async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> categorias = [];

    List<CategoriasModel> listadoCategorias = [];

    try {
      categorias = await conexionNegocio.query(
          "SELECT grupoid, descripcion FROM datos.grupos1 WHERE esventa = 0 ORDER BY ordenvista ASC");

      for (var categoria in categorias) {
        final nuevaCategoria = new CategoriasModel();

        nuevaCategoria.grupoid = categoria[0];
        nuevaCategoria.descripcion = categoria[1];

        listadoCategorias.add(nuevaCategoria);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error al obtener las categorias del dropdown');
    }
    await conexionNegocio.close();

    return listadoCategorias ?? [];
  }

  Future<void> crearNuevaCategoria(CategoriasModel nuevaCategoria, blocCategorias) async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> grupoid = await conexionNegocio
          .query("SELECT  COALESCE(MAX(grupoid) + 1, 1) FROM datos.grupos1");

      await conexionNegocio.query(
          "INSERT INTO datos.grupos1(grupoid, codigogrupo, descripcion, esventa, foto) VALUES(@grupoid, @codigogrupo, @descripcion, @esventa, @foto)",
          substitutionValues: {
            "grupoid": grupoid.first.first,
            "codigogrupo": nuevaCategoria.codigogrupo,
            "descripcion": nuevaCategoria.descripcion,
            "esventa": 0,
            "foto": nuevaCategoria.foto,
          });
      
      
      await blocCategorias.agregarCategorias(nuevaCategoria);

    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Problema al crear categoría');
    }

    await conexionNegocio.close();
  }

  Future<void> actualizarCategoria(CategoriasModel categoria) async {
   ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.grupos1 SET codigogrupo = @codigogrupo, descripcion = @descripcion, esventa = @esventa, foto = @foto WHERE grupoid = @grupoid",
          substitutionValues: {
            "grupoid": categoria.grupoid,
            "codigogrupo": categoria.codigogrupo,
            "descripcion": categoria.descripcion,
            // "esventa": 0,
            "foto": categoria.foto,
          });

      await DBProvider.db.actualizarCategoria(categoria);
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Problema al actualizar la categoria');
    }

    await conexionNegocio.close();
  }

  Future<void> eliminarCategoria(int grupoid) async {
    ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);


    ConexionNegocioProvider.conexionNegocio.abrirConexion(conexionActiva.ipservidor,
        puertobd,
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password,);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "DELETE FROM datos.grupos1 WHERE grupoid = @grupoid",
          substitutionValues: {
            "grupoid": grupoid,
          });
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('No se eliminó la categoría');
    }

    await conexionNegocio.close();
  }
}
