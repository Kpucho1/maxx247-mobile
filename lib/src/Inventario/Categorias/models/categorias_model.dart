import 'dart:convert';

CategoriasModel categoriasModelFromJson(String str) =>
    CategoriasModel.fromJson(json.decode(str));

String categoriasModelToJson(CategoriasModel data) =>
    json.encode(data.toJson());

class CategoriasModel {
  CategoriasModel({
    this.id,
    this.grupoid,
    this.codigogrupo,
    this.descripcion,
    this.foto,
    this.ordenvista,
    this.grupopadreid
  });

  int id;
  int grupoid;
  String codigogrupo;
  String descripcion;
  String foto;
  int ordenvista;
  int grupopadreid;

  factory CategoriasModel.fromJson(Map<String, dynamic> json) =>
      CategoriasModel(
        id: json["id"],
        grupoid: json["grupoid"],
        codigogrupo: json["codigogrupo"],
        descripcion: json["descripcion"],
        foto: json["foto"],
        ordenvista: json["ordenvista"],
        grupopadreid: json["grupopadreid"]
      );

  Map<String, dynamic> toJson() => {
        "grupoid": grupoid,
        "codigogrupo": codigogrupo,
        "descripcion": descripcion,
        "foto": foto,
        "ordenvista": ordenvista ?? 0,
        "grupopadreid": grupopadreid ?? 0
      };
}
