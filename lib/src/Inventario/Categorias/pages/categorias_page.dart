import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/provider.dart';

import 'package:maxxshop/src/widgets/image_list_tile.dart';
import 'package:maxxshop/src/Inventario/Categorias/pages/crear_categoria_page.dart';
import 'package:maxxshop/src/Inventario/Categorias/pages/editar_categoria_page.dart';

class CategoriasPage extends StatefulWidget {
  @override
  _CategoriasPageState createState() => _CategoriasPageState();
}

class _CategoriasPageState extends State<CategoriasPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final blocCategorias = ProviderInherited.categoriasBloc(context);
    blocCategorias.obtenerCategorias();

    

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              //TODO: Search delegate de categorías
            },
          )
        ],
        title: Text(
          "Categorias",
          style: TextStyle(color: Colors.black),
        ),
      ),
      floatingActionButton: _botonFlotante(),
      body: _listadoCategorias(blocCategorias),
    );
  }

  Widget _botonFlotante() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CrearCategoriaPage(),
          ),
        ).then((value) async{
          final blocCategorias = ProviderInherited.categoriasBloc(context);

          blocCategorias.obtenerCategorias();

          setState(() {
            
          });
        });
      },
      child: Icon(
        Icons.add,
        size: 27.0,
      ),
    );
  }

  Widget _listadoCategorias(CategoriasBloc bloc) {
    return StreamBuilder(
      stream: bloc.categoriasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
              child: LinearProgressIndicator());

        final List<CategoriasModel> listaCategorias = snapshot.data;


        return ListView.separated(
          separatorBuilder: (context, index) => Divider(),
          itemCount: listaCategorias.length,
          itemBuilder: (context, index){
            return Card(
              child: ListTile(
                leading: ImageListTile(
                  imageString: listaCategorias[index].foto,
                  primerLetra:  'C',
                ),
                title: Text(listaCategorias[index].descripcion),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditarCategoriaPage(
                    categoria: listaCategorias[index],
                  ))).then((value) {
                    bloc.obtenerCategorias();
                    setState(() {
                      
                    });
                  });
                },
              )
            );
          },
        );
      },
    );
  }
}
