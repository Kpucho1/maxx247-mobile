import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/Categorias/services/categorias_postgres_service.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/widgets/boton_subir_foto.dart';


class CrearCategoriaPage extends StatefulWidget {
  CrearCategoriaPage({Key key}) : super(key: key);

  @override
  _CrearCategoriaPageState createState() => _CrearCategoriaPageState();
}

class _CrearCategoriaPageState extends State<CrearCategoriaPage> {
  TextEditingController _codigoCategoriaController = TextEditingController();
  TextEditingController _nombreCategoriaController = TextEditingController();

  final botonAgregarFoto = BotonSubirFoto();

  @override
  void dispose() {
    _codigoCategoriaController?.dispose();
    _nombreCategoriaController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        actions: _buildActions(),
        title: Text(
          "Crear categoría",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: _formularioCrearCategoria(),
    );
  }

  List<Widget> _buildActions() {
    return <Widget>[
      TextButton(
        child: Text("Guardar", style: TextStyle(color: Colors.white)),
        onPressed: () async {
          List<int> imagenHex = botonAgregarFoto.imagenSeleccionada != null
              ? botonAgregarFoto.imagenSeleccionada.readAsBytesSync()
              : [];

          CategoriasModel nuevaCategoria = CategoriasModel(
              codigogrupo: _codigoCategoriaController.text,
              descripcion: _nombreCategoriaController.text,
              foto: hex.encode(imagenHex));
          
          final blocCategorias = ProviderInherited.categoriasBloc(context);


          CategoriasPostgresService categoriasPostgresService = CategoriasPostgresService();

          categoriasPostgresService.crearNuevaCategoria(nuevaCategoria, blocCategorias);

          Navigator.pop(context);
        },
      )
    ];
  }


  Widget _formularioCrearCategoria() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreCategoriaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              botonAgregarFoto
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código de la categoría*',
      ),
    );
  }

  Widget _nombreCategoriaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreCategoriaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre de la categoría*',
      ),
    );
  }

}
