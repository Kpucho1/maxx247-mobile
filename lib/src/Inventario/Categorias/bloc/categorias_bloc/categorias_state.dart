part of 'categorias_bloc.dart';

@immutable
abstract class CategoriasState extends Equatable {
  @override
  List<Object> get props => [];
}

//Estados para la creacion de nuevas categorias

class CrearCategoriaLoadingState extends CategoriasState {}

class CategoriaCreadaExitoState extends CategoriasState {}

class ErrorCrearCategoriaState extends CategoriasState {}

//Estados para la actualizacion/modificacion de categorias

class ActualizarCategoriaState extends CategoriasState {}

class ErrorActualizarCategoriaState extends CategoriasState {}

//Estados para la eliminación de categorías

class EliminarCategoriaState extends CategoriasState {}

class ErrorEliminarCategoriaState extends CategoriasState {}

//Estados referentes al listado de categorias a mostrar al usuario

class InitialCategoriasState extends CategoriasState {}

class ErrorObtenerCategoriasState extends CategoriasState {}

class ObtenerCategoriasState extends CategoriasState {
  //

  final List<CategoriasModel> listadoCategorias;

  ObtenerCategoriasState({List<CategoriasModel> lista})
      : this.listadoCategorias = lista ?? [];

  ObtenerCategoriasState copyWith({
    List<CategoriasModel> listadoCategorias,
  }) =>
      ObtenerCategoriasState(
        lista: listadoCategorias ?? this.listadoCategorias,
      );

  @override
  List<Object> get props => [listadoCategorias];
}
