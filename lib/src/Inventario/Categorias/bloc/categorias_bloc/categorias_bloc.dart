import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:maxxshop/src/Inventario/Categorias/models/categorias_model.dart';
import 'package:maxxshop/src/Inventario/Categorias/services/categorias_postgres_service.dart';

part 'categorias_event.dart';
part 'categorias_state.dart';

class CategoriasBloc extends Bloc<CategoriasEvent, CategoriasState> {
  CategoriasBloc() : super(InitialCategoriasState());

  final _categoriasPostgres = CategoriasPostgresService();

  @override
  Stream<CategoriasState> mapEventToState(
    CategoriasEvent event,
  ) async* {
    //
    final currentState = state;

    if (event is OnObtenerCategorias) {
      //
      try {
        if (currentState is InitialCategoriasState) {
          //
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          // yield ObtenerCategoriasState(lista: lista);
        }

        if (currentState is ErrorObtenerCategoriasState) {
          //
          yield InitialCategoriasState();

          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          // yield ObtenerCategoriasState(lista: lista);
        }

        if (currentState is ErrorCrearCategoriaState) {
          yield InitialCategoriasState();

          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          // yield ObtenerCategoriasState(lista: lista);
        }
        //
      } catch (_) {
        //
        yield ErrorObtenerCategoriasState();
      }
    }

    if (event is OnCrearCategoria) {
      try {
        if (currentState is ObtenerCategoriasState) {
          //
          yield CrearCategoriaLoadingState();

          // await _categoriasPostgres.crearNuevaCategoria(event.nuevaCategoria);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          yield CategoriaCreadaExitoState();
          Future.delayed(Duration(seconds: 2));
          // yield ObtenerCategoriasState(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorCrearCategoriaState();
      }
    }

    if (event is OnActualizarCategoria) {
      try {
        //
        if (currentState is ObtenerCategoriasState) {
          yield ActualizarCategoriaState();

          // await _categoriasPostgres.actualizarCategoria(event.categoria);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          // yield ObtenerCategoriasState(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorActualizarCategoriaState();
      }
    }

    if (event is OnEliminarCategoria) {
      try {
        if (currentState is ObtenerCategoriasState) {
          yield EliminarCategoriaState();

          await _categoriasPostgres.eliminarCategoria(event.grupoId);
          final lista = await _categoriasPostgres.obtenerTodasCategorias();

          // yield ObtenerCategoriasState(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorEliminarCategoriaState();
      }
    }
  }
}
