part of 'categorias_bloc.dart';

@immutable
abstract class CategoriasEvent {}

class OnObtenerCategorias extends CategoriasEvent {}

class OnCrearCategoria extends CategoriasEvent {
  final CategoriasModel nuevaCategoria;

  OnCrearCategoria({this.nuevaCategoria});
}

class OnActualizarCategoria extends CategoriasEvent {
  final CategoriasModel categoria;

  OnActualizarCategoria({this.categoria});
}

class OnEliminarCategoria extends CategoriasEvent {
  final int grupoId;

  OnEliminarCategoria({this.grupoId});
}
