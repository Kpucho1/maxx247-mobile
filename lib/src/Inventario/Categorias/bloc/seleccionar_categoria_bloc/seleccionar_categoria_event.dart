part of 'seleccionar_categoria_bloc.dart';

abstract class SeleccionarCategoriaEvent {
  const SeleccionarCategoriaEvent();
}

class OnCargarCategoriasDropDown extends SeleccionarCategoriaEvent {
  final List<CategoriasModel> listadoCategorias;

  OnCargarCategoriasDropDown({this.listadoCategorias});
}

class OnSeleccionarCategoria extends SeleccionarCategoriaEvent {
  final int categoriaSeleccionada;

  OnSeleccionarCategoria({this.categoriaSeleccionada});
}
