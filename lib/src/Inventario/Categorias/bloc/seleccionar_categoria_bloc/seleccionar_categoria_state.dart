part of 'seleccionar_categoria_bloc.dart';

abstract class SeleccionarCategoriaState extends Equatable {
  const SeleccionarCategoriaState();

  @override
  List<Object> get props => [];
}

class SeleccionarCategoriaInitial extends SeleccionarCategoriaState {}

class SeleccionarCategoriaLoadingState extends SeleccionarCategoriaState {}

class ErrorObtenerCategoriaSeleccionadaState extends SeleccionarCategoriaState {
}

class EnviarCargarProductosSeleccionadosState
    extends SeleccionarCategoriaState {}

class ObtenerListaCategoriasDropDownState extends SeleccionarCategoriaState {
  final int categoriaSeleccionada;
  final List<CategoriasModel> listadoCategorias;

  ObtenerListaCategoriasDropDownState(
      {int categoriaSeleccionada, List<CategoriasModel> lista})
      : this.categoriaSeleccionada = categoriaSeleccionada ?? null,
        this.listadoCategorias = lista ?? [];

  ObtenerListaCategoriasDropDownState copyWith({
    int categoria,
    List<CategoriasModel> listaCategorias,
  }) =>
      ObtenerListaCategoriasDropDownState(
        categoriaSeleccionada: categoria ?? this.categoriaSeleccionada,
        lista: listaCategorias ?? this.listadoCategorias,
      );

  @override
  List<Object> get props => [categoriaSeleccionada, listadoCategorias];
}
