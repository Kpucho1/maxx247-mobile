import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:maxxshop/src/Inventario/Categorias/models/categorias_model.dart';
import 'package:maxxshop/src/Inventario/Categorias/bloc/categorias_bloc/categorias_bloc.dart';

part 'seleccionar_categoria_event.dart';
part 'seleccionar_categoria_state.dart';

class SeleccionarCategoriaBloc
    extends Bloc<SeleccionarCategoriaEvent, SeleccionarCategoriaState> {
  //Crea una suscripcion al bloc de categorias para reaccionar a eventos
  //como el agregado, actualizado o eliminado de alguna categoría
  //
  final CategoriasBloc blocCategorias;
  StreamSubscription subscriptionBlocCategorias;

  SeleccionarCategoriaBloc({this.blocCategorias})
      : super(SeleccionarCategoriaInitial()) {
    subscriptionBlocCategorias = blocCategorias.listen((state) {
      if (state is ObtenerCategoriasState) {
        //Carga el dropdown luego que las categorias en general han empezado a ser cargadas
        //
        add(OnCargarCategoriasDropDown(
            listadoCategorias: state.listadoCategorias));
      }
    });
  }

  @override
  Stream<SeleccionarCategoriaState> mapEventToState(
    SeleccionarCategoriaEvent event,
  ) async* {
    final currentState = state;

    if (event is OnCargarCategoriasDropDown) {
      //
      try {
        if (currentState is SeleccionarCategoriaInitial) {
          yield ObtenerListaCategoriasDropDownState(
              lista: event.listadoCategorias);
        }

        if (currentState is ObtenerListaCategoriasDropDownState) {
          yield currentState.copyWith(listaCategorias: event.listadoCategorias);
        }

        if (currentState is ErrorObtenerCategoriaSeleccionadaState) {
          yield ObtenerListaCategoriasDropDownState(
              lista: event.listadoCategorias);
        }
        //
      } on TimeoutException catch (_) {
        //
        yield ErrorObtenerCategoriaSeleccionadaState();
        add(OnCargarCategoriasDropDown());
        //
      } catch (_) {
        //
        yield ErrorObtenerCategoriaSeleccionadaState();
      }
    }

    if (event is OnSeleccionarCategoria) {
      if (currentState is ObtenerListaCategoriasDropDownState) {
        yield currentState.copyWith(categoria: event.categoriaSeleccionada);
      }
    }
  }

  @override
  Future<void> close() {
    subscriptionBlocCategorias?.cancel();
    return super.close();
  }
}
