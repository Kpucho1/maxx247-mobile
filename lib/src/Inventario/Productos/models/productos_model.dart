import 'dart:convert';

ProductosModel productosModelFromJson(String str) =>
    ProductosModel.fromJson(json.decode(str));

String productosModelToJson(ProductosModel data) => json.encode(data.toJson());

class ProductosModel {
  ProductosModel({
    this.id,
    this.productoid,
    this.nombre,
    this.precio,
    this.grupo1id,
    this.centrocostoid,
    this.impuestoconsumo,
    this.codigo,
    this.precioivaventa,
    this.presentacion,
    this.foto,
    this.productoValorUnitario,
    this.productoImprimeComanda,
    this.productoEquivalencia,
    this.productoPorIva,
    this.productoPorIpo,
    this.esServicio,
    this.numeroMaximoCombos,
    this.ordenvista,
    this.estado,
    this.pidePeso,
    this.porTiempo,
    this.costo,
    this.grupo2id
  });

  int id;
  int productoid;
  String nombre;
  double precio;
  int grupo1id;
  int centrocostoid;
  double impuestoconsumo;
  String codigo;
  double precioivaventa;
  String presentacion;
  String foto;
  int productoValorUnitario;
  int productoImprimeComanda;
  int productoEquivalencia;
  double productoPorIva;
  double productoPorIpo;
  int esServicio;
  int numeroMaximoCombos;
  int ordenvista;
  int estado;
  int pidePeso;
  int porTiempo;
  double costo;
  int grupo2id;

  factory ProductosModel.fromJson(Map<String, dynamic> json) => ProductosModel(
        id: json["id"],
        productoid: json["productoid"],
        nombre: json["nombre"],
        precio: json["precio"].toDouble(),
        grupo1id: json["grupo1id"],
        centrocostoid: json["centrocostoid"],
        impuestoconsumo: json["impuestoconsumo"],
        codigo: json["codigo"],
        precioivaventa: json["precioivaventa"],
        presentacion: json["presentacion"],
        foto: json["foto"],
        productoValorUnitario: json["productovalorunitario"],
        productoImprimeComanda: json["productoimprimecomanda"],
        productoEquivalencia: json["productoequivalencia"],
        productoPorIva: json["productoporiva"],
        productoPorIpo: json["productoporipo"],
        esServicio: json["esservicio"],
        numeroMaximoCombos: json["numeromaximocombos"],
        ordenvista: json["ordenvista"],
        estado: json["estado"]
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "productoid": productoid,
        "nombre": nombre,
        "precio": precio,
        "grupo1id": grupo1id,
        "centrocostoid": centrocostoid,
        "impuestoconsumo": impuestoconsumo,
        "codigo": codigo,
        "precioivaventa": precioivaventa,
        "presentacion": presentacion,
        "foto": foto,
        "productovalorunitario": productoValorUnitario,
        "productoimprimecomanda": productoImprimeComanda,
        "productoequivalencia": productoEquivalencia,
        "productoporiva": productoPorIva,
        "productoporipo": productoPorIpo,
        "esservicio": esServicio,
        "numeromaximocombos": numeroMaximoCombos,
        "ordenvista": ordenvista,
        "estado": estado
      };
}
