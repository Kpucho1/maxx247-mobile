import 'package:convert/convert.dart';
import 'package:maxxshop/src/Conexiones/models/conexiones_model.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/Productos/models/productos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:postgres/postgres.dart';

class ProductosPostgresService {
  ProductosPostgresService._privateConstructor();

  static final ProductosPostgresService _instancia =
      ProductosPostgresService._privateConstructor();

  factory ProductosPostgresService() {
    return _instancia;
  }

  final prefs = PreferenciasUsuario();

  Future<List<ProductosModel>> obtenerTodosProductos(int grupoid) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> productos = [];

    List<ProductosModel> listadoProductos = [];

    try {
      if (grupoid != 0) {
        productos = await conexionNegocio.query(
            "SELECT productoid, nombre, CAST(precioventaconiva AS REAL), grupo1id, centrodecostoid, CAST(impuestoconsumo AS REAL), CAST(precioivaventa AS REAL), presentacion, producto, COALESCE(convert_from(foto, 'UTF8'), ''), CAST(precioventasiniva AS INTEGER), CAST(porcentajeivaventa AS REAL), CAST(porcentajeipoventa AS REAL), imprimecomanda, equivalenciasventa, esservicio, numeromaximocombos, diasgarantia, bloqueo  FROM datos.productos WHERE grupo1id = @grupoSeleccionado AND bloqueo = 0",
            substitutionValues: {"grupoSeleccionado": grupoid});
      } else {
        productos = await conexionNegocio.query(
            "SELECT productoid, nombre, CAST(precioventaconiva AS REAL), grupo1id, centrodecostoid, CAST(impuestoconsumo AS REAL), CAST(precioivaventa AS REAL), presentacion, producto, COALESCE(convert_from(foto, 'UTF8'), ''), CAST(precioventasiniva AS INTEGER), CAST(porcentajeivaventa AS REAL), CAST(porcentajeipoventa AS REAL), imprimecomanda, equivalenciasventa, esservicio, numeromaximocombos, diasgarantia, bloqueo  FROM datos.productos WHERE bloqueo = 0");
      }

      for (var producto in productos) {
        final nuevoProducto = ProductosModel();

        nuevoProducto.productoid = producto[0];
        nuevoProducto.nombre = producto[1];
        nuevoProducto.precio = producto[2];
        nuevoProducto.grupo1id = producto[3];
        nuevoProducto.centrocostoid = producto[4];
        nuevoProducto.impuestoconsumo = producto[5];
        nuevoProducto.precioivaventa = producto[6];
        nuevoProducto.presentacion = producto[7];
        nuevoProducto.codigo = producto[8];
        nuevoProducto.foto = producto[9].toString();
        nuevoProducto.productoValorUnitario = producto[10];
        nuevoProducto.productoPorIva = producto[11];
        nuevoProducto.productoPorIpo = producto[12];
        nuevoProducto.productoImprimeComanda = producto[13];
        nuevoProducto.productoEquivalencia = producto[14];
        nuevoProducto.esServicio = producto[15];
        nuevoProducto.numeroMaximoCombos = producto[16];
        nuevoProducto.ordenvista = producto[17];
        nuevoProducto.estado = producto[18];

        listadoProductos.add(nuevoProducto);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error obteniendo los productos');
    }

    await conexionNegocio.close();

    return listadoProductos;
  }

  Future<List<ProductosCuentaModel>> obtenerProductosPorNombre(String nombre) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> productos = [];

    List<ProductosCuentaModel> listadoProductos = [];

    try {
      productos = await conexionNegocio.query(
            "SELECT productoid, nombre, CAST(precioventaconiva AS REAL), grupo1id, centrodecostoid, CAST(impuestoconsumo AS REAL), CAST(precioivaventa AS REAL), presentacion, producto, COALESCE(convert_from(foto, 'UTF8'), ''), CAST(precioventasiniva AS INTEGER), CAST(porcentajeivaventa AS REAL), CAST(porcentajeipoventa AS REAL), imprimecomanda, equivalenciasventa, esservicio, numeromaximocombos, diasgarantia, bloqueo  FROM datos.productos WHERE LOWER(nombre) LIKE LOWER(@nombre) AND bloqueo = 0",
            substitutionValues: {"nombre": '%$nombre%'});
      
      print(nombre);

      for (var producto in productos) {
        final nuevoProducto = ProductosCuentaModel();

        nuevoProducto.productoid = producto[0];
        nuevoProducto.nombre = producto[1];
        nuevoProducto.costo = producto[2];
        nuevoProducto.grupo1id = producto[3];
        nuevoProducto.centrocostoid = producto[4];
        nuevoProducto.impuestoconsumo = producto[5];
        nuevoProducto.precioivaventa = producto[6];
        nuevoProducto.presentacion = producto[7];
        nuevoProducto.codigo = producto[8];
        nuevoProducto.foto = producto[9].toString();
        nuevoProducto.productoValorUnitario = producto[10];
        nuevoProducto.productoPorIva = producto[11];
        nuevoProducto.productoPorIpo = producto[12];
        nuevoProducto.productoImprimeComanda = producto[13];
        nuevoProducto.productoEquivalencia = producto[14];
        nuevoProducto.esServicio = producto[15];
        nuevoProducto.numeroMaximoCombos = producto[16];
        nuevoProducto.ordenvista = producto[17];
        nuevoProducto.estado = producto[18];

        listadoProductos.add(nuevoProducto);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error obteniendo los productos');
    }

    await conexionNegocio.close();

    return listadoProductos;
  }

  Future<List<CentroCostoModel>> obtenerCentrosCosto() async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> centroscosto = [];

    List<CentroCostoModel> listadoCentrosCosto = [];

    try {
      centroscosto = await conexionNegocio.query(
            "SELECT nombre, codigo, sucursalid, centrocostoid FROM datos.centroscosto WHERE sucursalid = @sucursalid",
            substitutionValues: {
              "sucursalid": prefs.sucursalid
            });
      

      for (var centro in centroscosto) {
        final nuevoCentroCosto = CentroCostoModel();

        nuevoCentroCosto.nombre = centro[0];
        nuevoCentroCosto.codigo = centro[1];
        nuevoCentroCosto.sucursalid = centro[2];
        nuevoCentroCosto.centrocostoid = centro[3];
        
        listadoCentrosCosto.add(nuevoCentroCosto);
      }
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error obteniendo los centros de costo');
    }

    await conexionNegocio.close();

    return listadoCentrosCosto;
  }


  Future<void> crearNuevoProducto(ProductosCuentaModel nuevoProducto, blocProductos) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> productoid = await conexionNegocio
          .query("SELECT COALESCE(MAX(productoid) + 1, 1) FROM datos.productos");

      await conexionNegocio.query(
          "INSERT INTO datos.productos(productoid, producto, nombre, grupo1id, precioventaconiva, costo, foto, esservicio, bloqueo, centrodecostoid, imprimecomanda) VALUES(@productoid, @codigo, @nombre, @categoria, @precio, @costo, @foto, @esservicio, @bloqueo, @centrocostoid, @imprimecomanda)",
          substitutionValues: {
            "productoid": productoid.first.first,
            "codigo": nuevoProducto.codigo,
            "nombre": nuevoProducto.nombre,
            "categoria": nuevoProducto.grupo1id,
            "precio": nuevoProducto.precio,
            "costo": nuevoProducto.costo,
            "foto": nuevoProducto.foto,
            "esservicio": nuevoProducto.esServicio,
            "bloqueo": nuevoProducto.estado,
            "centrocostoid": nuevoProducto.centrocostoid,
            "imprimecomanda": nuevoProducto.productoImprimeComanda
          });
      
      await blocProductos.agregarProductos(nuevoProducto);


      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
      throw Exception('Error al intentar crear el producto');
    }

    await conexionNegocio.close();
  }

  Future<void> actualizarProducto(ProductosCuentaModel producto, blocProductos) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      producto.ts = DateTime.now().toLocal().toString();
      await conexionNegocio.query(
          "UPDATE datos.productos SET producto = @codigo, nombre = @nombre, grupo1id = @categoria, precioventaconiva =  @precio, costo = @costo, imprimecomanda = @imprimecomanda, centrodecostoid = @centrocostoid, foto = @foto, ts = @ts, esservicio = @esservicio, portiempo = @portiempo, pidepeso = @pidepeso, bloqueo = @bloqueo WHERE productoid = @productoid",
          substitutionValues: {
            "productoid": producto.productoid,
            "codigo": producto.codigo,
            "nombre": producto.nombre,
            "categoria": producto.grupo1id,
            "precio": producto.precio,
            "costo": producto.costo,
            "imprimecomanda": producto.productoImprimeComanda,
            "centrocostoid": producto.centrocostoid,
            "foto": producto.foto,
            "ts": producto.ts,
            "esservicio": producto.esServicio,
            "portiempo": producto.porTiempo,
            "pidepeso": producto.pidePeso,
            "bloqueo": producto.estado
          });

      await DBProvider.db.actualizarProducto(producto);

      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }

  Future<void> actualizarCodigoBarrasProducto(cod, productoid, barcode) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      var ts = DateTime.now().toLocal();
      await conexionNegocio.query(
          "UPDATE datos.productos SET $cod = @barcode, ts = @ts  WHERE productoid = @productoid",
          substitutionValues: {
            "productoid": productoid,
            "barcode": barcode,
            "cod": cod,
            "ts": ts
          });

      await DBProvider.db.actualizarProductoBarras(barcode, cod == "codigobarras1" ? 1 : 2, productoid, ts);

      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }

  Future<void> eliminarProducto(int productoid) async {
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.productos SET bloqueo = 1 WHERE productoid = @productoid",
          substitutionValues: {
            "productoid": productoid,
          });

      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }

  Future<void> crearProductoGusto(ProductosGustosModel productosGustosModel) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      List<List<dynamic>> maxGustoId = await conexionNegocio.query("SELECT MAX(gustoid) + 1 FROM datos.productosgustos;");

      productosGustosModel.gustoid = maxGustoId.first[0];

      
      await conexionNegocio.query(
          "INSERT INTO datos.productosgustos(gustoid, productoid, nombre) VALUES (@gustoid, @productoid, @nombre)",
          substitutionValues: {
            "gustoid": productosGustosModel.gustoid,
            "productoid": productosGustosModel.productoid,
            "nombre": productosGustosModel.nombre
          });


      await DBProvider.db.agregarProductosGustos(productosGustosModel);

      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }

  Future<void> eliminarProductoGusto(int gustoid) async{
    ConexionesModel conexionActiva = await DBProvider.db.getConexionActiva();

    final puertobd = int.tryParse(conexionActiva.puertobd);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {

      await conexionNegocio.query(
          "DELETE FROM datos.productosgustos WHERE gustoid = @gustoid",
          substitutionValues: {
            "gustoid": gustoid,
            
          });


      await DBProvider.db.eliminarProductosGustos(gustoid);

      await conexionNegocio.close();
    } catch (e) {
      print(e);
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
  }
}
