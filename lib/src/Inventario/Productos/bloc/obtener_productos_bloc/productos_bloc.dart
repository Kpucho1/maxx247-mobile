import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:meta/meta.dart';

import 'package:maxxshop/src/Inventario/Productos/models/productos_model.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';
import 'package:maxxshop/src/Inventario/Productos/bloc/crear_producto_bloc/crear_producto_bloc.dart';

part 'productos_event.dart';
part 'productos_state.dart';

class ProductosBloc extends Bloc<ProductosEvent, ProductosState> {
  final CrearProductoBloc blocCrearProducto;
  StreamSubscription suscripcionBlocCrearProducto;
  final prefs = PreferenciasUsuario();

  ProductosBloc({this.blocCrearProducto}) : super(InitialProductosState()) {
    suscripcionBlocCrearProducto = blocCrearProducto.listen((state) {
      if (state is GuardadoExitosoState) {
        //
        print(state.categoriaSeleccionada);
        add(OnObtenerNuevosProductos(grupoId: state.categoriaSeleccionada));
      }
    });
  }

  final _productosPostgres = ProductosPostgresService();

  @override
  Stream<ProductosState> mapEventToState(
    ProductosEvent event,
  ) async* {
    //
    final currentState = state;
    //
    if (event is ObtenerProductos) {
      try {
        if (currentState is InitialProductosState) {
          final lista =
              await _productosPostgres.obtenerTodosProductos(event.grupoId);

          yield ObtenerProductosState(lista: lista);
        }

        if (currentState is ObtenerProductosState) {
          final lista =
              await _productosPostgres.obtenerTodosProductos(event.grupoId);

          yield currentState.copyWith(listadoProductos: lista);
        }

        if (currentState is ErrorObtenerProductos) {
          yield InitialProductosState();

          final lista =
              await _productosPostgres.obtenerTodosProductos(event.grupoId);

          yield ObtenerProductosState(lista: lista);
        }
      } catch (e) {
        print(e);
        yield ErrorObtenerProductos();
      }
    }

    if (event is OnObtenerNuevosProductos) {
      try {
        //
        if (currentState is ObtenerProductosState) {
          yield InitialProductosState();
          print(event.grupoId);

          final lista =
              await _productosPostgres.obtenerTodosProductos(event.grupoId);

          yield ObtenerProductosState(lista: lista);
        }
      } catch (_) {
        //
        yield ErrorObtenerProductos();
      }
    }
  }

  @override
  Future<void> close() {
    suscripcionBlocCrearProducto?.cancel();
    return super.close();
  }
}
