part of 'productos_bloc.dart';

@immutable
abstract class ProductosEvent {}

class ObtenerProductos extends ProductosEvent {
  final int grupoId;

  ObtenerProductos({this.grupoId});
}

class OnObtenerNuevosProductos extends ProductosEvent {
  final int grupoId;

  OnObtenerNuevosProductos({this.grupoId});
}
