part of 'productos_bloc.dart';

@immutable
abstract class ProductosState {
  const ProductosState();

  copyWith() {}
}

class InitialProductosState extends ProductosState {}

class ErrorObtenerProductos extends ProductosState {}

class ObtenerProductosState extends ProductosState {
  final bool existenProductos;
  final List<ProductosModel> listadoProductos;

  ObtenerProductosState({List<ProductosModel> lista})
      : listadoProductos = lista ?? [],
        existenProductos = (lista != null && lista != []) ? true : false,
        super();

  @override
  ObtenerProductosState copyWith({
    List<ProductosModel> listadoProductos,
  }) {
    super.copyWith();
    return ObtenerProductosState(
      lista: listadoProductos ?? this.listadoProductos,
    );
  }
}
