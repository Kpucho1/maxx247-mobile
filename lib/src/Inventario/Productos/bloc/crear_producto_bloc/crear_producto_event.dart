part of 'crear_producto_bloc.dart';

@immutable
abstract class CrearProductoEvent {}

class OnCrearProducto extends CrearProductoEvent {
  final int categoriaSeleccionada;
  final ProductosModel nuevoProducto;

  OnCrearProducto({this.nuevoProducto, this.categoriaSeleccionada});
}
