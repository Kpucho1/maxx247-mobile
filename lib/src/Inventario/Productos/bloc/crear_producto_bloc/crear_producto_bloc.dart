import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:maxxshop/src/Inventario/Productos/models/productos_model.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';

part 'crear_producto_event.dart';
part 'crear_producto_state.dart';

class CrearProductoBloc extends Bloc<CrearProductoEvent, NuevoProductoState> {
  CrearProductoBloc() : super(InitialState());

  final _repositorioProductos = ProductosPostgresService();

  @override
  Stream<NuevoProductoState> mapEventToState(
    CrearProductoEvent event,
  ) async* {
    final currentState = state;

    if (event is OnCrearProducto) {
      try {
        if (currentState is InitialState) {
          //yield CrearProductoLoadingProgress();

          // await _repositorioProductos.crearNuevoProducto(event.nuevoProducto);

          yield GuardadoExitosoState(
              categoriaSeleccionada: event.categoriaSeleccionada);
        }

        if (currentState is GuardadoExitosoState) {
          // await _repositorioProductos.crearNuevoProducto(event.nuevoProducto);

          yield GuardadoExitosoState(
              categoriaSeleccionada: event.categoriaSeleccionada);
        }
      } catch (_) {
        yield ErrorGuardadoState();
      }
    }
  }
}
