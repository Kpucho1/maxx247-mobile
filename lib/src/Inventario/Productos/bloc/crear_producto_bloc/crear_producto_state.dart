part of 'crear_producto_bloc.dart';

@immutable
abstract class NuevoProductoState {
  const NuevoProductoState();  
}

class InitialState extends NuevoProductoState {}

class CrearProductoState extends NuevoProductoState {
  //
  final String codigoProducto;
  final String nombreProducto;
  final String categoria;
  final double precio;
  final double costo;
  final bool imprimeComanda;
  final bool bloqueo;
  final bool esServicio;
  final bool porTiempo;
  final bool pidePeso;
  final double impuestoConsumo;
  final double precioIvaVenta;
  final double precioIvaCompra;
  final int tipoIvaCompra;
  final int tipoIvaVenta;
  final double porcentajeIvaVenta;
  final double porcentajeIvaCompra;
  final double porcentajeIpoVenta;
  final double precioIpoVenta;

  const CrearProductoState({
    this.codigoProducto,
    this.nombreProducto,
    this.categoria,
    this.precio,
    this.costo,
    this.imprimeComanda,
    this.bloqueo,
    this.esServicio,
    this.porTiempo,
    this.pidePeso,
    this.impuestoConsumo,
    this.precioIvaVenta,
    this.precioIvaCompra,
    this.tipoIvaCompra,
    this.tipoIvaVenta,
    this.porcentajeIvaVenta,
    this.porcentajeIvaCompra,
    this.porcentajeIpoVenta,
    this.precioIpoVenta,
  });

  CrearProductoState copyWith({
    String codigoProducto,
    String nombreProducto,
    String categoria,
    double precio,
    double costo,
    bool imprimeComanda,
    bool bloqueo,
    bool esServicio,
    bool porTiempo,
    bool pidePeso,
    double impuestoConsumo,
    double precioIvaVenta,
    double precioIvaCompra,
    int tipoIvaCompra,
    int tipoIvaVenta,
    double porcentajeIvaVenta,
    double porcentajeIvaCompra,
    double porcentajeIpoVenta,
    double precioIpoVenta,
  }) =>
      CrearProductoState(
        codigoProducto: codigoProducto ?? this.codigoProducto,
        nombreProducto: nombreProducto ?? this.nombreProducto,
        categoria: categoria ?? this.categoria,
        precio: precio ?? this.precio,
        costo: costo ?? this.costo,
        imprimeComanda: imprimeComanda ?? this.imprimeComanda,
        bloqueo: bloqueo ?? this.bloqueo,
        esServicio: esServicio ?? this.esServicio,
        porTiempo: porTiempo ?? this.porTiempo,
        pidePeso: pidePeso ?? this.pidePeso,
        impuestoConsumo: impuestoConsumo ?? this.impuestoConsumo,
        precioIvaVenta: precioIvaVenta ?? this.precioIvaVenta,
        precioIvaCompra: precioIvaCompra ?? this.precioIvaCompra,
        tipoIvaCompra: tipoIvaCompra ?? this.tipoIvaCompra,
        tipoIvaVenta: tipoIvaVenta ?? this.tipoIvaVenta,
        porcentajeIvaVenta: porcentajeIvaVenta ?? this.porcentajeIvaVenta,
        porcentajeIvaCompra: porcentajeIvaCompra ?? this.porcentajeIvaCompra,
        porcentajeIpoVenta: porcentajeIpoVenta ?? this.porcentajeIpoVenta,
        precioIpoVenta: precioIpoVenta ?? this.precioIpoVenta,
      );

  @override
  List<Object> get props => [
        codigoProducto,
        nombreProducto,
        categoria,
        precio,
        costo,
        imprimeComanda,
        bloqueo,
        esServicio,
        porTiempo,
        pidePeso,
        impuestoConsumo,
        precioIvaVenta,
        precioIvaCompra,
        tipoIvaCompra,
        tipoIvaVenta,
        porcentajeIvaVenta,
        porcentajeIvaCompra,
        porcentajeIpoVenta,
        precioIpoVenta
      ];
}

class CrearProductoLoadingProgress extends NuevoProductoState {}

class GuardadoExitosoState extends NuevoProductoState {
  final int categoriaSeleccionada;

  GuardadoExitosoState({this.categoriaSeleccionada});
}

class ErrorGuardadoState extends NuevoProductoState {}
