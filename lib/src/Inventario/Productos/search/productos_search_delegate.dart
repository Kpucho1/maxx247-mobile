import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/editar_producto_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/widgets/image_list_tile.dart';

class ProductosSearch extends SearchDelegate {
  @override
  String get searchFieldLabel => 'Buscar productos...';

  @override
  TextStyle get searchFieldStyle => TextStyle(
        color: Colors.grey,
      );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: Colors.red,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return Container();
    }

    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");

    return FutureBuilder(
      future: DBProvider.db.getProductosPorNombre(query),
      builder: (context, AsyncSnapshot<List<ProductosCuentaModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          final productos = snapshot.data;
          return ListView(
            children: productos.map((prod) {
              return Card(
                child: ListTile(
                      trailing: Text(prod.precio != null
                        ? '\$ ${formatCurrency.format(prod.precio)}'
                        : '\$ 0'),
                      title: Text(prod.nombre),
                      leading: ImageListTile(
                        imageString: prod.foto,
                        primerLetra: prod.nombre[0],
                      ),
                      onTap: () async{
                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditarProductoPage(
                              productoEditar: prod,
                            ),
                          ),
                        );
                      },
                  ),
              );
            }).toList(),
          );
        }
      },
    );
  }

  
}
