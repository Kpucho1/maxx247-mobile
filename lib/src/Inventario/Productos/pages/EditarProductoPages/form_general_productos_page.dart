import 'dart:io';

import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/widgets/boton_subir_foto.dart';
import 'package:path_provider/path_provider.dart';


class FormGeneralProductosPage extends StatefulWidget {
  final ProductosCuentaModel productoEditar;

  FormGeneralProductosPage({Key key, this.productoEditar}) : super(key: key);

  @override
  _FormGeneralProductosPageState createState() =>
      _FormGeneralProductosPageState();
}

class _FormGeneralProductosPageState extends State<FormGeneralProductosPage> {
  TextEditingController _codigoProductoController = TextEditingController();
  TextEditingController _nombreProductoController = TextEditingController();
  TextEditingController _precioProductoController = TextEditingController();
  TextEditingController _costoProductoController = TextEditingController();
  TextEditingController _presentacionProductoController = TextEditingController();

  bool _imprimeComanda = false;
  bool _bloqueo = false;
  bool _esServicio = false;
  bool _porTiempo = false;
  bool _pidePeso = false;

  int _categoriaSeleccionada;
  int _centroCostoSeleccionado;

  final _botonSubirFoto = BotonSubirFoto();



  File imagenSeleccionada;

  ImagePicker imagenPicker = ImagePicker();

  void dispose() {
    _codigoProductoController?.dispose();
    _nombreProductoController?.dispose();
    _precioProductoController?.dispose();
    _costoProductoController?.dispose();
    _presentacionProductoController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _cargarImagen();
    _codigoProductoController.text = widget.productoEditar.codigo;
    _nombreProductoController.text = widget.productoEditar.nombre;
    _categoriaSeleccionada = widget.productoEditar.grupo1id;
    _centroCostoSeleccionado = widget.productoEditar.centrocostoid;
    _imprimeComanda = widget.productoEditar.productoImprimeComanda == 1 ? true : false;
    _precioProductoController.text = widget.productoEditar.precio.toString();
    _pidePeso = widget.productoEditar.pidePeso == 1 ? true : false;
    _porTiempo = widget.productoEditar.porTiempo == 1 ? true : false;
    _bloqueo = widget.productoEditar.estado == 1 ? true : false;
    _esServicio = widget.productoEditar.esServicio == 1 ? true : false;
  }

  

  Future _cargarImagen() async{
    final directory = await getApplicationDocumentsDirectory();

    _botonSubirFoto.imagenSeleccionada = File(directory.path+'/img.jpg');

    _botonSubirFoto.imagenSeleccionada.writeAsBytesSync(hex.decode(widget.productoEditar.foto));

    _botonSubirFoto.image = PickedFile(directory.path+'/img.jpg');
  }

  @override
  Widget build(BuildContext context) {
    return _formularioCrearProducto();
  }

  Widget _formularioCrearProducto() {
    final blocCategorias = ProviderInherited.categoriasBloc(context);

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _botonActualizarProducto(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _codigoProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _categoriaProductoInput(blocCategorias),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _precioProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _costoProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _seccionSwitches(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              // _botonAgregarFoto()
              _botonSubirFoto
            ],
          ),
        ),
      ),
    );
  }

  Widget _botonActualizarProducto(){
    return TextButton(
      child: Text("Actualizar"),
      onPressed: () async{
        ProductosPostgresService productosPostgresService = ProductosPostgresService();

        final blocProductos = ProviderInherited.productosBloc(context);

        List<int> imagenHex = _botonSubirFoto.imagenSeleccionada != null
              ? _botonSubirFoto.imagenSeleccionada.readAsBytesSync()
              : [];


        ProductosCuentaModel producto = ProductosCuentaModel(
            productoid: widget.productoEditar.productoid,
            codigo: _codigoProductoController.text,
            nombre: _nombreProductoController.text,
            grupo1id: _categoriaSeleccionada,
            precio: double.tryParse(_precioProductoController.text),
            costo: double.tryParse(_costoProductoController.text),
            productoImprimeComanda: _imprimeComanda ? 1 : 0,
            centrocostoid: _centroCostoSeleccionado,
            foto: hex.encode(imagenHex),
            pidePeso: _pidePeso ? 1 : 0,
            esServicio: _esServicio ? 1 : 0,
            porTiempo: _porTiempo ? 1 : 0,
            estado: _bloqueo ? 1 : 0,            


        );

        await productosPostgresService.actualizarProducto(producto, blocProductos);

        Navigator.pop(context, _categoriaSeleccionada);

      }
    );
  }

  Widget _dropdownCentroCosto(){
    ProductosPostgresService productosPostgresService = ProductosPostgresService();
    return FutureBuilder(
      future: productosPostgresService.obtenerCentrosCosto(),
      builder:(context, AsyncSnapshot<List<CentroCostoModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else{
          List<CentroCostoModel> _listadoCentroCosto = snapshot.data;

          _listadoCentroCosto.insert(0, CentroCostoModel(centrocostoid: 0, nombre: "Ninguno"));

          return DropdownButton(
            onChanged: (value){
              setState(() {
                _centroCostoSeleccionado = value;
              });
            },
            value: _centroCostoSeleccionado,
            hint: Text("Centro costo"),
            items: _listadoCentroCosto.map((centro) {
              return DropdownMenuItem(
                child: Text(centro.nombre),
                value: centro.centrocostoid,
              );
            }).toList()
          );
        }
      },
    );
  }

  Widget _codigoProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código del producto*',
      ),
    );
  }

  Widget _nombreProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre del producto*',
      ),
    );
  }

  Widget _categoriaProductoInput(CategoriasBloc bloc) {
    return StreamBuilder(
      stream: bloc.categoriasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
              child: LinearProgressIndicator());

        final List<CategoriasModel> listaCategorias = snapshot.data;

        return DropdownButton(
          value: _categoriaSeleccionada,
          onChanged: (value)async{
            setState((){
                  _categoriaSeleccionada = value;
                });
          },
          hint: Text("Categoria"),
          items: listaCategorias.map((categoria) {
            return DropdownMenuItem(
              child: Text(categoria.descripcion),
              value: categoria.grupoid,
            );
          }).toList(),
        );
      },
    );;
  }

  Widget _precioProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _precioProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.monetization_on),
        labelText: 'Precio',
      ),
    );
  }

  Widget _seccionSwitches(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _switchImprimeComanda(),
        _imprimeComanda ? _dropdownCentroCosto() : SizedBox(),        
        _switchBloqueo(),
        _switchEsServicio(),
        _switchPorTiempo(),
        _switchPidePeso()
      ],
    );
  }

  Widget _switchBloqueo() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _bloqueo = value;
            });
          },
          value: _bloqueo,
          
        ),
        Text("Bloquear producto")
      ]
    );
  }

  Widget _switchImprimeComanda() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _imprimeComanda = value;
            });
          },
          value: _imprimeComanda,
          
        ),
        Text("Imprime comanda")
      ]
    );
  }

  Widget _switchEsServicio() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _esServicio = value;
            });
          },
          value: _esServicio,
          
        ),
        Text("Es servicio"),
      ]
    );
  }

  Widget _switchPorTiempo() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _porTiempo = value;
            });
          },
          value: _porTiempo,
          
        ),
        Text("Por tiempo"),
      ]
    );
  }

  Widget _switchPidePeso() {
    return Row(
      children: [
        Switch(
          onChanged: (value) {
            setState(() {
              _pidePeso = value;
            });
          },
          value: _pidePeso,
          
        ),
        Text("Pide peso"),
      ]
    );
  }

  

  Widget _costoProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _costoProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.monetization_on_outlined),
        labelText: 'Costo',
      ),
    );
  }
}
