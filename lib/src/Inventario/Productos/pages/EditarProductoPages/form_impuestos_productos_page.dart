import 'package:flutter/material.dart';

class FormImpuestosProductosPage extends StatefulWidget {
  FormImpuestosProductosPage({Key key}) : super(key: key);

  @override
  _FormImpuestosProductosPageState createState() => _FormImpuestosProductosPageState();
}

class _FormImpuestosProductosPageState extends State<FormImpuestosProductosPage> {
  TextEditingController _porcentajeIvaCompraController = TextEditingController();
  TextEditingController _porcentajeIvaVentaController = TextEditingController();
  TextEditingController _precioIvaVenta = TextEditingController();
  TextEditingController _precioIvaCompra = TextEditingController();
  TextEditingController _porcentajeIpoVenta = TextEditingController();
  TextEditingController _precioIpoVenta = TextEditingController();

  int _tipoIvaVenta = 0;
  int _tipoIvaCompra = 0;

  @override
  Widget build(BuildContext context) {
    return _formularioImpuestosProducto();
  }

   Widget _formularioImpuestosProducto() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _porcentajeIvaCompraInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _precioIvaCompraInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _porcentajeIvaVentaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _precioIvaVentaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _porcentajeIpoVentaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _precioIpoVentaInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              
            ],
          ),
        ),
      ),
    );
  }

  Widget _porcentajeIvaCompraInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _porcentajeIvaCompraController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Porcentaje iva compra',
      ),
    );
  }

  Widget _precioIvaCompraInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _precioIvaCompra,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Precio iva compra',
      ),
    );
  }

  Widget _porcentajeIvaVentaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _porcentajeIvaVentaController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Porcentaje iva venta',
      ),
    );
  }

  Widget _precioIvaVentaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _precioIvaVenta,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Precio iva venta',
      ),
    );
  }

  Widget _porcentajeIpoVentaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _porcentajeIpoVenta,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Porcentaje ipo venta',
      ),
    );
  }

  Widget _precioIpoVentaInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _precioIpoVenta,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Precio ipo venta',
      ),
    );
  }

  
}