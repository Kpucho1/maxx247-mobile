import 'package:flutter/material.dart';

class FormPresentacionProductosPage extends StatefulWidget {
  FormPresentacionProductosPage({Key key}) : super(key: key);

  @override
  _FormPresentacionProductosPageState createState() => _FormPresentacionProductosPageState();
}

class _FormPresentacionProductosPageState extends State<FormPresentacionProductosPage> {
  TextEditingController _codigoProductoController = TextEditingController();
  TextEditingController _nombreProductoController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return _formularioImpuestosProducto();
  }

   Widget _formularioImpuestosProducto() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _codigoProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _nombreProductoInput(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código del producto*',
      ),
    );
  }

  Widget _nombreProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre del producto*',
      ),
    );
  }
}