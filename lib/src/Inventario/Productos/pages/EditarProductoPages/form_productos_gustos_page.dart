import 'package:flutter/material.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';

class FormProductosGustosPage extends StatefulWidget {
  final ProductosCuentaModel productoEditar;
  FormProductosGustosPage({Key key, this.productoEditar}) : super(key: key);

  @override
  State<FormProductosGustosPage> createState() => _FormProductosGustosPageState();
}

class _FormProductosGustosPageState extends State<FormProductosGustosPage> {
  TextEditingController _gustoController = TextEditingController();
  ProductosPostgresService productosPostgresService = ProductosPostgresService();


  @override
  void dispose() {
    _gustoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async{
          await _createGustoDialog().then((value) {
            setState(() {
              
            });
          });
        }
      ),
      body: Scrollbar(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FutureBuilder(
            future: DBProvider.db.obtenerProductosGustosPorId(widget.productoEditar.productoid),
            builder: (context, snapshot){
              if (!snapshot.hasData)
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: CircularProgressIndicator(),
                    ),
                  );
              final List<ProductosGustosModel> listaProductosGustos = snapshot.data;

              if (listaProductosGustos.length > 0) {
                return ListView.separated(
                  itemCount: listaProductosGustos.length,
                  separatorBuilder: (context, index) => Divider(),
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index){
                    return Card(
                      child: ListTile(
                        title: Text(listaProductosGustos[index].nombre),
                        trailing: IconButton(
                          icon: Icon(Icons.delete, color: Colors.red,),
                          onPressed: () async{
                            await productosPostgresService.eliminarProductoGusto(listaProductosGustos[index].gustoid);
                            setState(() {
                              
                            });
                          },
                        ),
                      ),
                    );
                  },
                );
              }
              else{
                return Center(child: Text("Producto sin gustos"),);
              }
            },
          ),
        ),
      )
    );
  }

  Future<Widget> _createGustoDialog(){
    return showDialog(
      useSafeArea: true,
      context: context,
      builder: (context) {
        return AlertDialog(
          content: TextFormField(
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            controller: _gustoController,
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
              prefixIcon: Icon(Icons.badge),
              labelText: 'Gusto*',
            ),
          ),
          actions: [
            TextButton(
              child: Text("Cancelar"),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("Crear"),
              onPressed: () async{

                ProductosGustosModel productosGustosModel = ProductosGustosModel(
                  nombre: _gustoController.text,
                  productoid: widget.productoEditar.productoid
                );

                await productosPostgresService.crearProductoGusto(productosGustosModel);

                Navigator.pop(context);
              },
            ),
            
          ],
        );
      },  
    );
  }
}