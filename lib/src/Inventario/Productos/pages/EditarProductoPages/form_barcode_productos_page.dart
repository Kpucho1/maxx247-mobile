import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_svg/svg.dart';
import 'package:barcode/barcode.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';


class FormBarcodeProductosPage extends StatefulWidget {
  final ProductosCuentaModel productoEditar;

  FormBarcodeProductosPage({Key key, this.productoEditar}) : super(key: key);
  

  @override
  _FormBarcodeProductosPageState createState() => _FormBarcodeProductosPageState();
}

class _FormBarcodeProductosPageState extends State<FormBarcodeProductosPage> {
  TextEditingController _codigoBarras1Controller = TextEditingController();
  TextEditingController _codigoBarras2Controller = TextEditingController();

  
  @override
  Widget build(BuildContext context) {
    return _formularioImpuestosProducto();
  }

  @override
  void dispose() {
    _codigoBarras1Controller?.dispose();
    _codigoBarras2Controller?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _codigoBarras1Controller.text = widget.productoEditar.codigobarras1 ?? '';
    _codigoBarras2Controller.text = widget.productoEditar.codigobarras2 ?? '';

  }

  String buildBarcode(
    Barcode bc,
    String data, {
    String filename,
    double width,
    double height,
    double fontHeight,
  }) {
    /// Create the Barcode
    final svg = bc.toSvg(
      data,
      width: width ?? 200,
      height: height ?? 80,
      fontHeight: fontHeight,
    );

    return svg;
  }

   Widget _formularioImpuestosProducto() {
    ProductosPostgresService productosPostgresService = ProductosPostgresService();

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
               _codigoBarras1Input(),
              TextButton(
                child: Text("Escanear"),
                onPressed: () async {

                  String barcode = await FlutterBarcodeScanner.scanBarcode(
                      "#2B061E", "Cancelar", true, ScanMode.DEFAULT);

                  await productosPostgresService.actualizarCodigoBarrasProducto("codigobarras1", widget.productoEditar.productoid, barcode);


                  setState(() {
                    widget.productoEditar.codigobarras1 = barcode;
                    _codigoBarras1Controller.text = barcode;
                  });

                  final snackBar = SnackBar(
                    content: Text("Guardado con éxito", style: TextStyle(fontSize: 16.0)),
                    backgroundColor: Colors.green,
                  );

                  ScaffoldMessenger.of(context).showSnackBar(snackBar);

                },
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _codigoBarras1(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),

               _codigoBarras2Input(),
              TextButton(
                child: Text("Escanear"),
                onPressed: () async {

                  String barcode = await FlutterBarcodeScanner.scanBarcode(
                      "#2B061E", "Cancelar", true, ScanMode.DEFAULT);
                  

                  await productosPostgresService.actualizarCodigoBarrasProducto("codigobarras2", widget.productoEditar.productoid, barcode);

                  setState(() {
                    widget.productoEditar.codigobarras2 = barcode;
                    _codigoBarras2Controller.text = barcode;
                  });

                  final snackBar = SnackBar(
                    content: Text("Guardado con éxito", style: TextStyle(fontSize: 16.0)),
                    backgroundColor: Colors.green,
                  );

                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              _codigoBarras2(),

              
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoBarras1(){
    String svg = buildBarcode(Barcode.code39(), widget.productoEditar.codigobarras1 ?? '');
    return Container(
      child: SafeArea(
        child: Center(
          child: SvgPicture.string(svg, width: 80, height: 80)
        ),
      ),
    );
  }

  Widget _codigoBarras2(){
    String svg = buildBarcode(Barcode.code39(), widget.productoEditar.codigobarras2 ?? '');
    return Container(
      child: SafeArea(
        child: Center(
          child: SvgPicture.string(svg, width: 80, height: 80)
        ),
      ),
    );
  }

  Widget _codigoBarras1Input() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoBarras1Controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código de barras 1 del producto*',
      ),
    );
  }

  Widget _codigoBarras2Input() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoBarras2Controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código de barras 2 del producto*',
      ),
    );
  }

}