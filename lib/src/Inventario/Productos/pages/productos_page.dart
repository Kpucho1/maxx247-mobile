import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'package:maxxshop/src/Inventario/Productos/pages/crear_producto_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/editar_producto_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Inventario/Productos/search/productos_search_delegate.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:maxxshop/src/provider.dart';
import 'package:maxxshop/src/widgets/image_list_tile.dart';

class ProductosPage extends StatefulWidget {
  ProductosPage({Key key}) : super(key: key);

  @override
  _ProductosPageState createState() => _ProductosPageState();
}

class _ProductosPageState extends State<ProductosPage> {
  int _categoriaSeleccionada;
  final prefs = PreferenciasUsuario();
  List<ProductosCuentaModel> listadoProductos = [];

  @override
  void initState() {
    super.initState();

    if (_categoriaSeleccionada == null) {
      if (prefs.categoriaSeleccionada != null) {
        setState(() {
          _categoriaSeleccionada = prefs.categoriaSeleccionada ?? 1;
        });
      }
    }
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final blocProductos = ProviderInherited.productosBloc(context);
    blocProductos.obtenerProductos(_categoriaSeleccionada);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = ProviderInherited.productosBloc(context);
    final blocCategorias = ProviderInherited.categoriasBloc(context);


    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        title: _buildTitle(context, blocCategorias, bloc),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: ProductosSearch(),
              );
            },
          )
        ],
      ),
      body: _listaProductosPorCategoria(bloc),
      floatingActionButton: _botonFlotante(),
    );
  }

  Widget _botonFlotante() {
    return FloatingActionButton(
      onPressed: () {
        //
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CrearProductoPage(),
          ),
        ).then((value){
          final blocProductos = ProviderInherited.productosBloc(context);

          blocProductos.obtenerProductos(value);

          setState(() {
            _categoriaSeleccionada = value;
          });
        });
        
      },
      child: Icon(
        Icons.add,
        size: 27.0,
      ),
    );
  }

  Widget _buildTitle(BuildContext context, CategoriasBloc bloc, ProductosBloc blocProductos) {
    return StreamBuilder(
      stream: bloc.categoriasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
              child: LinearProgressIndicator());

        final List<CategoriasModel> listaCategorias = snapshot.data;

        _categoriaSeleccionada = listaCategorias.first.grupoid;

        return DropdownButton(
          value: _categoriaSeleccionada,
          onChanged: (value)async{
            await blocProductos.obtenerProductos(value);
            setState((){
                  _categoriaSeleccionada = value;
                  prefs.categoriaSeleccionada = value;
                });
          },
          items: listaCategorias.map((categoria) {
            return DropdownMenuItem(
              child: Text(categoria.descripcion),
              value: categoria.grupoid,
            );
          }).toList(),
        );
      },
    );
  }

  Widget _listaProductosPorCategoria(ProductosBloc bloc) {
    return Scrollbar(
      child: StreamBuilder(
        stream: bloc.productosStream,
        builder: (context, snapshot){
          if (!snapshot.hasData)
              return Center(
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: LinearProgressIndicator(),
                ),
              );
    
            final List<ProductosCuentaModel> listaProductos = snapshot.data;
          return ListView.builder(
              itemCount: listaProductos.length,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                return productos(context, listaProductos, index);
              },
            );
        },
      ),
    );
  }

  Widget productos(BuildContext context, List<ProductosCuentaModel> listaProductos, int index){
    final formatCurrency = NumberFormat.currency(decimalDigits: 0, symbol: "");
    String nombreProducto = listaProductos[index].nombre;
    
    return Card(
      child: ListTile(
        leading: ImageListTile(
          imageString: listaProductos[index].foto,
          primerLetra: nombreProducto[0],
        ), //Avatar del producto
        title: Text(nombreProducto), //Nombre del producto
        trailing: Text(listaProductos[index].precio != null
            ? '\$ ${formatCurrency.format(listaProductos[index].precio)}'
            : '\$ 0'), //Precio del producto
        onTap: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EditarProductoPage(
                productoEditar: listaProductos[index],
              ),
            ),
          ).then((value) {
            final blocProductos = ProviderInherited.productosBloc(context);

            blocProductos.obtenerProductos(value);

            setState(() {
              _categoriaSeleccionada = value;
            });
          });
        },
      ),
    );
  }
}
