import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:maxxshop/src/Inventario/Categorias2/pages/categorias_page.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/pages/centros_costo_page.dart';
import 'package:maxxshop/src/Inventario/InventarioFisico/pages/home_inventario_fisico_page.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';

import 'package:maxxshop/src/widgets/botones_redondeados.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/productos_page.dart';
import 'package:maxxshop/src/Inventario/Categorias/pages/categorias_page.dart';
// import 'package:maxxshop/src/Inventario/Categorias/bloc/seleccionar_categoria_bloc/seleccionar_categoria_bloc.dart';

class MenuProductosPage extends StatefulWidget {
  @override
  _MenuProductosPageState createState() => _MenuProductosPageState();
}

class _MenuProductosPageState extends State<MenuProductosPage> {
  //
  final prefs = PreferenciasUsuario();

  @override
  void initState() {
    super.initState();
    prefs.categoriaSeleccionada = null;

    // Deprecated
    // context.read<CategoriasBloc>().add(OnObtenerCategorias());
    // context.read<Categorias2Bloc>().add(OnObtenerCategorias2());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Productos"),
        centerTitle: true,
      ),
      body: Container(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          _imagenDecoracion(),
          _botonesRedondeados(),
        ]),
      ),
    );
  }

  Widget _imagenDecoracion() {
    return Container(
      child: SafeArea(
        child: Center(
          child: SvgPicture.asset(
            'assets/products.svg',
            height: 100.0,
          ),
        ),
      ),
    );
  }

  Widget _botonesRedondeados() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Table(
        children: [
          TableRow(
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.inventory,
                  texto: 'Productos',
                ),
                onTap: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProductosPage(),
                      ));
                },
              ),
              InkWell(
                onTap: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CategoriasPage(),
                      ));
                },
                borderRadius: BorderRadius.circular(20.0),
                splashColor: Color.fromRGBO(178, 203, 210, 1.0),
                highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
                child: BotonRedondeado(
                  color: Colors.grey[700],
                  icono: Icons.list,
                  texto: 'Categorías',
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Categorias2Page(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.list,
                texto: 'Categorías 2',
              ),
            ),
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePageInventarioFisico(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.south,
                texto: 'Inventario fisico',
              ),
            ),
          ]),
          TableRow(
            children: [
              SizedBox(
                height: 10.0,
              ),
              SizedBox(height: 10.0)
            ],
          ),
          TableRow(children: [
            InkWell(
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CentrosCostoPage(),
                    ));
              },
              borderRadius: BorderRadius.circular(20.0),
              splashColor: Color.fromRGBO(178, 203, 210, 1.0),
              highlightColor: Color.fromRGBO(178, 203, 210, 0.7),
              child: BotonRedondeado(
                color: Colors.grey[700],
                icono: Icons.list,
                texto: 'Centros de produccion',
              ),
            ),
            SizedBox()
          ]),
        ],
      ),
    );
  }
}
