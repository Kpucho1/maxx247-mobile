import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:maxxshop/src/Home/pages/home_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/menu_productos_page.dart';
import 'package:maxxshop/src/Utils/postgres_functions.dart';
import 'package:maxxshop/src/provider.dart';

class ProductosLoadingPage extends StatefulWidget {
  ProductosLoadingPage({Key key}) : super(key: key);

  @override
  _ProductosLoadingPageState createState() => _ProductosLoadingPageState();
}

class _ProductosLoadingPageState extends State<ProductosLoadingPage> {

  @override
  void didChangeDependencies() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _cargarInfoNegocio(context);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: Image(
          image: AssetImage('assets/jar_loading.gif'),
        ),
      ),
    );
  }

  Future<void> _cargarInfoNegocio(BuildContext context) async {

    try {  

      final blocCategorias = ProviderInherited.categoriasBloc(context);
      final blocProductos = ProviderInherited.productosBloc(context);
      final blocProductosGustos = ProviderInherited.productosGustosBloc(context);

      final postFunctions = new PostgresFunctions();

      await postFunctions.cargarProductosYCategorias(blocCategorias, blocProductos, blocProductosGustos);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MenuProductosPage(),
        ),
      ).then((value) => Navigator.pop(context));
    } catch (e) {
      print(e);
      //print('de aca viene el error');
      _alertaSinConexion(context);
    }
  }

  Future _alertaSinConexion(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Icon(
          Icons.warning,
          color: Colors.red,
          size: 50.0,
        ),
        content: Text(
          'Error al intentar conectar. Por favor intenta de nuevo.',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(),
                ),
                (route) => false),
            child: Text(
              'Volver',
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );
  }
}