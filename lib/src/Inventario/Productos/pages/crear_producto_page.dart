import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxxshop/src/Inventario/Categorias2/bloc/categorias_bloc/categorias_bloc.dart';
import 'package:maxxshop/src/Inventario/CentrosCosto/models/centrocosto_model.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/editar_producto_page.dart';
import 'package:maxxshop/src/Inventario/Productos/services/productos_postgres_services.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/categorias_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/provider.dart';

import 'package:maxxshop/src/widgets/boton_subir_foto.dart';
class CrearProductoPage extends StatefulWidget {
  CrearProductoPage({Key key}) : super(key: key);

  @override
  _CrearProductoPageState createState() => _CrearProductoPageState();
}

class _CrearProductoPageState extends State<CrearProductoPage> {
  TextEditingController _codigoProductoController = TextEditingController();
  TextEditingController _nombreProductoController = TextEditingController();
  TextEditingController _precioProductoController = TextEditingController();
  TextEditingController _costoProductoController = TextEditingController();
  TextEditingController _presentacionProductoController =
      TextEditingController();

  bool _imprimeComanda = false;
  bool _bloqueo = false;
  bool _esServicio = false;
  bool _porTiempo = false;
  bool _pidePeso = false;

  int _categoriaSeleccionada;
  int _categoria2Seleccionada;
  int _centroCostoSeleccionado;

  final _botonSubirFoto = BotonSubirFoto();

  void dispose() {
    _codigoProductoController?.dispose();
    _nombreProductoController?.dispose();
    _precioProductoController?.dispose();
    _costoProductoController?.dispose();
    _presentacionProductoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          elevation: 0.5,
          backgroundColor: Colors.grey[200],
          leading: BackButton(),
          automaticallyImplyLeading: false,
          title: Text(
            "Crear producto",
            style: TextStyle(color: Colors.black),
          ),
          actions: _buildActions(),
        ),
        body: _bodyCrearProducto(),
        //bottomNavigationBar: _bottomNavBarMantenimientoProducto(),
      ),
    );
  }

  Widget _bodyCrearProducto() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(child: _formularioCrearProducto()),
      ],
    );
  }

  // Widget _bottomNavBarMantenimientoProducto() {
  //   return Theme(
  //     data: Theme.of(context).copyWith(
  //       primaryColor: Color(0xffc51f5d),
  //       textTheme: Theme.of(context).textTheme.copyWith(
  //             caption: TextStyle(
  //               color: Color.fromRGBO(36, 52, 71, 1.0),
  //             ),
  //           ),
  //     ),
  //     child: BottomNavigationBar(
  //       type: BottomNavigationBarType.fixed,
  //       currentIndex: _currentIndex,
  //       onTap: (value) {
  //         setState(() {
  //           _currentIndex = value;
  //         });
  //       },
  //       items: [
  //         BottomNavigationBarItem(
  //             icon: Icon(Icons.assignment), label: 'General'),
  //         BottomNavigationBarItem(
  //             icon: Icon(Icons.attach_money_rounded), label: 'Impuestos'),
  //         BottomNavigationBarItem(
  //             icon: Icon(Icons.account_tree), label: 'Formulación'),
  //         BottomNavigationBarItem(
  //             icon: Icon(Icons.free_breakfast), label: 'Presentación'),
  //         BottomNavigationBarItem(
  //             icon: Icon(Icons.qr_code_scanner_sharp), label: 'Codigos Barra'),
  //       ],
  //     ),
  //   );
  // }

  List<Widget> _buildActions() {
    return <Widget>[
      TextButton(
        child: Text(
          "Guardar",
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
        onPressed: () async {
          List<int> imagenHex = _botonSubirFoto.imagenSeleccionada != null
              ? _botonSubirFoto.imagenSeleccionada.readAsBytesSync()
              : [];
          final blocProductos = ProviderInherited.productosBloc(context);


          ProductosCuentaModel nuevoProducto = ProductosCuentaModel(
              codigo: _codigoProductoController.text,
              nombre: _nombreProductoController.text,
              grupo1id: _categoriaSeleccionada,
              precio: double.tryParse(_precioProductoController.text),
              productoImprimeComanda: _imprimeComanda ? 1 : 0,
              foto: hex.encode(imagenHex),
              esServicio: _esServicio ? 1 : 0,
              estado: _bloqueo ? 1 : 0,
              centrocostoid: _centroCostoSeleccionado,
              costo: double.tryParse(_costoProductoController.text));

          // context.read<CrearProductoBloc>().add(
          //       OnCrearProducto(
          //         nuevoProducto: nuevoProducto,
          //         categoriaSeleccionada: _categoriaSeleccionada,
          //       ),
          //     );

          ProductosPostgresService productosPostgresService = ProductosPostgresService();

          await productosPostgresService.crearNuevoProducto(nuevoProducto, blocProductos);

          Navigator.pop(context, _categoriaSeleccionada);
        },
      )
    ];
  }

  Widget _formularioCrearProducto() {
    final blocCategorias = ProviderInherited.categoriasBloc(context);

    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Card(
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      _codigoProductoInput(),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      _nombreProductoInput(),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      _categoriaProductoInput(blocCategorias),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      // _categoria2ProductoInput(),
                      // SizedBox(
                      //   height: MediaQuery.of(context).size.height * 0.02,
                      // ),
                      _precioProductoInput(),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      _costoProductoInput(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              _seccionSwitches(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Card(
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _botonSubirFoto,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _codigoProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _codigoProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.insert_drive_file),
        labelText: 'Código del producto*',
      ),
      onChanged: (value) async{
        ProductosCuentaModel producto =  await DBProvider.db.getProductosPorCodigo(value);
        if (producto != null) {
          Navigator.push(context, MaterialPageRoute(builder: (context) => EditarProductoPage(productoEditar: producto,))).then((value) => Navigator.pop(context));
        }
      },
    );
  }

  Widget _nombreProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.text,
      textInputAction: TextInputAction.next,
      controller: _nombreProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.badge),
        labelText: 'Nombre del producto*',
      ),
    );
  }

  Widget _dropdownCentroCosto(){
    ProductosPostgresService productosPostgresService = ProductosPostgresService();
    return FutureBuilder(
      future: productosPostgresService.obtenerCentrosCosto(),
      builder:(context, AsyncSnapshot<List<CentroCostoModel>> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else{
          List<CentroCostoModel> _listadoCentroCosto = snapshot.data;

          _listadoCentroCosto.insert(0, CentroCostoModel(centrocostoid: 0, nombre: "Ninguno"));

          return DropdownButton(
            onChanged: (value){
              setState(() {
                _centroCostoSeleccionado = value;
              });
            },
            value: _centroCostoSeleccionado,
            hint: Text("Centro costo"),
            items: _listadoCentroCosto.map((centro) {
              return DropdownMenuItem(
                child: Text(centro.nombre),
                value: centro.centrocostoid,
              );
            }).toList()
          );
        }
      },
    );
  }

  Widget _categoriaProductoInput(CategoriasBloc bloc) {
    return StreamBuilder(
      stream: bloc.categoriasStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
              child: LinearProgressIndicator());

        final List<CategoriasModel> listaCategorias = snapshot.data;

        return DropdownButton(
          value: _categoriaSeleccionada,
          onChanged: (value)async{
            setState((){
                  _categoriaSeleccionada = value;
                });
          },
          hint: Text("Categoria"),
          items: listaCategorias.map((categoria) {
            return DropdownMenuItem(
              child: Text(categoria.descripcion),
              value: categoria.grupoid,
            );
          }).toList(),
        );
      },
    );
  }

  Widget _categoria2ProductoInput() {
    return BlocBuilder<Categorias2Bloc, Categorias2State>(
      builder: (context, state) {
        Widget _widget;
        //
        if (state is InitialCategorias2State) {
          //
          _widget = Center(child: CircularProgressIndicator());
          //
        } else if (state is ObtenerCategorias2State) {
          final _listaCategorias = state.listadoCategorias;

          _widget = DropdownButtonFormField(
            value: _categoria2Seleccionada,
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
              prefixIcon: Icon(Icons.list),
              labelText: 'Seleccionar categoria 2',
            ),
            items: _listaCategorias.map((categoria) {
              return DropdownMenuItem(
                child: Text(categoria.descripcion),
                value: categoria.grupoid,
              );
            }).toList(),
            onChanged: (value) async {
              setState(() {
                _categoria2Seleccionada = value;
              });
            },
          );
        }

        return _widget;
      },
    );
  }

  Widget _precioProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _precioProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.monetization_on),
        labelText: 'Precio',
      ),
    );
  }

  Widget _seccionSwitches() {
    return Card(
      elevation: 0.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _switchImprimeComanda(),
            _imprimeComanda ? _dropdownCentroCosto() : SizedBox(),
            _switchBloqueo(),
            _switchEsServicio(),
            _switchPorTiempo(),
            _switchPidePeso()
          ],
        ),
      ),
    );
  }

  Widget _switchBloqueo() {
    return Row(children: [
      Switch(
        onChanged: (value) {
          setState(() {
            _bloqueo = value;
          });
        },
        value: _bloqueo,
      ),
      Text("Bloquear producto")
    ]);
  }

  Widget _switchImprimeComanda() {
    return Row(children: [
      Switch(
        onChanged: (value) {
          setState(() {
            _imprimeComanda = value;
          });
        },
        value: _imprimeComanda,
      ),
      Text("Imprime comanda")
    ]);
  }

  Widget _switchEsServicio() {
    return Row(children: [
      Switch(
        onChanged: (value) {
          setState(() {
            _esServicio = value;
          });
        },
        value: _esServicio,
      ),
      Text("Es servicio"),
    ]);
  }

  Widget _switchPorTiempo() {
    return Row(children: [
      Switch(
        onChanged: (value) {
          setState(() {
            _porTiempo = value;
          });
        },
        value: _porTiempo,
      ),
      Text("Por tiempo"),
    ]);
  }

  Widget _switchPidePeso() {
    return Row(children: [
      Switch(
        onChanged: (value) {
          setState(() {
            _pidePeso = value;
          });
        },
        value: _pidePeso,
      ),
      Text("Pide peso"),
    ]);
  }

  Widget _costoProductoInput() {
    return TextFormField(
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      controller: _costoProductoController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
        prefixIcon: Icon(Icons.monetization_on_outlined),
        labelText: 'Costo',
      ),
    );
  }
}
