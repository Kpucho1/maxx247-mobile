import 'package:flutter/material.dart';

import 'package:maxxshop/src/Inventario/Productos/pages/EditarProductoPages/form_barcode_productos_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/EditarProductoPages/form_general_productos_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/EditarProductoPages/form_impuestos_productos_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/EditarProductoPages/form_presentacion_productos_page.dart';
import 'package:maxxshop/src/Inventario/Productos/pages/EditarProductoPages/form_productos_gustos_page.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';

class EditarProductoPage extends StatefulWidget {
  final ProductosCuentaModel productoEditar;

  EditarProductoPage({Key key, this.productoEditar}) : super(key: key);

  @override
  _EditarProductoPageState createState() => _EditarProductoPageState();
}

class _EditarProductoPageState extends State<EditarProductoPage> {
  
  int _indexMenu = 0;


  Widget _body(){
    switch(_indexMenu){
      case 0:
        return FormGeneralProductosPage(productoEditar: widget.productoEditar);
        break;

      case 1:
        return FormImpuestosProductosPage();

      case 2:
        return FormPresentacionProductosPage();      
      
      case 3:
        return FormBarcodeProductosPage(productoEditar: widget.productoEditar,);

      case 4:
        return FormProductosGustosPage(productoEditar: widget.productoEditar,);

      default: 
        return FormGeneralProductosPage(productoEditar: widget.productoEditar,);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(onPressed: (){
          Navigator.pop(context, widget.productoEditar.grupo1id);
        },),
        automaticallyImplyLeading: false,
        title: Text(
          widget.productoEditar.nombre,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: _body(),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (value){
          setState(() {
            _indexMenu = value;
          });
        },
        type: BottomNavigationBarType.fixed,
        currentIndex: _indexMenu,
        items: [
          BottomNavigationBarItem(
            label: 'General',
            icon: Icon(Icons.home)
          ),
          BottomNavigationBarItem(
            label: 'Impuestos',
            icon: Icon(Icons.money)
          ),
          BottomNavigationBarItem(
            label: 'Presentacion',
            icon: Icon(Icons.present_to_all)
          ),
          BottomNavigationBarItem(
            label: 'Codigo barras',
            icon: Icon(Icons.qr_code_2_outlined)
          ),
          BottomNavigationBarItem(
            label: 'Gustos',
            icon: Icon(Icons.inventory),
          ),
        ],
      ),
    );
  }


}
