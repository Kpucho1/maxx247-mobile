import 'package:postgres/postgres.dart';

class PostgresProvider {
  static PostgreSQLConnection connection;

  static final PostgresProvider conexionPostgres = PostgresProvider._private();

  PostgresProvider._private();

  get abrirConexion {
    // if (_connection != null) return await _connection.open();

    connection = PostgreSQLConnection(
        'maxxcontrol.com.co', 5437, 'maxxcontrol_master',
        username: 'postgres', password: 'desarrollo321.*');

    // await connection.open();
  }

  Future<PostgreSQLConnection> retornaConexion() async {
    await connection.open();
    return connection;
  }

  Future<void> getDatosPorLicencia(String licencia) async {
    // final connection = await conexionPostgres.conexion;

    List<List<dynamic>> results = await connection.query(
        'SELECT ipservidorsql FROM datos.empresas WHERE licencia = @licencia',
        substitutionValues: {'licencia': licencia});

    print(results);
  }
}
