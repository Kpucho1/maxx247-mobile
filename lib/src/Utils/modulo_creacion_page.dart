import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ModuloCreacionPage extends StatelessWidget {
  const ModuloCreacionPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Modulo en creacion"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/sin_conexion.svg',
              alignment: Alignment.center,
              height: 320,
              width: 320,
            ),
            Text(
              "Este modulo se encuentra bajo desarrollo, pronto estará disponible.",
              style: TextStyle(
                fontSize: 26,
              ),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
