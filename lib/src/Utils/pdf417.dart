class Pdf417{

  Map<String, dynamic> process(String responseCode, ) {

    Map<String, dynamic> datosEscaneados = Map();
    
    var alphaAndDigits = responseCode.replaceAll(new RegExp("[^0-9a-zA-Z]+"), " ");

    List<String> splitStr = alphaAndDigits.split(new RegExp("\\s+"));

    for (var i in splitStr) {
      print(i);
    }


    if(alphaAndDigits.contains("PubDSK")){
      RegExp digits = new RegExp("\\d+", caseSensitive: false);
      RegExp alpha = new RegExp("[a-zA-Z]", caseSensitive: false);

      Match cedula = digits.firstMatch(splitStr[4]);
      String primerApellido = "";
      String fechaNacimiento = splitStr[8].substring(2, 10);
      String year = fechaNacimiento.substring(0, 4);
      String month = fechaNacimiento.substring(4, 6);
      String day = fechaNacimiento.substring(6, 8);

      fechaNacimiento = day+"/"+month+"/"+year;

      Iterable<Match> primerApellidoRegex = alpha.allMatches(splitStr[4]);

      for (Match c in primerApellidoRegex) {
        primerApellido += c.group(0);
      }

      var segundoApellido = splitStr[5];

      var primerNombre = splitStr[6];

      datosEscaneados = {
        "cedula": cedula.group(0),
        "nombre": primerNombre+" "+primerApellido+" "+segundoApellido,
        "fechaNacimiento": fechaNacimiento,
      };

      return datosEscaneados;
      
    }
    else{
      print(responseCode);
      datosEscaneados = {
        "cedula": " ",
        "nombre": " ",
        "fechaNacimiento": " ",
      };
      return datosEscaneados;
    }
    
    
    
  }
  

}