import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/esquema_model.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';

class SqliteFunctions {
  final mesasRefreshBloc = new MesasRefreshBloc();
  final esquemasBloc = EsquemasBloc();

  Future<void> processSchemaAndTables(List<List<dynamic>> esquema,
      List<List<dynamic>> mesas, EsquemasBloc esquemasBloc, int cont) async {
    final nuevaMesa = new MesaModel();
    final nuevoEsquema = new EsquemaModel();

    nuevoEsquema.id = cont;
    nuevoEsquema.esquemaid = esquema[0][0];
    nuevoEsquema.nombre = esquema[0][1];
    nuevoEsquema.colorvacialimpiafondo = esquema[0][2];
    nuevoEsquema.colorvacialimpiatexto = esquema[0][3];
    nuevoEsquema.colorocupadasinpedidofondo = esquema[0][4];
    nuevoEsquema.colorocupadasinpedidotexto = esquema[0][5];
    nuevoEsquema.colorocupadaconpedidotexto = esquema[0][6];
    nuevoEsquema.colorocupadaconpedidofondo = esquema[0][7];
    nuevoEsquema.colorsuciafondo = esquema[0][8];
    nuevoEsquema.colorsuciatexto = esquema[0][9];
    nuevoEsquema.colorpendienteporpagarfondo = esquema[0][10];
    nuevoEsquema.colorpendienteporpagartexto = esquema[0][11];
    nuevoEsquema.colorreparacionfondo = esquema[0][12];
    nuevoEsquema.colorreaparaciontexto = esquema[0][13];
    nuevoEsquema.colordisponiblefondo = esquema[0][14];
    nuevoEsquema.colordisponbiletexto = esquema[0][15];
    nuevoEsquema.colorocupadafondo = esquema[0][16];
    nuevoEsquema.colorocupadatexto = esquema[0][17];
    nuevoEsquema.colorantesdevencersefondo = esquema[0][18];
    nuevoEsquema.colorantesdevencersetexto = esquema[0][19];

    esquemasBloc.agregarEsquema(nuevoEsquema);

    if (mesas != []) {
      for (var row in mesas) {
        nuevaMesa.id = row[0];
        nuevaMesa.objetoid = row[0];
        nuevaMesa.nombre = row[1];
        nuevaMesa.estado = row[2];
        nuevaMesa.consumido = row[3];
        nuevaMesa.esquemaid = esquema[0][0];
        nuevaMesa.nombreVendedor = row[5]; 
        nuevaMesa.numeroCuentas = row[6];
        nuevaMesa.vendedorid = row[7];

        await mesasRefreshBloc.agregarMesas(nuevaMesa);
      }
    }

    await mesasRefreshBloc.obtenerTodasMesas();
    await esquemasBloc.obtenerEsquemaActivo(1);
  }
}
