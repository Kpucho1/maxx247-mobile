import 'dart:async';

import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/esquemas_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/Mesas/models/mesa_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/db/conexion_db_timer.dart';
import 'package:maxxshop/src/db/conexion_db_timer_productos.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';

class TimerConsultaBD {
  final mesasRefreshBloc = MesasRefreshBloc();
  final esquemasBloc = EsquemasBloc();

  final productosBloc = ProductosBloc();

  String nombreListaPrecio = "";

  static Timer timer;

  static final TimerConsultaBD timerConsultaBD = TimerConsultaBD._private();

  TimerConsultaBD._private();

  final prefs = PreferenciasUsuario();

  get iniciarTimer {
    timer = Timer.periodic(Duration(seconds: 5), (Timer timer) async {
      await actualizarListadoMesas();
      await revisarListaPrecio();
    });
  }

  get detenerTimer {
    // ignore: null_aware_in_condition
    if (timer?.isActive) {
      timer.cancel();
      ConexionTimerProvider.conexionTimer.cerrarConexion();
    }
  }

  Future<void> actualizarListadoMesas() async {
    //final esquemasBloc = Provider.esquemasBloc(context);
    final conexion = await DBProvider.db.getConexionActiva();
    final puertobd = int.tryParse(conexion.puertobd);
    ConexionTimerProvider.conexionTimer.abrirConexion(
      conexion.ipservidor,
      puertobd,
      conexion.basedatos,
      conexion.usuario,
      conexion.password,
    );

    PostgreSQLConnection conexionTimer =
        await ConexionTimerProvider.conexionTimer.retornaConexion();

    try {
      // String localIp = await Wifi.ip;

      await conexionTimer.transaction((cnt) async {
        List<List<dynamic>> objetoid =
            await cnt.query('SELECT MAX (objetoid) FROM datos.objetos');

        final objetoidSQLite = await DBProvider.db.getRefreshMesas();
        final objetoFinal = objetoidSQLite[0];
        final resultadoPostgres = objetoid[0][0];

        if (resultadoPostgres > objetoFinal.objetoid) {
          List<List<dynamic>> resultadoComparativa = await cnt.query(
              'SELECT objetoid, nombre, estado FROM datos.objetos WHERE objetoid > @objetoid',
              substitutionValues: {"objetoid": objetoFinal.objetoid});

          final nuevaMesa = new MesaModel();

          for (var row in resultadoComparativa) {
            nuevaMesa.objetoid = row[0];
            nuevaMesa.nombre = row[1];
            nuevaMesa.estado = row[2];
            nuevaMesa.consumido = 0;

            await mesasRefreshBloc.agregarMesas(nuevaMesa);
          }
        } else if (resultadoPostgres < objetoFinal.objetoid) {
          await mesasRefreshBloc.borrarMesasSobrantes(resultadoPostgres);
        }

        List<List<dynamic>> results = await cnt.query(
            "SELECT DISTINCT ON (O.objetoid) objetoid, O.nombre, O.estado, SUM(COALESCE(CAST(C.totalapagar AS DOUBLE PRECISION), 0)) AS consumido, O.esquemaid, COALESCE(C.nombrevendedor, '' ), COUNT(C.numerofactura) AS cuentas, CAST(COALESCE(C.vendedorid, 0) AS INTEGER) FROM datos.objetos AS O LEFT JOIN datos.cuentasactivas AS C ON O.objetoid = C.mesahabitacionid  AND C.estadocuenta <> 2 AND C.numerofactura = 0 WHERE O.esquemaid = @esquemaid AND O.sucursalid = @sucursalid AND bloqueo = 0 GROUP BY O.objetoid, O.nombre, O.estado, C.nombrevendedor, O.esquemaid, C.vendedorid  ORDER BY O.objetoid ASC;",
            substitutionValues: {
              "esquemaid": prefs.esquemaid,
              "sucursalid": prefs.sucursalid
            });

        final nuevaMesa = new MesaModel();

        for (var row in results) {
          nuevaMesa.id = row[0];
          nuevaMesa.objetoid = row[0];
          nuevaMesa.nombre = row[1];
          nuevaMesa.estado = row[2];
          nuevaMesa.consumido = row[3];
          nuevaMesa.esquemaid = row[4];
          nuevaMesa.nombreVendedor = row[5];
          nuevaMesa.numeroCuentas = row[6];
          nuevaMesa.vendedorid = row[7];

          await mesasRefreshBloc.actualizarMesas(nuevaMesa, row[0]);
        }
      });

      // await conexionTimer.query(
      //     "select  pg_terminate_backend(pid) from pg_stat_activity where state = 'idle' and client_addr = @ipDispositivo and datname = @nombreBd and (backend_start < NOW() - INTERVAL '3 minute' or  query_start < NOW() - INTERVAL '3 minute') ;",
      //     substitutionValues: {
      //       "ipDispositivo": localIp,
      //       "nombreBd": conexion.basedatos
      //     });

      await conexionTimer.close();

      conexionTimer = null;

      //para poder hacer el refresh de las mesas ingresa las actualizaciones al stream
      mesasRefreshBloc.obtenerMesasPorEsquema(esquemasBloc.esquemaActivo);
    } on PostgreSQLException catch (e) {
      conexionTimer.cancelTransaction();
      await conexionTimer.close();
      await ConexionTimerProvider.conexionTimer.cerrarConexion();
      print("_actualizarListadoMesas");

      print(e);
    }
  }

  Future<void> revisarListaPrecio() async {
    //final esquemasBloc = Provider.esquemasBloc(context);
    final conexion = await DBProvider.db.getConexionActiva();
    final puertobd = int.tryParse(conexion.puertobd);
    ConexionTimerProductosProvider.conexionTimer.abrirConexion(
      conexion.ipservidor,
      puertobd,
      conexion.basedatos,
      conexion.usuario,
      conexion.password,
    );

    PostgreSQLConnection conexionTimer =
        await ConexionTimerProductosProvider.conexionTimer.retornaConexion();

    final diaActual = DateTime.now().weekday;
    String diaQuery = "";
    int listaPrecioActiva = 0;

    switch (diaActual) {
      case 1:
        diaQuery = "lunes = 1";
        break;
      case 2:
        diaQuery = "martes = 1";
        break;
      case 3:
        diaQuery = "miercoles = 1";
        break;
      case 4:
        diaQuery = "jueves = 1";
        break;
      case 5:
        diaQuery = "viernes = 1";
        break;
      case 6:
        diaQuery = "sabado = 1";
        break;
      case 7:
        diaQuery = "domingo = 1";
        break;
    }

    try {
      await conexionTimer.transaction((cnt) async {
        List<List<dynamic>> listasPrecio = await cnt.query(
            "SELECT L.listaprecioid, L.nombre, luneshorainicial::text, luneshorafinal::text, marteshorainicial::text, marteshorafinal::text, miercoleshorainicial::text, miercoleshorafinal::text, jueveshorainicial::text, jueveshorafinal::text, vierneshorainicial::text, vierneshorafinal::text, sabadohorainicial::text, sabadohorafinal::text, domingohorainicial::text, domingohorafinal::text FROM datos.listapreciocabecera L, datos.esquemas E  WHERE (L.sucursalid = @sucursalid OR espublica = 1) AND estado = 0 AND E.esquemaid = @esquemaid AND E.listapreciosid = L.listaprecioid AND " +
                diaQuery,
            substitutionValues: {
              "sucursalid": prefs.sucursalid,
              "esquemaid": prefs.esquemaid
            }); //Aqui obtiene las listas precio del día actual y que esté activa

        if (listasPrecio.length > 0) {
          String hi, hf = "";
          for (var lista in listasPrecio) {
            //Aquí se recorre lista por lista
            switch (diaActual) {
              case 1:
                hi = lista[2];
                hf = lista[3];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 2:
                hi = lista[4];
                hf = lista[5];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 3:
                hi = lista[6];
                hf = lista[7];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 4:
                hi = lista[8];
                hf = lista[9];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 5:
                hi = lista[10];
                hf = lista[11];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 6:
                hi = lista[12];
                hf = lista[13];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 7:
                hi = lista[14];
                hf = lista[15];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
            }
          }
        }

        if (listaPrecioActiva != 0) {
          for (var lista in listasPrecio) {
            if (lista[0] == listaPrecioActiva) {
              nombreListaPrecio = lista[1];
            }
          }

          List<List<dynamic>> listaPrecioDetalle = await cnt.query(
              "SELECT productoid, precio::real, ipo::real, iva::real, estado, equivalenciaid FROM datos.listapreciodetalle WHERE listaprecioid = @listaprecio",
              substitutionValues: {"listaprecio": listaPrecioActiva});

          if (listaPrecioDetalle.length > 0) {
            for (var detalle in listaPrecioDetalle) {
              if (detalle[5] == 0) {
                ProductosCuentaModel producto = ProductosCuentaModel(
                  productoid: detalle[0],
                  costo: detalle[1],
                  productoPorIpo: detalle[2],
                  productoPorIva: detalle[3],
                  estado: detalle[4],
                );

                productosBloc.actualizarProductosListaPrecio(producto);
              } else {
                ProductosEquivalenciasModel proEquiModel =
                    ProductosEquivalenciasModel(
                        equivalenciaid: detalle[5],
                        porcentajeiva: detalle[3].round(),
                        porcentajeipo: detalle[2].round(),
                        estado: detalle[4],
                        precioventaincluido: detalle[1].round(),
                        productoid: detalle[0]);
                DBProvider.db
                    .actualizarProductosEquivalenciasListaPrecio(proEquiModel);
              }
            }
          }
        } else {
          List<List<dynamic>> productosNormal = await cnt.query(
              "SELECT productoid, precioventaconiva::real, porcentajeipoventa::real, porcentajeivaventa::real, bloqueo FROM datos.productos");

          if (productosNormal.length > 0) {
            for (var detalle in productosNormal) {
              ProductosCuentaModel producto = ProductosCuentaModel(
                productoid: detalle[0],
                costo: detalle[1],
                productoPorIpo: detalle[2],
                productoPorIva: detalle[3],
                estado: detalle[4],
              );

              productosBloc.actualizarProductosListaPrecio(producto);
            }
          }
        }
      });

      await conexionTimer.close();

      //para poder hacer el refresh de las mesas ingresa las actualizaciones al stream
    } on PostgreSQLException catch (e) {
      await conexionTimer.close();
      await ConexionTimerProductosProvider.conexionTimer.cerrarConexion();
      print("_revisarListaPrecio");

      print(e);
    }
  }

  int revisaHora(String hi, String hf, int listaPrecioId) {
    var horaInicial = TimeOfDay(
        hour: int.parse(hi.split(":")[0]), minute: int.parse(hi.split(":")[1]));

    var horaFinal = TimeOfDay(
        hour: int.parse(hf.split(":")[0]), minute: int.parse(hf.split(":")[1]));

    if (DateTime.now().toLocal().hour < horaInicial.hour) {
      return 0;
    } else {
      if (DateTime.now().toLocal().hour == horaInicial.hour) {
        if (DateTime.now().toLocal().minute < horaInicial.minute) {
          return 0;
        }
      }
    }

    if (DateTime.now().toLocal().hour < horaFinal.hour) {
      return listaPrecioId;
    } else {
      if (DateTime.now().toLocal().hour == horaFinal.hour) {
        if (DateTime.now().toLocal().minute <= horaFinal.minute) {
          return listaPrecioId;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  }
}
