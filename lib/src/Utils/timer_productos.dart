import 'dart:async';

import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Mesas/blocs/mesas_refresh_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/blocs/productos_bloc.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Usuario/models/vendedor_model.dart';
import 'package:maxxshop/src/db/conexion_db_timer_productos.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:postgres/postgres.dart';

class TimerConsultaProductos {
  final mesasRefreshBloc = MesasRefreshBloc();
  final productosBloc = ProductosBloc();

  static Timer timer;

  static final TimerConsultaProductos timerConsultaProductos =
      TimerConsultaProductos._private();

  TimerConsultaProductos._private();

  get iniciarTimer async {
    await _revisarListaPrecio();
    timer = Timer.periodic(Duration(seconds: 30), (Timer timer) async {
      print("timer productos");
      await _revisarListaPrecio();
    });
  }

  get detenerTimer {
    if (timer.isActive) {
      timer.cancel();
      ConexionTimerProductosProvider.conexionTimer.cerrarConexion();
    }
  }

  Future<void> _revisarListaPrecio() async {
    //final esquemasBloc = Provider.esquemasBloc(context);
    final conexion = await DBProvider.db.getConexionActiva();
    final puertobd = int.tryParse(conexion.puertobd);
    ConexionTimerProductosProvider.conexionTimer.abrirConexion(
      conexion.ipservidor,
      puertobd,
      conexion.basedatos,
      conexion.usuario,
      conexion.password,
    );

    PostgreSQLConnection conexionTimer =
        await ConexionTimerProductosProvider.conexionTimer.retornaConexion();

    VendedorModel vendedor = await DBProvider.db.obtenerVendedor();

    final diaActual = DateTime.now().weekday;
    String diaQuery = "";
    int listaPrecioActiva = 0;

    switch (diaActual) {
      case 1:
        diaQuery = "lunes = 1";
        break;
      case 2:
        diaQuery = "martes = 1";
        break;
      case 3:
        diaQuery = "miercoles = 1";
        break;
      case 4:
        diaQuery = "jueves = 1";
        break;
      case 5:
        diaQuery = "viernes = 1";
        break;
      case 6:
        diaQuery = "sabado = 1";
        break;
      case 7:
        diaQuery = "domingo = 1";
        break;
    }

    try {
      await conexionTimer.transaction((cnt) async {
        List<List<dynamic>> listasPrecio = await cnt.query(
            "SELECT listaprecioid, nombre, luneshorainicial::text, luneshorafinal::text, marteshorainicial::text, marteshorafinal::text, miercoleshorainicial::text, miercoleshorafinal::text, jueveshorainicial::text, jueveshorafinal::text, vierneshorainicial::text, vierneshorafinal::text, sabadohorainicial::text, sabadohorafinal::text, domingohorainicial::text, domingohorafinal::text FROM datos.listapreciocabecera L WHERE (sucursalid = @sucursalid OR espublica = 1) AND estado = 0 AND " +
                diaQuery,
            substitutionValues: {
              "sucursalid": vendedor.sucursalid
            }); //Aqui obtiene las listas precio del día actual y que esté activa

        print(listasPrecio);

        if (listasPrecio.length > 0) {
          String hi, hf = "";
          for (var lista in listasPrecio) {
            //Aquí se recorre lista por lista
            switch (diaActual) {
              case 1:
                hi = lista[2];
                hf = lista[3];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 2:
                hi = lista[4];
                hf = lista[5];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 3:
                hi = lista[6];
                hf = lista[7];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 4:
                hi = lista[8];
                hf = lista[9];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 5:
                hi = lista[10];
                hf = lista[11];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 6:
                hi = lista[12];
                hf = lista[13];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
              case 7:
                hi = lista[14];
                hf = lista[15];
                listaPrecioActiva = revisaHora(hi, hf, lista[0]);
                break;
            }
          }
        }

        if (listaPrecioActiva != 0) {
          List<List<dynamic>> listaPrecioDetalle = await cnt.query(
              "SELECT productoid, precio::real, ipo::real, iva::real FROM datos.listapreciodetalle WHERE listaprecioid = @listaprecio AND estado = 0",
              substitutionValues: {"listaprecio": listaPrecioActiva});

          if (listaPrecioDetalle.length > 0) {
            for (var detalle in listaPrecioDetalle) {
              ProductosCuentaModel producto = ProductosCuentaModel(
                productoid: detalle[0],
                costo: detalle[1],
                productoPorIpo: detalle[2],
                productoPorIva: detalle[3],
              );

              productosBloc.actualizarProductosListaPrecio(producto);
            }
          }
        } else {
          List<List<dynamic>> productosNormal = await cnt.query(
              "SELECT productoid, precioventaconiva::real, porcentajeipoventa::real, porcentajeivaventa::real FROM datos.productos WHERE bloqueo = 0");

          if (productosNormal.length > 0) {
            for (var detalle in productosNormal) {
              ProductosCuentaModel producto = ProductosCuentaModel(
                productoid: detalle[0],
                costo: detalle[1],
                productoPorIpo: detalle[2],
                productoPorIva: detalle[3],
              );

              productosBloc.actualizarProductosListaPrecio(producto);
            }
          }
        }
      });

      await conexionTimer.close();

      //para poder hacer el refresh de las mesas ingresa las actualizaciones al stream
    } on PostgreSQLException catch (e) {
      await conexionTimer.close();
      await ConexionTimerProductosProvider.conexionTimer.cerrarConexion();
      print("_revisarListaPrecio");

      print(e);
    }
  }

  int revisaHora(String hi, String hf, int listaPrecioId) {
    var horaInicial = TimeOfDay(
        hour: int.parse(hi.split(":")[0]), minute: int.parse(hi.split(":")[1]));

    var horaFinal = TimeOfDay(
        hour: int.parse(hf.split(":")[0]), minute: int.parse(hf.split(":")[1]));

    if (DateTime.now().hour < horaInicial.hour) {
      return 0;
    } else {
      if (DateTime.now().hour == horaInicial.hour) {
        if (DateTime.now().minute < horaInicial.minute) {
          return 0;
        }
      }
    }

    if (DateTime.now().hour < horaFinal.hour) {
      return listaPrecioId;
    } else {
      if (DateTime.now().hour == horaFinal.hour) {
        if (DateTime.now().minute <= horaFinal.minute) {
          return listaPrecioId;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  }
}
