import 'package:flutter/material.dart';
import 'package:maxxshop/src/Facturacion/Clientes/models/cliente_model.dart';
import 'package:maxxshop/src/Facturacion/Comanda/blocs/cabecera_service_bloc.dart';
import 'package:maxxshop/src/Facturacion/Comanda/models/cuenta_cabecera_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/categorias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_combos_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_equivalencias_model.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_gustos_mode.dart';
import 'package:maxxshop/src/Facturacion/ProductosCuenta/models/productos_cuenta_model.dart';
import 'package:maxxshop/src/Utils/sqlite_functions.dart';
import 'package:maxxshop/src/db/conexion_db_negocio.dart';
import 'package:maxxshop/src/db/db_sqlite_provider.dart';
import 'package:maxxshop/src/db/preferencias_usuario_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:provider/provider.dart';

class PostgresFunctions {
  //

  final prefs = PreferenciasUsuario();

  Future<List<List<dynamic>>> consultaMesasNegocio(
      PostgreSQLConnection conexionNegocio,
      int esquemaid,
      int sucursalid) async {
    List<List<dynamic>> results = await conexionNegocio.query(
        "SELECT DISTINCT ON (O.objetoid) objetoid, O.nombre, O.estado, SUM(COALESCE(CAST(C.totalapagar AS DOUBLE PRECISION), 0)) AS consumido, O.esquemaid, COALESCE(C.nombrevendedor, '' ), COUNT(C.numerofactura) AS cuentas, CAST(COALESCE(C.vendedorid, 0) AS INTEGER) FROM datos.objetos AS O LEFT JOIN datos.cuentasactivas AS C ON O.objetoid = C.mesahabitacionid  AND C.estadocuenta <> 2 AND C.numerofactura = 0 WHERE O.esquemaid = @esquemaid AND O.sucursalid = @sucursalid AND bloqueo = 0 GROUP BY O.objetoid, O.nombre, O.estado, C.nombrevendedor, O.esquemaid, C.vendedorid  ORDER BY O.objetoid ASC;",
        substitutionValues: {
          "esquemaid": esquemaid,
          "sucursalid": sucursalid,
        }); //Aqui se obtienen las mesas de un único esquema

    return results.length > 0 ? results : [];
  }

  ///En esta función se trae toda la información de la empresa para trabajar con esos datos localmente con sqlite
  Future<void> schemasAndTables(
      blocCategorias,
      blocProductos,
      blocProductosGustos,
      esquemasBloc) async {
    final sqliteFunctions = SqliteFunctions();

    final ConexionesModel conexionModel =
        await DBProvider.db.getConexionActiva();


    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionModel.ipservidor,
        int.parse(conexionModel.puertobd),
        conexionModel.basedatos,
        conexionModel.usuario,
        conexionModel.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();


    if (prefs.esquemaid != 0) {
      //Cuando solo hay un esquema
      List<List<dynamic>> results = await consultaMesasNegocio(
          conexionNegocio, prefs.esquemaid, prefs.sucursalid);

      List<List<dynamic>> esquema = await conexionNegocio.query(
          "SELECT esquemaid, nombre, colorvacialimpiafondo, colorvacialimpiatexto,colorocupadasinpedidofondo,colorocupadasinpedidotexto,colorocupadaconpedidotexto,colorocupadaconpedidofondo,colorsuciafondo,colorsuciatexto,colorpendienteporpagarfondo,colorpendienteporpagartexto,colorreparacionfondo,colorreparaciontexto,colordisponiblefondo,colordisponibletexto,colorocupadafondo,colorocupadatexto,colorantesdevencersefondo,colorantesdevencersetexto FROM datos.esquemas WHERE esquemaid = @esquemaid AND sucursalid = @sucursalid",
          substitutionValues: {
            "esquemaid": prefs.esquemaid,
            "sucursalid": prefs.sucursalid
          });

      await sqliteFunctions.processSchemaAndTables(
          esquema, results, esquemasBloc, 1);
    } else {
      //Cuando hay varios esquemas
      List<List<dynamic>> esquemas = await conexionNegocio.query(
          "SELECT esquemaid, nombre, colorvacialimpiafondo, colorvacialimpiatexto,colorocupadasinpedidofondo,colorocupadasinpedidotexto,colorocupadaconpedidotexto,colorocupadaconpedidofondo,colorsuciafondo,colorsuciatexto,colorpendienteporpagarfondo,colorpendienteporpagartexto,colorreparacionfondo,colorreparaciontexto,colordisponiblefondo,colordisponibletexto,colorocupadafondo,colorocupadatexto,colorantesdevencersefondo,colorantesdevencersetexto FROM datos.esquemas WHERE sucursalid = @sucursalid ORDER BY esquemaid",
          substitutionValues: {"sucursalid": prefs.sucursalid});

      int cont = 1;
      for (var esquema in esquemas) {
        List<List<dynamic>> results = await consultaMesasNegocio(
            conexionNegocio,
            esquema[0],
            prefs.sucursalid); //Aqui hubo cambio de uno a cero

        await sqliteFunctions
            .processSchemaAndTables([esquema], results, esquemasBloc, cont);

        cont++;
      }
    }
    //Prueba para determinar si se puede remover la barra de esquemas en las mesas cuando solo hay un esquema
    esquemasBloc.obtenerEsquemas();

    ///Categorías y productos
    await conexionNegocio.close();

    await cargarProductosYCategorias(blocCategorias, blocProductos, blocProductosGustos);


  }

  Future<void> cargarProductosYCategorias(blocCategorias,
      blocProductos,
      blocProductosGustos) async{

    final ConexionesModel conexionModel =
        await DBProvider.db.getConexionActiva();


    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionModel.ipservidor,
        int.parse(conexionModel.puertobd),
        conexionModel.basedatos,
        conexionModel.usuario,
        conexionModel.password,
      );

      PostgreSQLConnection conexionNegocio =
          await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    final consultaExistencia = await DBProvider.db.consultaExistencias();

    if (consultaExistencia[0]["total"] == 0) {
      //Aqui revisa si no hay categorías

      List<List<dynamic>> results = await conexionNegocio.query(
          "SELECT grupoid, codigogrupo, descripcion, COALESCE(convert_from(foto, 'UTF8'), ''), ordenvista, grupoidpadre FROM datos.grupos1 WHERE grupoid IN (SELECT DISTINCT G.grupoid  FROM datos.grupos1 G   EXCEPT (SELECT  grupoid FROM datos.esquemasgrupos WHERE sucursalid = @sucursalid AND esquemaid = @esquemaid)  ) AND esventa = 0 ORDER BY ordenvista ASC",
          substitutionValues: {
            "esquemaid": prefs.esquemaid,
            "sucursalid": prefs.sucursalid
          });

      final nuevaCategoria = new CategoriasModel();

      for (var row in results) {
        nuevaCategoria.grupoid = row[0];
        nuevaCategoria.codigogrupo = row[1];
        nuevaCategoria.descripcion = row[2];
        nuevaCategoria.foto = row[3].toString();
        nuevaCategoria.ordenvista = row[4];
        nuevaCategoria.grupopadreid = row[5];

        await blocCategorias.agregarCategorias(nuevaCategoria);
        final consultaExistencia =
            await DBProvider.db.consultaExistenciasProductos(row[0]);

        if (consultaExistencia[0]["total"] == 0) {
          List<List<dynamic>> results = await conexionNegocio.query(
              "SELECT productoid, nombre, CAST(precioventaconiva AS REAL), grupo1id, centrodecostoid, CAST(impuestoconsumo AS REAL), CAST(precioivaventa AS REAL), presentacion, producto, COALESCE(convert_from(foto, 'UTF8'), ''), CAST(precioventasiniva AS INTEGER), CAST(porcentajeivaventa AS REAL), CAST(porcentajeipoventa AS REAL), imprimecomanda, equivalenciasventa, esservicio, numeromaximocombos, diasgarantia, bloqueo, CAST(costo as REAL), codigobarras1, codigobarras2, CAST(ts AS TEXT)  FROM datos.productos WHERE grupo1id = @grupoSeleccionado AND bloqueo = 0",
              substitutionValues: {"grupoSeleccionado": row[0]});

          for (var rowP in results) {
            final nuevoProducto = ProductosCuentaModel();

            nuevoProducto.productoid = rowP[0];
            nuevoProducto.nombre = rowP[1];
            nuevoProducto.precio = rowP[2];
            nuevoProducto.grupo1id = rowP[3];
            nuevoProducto.centrocostoid = rowP[4];
            nuevoProducto.impuestoconsumo = rowP[5];
            nuevoProducto.precioivaventa = rowP[6];
            nuevoProducto.presentacion = rowP[7];
            nuevoProducto.codigo = rowP[8];
            nuevoProducto.foto = rowP[9].toString();
            nuevoProducto.productoValorUnitario = rowP[10];
            nuevoProducto.productoPorIva = rowP[11];
            nuevoProducto.productoPorIpo = rowP[12];
            nuevoProducto.productoImprimeComanda = rowP[13];
            nuevoProducto.productoEquivalencia = rowP[14];
            nuevoProducto.esServicio = rowP[15];
            nuevoProducto.numeroMaximoCombos = rowP[16];
            nuevoProducto.ordenvista = rowP[17];
            nuevoProducto.estado = rowP[18];
            nuevoProducto.costo = rowP[19];
            nuevoProducto.codigobarras1 = rowP[20];
            nuevoProducto.codigobarras2 = rowP[21];
            nuevoProducto.ts = rowP[22];

            await blocProductos.agregarProductos(nuevoProducto);
          }
        }
      }

      ///ProductosGustos
      List<List<dynamic>> productosGustosList = await conexionNegocio.query(
          "SELECT id, gustoid, productoid, nombre FROM datos.productosgustos");

      for (var res in productosGustosList) {
        final proGusModel = ProductosGustosModel();
        proGusModel.id = res[0];
        proGusModel.gustoid = res[1];
        proGusModel.productoid = res[2];
        proGusModel.nombre = res[3];

        blocProductosGustos.agregarProductosGustos(proGusModel);
      }

      ///ProductosCombos
      List<List<dynamic>> productosCombosList = await conexionNegocio.query(
          "SELECT C.id, C.productoid, C.productoidhijo, C.presentacion, CAST(C.cantidadcombo AS INTEGER), CAST(C.preciocombo AS REAL), CAST(C.valor_adicional AS REAL), P.nombre, P.producto, P.imprimecomanda, P.centrodecostoid FROM datos.productoscombos C LEFT JOIN datos.productos P ON C.productoidhijo = P.productoid ORDER BY C.id");

      for (var res in productosCombosList) {
        final proComModel = ProductosCombosModel();

        proComModel.id = res[0];
        proComModel.productoid = res[1];
        proComModel.productoidhijo = res[2];
        proComModel.presentacion = res[3];
        proComModel.cantidadcombo = res[4];
        proComModel.preciocombo = res[5];
        proComModel.valoradicional = res[6];
        proComModel.nombre = res[7];
        proComModel.codigohijo = res[8];
        proComModel.productoImprimeComanda = res[9];
        proComModel.centrocostoid = res[10];

        DBProvider.db.agregarProductosCombos(proComModel);
      }

      //ProductosEquivalencias
      List<
          List<
              dynamic>> productosEquivalenciasList = await conexionNegocio.query(
          "SELECT productoid, equivalenciaid, presentacion, equivalencia::int4, porcentajeiva::int4, porcentajeipo::int4, precioventaincluido::int4, tipoequivalencia, estado, principal FROM datos.productosequivalencias WHERE tipoequivalencia < 4 AND tipoequivalencia > 1 AND estado = 0 ORDER BY productoid");

      for (var item in productosEquivalenciasList) {
        final proEquiModel = ProductosEquivalenciasModel();

        proEquiModel.productoid = item[0];
        proEquiModel.equivalenciaid = item[1];
        proEquiModel.presentacion = item[2];
        proEquiModel.equivalencia = item[3];
        proEquiModel.porcentajeiva = item[4];
        proEquiModel.porcentajeipo = item[5];
        proEquiModel.precioventaincluido = item[6];
        proEquiModel.tipoequivalencia = item[7];
        proEquiModel.estado = item[8];
        proEquiModel.principal = item[9];

        DBProvider.db.agregarProductosEquivalencias(proEquiModel);
      }
    }
    else{
      List<List<dynamic>> maxTsPostgres = await conexionNegocio.query("SELECT CAST(MAX(ts) AS TEXT), COUNT(id) FROM datos.productos");
      String maxTsSqlite = await DBProvider.db.getMaxTsProductos();

      DateTime tsPostgresParsed = DateTime.parse(maxTsPostgres.first.first);
      DateTime tsSqliteParsed = DateTime.parse(maxTsSqlite);

      if (tsSqliteParsed.difference(tsPostgresParsed).inSeconds > 0) {
        List<List<dynamic>> productosNuevos = await conexionNegocio.query("SELECT productoid, nombre, CAST(precioventaconiva AS REAL), grupo1id, centrodecostoid, CAST(impuestoconsumo AS REAL), CAST(precioivaventa AS REAL), presentacion, producto, COALESCE(convert_from(foto, 'UTF8'), ''), CAST(precioventasiniva AS INTEGER), CAST(porcentajeivaventa AS REAL), CAST(porcentajeipoventa AS REAL), imprimecomanda, equivalenciasventa, esservicio, numeromaximocombos, diasgarantia, bloqueo, CAST(costo as REAL), codigobarras1, codigobarras2, CAST(ts AS TEXT)  FROM datos.productos WHERE ts > @tsSqlite", substitutionValues: {
          "tsSqlite": maxTsSqlite
        });

        for (var rowP in productosNuevos) {
          final nuevoProducto = ProductosCuentaModel();

          nuevoProducto.productoid = rowP[0];
          nuevoProducto.nombre = rowP[1];
          nuevoProducto.precio = rowP[2];
          nuevoProducto.grupo1id = rowP[3];
          nuevoProducto.centrocostoid = rowP[4];
          nuevoProducto.impuestoconsumo = rowP[5];
          nuevoProducto.precioivaventa = rowP[6];
          nuevoProducto.presentacion = rowP[7];
          nuevoProducto.codigo = rowP[8];
          nuevoProducto.foto = rowP[9].toString();
          nuevoProducto.productoValorUnitario = rowP[10];
          nuevoProducto.productoPorIva = rowP[11];
          nuevoProducto.productoPorIpo = rowP[12];
          nuevoProducto.productoImprimeComanda = rowP[13];
          nuevoProducto.productoEquivalencia = rowP[14];
          nuevoProducto.esServicio = rowP[15];
          nuevoProducto.numeroMaximoCombos = rowP[16];
          nuevoProducto.ordenvista = rowP[17];
          nuevoProducto.estado = rowP[18];
          nuevoProducto.costo = rowP[19];
          nuevoProducto.codigobarras1 = rowP[20];
          nuevoProducto.codigobarras2 = rowP[21];
          nuevoProducto.ts = rowP[22];

          if (await DBProvider.db.productoPorCodigoExiste(rowP[8])) {
            //Actualiza
            await DBProvider.db.actualizarProductoLocal(nuevoProducto);
          }
          else{
            //crea
            await blocProductos.agregarProductos(nuevoProducto);
          }
        }
      }
    }

    

    await conexionNegocio.close();
  }

  Future<bool> crearCuentaEnBD(int objetoid, String nombre,
      BuildContext context, String nombreMesa, int numeropersonas) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    final puertobd = int.tryParse(conexionActiva.puertobd);

    final cabeceraModel = MesaCabecera();

    bool creado = false;

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      puertobd,
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> mesaOcupada = await conexionNegocio.query(
        "SELECT estado FROM datos.objetos WHERE objetoid = @objetoid",
        substitutionValues: {"objetoid": objetoid});

    if (mesaOcupada[0][0] == 1) {
      try {
        await conexionNegocio.transaction((connection) async {
          List<List<dynamic>> resultado = await connection.query(
              'SELECT prefijo, CAST(numeroactual AS INTEGER), CAST(id AS INTEGER) FROM datos.documentosconsecutivos WHERE tipodocumento = @tipo AND sucursalid = @sucursalid AND estado = 0',
              substitutionValues: {
                "tipo": "2",
                "sucursalid": prefs.sucursalid
              });

          print(resultado);

          List<List<dynamic>> existeNumeroCuenta = await connection.query(
              "SELECT COUNT(*) FROM datos.cuentacabecera WHERE prefijocuenta = @prefijo AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
              substitutionValues: {
                "prefijo": resultado[0][0],
                "numerocuenta": resultado[0][1] + 1,
                "sucursalid": prefs.sucursalid
              });

          if (existeNumeroCuenta[0][0] == 0) {
            //Si no existe
            await connection.query(
                'INSERT INTO datos.cuentasactivas (prefijocuenta, numerocuenta, mesahabitacionid, sucursalid, nombrecuenta, nombrevendedor, vendedorid, cantidadpersonas) VALUES (@prefijo, @numero, @objetoid, @sucursalid, @nombrecuenta, @nombrevendedor, @vendedorid, @cantidadpersonas)',
                substitutionValues: {
                  "prefijo": resultado[0][0],
                  "numero": resultado[0][1] + 1,
                  "objetoid": objetoid,
                  "sucursalid": prefs.sucursalid,
                  "nombrecuenta": nombre,
                  "nombrevendedor": 'Admin',
                  "vendedorid": prefs.usuarioid,
                  "cantidadpersonas": numeropersonas
                });
            await connection.query(
                'UPDATE datos.documentosconsecutivos SET numeroactual = @numero WHERE id = @id',
                substitutionValues: {
                  "numero": resultado[0][1] + 1,
                  "id": resultado[0][2]
                });

            cabeceraModel.prefijocuenta = resultado[0][0];
            cabeceraModel.numerocuenta = resultado[0][1] + 1;
            cabeceraModel.nombreCliente = "";
            cabeceraModel.nitCliente = "";
            cabeceraModel.nombreCuenta = nombre;
            cabeceraModel.objetoid = objetoid;
            cabeceraModel.nombreMesa = nombreMesa;
            cabeceraModel.clienteid = 0;
            cabeceraModel.fechahoravencimiento =
                DateTime.now().toLocal().toString();
            cabeceraModel.totalapagar = 0;

            cabeceraProvider.mesaCabecera = cabeceraModel;
          } else {
            //conexionNegocio.cancelTransaction();
            await connection.query(
                'UPDATE datos.documentosconsecutivos SET numeroactual = @numero WHERE id = @id',
                substitutionValues: {
                  "numero": resultado[0][1] + 1,
                  "id": resultado[0][2]
                });
            await conexionNegocio.close();
            await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

            await crearCuentaEnBD(
                objetoid, nombre, context, nombreMesa, numeropersonas);
          }
        });

        await conexionNegocio.close();
      } catch (e) {
        await conexionNegocio.close();
        await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

        print(e);
      }
    } else {
      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      creado = true;
    }

    return creado;
  }

  Future<void> actualizarNumeroValidacionMesa(int numero, int objetoid) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.objetos SET observacion = @numero WHERE objetoid = @objetoid",
          substitutionValues: {"numero": numero, "objetoid": objetoid});
    } catch (e) {
      conexionNegocio.cancelTransaction();
    }

    await conexionNegocio.close();
    ConexionNegocioProvider.conexionNegocio.cerrarConexion();
  }

  Future<bool> validarNumeroValidacionMesa(int numero, int objetoid) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> numeroBd = await conexionNegocio.query(
        "SELECT observacion FROM datos.objetos WHERE objetoid = @objetoid",
        substitutionValues: {"objetoid": objetoid});

    await conexionNegocio.close();
    ConexionNegocioProvider.conexionNegocio.cerrarConexion();

    return int.parse(numeroBd[0][0]) == numero;
  }

  Future<bool> agregarProducto(
      ProductosCuentaModel producto,
      int cantidad,
      CabeceraService cabeceraProvider,
      List<String> gustos,
      List<ProductosCombosModel> comboModel,
      List<ProductosEquivalenciasModel> equivalenciaModel) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);
    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    List<List<dynamic>> cond = await conexionNegocio.query(
        "SELECT COUNT(*) FROM datos.cuentacabecera WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND prefijofactura IS NOT NULL AND numerofactura <> 0",
        substitutionValues: {
          "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
          "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
        });

    try {
      int productoBase = 0;
      int productoValorIva = 0;
      int productoValorIpo = 0;

      int costo = equivalenciaModel.length > 0
          ? equivalenciaModel.first.precioventaincluido
          : producto.precio.round();
      int productoPorIva = equivalenciaModel.length > 0
          ? equivalenciaModel.first.porcentajeiva
          : producto.productoPorIva.round();
      int productoPorIpo = equivalenciaModel.length > 0
          ? equivalenciaModel.first.porcentajeipo
          : producto.productoPorIpo.round();

      if (productoPorIva > 0) {
        productoBase = (costo / ((productoPorIva / 100) + 1)).round();
        productoValorIva =
            ((productoBase * cantidad) * (productoPorIva / 100)).round();
      } else if (productoPorIpo > 0) {
        productoBase = (costo / ((productoPorIpo / 100) + 1)).round();
        productoValorIpo =
            ((productoBase * cantidad) * (productoPorIpo / 100)).round();
      } else {
        productoBase = costo.round();
      }

      if (cond[0][0] == 0) {
        List<List<dynamic>> maxCuentaDetalleId = await conexionNegocio.query(
            "SELECT COALESCE(MAX(cuentadetalleid), 0) FROM datos.cuentadetalle");

        List<List<dynamic>> existeCuentaDetalleId = await conexionNegocio.query(
            "SELECT COUNT(*) FROM datos.cuentadetalle WHERE cuentadetalleid = @cuentadetalleid",
            substitutionValues: {
              "cuentadetalleid": maxCuentaDetalleId[0][0] + 1
            });

        if (existeCuentaDetalleId[0][0] == 0) {
          await conexionNegocio.transaction((cnt) async {
            await cnt.query(
                "INSERT INTO datos.cuentadetalle(prefijocuenta, numerocuenta, productonombre, productocantidad, productovalortotal, centrocostoid, sucursalid, productogusto, productoid, productovalorunitario, productoporiva, productoporipo, productovaloriva, productovaloripo, productocodigo, productoimprimecomanda, productoequivalencia, productopresentacion, productobase, cuentadetalleid, productoservicio, bodegaid) VALUES(@prefijocuenta, @numerocuenta, @productonombre, @productocantidad, @productovalortotal, @centrocostoid, @sucursalid, @productogusto, @productoid, @valorunitario, @productoporiva, @productoporipo, @productovaloriva, @productovaloripo, @productocodigo, @productoimprimecomanda, @productoequivalencia, @productopresentacion, @productobase, @cuentadetalleid, @productoservicio, @bodegaid)",
                substitutionValues: {
                  "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                  "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                  "productonombre": producto.nombre,
                  "productocantidad": cantidad,
                  "productovalortotal": (costo * cantidad),
                  "centrocostoid": producto.centrocostoid,
                  "sucursalid": prefs.sucursalid,
                  "productogusto": gustos.join("/"),
                  "productoid": producto.productoid,
                  "valorunitario": costo,
                  "productoporiva": productoPorIva.round(),
                  "productoporipo": productoPorIpo.round(),
                  "productovaloriva": productoValorIva,
                  "productovaloripo": productoValorIpo,
                  "productocodigo": producto.codigo,
                  "productoimprimecomanda": producto.productoImprimeComanda,
                  "productoequivalencia": equivalenciaModel.length > 0
                      ? equivalenciaModel.first.equivalencia
                      : 1,
                  "productopresentacion": equivalenciaModel.length > 0
                      ? equivalenciaModel.first.presentacion
                      : "Unidad",
                  "productobase": productoBase * cantidad,
                  "cuentadetalleid": maxCuentaDetalleId[0][0] + 1,
                  "productoservicio": producto.esServicio,
                  "vendedorid": prefs.usuarioid,
                  "bodegaid": 1
                });

            if (comboModel.length > 0) {
              for (ProductosCombosModel combo in comboModel) {
                List<List<dynamic>> maxCuentaDetalleComboId = await cnt.query(
                    "SELECT COALESCE(MAX(cuentadetallecombosid), 0) FROM datos.cuentadetallecombos");

                await cnt.query(
                    "INSERT INTO datos.cuentadetallecombos(cuentadetallecombosid, sucursalid, prefijocuenta, numerocuenta, cuentadetalleid, producto, productoid, productonombre, cantidad, productogusto, productoimprimecomanda, centrocostoid) VALUES(@cuentadetallecombosid, @sucursalid, @prefijocuenta, @numerocuenta, @cuentadetalleid, @producto, @productoid, @productonombre, @cantidad, @productogusto, @productoimprimecomanda, @centrocostoid)",
                    substitutionValues: {
                      "cuentadetallecombosid":
                          maxCuentaDetalleComboId[0][0] + 1,
                      "sucursalid": prefs.sucursalid,
                      "prefijocuenta":
                          cabeceraProvider.mesaCabecera.prefijocuenta,
                      "numerocuenta":
                          cabeceraProvider.mesaCabecera.numerocuenta,
                      "cuentadetalleid": maxCuentaDetalleId[0][0] + 1,
                      "producto": combo.codigohijo,
                      "productoid": combo.productoidhijo,
                      "productonombre": combo.nombre,
                      "cantidad": combo.cantidadseleccionada != null
                          ? combo.cantidadseleccionada
                          : 1,
                      "productogusto": combo.gusto != null ? combo.gusto : "",
                      "productoimprimecomanda": combo.productoImprimeComanda,
                      "centrocostoid": combo.centrocostoid
                    });
              }
            }

            List<List<dynamic>> totalAcumulado = await cnt.query(
                "SELECT CAST(totalapagar AS REAL), CAST(ivacuenta AS INTEGER), CAST(impuestoconsumo AS INTEGER) FROM datos.cuentasactivas WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
                substitutionValues: {
                  "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                  "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                  "sucursalid": prefs.sucursalid
                });

            await cnt.query(
                "UPDATE datos.cuentasactivas SET totalapagar = @totalapagar, ivacuenta = @ivacuenta, impuestoconsumo = @impuestoconsumo WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
                substitutionValues: {
                  "totalapagar":
                      totalAcumulado[0][0] + (producto.costo * cantidad),
                  "ivacuenta": totalAcumulado[0][1] + productoValorIva,
                  "impuestoconsumo": totalAcumulado[0][2] + productoValorIpo,
                  "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
                  "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
                  "sucursalid": prefs.sucursalid
                });

            await cnt.query(
              'UPDATE datos.objetos SET estado = 4, fechahoraultimoevento = @fecha WHERE objetoid = @objetoid',
              substitutionValues: {
                "objetoid": cabeceraProvider.mesaCabecera.objetoid,
                "fecha": DateTime.now().toLocal()
              },
            );
          });

          cabeceraProvider.mesaCabecera.totalapagar +=
              producto.costo.round() * cantidad;
        } else {
          conexionNegocio.cancelTransaction();
          await conexionNegocio.close();
          ConexionNegocioProvider.conexionNegocio.cerrarConexion();

          await agregarProducto(producto, cantidad, cabeceraProvider, gustos,
              comboModel, equivalenciaModel);
        }
      }

      await conexionNegocio.close();
      await ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } catch (e) {
      conexionNegocio.cancelTransaction();
      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      await agregarProducto(producto, cantidad, cabeceraProvider, gustos,
          comboModel, equivalenciaModel);

      print(e);
    }
    return cond[0][0] == 0;
  }

  Future<void> asociarCliente(
      ClienteModel cliente, BuildContext context) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();
    final cabeceraProvider =
        Provider.of<CabeceraService>(context, listen: false);

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
      conexionActiva.ipservidor,
      int.tryParse(conexionActiva.puertobd),
      conexionActiva.basedatos,
      conexionActiva.usuario,
      conexionActiva.password,
    );

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    await conexionNegocio.query(
        "UPDATE datos.cuentacabecera SET clienteid = @clienteid, direccioncliente = @direccioncliente, cliente = @cliente, nombrecliente = @nombrecliente, telefonocliente = @telefonocliente, ciudadcliente = @ciudadcliente WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
        substitutionValues: {
          "clienteid": cliente.clienteid,
          "direccioncliente": cliente.direccion,
          "cliente": cliente.nit,
          "nombrecliente": cliente.nombrecompleto,
          "telefonocliente": cliente.telefono,
          "ciudadcliente": cliente.ciudad,
          "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
          "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
          "sucursalid": prefs.sucursalid
        });

    cabeceraProvider.mesaCabecera.nitCliente = cliente.nit;
    cabeceraProvider.mesaCabecera.nombreCliente = cliente.nombrecompleto;
    cabeceraProvider.mesaCabecera.telefonoCliente = cliente.telefono;
    cabeceraProvider.mesaCabecera.direccionCliente = cliente.direccion;
    cabeceraProvider.mesaCabecera.clienteid = cliente.clienteid;
    cabeceraProvider.mesaCabecera.ciudadCliente = cliente.ciudad;

    await conexionNegocio.close();
    ConexionNegocioProvider.conexionNegocio.cerrarConexion();
  }

  Future<void> editarAliasCuentaCabecera(
      CabeceraService cabeceraProvider, String nombre) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();
    try {
      await conexionNegocio.query(
          "UPDATE datos.cuentacabecera SET nombrecuenta = @nombrecuenta WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
          substitutionValues: {
            "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
            "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
            "sucursalid": prefs.sucursalid,
            "nombrecuenta": nombre,
          });

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }
  }

  Future<void> editarFechaHoraVencimientoCuenta(
      CabeceraService cabeceraProvider, String fecha) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.cuentacabecera SET fechahoravencimiento = @fechahoravencimiento WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
          substitutionValues: {
            "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
            "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
            "sucursalid": prefs.sucursalid,
            "fechahoravencimiento": fecha,
          });

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }
  }

  Future<void> anularCuentaCabecera(
      CabeceraService cabeceraProvider) async {
    final ConexionesModel conexionActiva =
        await DBProvider.db.getConexionActiva();

    ConexionNegocioProvider.conexionNegocio.abrirConexion(
        conexionActiva.ipservidor,
        int.tryParse(conexionActiva.puertobd),
        conexionActiva.basedatos,
        conexionActiva.usuario,
        conexionActiva.password);

    PostgreSQLConnection conexionNegocio =
        await ConexionNegocioProvider.conexionNegocio.retornaConexion();

    try {
      await conexionNegocio.query(
          "UPDATE datos.cuentacabecera SET estadocuenta = 2 WHERE prefijocuenta = @prefijocuenta AND numerocuenta = @numerocuenta AND sucursalid = @sucursalid",
          substitutionValues: {
            "prefijocuenta": cabeceraProvider.mesaCabecera.prefijocuenta,
            "numerocuenta": cabeceraProvider.mesaCabecera.numerocuenta,
            "sucursalid": prefs.sucursalid,
          });
      await conexionNegocio.query(
          "UPDATE datos.objetos SET estado = 2 WHERE objetoid = @objetoid AND sucursalid = @sucursalid",
          substitutionValues: {
            "objetoid": cabeceraProvider.mesaCabecera.objetoid,
            "sucursalid": prefs.sucursalid,
          });

      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();
    } on PostgreSQLException catch (e) {
      await conexionNegocio.close();
      ConexionNegocioProvider.conexionNegocio.cerrarConexion();

      print(e);
    }
  }
}
